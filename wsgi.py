
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os
from app import create_app

myApp=create_app('default')

if __name__ == '__main__':
   myApp.run()
