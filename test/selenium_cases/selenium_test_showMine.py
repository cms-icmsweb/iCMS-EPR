
import time

from .SeleniumTestBase import SeleniumTestCase, debugLog

class ShowMineTestCase( SeleniumTestCase ):

    def setUp(self):
        super( ShowMineTestCase, self ).setUp( )
        self.id = 'showMine'

        # make sure this test runs as the test shifter, otherwise the random ordering of the tables make the results instable
        self.auth = ['shifter', 'foo']


    def test_showMyShifts2016(self):

        self.go_to('/auth/login')
        self.assertIn('iCMS-EPR - Login', self.driver.title)
        self.driver.find_element_by_id('username').send_keys(self.auth[0])
        self.driver.find_element_by_id('password').send_keys(self.auth[1])
        self.driver.find_element_by_id('submit').click()

        debugLog( 'showMine-0', self.driver.page_source.encode('utf-8') )

        # this needs to be updated along the "news" section. At least there is one line
        # with the warning that it's not the production version ...
        alertWarn = self.driver.find_elements_by_class_name('alert-warning')
        if len(alertWarn) > 0:
            logFileName = '%s-test-showMine-alerWarn' % self.id
            debugLog( logFileName, self.driver.page_source.encode( 'utf-8' ) )
        self.assertListEqual( alertWarn , [])

        self.go_to( '/showMine' )

        self.assertIn( 'iCMS - EPR Pledge Overview for Tester, Shifter', self.driver.title )
        debugLog( 'showMine-1', self.driver.page_source.encode('utf-8') )

        self.assertIn( 'Shift summary in', self.driver.page_source )
        self.driver.save_screenshot( '../logs/%s.png' % 'showMine-1' )

        # this assumes that we have only one shift:
        # see also http://www.w3schools.com/xsl/xpath_syntax.asp
        # NOTE: counting of <td>s starts at 1, not 0
        # for i in range(1, 10):
        #     print i, self.driver.find_element_by_xpath( '//table[@id="shiftTable"]/tbody/tr/td[%d]' % i ).text
        self.assertEqual( self.driver.find_element_by_xpath( '//table[@id="shiftTable"]/tbody/tr/td[2]' ).text, 'pr1' )
        self.assertEqual( self.driver.find_element_by_xpath( '//table[@id="shiftTable"]/tbody/tr/td[3]' ).text, 'sType0' )
        self.assertEqual( self.driver.find_element_by_xpath( '//table[@id="shiftTable"]/tbody/tr/td[4]' ).text, 'main' )
        self.assertEqual( self.driver.find_element_by_xpath( '//table[@id="shiftTable"]/tbody/tr/td[5]' ).text, 'Tester, Shifter' )
        self.assertEqual( self.driver.find_element_by_xpath( '//table[@id="shiftTable"]/tbody/tr/td[6]' ).text, 'CERN' )
        self.assertEqual( self.driver.find_element_by_xpath( '//table[@id="shiftTable"]/tbody/tr/td[13]' ).text, '0.00' )

    def test_showMyInst( self ) :

        self.go_to( '/auth/login' )
        self.assertIn( 'iCMS-EPR - Login', self.driver.title )
        self.driver.find_element_by_id( 'username' ).send_keys( self.auth[ 0 ] )
        self.driver.find_element_by_id( 'password' ).send_keys( self.auth[ 1 ] )
        self.driver.find_element_by_id( 'submit' ).click( )

        debugLog( 'showMyInst-0', self.driver.page_source.encode( 'utf-8' ) )

        self.go_to( '/showMyInst' )
        time.sleep(1)

        debugLog( 'showMyInst-1', self.driver.page_source.encode('utf-8') )
        self.driver.save_screenshot( '../logs/%s.png' % 'showMyInst-1' )

        self.assertIn( 'iCMS - EPR Institute Information for CERN', self.driver.page_source )

        self.assertEqual( self.driver.find_element_by_xpath( '//table[@id="instituteTable"]/tbody/tr[3]/td[3]' ).text.replace('\n', ''), u'3.00' )

        # make sure the new tab is there ...
        self.assertIn( '<a href="#tab-table3" data-toggle="tab">Individual Pledges</a> </li>', self.driver.page_source )


    def test_showMyCountry( self ) :
        self.go_to( '/auth/login' )
        self.assertIn( 'iCMS-EPR - Login', self.driver.title )
        self.driver.find_element_by_id( 'username' ).send_keys( self.auth[ 0 ] )
        self.driver.find_element_by_id( 'password' ).send_keys( self.auth[ 1 ] )
        self.driver.find_element_by_id( 'submit' ).click( )

        debugLog( 'showMyCountry-0', self.driver.page_source.encode( 'utf-8' ) )

        self.go_to( '/showMyCountry' )
        time.sleep(2)

        debugLog( 'showMyCountry-1', self.driver.page_source.encode( 'utf-8' ) )
        self.driver.save_screenshot( '../logs/%s.png' % 'showMyCountry-1' )

        self.assertIn( 'iCMS - EPR Country Information for SWITZERLAND', self.driver.page_source )

        self.assertEqual( self.driver.find_element_by_xpath( '//div[@class="summary-info"]/table/tbody/tr[3]/td[6]' ).text.replace( '\n', '' ), u'0.28( 0.05 )' )


    def test_showMyFavs( self ) :
        self.go_to( '/auth/login' )
        self.assertIn( 'iCMS-EPR - Login', self.driver.title )
        self.driver.find_element_by_id( 'username' ).send_keys( self.auth[ 0 ] )
        self.driver.find_element_by_id( 'password' ).send_keys( self.auth[ 1 ] )
        self.driver.find_element_by_id( 'submit' ).click( )

        self.debugLogWithScreenShot( 'showMyFavs-0' )

        # set some favourites:
        self.go_to( '/showTask/t4' ) # task "ta4", id 4
        debugLog( 'showMyFavs-1', self.driver.page_source.encode( 'utf-8' ) )
        favT = self.driver.find_element_by_id( 'fav' )
        favT.click()
        self.go_to( '/showPledge/4:7:2' )
        self.debugLogWithScreenShot( 'showMyFavs-1' )
        favP = self.driver.find_element_by_id( 'fav' )
        favP.click()

        self.go_to( '/showFavourites' )

        self.debugLogWithScreenShot( 'showMyFavs-2' )

        self.assertIn( 'iCMS - EPR Favourites for', self.driver.page_source )
        self.assertIn( 'Favourite tasks for %s' % self.selYear, self.driver.page_source )
        self.assertIn( 'Favourite pledges for %s' % self.selYear, self.driver.page_source )

        prevYear = self.selYear - 1
        self.go_to( '/setYear/%s' % prevYear )
        self.assertNotIn( 'iCMS - EPR Favourites for', self.driver.page_source )
        # self.assertIn( 'no favourite tasks or pledges found for', self.driver.page_source )
        self.assertIn( 'iCMS - EPR Pledge Overview for', self.driver.title )
