
# -*- coding: utf-8 -*-

from .SeleniumTestBase import SeleniumTestCase, loginUser

from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .pledges import createPledge, updatePledge, rejectPledge

class eGroupManagementTestCase(SeleniumTestCase):

    def setUp( self ) :
        super(eGroupManagementTestCase, self).setUp()

        self.id = 'egmgt'
        self.username = 'egPM' # manager via egroup of project 2
        loginUser( username = self.username, theTest=self )
        self.logger.info(" %s logged in ... " % self.username)

    def addField( self, idName, value) :
        field = self.driver.find_element_by_xpath( '//input[@id="%s"]' % idName )
        field.clear( )
        field.send_keys( value )

    def addTextArea( self, idName, value) :
        field = self.driver.find_element_by_xpath( '//textarea[@id="%s"]' % idName )
        field.clear( )
        field.send_keys( value )

    def selField( self, idName, optVal) :
        fieldSel = Select( self.driver.find_element_by_xpath( '//select[@id="%s"]' % idName ) )
        fieldSel.select_by_visible_text( optVal.strip() )

    def boolField( self, idName, optVal) :
        field = self.driver.find_element_by_xpath( '//input[@id="%s"]' % idName )
        if optVal: field.click()

    def testManageProjectEGroup( self ) :

        self.go_to( '/' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot('manageProjEGrp-0')
        self.assertNotIn( 'You are not a manager for this project, sorry', self.driver.page_source )

        selMgt = self.driver.find_elements_by_xpath( "//a[@href='/manage/']" )
        self.assertGreater( len(selMgt), 0)

        self.go_to( '/manage/mine' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot('manageProjEGrp-1')
        self.assertNotIn( 'You are not a manager for this project, sorry', self.driver.page_source )


        self.go_to( '/manage/project/p2' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot('manageProjEGrp-2')
        self.assertNotIn( 'You are not a manager for this project, sorry', self.driver.page_source )

        # update project name:
        selName = self.driver.find_element_by_id( 'name' )
        oldName = selName.text
        self.logger.debug( "found name: \n %s" % selName.text )
        selName.clear()
        selName.send_keys( "Project 2 - newName" )

        updateBtn = self.driver.find_element_by_id( 'updateName' )
        updateBtn.click()

        self.assertIn( 'Successfully updated name for Project 2', self.driver.page_source )
        self.assertIn( 'Project 2 - newName', self.driver.page_source )

        self.debugLogWithScreenShot('manageProjEgrp-3')

        # update project description:
        selDesc = self.driver.find_element_by_id( 'description' )
        oldDes = selDesc.text
        self.logger.debug( "found description: \n %s" % selDesc.text )
        selDesc.clear()
        selDesc.send_keys( "new desc for Project 2" )

        updateBtn = self.driver.find_element_by_id( 'update' )
        updateBtn.click()
        self.debugLogWithScreenShot('manageProjEgrp-4')

        self.assertIn( 'Successfully updated description for Project 2', self.driver.page_source )
        self.assertIn( 'new desc for Project 2', self.driver.page_source )

        # now add a new manager:
        addMgr = self.driver.find_element_by_xpath( "//input[@id='addMgrs']" )
        addMgr.click()

        selMgrTable = self.driver.find_element_by_css_selector ( "#userTable.display.dataTable" )
        selTesterRow = selMgrTable.find_element_by_xpath( "//tr[3]" )
        ActionChains( self.driver).move_to_element( selTesterRow ).click( selTesterRow ).perform()

        self.debugLogWithScreenShot('manageProjEgrp-addMgrSel0')

        asu = self.driver.find_element_by_xpath( "//div[@class='dt-buttons']/a[@class='dt-button']" )
        asu.click()

        self.debugLogWithScreenShot('manageProjEgrp-addMgrSel1')

        wait = WebDriverWait( self.driver, 10 )
        confirmSelUser = wait.until( EC.element_to_be_clickable( (By.XPATH, "//form[@id='done']/input[@type='submit']") ) )

        confirmSelUserForm = self.driver.find_element_by_xpath("//form[@id='done']")
        confirmSelUser.submit()
        # ActionChains( self.driver ).move_to_element( confirmSelUserForm ).click( confirmSelUser ).perform( )

        confirmSelUser = wait.until( EC.title_contains("Manage Project") )

        self.debugLogWithScreenShot('manageProjEgrp-addMgrClk')

        # and remove that manager again:
        mgrList = Select(self.driver.find_element_by_name('managers'))
        mgrList.select_by_visible_text("Tester, Master")
        removeMgr = self.driver.find_element_by_xpath("//input[@value='Remove selected managers']")
        removeMgr.click()

        self.debugLogWithScreenShot('manageUpdateProjDescEgrp-rmMgrClk')
        self.assertIn( 'Successfully removed manager(s) Tester', self.driver.page_source )

        # print self.driver.page_source
