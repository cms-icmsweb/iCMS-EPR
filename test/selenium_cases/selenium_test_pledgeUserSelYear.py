
import time

from .SeleniumTestBase import SeleniumTestCase, loginUser

from selenium.webdriver.common.action_chains import ActionChains

from .pledges import createPledge, updatePledge, rejectPledge

class PledgeUserSelYearTestCase(SeleniumTestCase):

    def setUp(self):
        super(PledgeUserSelYearTestCase, self).setUp()
        self.id = 'plU%s' % self.selYear

        self.username = 'pfeiffer'
        loginUser( username = self.username, theTest=self )

        self.logger.info("%s> %s logged in ... " % (self.id, self.username) )

    def testPledgeUserSelYear( self ) :

        # loginUser('user1', self)

        self.logger.info( 'going to create pledge for user %s in %s' % (self.username, self.selYear) )

        # first, set the year to the actual selYear.
        # I did not find a way to do this from the nav menu with Selenium (the drop-down menu was found but did not open)
        self.go_to('/setYear/%s' % self.selYear)

        self.debugLogWithScreenShot('testPledgeUser%s' % self.selYear)

        createPledge( self, username=self.username, name='Pfeiffer, Andreas', instMgr=True )

        self.debugLogWithScreenShot('testPledgeUser%s-afterCreate' % self.selYear)

        self.assertNotIn( 'New pledges are not allowed for %s' % self.selYear, self.driver.page_source )
        # for the future :-) - if you need this, don't forget to change the corresponding line (~845) in main/views.py:
        # self.assertIn( 'A new pledge was successfully created for user pfeiffer', self.driver.page_source )

        # as we have now created the pledge, make sure we reject it again to allow future pledging:
        rejectPledge( self, self.username)

    def testNavBarSelYear(self):

        self.debugLogWithScreenShot('navBar%s-0' % self.selYear)

        selTask = self.driver.find_element_by_xpath( '//a[contains(text(),"Tasks")]' )
        ActionChains( self.driver ).move_to_element( selTask ).click( selTask ).perform( )

        tasksPr1 = self.driver.find_elements_by_xpath( '//a[contains(@href,"showProjectTasks")]' )
        # self.logger.info( 'found %s tasks in %s: %s ' % (len(tasksPr1), self.selYear, str(tasksPr1) ) )
        self.assertEqual( len(tasksPr1), 6 )

        # wait = WebDriverWait( self.driver, 10 )
        # wait.until( EC.visibility_of( (By.PARTIAL_LINK_TEXT, 'Tasks for pr1') ) )
        # ActionChains( self.driver ).move_to_element( confirmSelUserForm ).click( confirmSelUser ).perform( )

        self.debugLogWithScreenShot('navBar%s-1' % self.selYear)

        # now the same after setting the year to the actual self.selYear.
        # I did not find a way to do this from the nav menu with Selenium (the drop-down menu was found but did not open)
        self.go_to( '/setYear/%s' % self.selYear)

        selTask = self.driver.find_element_by_xpath( '//a[contains(text(),"Tasks")]' )
        ActionChains( self.driver ).move_to_element( selTask ).click( selTask ).perform( )

        self.debugLogWithScreenShot( 'navBar%s-2' % self.selYear )

        time.sleep(2) # wait a bit for element to show ...
        tasksPr1a = self.driver.find_elements_by_xpath( '//a[contains(@href,"showProjectTasks")]' )
        self.logger.info( 'found %s tasks in %s: %s ' % (self.selYear, len(tasksPr1a), [x.get_attribute('href') for x in tasksPr1a] ) )
        self.assertEqual( len(tasksPr1a), 6 )
