
from .SeleniumTestBase import SeleniumTestCase, loginUser

from .pledges import createPledge, updatePledge, rejectPledge

class PledgeUserTestCase(SeleniumTestCase):

    def setUp(self):
        super(PledgeUserTestCase, self).setUp()
        self.id = 'plU'

        self.username = 'pfeiffer'
        loginUser( username = self.username, theTest=self )

        self.logger.info("%s> %s logged in ... " % (self.id, self.username) )

    def testPledgeUser( self ) :

        loginUser('pfeiffer', self)

        self.logger.info( 'going to create pledge for user %s ', self.username )
        createPledge( self, username=self.username, name='Pfeiffer, Andreas', instMgr=True )

        # ---------- modify the pledge ----------
        # this part assumes that we only have one pledge !!!

        self.logger.info( 'going to update pledge for user %s ', self.username )
        updatePledge( self, username=self.username )

        # ---------- finally go and reject the pledge ----------
        self.logger.info( 'going to reject pledge for user %s ', self.username )
        rejectPledge( self, username=self.username )

        #        print self.browser.page_source
