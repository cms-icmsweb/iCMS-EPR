
import time

import sys
import os
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import updateInstView

from .SeleniumTestBase import SeleniumTestCase

# Test validating the fix for ICMSEPR-73

class InstViewTestCase(SeleniumTestCase):

    def setUp(self):
        super(InstViewTestCase, self).setUp()
        self.id = 'instView'

        # prepare the instView
        updateInstView.main(self.selYear)

    def test_instView(self):
        self.go_to('/auth/login')
        self.debugLogWithScreenShot('instView-0' )

        self.assertIn('iCMS-EPR - Login', self.driver.title)
        self.driver.find_element_by_id('username').send_keys(self.auth[0])
        self.driver.find_element_by_id('password').send_keys(self.auth[1])
        self.driver.find_element_by_id('submit').click()

        # this needs to be updated along the "news" section. At least there is one line
        # with the warning that it's not the production version ...
        alertWarn = self.driver.find_elements_by_class_name('alert-warning')
        if len(alertWarn) > 0:
            self.debugLogWithScreenShot('instView-0alerts' )
        self.assertListEqual( alertWarn , [])

        self.debugLogWithScreenShot('instView-1' )
        self.assertIn( 'iCMS - EPR Pledge Overview for Pfeiffer, Andreas', self.driver.title )

        self.go_to('/showInstitutes')

        # debugLogWithScreenShot(self, 'instView-2' )
        self.assertIn( 'iCMS - EPR Institute Overview in ', self.driver.title )

        # wait a bit to let the table get filled ...
        time.sleep(2)

        self.debugLogWithScreenShot('instView-2' )

        # check for some values in the rows of the table:
        checkInstMap = { 'CERN' : int(11.*self.monthsPerAuthor),
                         'ETHZ' : int(2.*self.monthsPerAuthor),
                         'FNAL' : int(3.*self.monthsPerAuthor) }
        foundInsts = []
        for row in self.driver.find_elements_by_tag_name('tr'):
            for k, v in checkInstMap.items():
                if k in row.text:
                    foundInsts.append(k)
                    self.assertIn( str(v), row.text )

        self.assertEqual(set( foundInsts ), set( checkInstMap.keys() ) )

