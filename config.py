#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os
import sys
import socket

basedir = os.path.abspath(os.path.dirname(__file__))

if '/Users/ap/PycharmProjects/secrets' not in sys.path:
   sys.path.append('/Users/ap/PycharmProjects/secrets')

if '/data/secrets' not in sys.path:
   sys.path.append('/data/secrets')

prodHostList = ['vocms0110', 'vocms0890', 'vocms0190']

def isProdHost():
    for prodHost in prodHostList:
        if prodHost in socket.gethostname() : return True
    return  False


# determine production level by host the process is running on
def getProdLevel( ) :
    if any(x in socket.gethostname() for x in prodHostList) :
        prodLevel = 'prod'
    elif any(x in socket.gethostname() for x in ('vocms002', 'snowfake')) :
            prodLevel = 'dev'
    elif any(x in socket.gethostname() for x in ('vocms012', 'vocms0190')) :
        prodLevel = 'int'
    elif 'os-ap-slc7' in socket.gethostname( ) :
        prodLevel = 'dev'
    else :
        prodLevel = 'testing'

    return prodLevel

if 'TEST_DATABASE_URL' not in os.environ:
    import secrets
    prodLevel = getProdLevel()
else:
    prodLevel = 'testing'


class Config:

    # The following variable is overridden in instance/config.py on a per-host basis
    SECRET_KEY = 'fooBar ... '

    SSL_DISABLE = False

    WTF_CSRF_ENABLED = True

    if 'TEST_DATABASE_URL' in os.environ:
        SERVER_NAME="localhost.localdomain:5010"
    else:
        SERVER_NAME = None

    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_RECORD_QUERIES = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # uncomment the line below to see _all_ queries printed (and make sure
    # logging is off, otherwise you'll get everything double):
    # SQLALCHEMY_ECHO = True

    MAIL_SERVER = 'smtp.cern.ch'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    # MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    # MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    SECURITY_EMAIL_SENDER = 'andreas.pfeiffer@cern.ch'
    ICMS_MAIL_SUBJECT_PREFIX = '[iCMS-EPR]'
    ICMS_MAIL_SENDER = 'icms-support@cern.ch'
    ICMS_MAIL_BCC = 'icms-support@cern.ch'
    ICMS_MAIL_REPLY_TO = 'icms-support@cern.ch'
    ICMS_ADMIN = os.environ.get('iCMS_ADMIN')

    # do not intercept redirects in debug tool ... set
    # this to true if you do want the intercepts
    DEBUG_TB_INTERCEPT_REDIRECTS = False

    ICMS_SLOW_DB_QUERY_TIME=1.5

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):

    host = socket.gethostname()
    if not isProdHost() : # production
        DEBUG = True

    # The following variables are overridden in instance/config.py on a per-host basis
    SQLALCHEMY_DATABASE_URI = 'DB-uri dev ... '

class TestingConfig(Config):

    TESTING = True

    # The following variables are overridden in instance/config.py on a per-host basis
    SQLALCHEMY_DATABASE_URI = 'DB-uri test ... '
    SQLALCHEMY_ONLSHIFTDB_URI = 'DB-uri test ... '

    SSL_DISABLE = False

    WTF_CSRF_ENABLED = False


class ProductionConfig(Config):

    # The following variables are overridden in instance/config.py on a per-host basis
    SQLALCHEMY_DATABASE_URI = 'DB-uri ... '
    SQLALCHEMY_ONLSHIFTDB_URI = 'DB-uri ... '

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)

        # email errors to the administrators
        import logging
        from logging.handlers import SMTPHandler
        credentials = None
        secure = None
        if getattr(cls, 'MAIL_USERNAME', None) is not None:
            credentials = (cls.MAIL_USERNAME, cls.MAIL_PASSWORD)
            if getattr(cls, 'MAIL_USE_TLS', None):
                secure = ()
        mail_handler = SMTPHandler(
            mailhost=(cls.MAIL_SERVER, cls.MAIL_PORT),
            fromaddr=cls.ICMS_MAIL_SENDER,
            toaddrs=[cls.ICMS_ADMIN],
            subject=cls.ICMS_MAIL_SUBJECT_PREFIX + ' Application Error',
            credentials=credentials,
            secure=secure)
        mail_handler.setLevel(logging.INFO)
        app.logger.addHandler(mail_handler)


class UnixConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # log to syslog
        import logging
        from logging.handlers import SysLogHandler
        syslog_handler = SysLogHandler()
        syslog_handler.setLevel(logging.WARNING)
        app.logger.addHandler(syslog_handler)


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'unix': UnixConfig,

    'default': DevelopmentConfig
}
