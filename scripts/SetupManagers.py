
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import time
import json

from sqlalchemy.exc import IntegrityError

from app import db
from app.models import Role, Permission, EprUser, EprInstitute, Manager, Project, CMSActivity, Task, Pledge, getTaskTypes


def setOrUpdate( user, params):

    patType, patCode, permOld, permNew = params

    # set up managers. First check if they are already in, if so: update, else: insert

    try:
        m = db.session.query(Manager).filter_by(itemType=patType, itemCode=patCode, userId=user.id, role=permOld).one()
        m.role = permNew
        print( "updated existing manager :", m )
    except:
        m = Manager(itemType=patType, itemCode=patCode, userId=user.id, role=permNew)
        print( "created new manager :", m )
        db.session.add(m)

    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        print( "ERROR when committing for ", m )


def setupAdmins():

    # grant these the admin role

    admins = ['pfeiffer', 'tbawej']

    for uName in admins:
        admin = db.session.query(EprUser).filter_by(username=uName).one()

        adminRole = db.session.query(Role).filter_by(name='Administrator').first()
        admin.grantRole(adminRole)
        db.session.commit()

    print( "%s admins set up.\n" % len(admins) )

def setupInstMgrs():

    start = time.time()
    with open('imports/iCMS_Institute.json', 'r') as pmFile:
        insts = json.load( pmFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i inst items' % len(insts) )

    existingInsts = db.session.query(EprInstitute.code).all()

    nOK = 0
    for i in insts:

        # if i['year'] < 2015 : continue

        iCode = '%s' % (i['code'],)
        if (iCode,) not in existingInsts:
            print( "no inst found in DB for" ,iCode, 'skipping.' )
            continue

        print( 'processing inst ', iCode )

        for key in ['cbiCmsId', 'cbdCmsId', 'cbd2CmsId']:
            if not i[key] : continue
            userCmsId = int( i[key] )
            if userCmsId == 0: continue

            try:
                user = db.session.query(EprUser).filter_by(cmsId = userCmsId).one()
            except Exception as e:
                if 'No row was found for one' in str(e):
                    print( "ERROR user with cmsId %i not found for inst %s - skipping" % (userCmsId, iCode) )
                    continue
                else:
                    print( "ERROR from querying user %i for inst %s - got %s " % (userCmsId, iCode, str(e)) )
                    continue

            db.session.add( Manager(itemType='institute', itemCode=iCode, userId=user.id, role=Permission.MANAGEINST) )
        try:
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR when adding managers for institute %s - got %s" % (iCode, str(e)) )

    print( "imported %d institute managers\n" % nOK )

def setupProjMgrs():

    start = time.time()
    with open('imports/iCMS_ProjectManagement.json', 'r') as pmFile:
        projs = json.load( pmFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i projMgt items' % len(projs) )

    projExist = db.session.query(Project.code).all()

    nOK = 0
    for i in projs:

        if i['year'] < 2015 : continue

        pCode = '%s' % (i['projectId'],)
        if (pCode,) not in projExist:
           print( "no project found for ", pCode, ' skipping.' )
           continue

        print( 'Processing proj ', pCode )

        for key in ['cmsId', 'cmsId2', 'cmsId3', 'cmsId4']:
            userId = int(i[key])
            if userId == 0: continue
            try:
                user = db.session.query(EprUser).filter_by(cmsId = userId).one()
            except Exception as e:
                if 'No row was found for one' in str(e):
                    print( "ERROR user with cmsId %i not found for proj %s - skipping" % (userId, pCode) )
                    continue
                else:
                    print( "ERROR from querying user %i for proj %s - got %s " % (userId, pCode, str(e)) )
                    continue

            db.session.add( Manager(itemType='project', itemCode=pCode, userId=user.id, role=Permission.MANAGEPROJ) )
        try:
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR when adding managers for proj %s - got %s" % (pCode, str(e)) )

    print( "imported %d project managers\n" % nOK )

def setupProjMgrs2016():

    print( "dummy function setupProjMgrs2016 called ... " )

    return

def setupActMgrs():

    start = time.time()
    with open('imports/iCMS_ActivityManagement.json', 'r') as actFile:
        acts = json.load( actFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i actMgt items' % len(acts) )

    actExist = db.session.query(CMSActivity.code).all()

    nOK = 0
    for i in acts:

        if i['year'] < 2015 : continue

        aCode = '[%s-%s]' % (i['projectId'], i['activityId'])
        if (aCode,) not in actExist:
           print( "no activity found for ", aCode, ' skipping.')
           continue

        print( 'Processing act ', aCode )

        for key in ['cmsId', 'cmsId2', 'cmsId3', 'cmsId4']:
            userId = int(i[key])
            if userId == 0: continue
            try:
                user = db.session.query(EprUser).filter_by(cmsId = userId).one()
            except Exception as e:
                if 'No row was found for one' in str(e):
                    print( "ERROR user with cmsId %i not found for act %s - skipping" % (userId, aCode) )
                    continue
                else:
                    print( "ERROR from querying user %i for act %s - got %s " % (userId, aCode, str(e)) )
                    continue

            db.session.add( Manager(itemType='activity', itemCode=aCode, userId=user.id, role=Permission.MANAGEACT) )
        try:
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR when adding managers for act %s - got %s" % (aCode, str(e)) )

    print( "imported %d activity managers\n" % nOK )

def setupTaskMgrs():

    start = time.time()
    with open('imports/iCMS_TaskManagement.json', 'r') as actFile:
        tasks = json.load( actFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i taskMgt items' % len(tasks) )

    taskExist = db.session.query(Task.code).all()

    nOK = 0
    for i in tasks:

        if i['year'] < 2015 : continue

        tCode = '[[%s-%s]-%s]' % (i['projectId'], i['activityId'], i['taskId'])
        if (tCode,) not in taskExist:
           print( "no task found for ", tCode, ' skipping.' )
           continue

        print( 'Processing task ', tCode )

        for key in ['cmsId', 'cmsId2', 'cmsId3', 'cmsId4']:
            userId = int(i[key])
            if userId == 0: continue
            try:
                user = db.session.query(EprUser).filter_by(cmsId = userId).one()
            except Exception as e:
                if 'No row was found for one' in str(e):
                    print( "ERROR user with cmsId %i not found for act %s - skipping" % (userId, tCode) )
                    continue
                else:
                    print( "ERROR from querying user %i for task %s - got %s " % (userId, tCode, str(e)) )
                    continue

            db.session.add( Manager(itemType='task', itemCode=tCode, userId=user.id, role=Permission.MANAGETASK) )
        try:
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR when adding managers for task %s - got %s" % (tCode, str(e)) )

    print( "imported %d task managers\n" % nOK )

def setup():
    setupAdmins()
    setupProjMgrs()
    setupActMgrs()
    setupTaskMgrs()
    setupInstMgrs()
    setupProjMgrs2016()
