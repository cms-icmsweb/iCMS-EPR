
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import time, datetime
import json
import sys
from uuid import uuid4

from sqlalchemy.exc import IntegrityError

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db
from app.models import Project, CMSActivity, Task, Pledge

from app.models import commitUpdated, commitNew, CMSDbException

# OpenPyXL - A Python library to read/write Excel 2010 xlsx/xlsm files
#
# for info and docs see: https://openpyxl.readthedocs.org/en/default/index.html
#
from openpyxl import load_workbook

stdHeaders = [u'Project', u'Activity',
                 u'Task Name', u'Task Type', u'Shift Type', u'Task Description',
                 u'Skills Required', u'Percentage at CERN',
                 u'Earliest start date', u'Latest end date',
                 u'#Months', u'Core?']

headerMap = enumerate(  stdHeaders )

def remap():
    res = {}
    for item in headerMap:
        k, v = item
        res[v] = k+1
    return res

indexMap = remap()

# print "headermap: ", headerMap
# print 'indexmap : ', indexMap


def removeOldActivities():

    oldActNames = [ "Cassettes", "Detector Simulation Development", "Management", "Mechanical Engineering",
                    "Modules", "Physics Performance", "Sensors", "Trigger Development" ]

    hgcalProj = Project.query.filter_by(name='HGCAL', year=2018).one()
    print( "found proj: %s" % hgcalProj )
    oldActs = (db.session.query( CMSActivity )
                        .filter( CMSActivity.projectId == hgcalProj.id )
                        .filter( CMSActivity.year == 2018 )
                        .filter( CMSActivity.name.in_( oldActNames ) )
                        .order_by( CMSActivity.name )
                        .all())

    print( "found %d activities in HGCAL for 2018 to be removed" % len(oldActs) )
    tasks = {}
    pledges = {}
    nActDel = 0
    for act in oldActs:
        tasks[act.id] = db.session.query(Task).filter( Task.activityId == act.id ).filter( Task.status != 'DELETED' ).all()
        nPledges = 0
        pledges[ act.id ] = { }
        nTaskDel = 0
        for t in tasks[act.id]:
            pledges[act.id][t.id] = db.session.query( Pledge ).filter( Pledge.taskId == t.id).all()
            nPledges += len( pledges[act.id][t.id] )
            nPlDel = 0
            for pl in pledges[act.id][t.id]:
                pl.status = 'rejected'
                pl.code   = pl.code + time.strftime('%Y%m%d-%H%M%S')
                pl.timestamp = datetime.datetime.utcnow()
                nPlDel += 1
            print('... found and deleted %s pledges for task %s of activity %s in %s ' % (nPlDel, t.name, act.name, act.year))
            t.neededWork = 0
            t.status = 'DELETED'
            if not t.comment: t.comment = ''
            t.comment += 'Cleanup of wrong HGCal PAT - 2018-05-16'

            nTaskDel += 1
        print( '. found and deleted %s (of %s) tasks (with %s pledges) for activity %s in %s \n' % ( nTaskDel, len( tasks[act.id] ), nPledges, act.name, act.year) )
        act.status = 'DELETED'
        nActDel += 1

    print( 'found and deleted %s (of %s) activities of HGCal in %s \n' % ( nActDel, len(oldActs), act.year) )

    db.session.rollback()

if __name__ == '__main__':
    from app import create_app
    theApp = create_app('default')
    with theApp.app_context():
        removeOldActivities()
