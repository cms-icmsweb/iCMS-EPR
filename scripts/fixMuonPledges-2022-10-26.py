#!/usr/bin/env python

#  Copyright (c) 2022 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

# tasks which were created using the tool during 2016 had the "kind" set to "Core"
# and were not found during the cloning exercise to create the 2017 entries, as
# the script was looking for kind of "CORE", ignoring the "Core" ones.
# This script looks for the "Core" tasks in 2016 (there were six at the time of writing)
# then copies them to 2017 finding the corresponding activity, and adds the 2017 managers.
# Finally it changes the kind for 2016 to "CORE".

import time
import json
import sys
import copy
import difflib
from uuid import uuid4
from pprint import pprint

# from sqlalchemy.exc import IntegrityError

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db
from app.models import Project, CMSActivity, Task, Pledge

from app.models import commitNew, commitUpdated
from app.main.HistoryHelpers import getPledgeHistory

plCodes = [
'10820+2090+209',
'10822+5010+236',
'10916+5034+94',
'10793+9017+266',
'10793+18033+266',
'10967+5034+94',
'10790+5010+236',
'10862+881+192',
'10818+5010+236',
'10868+17538+14',
'10972+18641+266',
'10956+20534+140',
'10959+7712+301',
'10937+5767+211',
'10941+5767+211',
'10956+2515+140',
'10914+3537+210',
'10957+120+140',
'10896+20509+94',
'10972+18033+266',
'10853+2039+190',
'10956+9792+140',
'10956+8716+140',
'10956+19762+140',
'10866+4641+92',
'10913+7974+210',
'10956+9849+211',
'10967+19778+94',
'10896+18562+481',
'10967+18264+94',
'10896+5034+94',
'10896+18838+193',
'10972+9017+266',
'10972+4440+266',
'10896+18410+14',
'10944+9849+211',
'10956+6582+140',
'10957+6582+140',
'10967+19781+140',
'10966+7712+301',
'10958+7712+301',
'10896+4641+92',
'10896+20494+92',
'10914+18189+210',
'10966+9849+211',
'10793+18641+266',
'10841+2039+190',
'10896+20198+481',
'10892+20198+481',
'10895+2182+41',
'10986+17908+2',
'10832+5516+2',
'10833+219+32',
'10834+2039+190',
'10835+1315+188',
'10835+5286+190',
'10830+5161+2',
'10855+5286+190',
'10859+7247+190',
'10790+5936+180',
'10858+7247+190',
'10859+5286+190',
'10873+58+229',
'10872+58+229',
'10796+5936+180',
'10851+5286+190',
'10812+5936+180',
'10971+7247+190',
'10973+5936+180',
'10944+19051+531',
'10840+19451+2',
'10944+18957+531',
'10951+4976+92',
'10829+19451+2',
'10829+264+190',
'10858+19561+190',
'10883+5413+92',
'10937+19583+531',
'10859+19561+190',
'10951+5413+92',
'10828+2904+264',
'10800+5936+180',
'10951+4581+92',
'10958+7143+151',
'10802+2904+264',
'10863+3229+90',
'10926+10865+232',
'10964+19061+269',
'10872+4951+229',
'10829+5161+2',
'10966+7143+151',
'10986+19970+90',
'10986+20762+90',
'10850+1623+68',
'10978+20780+506',
'10912+10053+152',
'10818+2968+264',
'10978+17139+506',
'10830+9754+32',
'10981+20762+90',
'10956+19508+269',
'10943+65+232',
'10926+18622+232',
'10945+3103+232',
'10918+3103+232',
'10944+65+232',
'10895+18635+282',
'10977+17521+90',
'10896+9376+90',
'10918+18622+232',
'10809+2968+264',
'10831+9754+32',
'10829+9754+32',
'10850+5161+2',
'10801+2968+264',
'10958+19061+269',
'10964+19508+269',
'10945+65+232',
'10797+344+81',
'10814+343+81',
'10963+3101+232',
'10798+3354+81',
'10880+10300+263',
'10965+3415+232',
'10919+3415+232',
'10978+18921+506',
'10817+19380+81',
'10978+18922+506',
'10958+19508+269',
'10877+8673+528',
'10849+9754+32',
'10799+343+81',
'10850+9754+32',
'10981+19970+90',
'10937+3101+232',
'10956+19061+269',
'10817+5587+81',
'10798+344+81',
'10797+3739+81',
'10817+344+81',
'10805+332+81',
'10816+343+81',
'10817+3691+81',
'10986+17521+90',
'10814+8376+81',
'10811+1279+236',
'10810+3637+264',
'10787+3637+264',
'10817+4606+81',
'10851+3671+32',
'10795+3240+236',
'10958+6159+151',
'10799+344+81',
'10837+3267+32',
'10786+3637+264',
'10958+8875+151',
'10958+18357+151',
'10797+3354+81',
'10796+3240+236',
'10822+3637+264',
'10815+3637+264',
'10954+7326+229',
'10830+18814+255',
'10792+3240+236',
'10796+3637+264',
'10818+3240+236',
'10800+3637+264',
'10833+4246+32',
'10843+18814+255',
'10836+175+2',
'10829+6362+32',
'10845+6362+32',
'10849+3671+32',
'10916+3307+92',
'10942+7326+229',
'10971+2338+255',
'10901+17217+175',
'10901+10388+175',
'10837+3268+32',
'10837+19830+32',
'10844+4246+32',
'10844+18814+255',
'10847+2338+255',
'10944+7326+229',
'10849+8364+255',
'10849+8473+255',
'10856+1610+255',
'10852+8364+255',
'10901+5137+175',
'10901+16742+175',
'10901+5802+175',
'10963+19823+14',
'10901+3600+175',
'10901+3306+175',
'10852+8365+255',
'10901+3718+175',
'10917+7459+92',
'10849+8365+255',
'10858+8364+255',
'10837+9343+255',
'10856+1441+255',
'11032+1441+255',
'10849+1441+255',
'10832+8473+255',
'10859+8473+255',
'10912+7459+92',
'10925+7459+92',
'10831+1610+255',
'10837+1610+255',
'10844+8473+255',
'10900+17217+175',
'10958+19823+14',
'10900+16742+175',
'10900+10388+175',
'10900+5802+175',
'10900+5137+175',
'10900+3718+175',
'10900+3600+175',
'10900+3306+175',
'11023+3306+175',
'11023+17217+175',
'11023+3718+175',
'11023+5802+175',
'11023+16742+175',
'11023+10388+175',
'10877+10127+2',
'10850+6073+68',
'10809+708+186',
'10827+708+186',
'10972+708+186',
'10793+708+186',
'10851+3165+145',
'10855+3165+145',
'10958+10053+152',
'10856+2018+145',
'10855+8954+145',
'10836+6379+145',
'10842+443+145',
'10859+6379+145',
'10853+6379+145',
'10837+4214+145',
'10845+226+32',
'10971+226+32',
'10854+5269+145',
'10835+3165+145',
'10836+2651+145',
'10851+10849+145',
'10854+6379+145',
'10829+10849+145',
'10967+10053+152',
'10849+18176+2',
'10836+2579+2',
'10850+18176+2',
'10873+4951+229',
'10896+20245+193',
'10834+18176+2',
'10859+18176+2',
'10856+18176+2',
'10836+19650+2',
'10896+17908+2',
'10896+2579+2',
'10810+19111+282',
'10816+19111+282',
'10816+6391+282',
'10896+18393+193',
'10843+3795+188',
'10805+6391+282',
'10896+9835+14',
'10877+17908+2',
'10875+17908+2',
'10944+15865+301',
'10912+4774+344',
'10856+221+32',
'10786+7945+282',
'10796+7945+282',
'10822+7945+282',
'10807+7945+282',
'10917+11045+344',
'10967+18802+94',
'10795+2267+236',
'10809+20134+236',
'10812+2267+236',
'10912+11045+344',
'10958+18802+94',
'10798+1279+236',
'10917+4774+344',
'10939+15865+301',
'10796+2267+236',
'10916+18802+94',
'10811+2267+236',
'10958+4774+344',
'10821+7945+282',
'10823+2267+236',
'10808+20134+236',
'10823+20134+236',
'10812+20134+236',
'10958+11045+344',
'10795+1279+236',
'10808+2267+236',
'10811+20134+236',
'10812+2973+236',
'10809+2973+236',
'10808+2973+236',
'10823+2973+236',
'10811+2973+236',
'10794+1279+236',
'10812+1279+236',
'10808+1279+236',
'10790+1279+236',
]

# plCodes = ['10820+2090+209', '10808+2267+236']

def fixMuonPledges( ) :

    pledges = ( db.session.query(Pledge)
                          .join( Task, Task.id == Pledge.taskId )
                          .join( CMSActivity, CMSActivity.id == Task.activityId )
                          .join( Project, Project.id == CMSActivity.projectId )
                          .filter( Pledge.year == 2022 )
                          .filter( Pledge.status != 'rejected' )
                          .filter( Project.name == 'Muon' )
                          .all())

    selPld = []
    selPldIds = []
    plHist = {}
    for pl in pledges:
        plId = f'{pl.taskId}+{pl.userId}+{pl.instId}'
        if plId.strip() != '' and plId in plCodes:
            if pl.id in selPldIds:
                print( f'found dup - id: {pl.id} - pledge: {pl}' )
            selPldIds.append(pl.id)
            selPld.append( pl )
            plHist[pl.id] = (pl, getPledgeHistory(pl.code, 2022))
            
    print( f'found {len(selPld)} pledges to fix in {len(pledges)} (from {len(plCodes)})')

    print( f'history: ')
    index = 0
    not2 = {}
    toFix = {}
    for id, (pl, h) in plHist.items():
        index += 1
        # if index > 5: break
        # print( f'{pl}' )
        # print( f'{h}' )
        if len(h) < 2:
            print( f'{id} - {len(h)} -- {pl.taskId}+{pl.userId}+{pl.instId}')
            not2[id] = (pl, h)
        elif len(h) > 2:
            if h[0]['status_mod'] and h[0]['status'] == 'accepted' and h[2]['status'] == 'new' and not h[1]['status_mod']:
                toFix[id] = (pl, h)
            else:
                print( f'{id} - {len(h)} -- {pl.taskId}+{pl.userId}+{pl.instId}')
                not2[id] = (pl, h)
        else:
            if h[0]['status_mod'] and h[0]['status'] == 'accepted' and h[1]['status'] == 'new':
                toFix[id] = (pl, h)
            else:
                changes, otherModsIn1 = getHistoryChanges(h, showDetails=True)
                print( f'non-canonical status change detected for {id}  -- {pl.taskId}+{pl.userId}+{pl.instId} \n\t{pl}\n\t{changes}\n\t{otherModsIn1}\n')


    print( '-'*80)
    print( f'\nfound {len(not2)} with not-2 entries in history')
    showHistory( not2, showDetails=True )

    print( '-'*80)
    print( f'\nfound {len(toFix)} pledges to fix')
    showHistory( toFix, showDetails=True )

    print( '-'*80)
    
    # nFixed = 0
    # for id, (pl, h) in toFix.items():
    #     if id != pl.id:
    #         print( f'ERROR when trying to fix pledge {id} != {pl.id} -- {pl.taskId}+{pl.userId}+{pl.instId}), status {pl.status} ' )
    #         continue
        
    #     pl.status = 'new'
    #     commitUpdated(pl)
        
    #     nFixed += 1
        
    # print( f'fixed {nFixed} pledges for Muon')
    
    print( f'cowardly refusing to fix anything right now as it was already fixed .... ')

def getHistoryChanges( hList, showDetails=False):
    change = ''
    index = 0
    otherModIn1 = None
    for entry in hList:
        index += 1
        for k, v in entry.items():
            if k.endswith('_mod') and v:
                if showDetails: 
                    change += f'\n\t{index}: {k} changed, was: {entry[k.replace("_mod", "")]}'
                if index == 1 and k != 'status_mod':
                    otherModIn1 = (k, v)
    return change, otherModIn1

def showHistory( histMap, showDetails=False ):
    for id, (pl, hList) in histMap.items():
        changes, otherModsIn1 = getHistoryChanges(hList, showDetails=True)
        if changes and otherModsIn1: 
            print( f'{id} -- {pl.taskId}+{pl.userId}+{pl.instId}: ')
            print( f'{changes}\n\t{otherModsIn1}\n' )
        
if __name__ == '__main__':

    from app import create_app
    theApp = create_app('default')
    with theApp.app_context():

        fixMuonPledges( )
