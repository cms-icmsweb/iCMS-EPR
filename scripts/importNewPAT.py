
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import time
import json
import sys
import copy
import difflib
from uuid import uuid4

from sqlalchemy.exc import IntegrityError

from app import db
from app.models import Project, CMSActivity, Task

from app.models import commitUpdated, commitNew, CMSDbException

# OpenPyXL - A Python library to read/write Excel 2010 xlsx/xlsm files
#
# for info and docs see: https://openpyxl.readthedocs.org/en/default/index.html
#
from openpyxl import load_workbook

headerMap = enumerate(  [u'Project', u'Activity',
                         u'Task Name', u'Task Type', u'Shift Type', u'Task Description',
                         u'Skills Required', u'Percentage at CERN',
                         u'Earliest start date', u'Latest end date',
                         u'#Months', u'Core?'] )

def remap():
    res = {}
    for item in headerMap:
        k, v = item
        res[v] = k+1
    return res

indexMap = remap()

print("headermap: ", str(headerMap))
print('indexmap : ', indexMap)

def analyseRow(row):

    info = {}
    for cell in row:
        if cell.value: # found something, check where we are:
            info[cell.column] = cell.value

    return info

def analyseSheet(workSheet):

    projs = []
    acts  = []
    tasks = []
    pName = ''
    aName = ''
    for row in workSheet.iter_rows(row_offset=1):
        info = analyseRow(row)
        if not info: continue # ignore empty rows

        if 12 not in info.keys(): continue

        if 1 in info.keys():
            if not pName or info[1] != pName: pName = info[1]
        else:
            info[1] = pName

        if 2 in info.keys():
            if not aName or info[2] != aName: aName = info[2]
        else:
            info[2] = aName

        ## if 'Yes' not in info[12]: continue # only Core tasks
        if 'yes' in info[12].lower(): continue # only non-Core tasks

        if info[1] not in projs: projs.append( info[1] )
        if info[2] not in acts: acts.append( info[2] )

        # print "got:", info
        tasks.append( info )

    return projs, acts, tasks

def makeNewProject(pName):

    p = Project(name=pName, desc='', code=str(uuid4()), year=2016)
    try:
        commitNew(p)
    except Exception as e:
        print("ERROR committing new project %s, got %s" % (pName, str(e)))
        raise e

    return

def makeNewActivity(aName, proj):

    p = CMSActivity(name=aName, desc='', proj=proj, code=str(uuid4()), year=2016)
    try:
        commitNew(p)
    except Exception as e:
        print("ERROR committing new activity %s, got %s" % (aName, str(e)))
        raise e

    return

def convertString(inString):

    try:
        outString = inString.encode('utf-8', 'xmlcharrefreplace')
        return outString
    except UnicodeEncodeError:
        outString = inString.decode('latin-1').encode('ascii', 'xmlcharrefreplace')
        return outString
    except:
        raise

def createTask(task, act, proj):

    # fill in missing values
    for i in range(len(indexMap)):
        if i in task : continue
        if i == 8 : task[i] = 0 # percentage at CERN
        elif i == 11 : task[i] = -1 # needed work
        else: task[i] = ''

    taskName = convertString( task[ indexMap['Task Name'] ] )
    taskDesc = convertString( task[ indexMap['Task Description'] ] )
    taskSkills = convertString( task[ indexMap['Skills Required'] ] )

    if db.session.query(Task).filter_by(name=taskName, activityId=act.id, year=2016).all() != []:
        print("Skipping already existing task %s for actId %i actName %s " % (str(task), act.id, act.name))
        return False

    comment = taskSkills
    pctAtCern = 0
    try:
        pctAtCern = int( task[ indexMap['Percentage at CERN'] ] )
    except:
        pctAtCern = 0
        comment += '; ' + task[ indexMap['Percentage at CERN'] ]

    kind = "CORE"
    if 'yes' not in task[12].lower():
        kind = 'NON-CORE'
    else:
        print("?!?!?!?> found yes in core column for non-core task ????", task, act, proj)

    taskType = task[ indexMap[ 'Task Type' ] ]
    print('')
    if taskType.strip() == '':
        taskType = 'Perennial'

    taskNew = None
    try:
        taskNew = Task( name = taskName,
                        desc = taskDesc,
                        act  = act,
                        code = str(uuid4()),
                        needs     = float( task[ indexMap['#Months'] ] ),
                        pctAtCERN = pctAtCern,
                        tType     = taskType,
                        comment   = comment,
                        level=1, parent='',
                        shiftTypeId = task[ indexMap['Shift Type'] ],
                        status = 'ACTIVE',
                        locked = False,
                        kind = kind,
                        year=2016,
                    )
    except ValueError as e:
        print("ERROR creating task %s for proj %s act %s - got %s " % (task[ indexMap['Task Name'] ], proj.name, act.name, str(e)))
        return False

    if not taskNew:
        print("ERROR: can not create task for %s " % (task[ indexMap['Task Name'] ], ))
        raise Exception('error creating task')

    try:
        commitNew(taskNew)
    except Exception as e:
        print("ERROR: got: ", str(e))
        raise e

    return True


def importNewPAT():
    inFileName = 'imports/global_epr_2016-v8_forAndreas.xlsx'
    wb = load_workbook(filename=inFileName, read_only=True)

    sheets = wb.get_sheet_names()

    print("found the following worksheets:", sheets)

    oldHeaders = []
    for sheetName in sheets:

        if 'activities' in sheetName: continue

        #-ap 2016-06-23: Tracker has already the non-Core tasks in, so do not load them again to avoid duplicates
        if 'tracker' in sheetName.lower():
            print("Skipping Tracker tasks for non-CORE importing ... ")
            continue

        workSheet = wb.get_sheet_by_name(sheetName)

        headers = []
        for row in workSheet['A1:L1']:
            for cell in row:
                headers.append( cell.value )

        # raises if headers are different in a workSheet
        diffHeaders( headers, oldHeaders, sheetName )

        proj, act, tasks = analyseSheet(workSheet)

        print("-"*80)
        print("worksheet : ", sheetName)
        print(len(proj) , "projects  : ", proj)
        print(len(act)  , 'activities: ', act)
        print(len(tasks), 'NON-CORE tasks ') # , tasks

        # create projects and tasks first, then find and hand over the activity for the task:

        nOK = 0
        for t in tasks:

            projName = t[ indexMap['Project'] ]
            actName  = t[ indexMap['Activity'] ]

            try:
                proj =db.session.query(Project).filter_by(name=projName, year=2016).one()
            except Exception as e:
                if "No row was found for " in str(e):
                    db.session.autoflush=False
                    makeNewProject(projName)
                    db.session.autoflush=False
                else:
                    raise e

            proj =db.session.query(Project).filter_by(name=projName, year=2016).one()

            try: # to find the corresponding activity:
                act  =db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=2016).one()
            except Exception as e:
                if "No row was found for " in str(e):
                    makeNewActivity(actName, proj)
                elif "Multiple rows were found for one" in str(e):
                    print("ERROR multiple rows found for act %s, projId %i :" % (actName, proj.id))
                    print('\n'.join(db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=2016).all() ))
                    raise e
                else:
                    raise e

            act  =db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=2016).one()

            if not createTask(t, act, proj):
                # print "ERROR creating task %s " % (t,)
                continue
            print('created task for:', t)
            nOK += 1

    print("imported %i new tasks for 2016" % nOK)

def diffHeaders( headers, oldHeaders, sheetName ) :

    if not oldHeaders :
        oldHeaders = copy.copy( headers )
        # print 'copied headers to:', oldHeaders
    else :
        try :
            delta = list( difflib.context_diff( oldHeaders, headers ) )
        except Exception as e :
            print("processing %s got: %s" % (sheetName, str( e )))
            print("old: ", oldHeaders)
            print('new: ', headers)
            raise e

        if len( delta ) > 0 :
            msg = "found different headers for %s:" % (sheetName,)
            for line in delta :
                sys.stdout.write( line )
            raise Exception( msg )
        else :
            print("headers match ... ")

def importAll():
    importNewPAT()
