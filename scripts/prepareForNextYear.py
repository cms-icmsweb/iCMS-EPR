#!/usr/bin/env python

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import datetime
import sys
from uuid import uuid4
import traceback as tb

import logging
logging.basicConfig(format = '%(asctime)-15s - %(levelname)s: %(message)s', level=logging.INFO)

# from sqlalchemy.exc import IntegrityError
from sqlalchemy import and_, func, or_

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db
from app.models import Project, CMSActivity, Task, TimeLineInst, TimeLineUser, Level3Name, EprCeiling
from app.models import Manager

from app.models import commitNew, CMSDbException

projCodeMap = {}
actCodeMap = {}
tskCodeMap = {}

selectedProjList = ['MTD', 'PPS', 'Trigger Coordination', 'Run Coordination', 'HCAL', 'ECAL', 'L1 Trigger']

selectedProjectsOnly = False
allProjects = True

# projVetoList = ['Run Coordination']
# for testing, veto the big ones and see ... 
projVetoList = [ # 'Trigger Coordination', 
                 #  'Tracker', 'PPD', 
                 # 'MTD', 
                 #  'L1 Trigger', 'HGCAL (CE)', 'BRIL', 'General', 
                 #  'Muon', 
                 # 'ECAL', 
                 # 'PPS', 
                 #  'Offline Software And Computing', 
                 # 'HCAL', 
                 #  'DAQ', 
                 # 'Run Coordination' 
               ]

skipList = { 'acts': [], 'tasks': [] }

assert( bool(selectedProjectsOnly) != bool(allProjects)), 'Only one can be True !! ' 

if selectedProjectsOnly:
    logging.info('going to process selected project(s) [%s] ' % ', '.join(selectedProjList) )
else:
    logging.info('going to process ALL projects, except [%s] ' % ', '.join(projVetoList) )

def isDuplicate( entry, refList):
    for item in refList:
        if ( item.itemCode == entry.itemCode and 
             ( item.userId == entry.userId or item.egroup == entry.egroup )
            ): return True
    return False 

def copyMgrItems( codeMap, itemType, fromYear, toYear, verbose=False ):

    alreadyIn = (db.session.query(Manager)
                          .filter(Manager.year == toYear)
                          .filter(Manager.itemType == itemType)
                          .filter(Manager.status.in_( ['active', 'alllowed' ]) )
                          .all())
    logging.info( "found %s pre-existing %s managers for %s " % (len(alreadyIn), itemType, fromYear) )

    oldItemsActive = (db.session.query(Manager)
                          .filter(Manager.year == fromYear)
                          .filter(Manager.itemType == itemType)
                          .filter(Manager.status == 'active')
                          .all())
    logging.info( "found %s active %s managers for %s " % (len(oldItemsActive), itemType, fromYear) )
    oldItemsAllowed = (db.session.query(Manager)
                          .filter(Manager.year == fromYear)
                          .filter(Manager.itemType == itemType)
                          .filter(Manager.status == 'allowed' )
                          .all())
    logging.info( "found %s allowed %s managers for %s " % (len(oldItemsAllowed), itemType, fromYear) )

    oldItems = oldItemsActive + oldItemsAllowed

    nSkip = 0
    for oldItem in oldItems:
        try:
            if isDuplicate(oldItem, alreadyIn):
                nSkip += 1
                continue
            if codeMap and oldItem.itemCode not in codeMap: continue
            if codeMap:
                itemCode = codeMap[oldItem.itemCode].code
                itemName = codeMap[oldItem.itemCode].name
            else:
                itemCode = oldItem.itemCode
                itemName = oldItem.itemCode
            newItem = Manager(itemType = oldItem.itemType,
                            itemCode = itemCode,
                            year = toYear,
                            userId = oldItem.userId,
                            comment = oldItem.comment,
                            role = oldItem.role,
                            egroup = oldItem.egroup)
            newItem.status = oldItem.status  # not in c'tor, so we add/update it manually afterwards

            if verbose : logging.info( '++> going to set %s manager for %s in %s to: %s ' % 
                                      (itemType, itemName, toYear, str(newItem)) )
            commitNew( newItem )
        except Exception as e :
            logging.error( "++> ERROR when copying manager for %s %s from %s to %s, got: %s" % 
                          ( itemType, oldItem.itemCode, fromYear, toYear, ''.join(tb.format_exception(None, e, e.__traceback__)) ) )

    logging.info( "%s %s managers set up for %s (%s existing ones skipped)" % (len(oldItems), itemType, toYear, nSkip) )

def copyManagers(prjCodeMap, actCodeMap, tskCodeMap, fromYear, toYear, verbose=False):

    copyMgrItems( codeMap=prjCodeMap, itemType='project', fromYear=fromYear, toYear=toYear, verbose=verbose )
    copyMgrItems( codeMap=actCodeMap, itemType='activity', fromYear=fromYear, toYear=toYear, verbose=verbose )
    copyMgrItems( codeMap=tskCodeMap, itemType='task', fromYear=fromYear, toYear=toYear, verbose=verbose )

    instCodeMap = None
    copyMgrItems( codeMap=instCodeMap, itemType='institute', fromYear=fromYear, toYear=toYear, verbose=verbose )


def copyPAT( fromYear, toYear, verbose=False ):
    copyProjs( fromYear, toYear, verbose )
    copyActs( fromYear, toYear, verbose )
    copyL3Ns( fromYear, toYear, verbose )
    copyTasks( fromYear, toYear, verbose )

def isValidProj( pName ):
    global allProjects, projVetoList, selectedProjList, selectedProjectsOnly
    if allProjects and pName in projVetoList: return False
    if selectedProjectsOnly and pName not in selectedProjList: return False
    return True

def projExists( pName, toYear ):
    entry = db.session.query(Project).filter_by(name=pName, year=toYear, status='ACTIVE').one_or_none()
    if entry:
        return True
    return False

def getProjCode( name, year ):

    projCode = db.session.query(Project.code).filter_by(name=name, year=year, status='ACTIVE').one()
    return projCode


def copyProjs( fromYear, toYear, verbose=False ):
    # proj: name, desc, code
    # act:  name, desc, proj, code
    # task: name, desc, act, needs, pctAtCERN, tType, comment, code, year=2015, shiftTypeId=-1,
    #             status='ACTIVE', locked=False, kind='CORE'
    #             parent='', earliestStart='', latestEnd='', level=1,
    #
    # use the occasion to set the descriptions to the name if not yet set ...

    # projects:
    global projCodeMap

    allProjFrom = db.session.query(Project).filter_by(year=fromYear, status='ACTIVE').all()
    if verbose: logging.info( 'found %s active projects in %s ' % (len(allProjFrom), fromYear) )

    newProjs = []
    projCodeMap = {}  # we need to keep a mapping for the codes for the further processing
    for p in allProjFrom:
        if not isValidProj(p.name): continue
        if projExists( p.name, toYear):
            if selectedProjectsOnly: # for selected projects, crash here:
                raise Exception( '\nERROR: Project %s already exists in DB for year %s -- aborting' % (p.name, toYear) )
            else: # for all processing, update projVetoList and continue
                projVetoList.append( p.name )
                logging.warning( 'Project %s already exists in DB for year %s -- skipping' % (p.name, toYear) )
        
        newPr = Project(name=p.name,
                        desc=p.description if p.description else p.name,
                        code=str(uuid4()))
        newPr.year = toYear
        newProjs.append( newPr )
        projCodeMap[p.code] = newPr

    if verbose : logging.info( f"+++>> {len(newProjs)} new Projects:" )
    for np in newProjs:
        if verbose : logging.info( ' - %s' % np.name )
        try:
            commitNew( np )
        except CMSDbException as e:
            logging.warning( "ERROR when committing project %s - got: %s " % (np.name, str(e)) )

    logging.info( "%s Projects set up for %s " % (len(newProjs), toYear) )

def copyActs( fromYear, toYear, verbose=False ):
    # activities:
    global projCodeMap, actCodeMap
    allActs = db.session.query(CMSActivity).filter_by(year=fromYear, status='ACTIVE').all()
    if verbose : logging.info( 'found %s active activities in %s ' % (len(allActs), fromYear) )
    newActs = [ ]
    actCodeMap = {}  # we need to keep a mapping for the codes for the further processing
    for a in allActs :
        # skip projects in veto list or not selected ones
        if not isValidProj(a.project.name): 
            skipList['acts'].append( (a.project.name, a.name) )
            continue
        newA = CMSActivity( name=a.name,
                            desc=a.description if a.description else a.name,
                            proj=projCodeMap[a.project.code],
                            code=str( uuid4( ) ) )
        newA.year = toYear
        newActs.append( newA )
        actCodeMap[a.code] = newA

    if verbose : logging.info( f"+++>> {len(newActs)} new Activities:" )
    for na in newActs :
        if verbose : logging.info( ' -- %s ' % na.name )
        try:
            commitNew( na )
        except CMSDbException as e:
            logging.warning( "ERROR when committing project %s - got: %s " % (np.name, str(e)) )

    logging.info( "%s Activities set up for %s (%d skipped)" % (len(newActs), toYear, len(skipList['acts'])) )

def copyL3Ns( fromYear, toYear, verbose=False ):

    # level-3s
    allLvl3s = ( db.session.query(Level3Name)
                    .filter(Level3Name.startYear <= int(fromYear) )
                    # .filter(Level3Name.endYear >= fromYear)
                    .all() )
    if verbose : 
        logging.warning( 'found %s active lvl3Names in %s ' % (len(allLvl3s), fromYear) )

    # check all existing lvl3 names and extend the one which are not open-ended
    # (in 2020 all were open-ended, so the extension needs to be written/debugged later)
    for l3n in allLvl3s :
        if l3n.endYear > 0 and l3n.endYear < toYear:
            logging.warning('found expired l3 name, startyear, endyear: %s %s %s' % (l3n.name, l3n.startYear, l3n.endYear) )

def copyTasks( fromYear, toYear, verbose=False ):

    # tasks
    global projCodeMap, actCodeMap, tskCodeMap
    allTasks = db.session.query(Task).filter_by( year=fromYear, status='ACTIVE')\
                         .filter(Task.kind.ilike("CORE") )\
                         .all( )
    if verbose : logging.info( 'found %s active core tasks in %s ' % (len( allTasks ), fromYear) )
    newTasks = [ ]
    tskCodeMap = {}
    for t in allTasks :
        if t.name == None :
            logging.info( "==> task with no name found (code %s) - skipping" % t.code )
            continue

        # skip tasks for projects in veto list
        if not isValidProj( t.activity.project.name ): 
            skipList['tasks'].append( (t.activity.project.name, t.name) )
            continue

        newT = Task( name=t.name,
                     desc=t.description if t.description else t.name,
                     act = actCodeMap[t.activity.code],
                     level3 = t.level3,
                     needs=t.neededWork,
                     pctAtCERN=t.pctAtCERN,
                     tType=t.tType,
                     comment = t.comment,
                     code=str( uuid4( ) ),
                     year=toYear,
                     shiftTypeId=t.shiftTypeId,
                     status='ACTIVE',
                     locked=t.locked,
                     kind='CORE',
                     )
        newT.year = toYear
        newTasks.append( newT )
        tskCodeMap[ t.code ] = newT

    if verbose : logging.info( f"+++>> {len(newTasks)} new Tasks:" )
    for nt in newTasks :
        if verbose : logging.info( ' --- %s ' % nt.name )
        try:
            commitNew( nt )
        except CMSDbException as e:
            logging.warning( "ERROR when committing project %s - got: %s " % (np.name, str(e)) )

    logging.info( "%s Tasks set up for %s (%d skipped)" % (len(newTasks), toYear, len(skipList['tasks'])) )

    copyManagers( projCodeMap, actCodeMap, tskCodeMap, fromYear, toYear, verbose )


def DoNot_copyTimeLines(fromYear, toYear, verbose):

    # get the list of all institutes with a timeline:
    tlInstCodesFrom = db.session.query( TimeLineInst.code.distinct() )\
                                .filter(TimeLineInst.year == fromYear)\
                                .all()

    if verbose: logging.info( 'found %s active timelines for institutes in %s ' % (len(tlInstCodesFrom), fromYear) )

    newTlInsts = []
    for tliCode, in tlInstCodesFrom:
        oldTli = db.session.query(TimeLineInst).filter_by(code=tliCode, year=fromYear).order_by( TimeLineInst.timestamp.desc() ).first()
        newTlI = TimeLineInst()
        newTlI.code      = tliCode
        newTlI.year      = toYear
        newTlI.cmsStatus = oldTli.cmsStatus # keep that value from last year's entry
        newTlI.comment   = oldTli.comment     # keep that value from last year's entry
        newTlInsts.append( newTlI )

    if verbose : logging.info( "\nnew TimeLines for Institutes:" )
    index = 0
    for newItem in newTlInsts :
        index += 1
        if verbose : logging.info( "%d/%d - %s" % (index, len(newTlInsts), newItem) )
        db.session.add( newItem )
    db.session.commit()

    logging.info( "%s TimeLines for Institutes set up for %s " % (len(newTlInsts), toYear) )

    # get the list of all institutes with a timeline:
    tlUserCmsIdFrom = db.session.query( TimeLineUser.cmsId.distinct( ) ) \
        .filter( TimeLineUser.year == fromYear ) \
        .all( )

    if verbose : logging.info( 'found %s active timelines for users in %s ' % (len( tlUserCmsIdFrom), fromYear) )

    newTlUsers = [ ]
    for tluID, in tlUserCmsIdFrom:
        oldTlu = db.session.query(TimeLineUser).filter_by( cmsId=tluID, year=fromYear ).order_by( TimeLineUser.timestamp.desc() ).first()
        newTlu = TimeLineUser()
        newTlu.cmsId        = tluID
        newTlu.instCode     = oldTlu.instCode
        newTlu.year         = toYear
        newTlu.mainProj     = oldTlu.mainProj
        newTlu.isAuthor     = oldTlu.isAuthor
        newTlu.isSuspended  = oldTlu.isSuspended
        newTlu.status       = oldTlu.status
        newTlu.category     = oldTlu.category
        newTlu.dueApplicant = oldTlu.dueApplicant
        newTlu.dueAuthor    = oldTlu.dueAuthor
        newTlu.yearFraction = oldTlu.yearFraction
        newTlu.comment      = oldTlu.comment  # keep that value from last year's entry
        newTlUsers.append( newTlu )

    if verbose : logging.info( "\nnew TimeLines for Users:" )
    # add all new timelines and commit them only at the end to speed up
    index =0
    for newItem in newTlUsers :
        index += 1
        if verbose : logging.info( "%d/%d - %s" % (index, len(newTlUsers), newItem) )
        db.session.add( newItem )
    db.session.commit()

    logging.info( "%s TimeLines for Users set up for %s " % (len(newTlUsers), toYear) )

def prepareFor(fromYear, toYear, verbose=False):

    copyPAT( fromYear=fromYear, toYear=toYear, verbose=verbose )

    # timelines are now created when they are needed (change of year), 
    # so we do not need to copy them over here. 
    ####  copyTimeLines( fromYear=fromYear, toYear=toYear, verbose=verbose )

def updateCeilings( toYear ):
    
    sumNeeds2024 = {
        'BRIL' : 320.0,
        'DAQ' : 294.0,
        'ECAL' : 554.0,
        'General' : 66.0,
        'HCAL' : 475.0,
        'HGCAL (CE)' : 1092.0,
        'L1 Trigger' : 637.0,
        'MTD' : 491.0,
        'Muon' : 1840.0,
        'Offline Software And Computing' : 2164.0,
        'PPD' : 1528.0,
        'PPS' : 368.0,
        'Run Coordination' : 766.0,
        'Tracker' : 1626.0,
        'Trigger Coordination' : 485.0,
    }
    
    cActRaw = db.session.query(EprCeiling).filter(EprCeiling.year == toYear).all()
    cAct = { x.code: x for x in cActRaw }
    print( f'existing ceilings for {toYear}: {cAct}')

    newCeil25 = []
    for p, ceil in sumNeeds2024.items():
        projCode25Raw = db.session.query(Project.code).filter_by(name=p, year=2025, status='ACTIVE').one_or_none()
        
        print( f'{p} - {ceil} -- {projCode25Raw}')
        if projCode25Raw is None: continue
        
        projCode25 = projCode25Raw[0]
        if projCode25 in cAct.keys():
            if cAct[ projCode25 ].ceiling != ceil:
                print( f'found existing ceiling {cAct[projCode25]} for {projCode25} in {toYear} -- going to update ceiling from {cAct[ projCode25 ].ceiling} to {ceil}')
                cAct[ projCode25 ].ceiling = ceil
                cAct[ projCode25 ].timestamp = datetime.datetime.now(datetime.timezone.utc)
        else:       
            newCeil = EprCeiling( code=projCode25, year=toYear, ceiling=ceil )
            newCeil25.append( newCeil )

    print( f'found {len(newCeil25)} new ceilings: ')
    for c25 in newCeil25:
        commitNew( c25 )
    
    db.session.commit()
    
    # print( f'{newCeil25}')


if __name__ == '__main__':

    from app import create_app
    theApp = create_app('default')
    with theApp.app_context():

        # prepareFor( fromYear=2016, toYear=2017, verbose=True )
        # prepareFor( fromYear=2017, toYear=2018, verbose=True )
        # prepareFor( fromYear=2018, toYear=2019, verbose=True )
        # prepareFor( fromYear=2020, toYear=2021, verbose=False )

        fromYear, toYear = (2024, 2025)

        print( f'\n++++ going to prepare for {toYear} copying from {fromYear} \n')
        prepareFor( fromYear=fromYear, toYear=toYear, verbose=True )

        # updating the ceilings need the new project codes, so do this after the "copy over":
        print( f'going to update ceilings for {toYear} ... ')
        updateCeilings( toYear )
