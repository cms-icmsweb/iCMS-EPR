# -*- coding: utf-8 -*-

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import time
from sqlalchemy.exc import IntegrityError

from app import db
from app.models import EprInstitute, EprUser, Role, Category
from app.ldapAuth import getLogin

def updateInst(selInstCode=None, year=2016):

    start = time.time()
    with open('imports/iCMS_Institute.json', 'r') as instFile:
        institutes = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i institutes' % len(institutes) )

    exitingInsts = db.session.query(EprInstitute.code).filter(EprInstitute.year==year).all()
    exitingCernIds = db.session.query(EprInstitute.cernId).filter(EprInstitute.year==year).all()

    # print 'first 10 ids: ', exitingInsts[:10]

    start = time.time()
    addedInst = []
    nOK = 0
    offset = 0
    fakeCernInstId = -10

    if selInstCode:
        print( "Updating selected institute, code='%s'" % selInstCode )
        if (selInstCode,) in exitingInsts:
            print( "selected institute ", selInstCode, 'already in DB. Exiting.' )
            return

    for i in institutes:

        iCode = i['code'].decode('latin-1').encode("utf-8")

        if selInstCode and selInstCode.lower() not in iCode.lower():
            continue

        if (iCode,) in exitingInsts: continue

        print( '\nprocessing: ', str( i ).encode('ascii', 'xmlcharrefreplace') )

        iName = None
        if i['shortName']:
            iName = i['shortName'].encode('utf-8')    # .encode('ascii', 'xmlcharrefreplace')
        if not iName and i['name']:
            iName = i['name'].encode("utf-8")
        if not iName:
            print( "ERROR: Ignoring Institute w/o name : ", str(i).encode('ascii', 'xmlcharrefreplace') )
            continue

        if not i['cernInstId'] :
            fakeCernInstId -= 1
            print( "Handling Institute w/o cernInstID (new fake one is: %i) : %s " % (fakeCernInstId, str(i).encode('ascii', 'xmlcharrefreplace') ) )
            cernInstId = fakeCernInstId
        else:
            cernInstId = int( i['cernInstId'] )

        # handle duplicate cernInstIds:
        comment = ''
        if (cernInstId,) in exitingCernIds:
            fakeCernInstId -= 1
            print( "handling duplicate cernInstId %i - set to %i " % (cernInstId, fakeCernInstId) )
            comment = "duplicate cernInstId: %i - set to %i " % (cernInstId, fakeCernInstId)
            cernInstId = fakeCernInstId

        countryName = i['country'].decode('latin-1').encode("utf-8")[0].upper()+i['country'].decode('latin-1').encode("utf-8")[1:].lower()

        db.session.autoflush = False

        if cernInstId in addedInst:
            print( "ERROR duplicate found for %i after handling dups ! - this should not happen!! skipping ! " % cernInstId )
            continue

        inst = EprInstitute(cernId  = cernInstId,
                         name    = iName,
                         country = countryName,
                         code    = i['code'].decode('latin-1').encode("utf-8"),
                         comment = comment,
                         cmsStatus= i['cmsStatus'],
                         year=year
                         )
        try:
            db.session.add(inst)
            db.session.flush()
        except Exception as e:
            print( "ERROR: flushing newly added inst %s ... got: %s " % (inst, str(e)) )
            db.session.rollback()
            continue

        try:
            db.session.commit()
            nOK += 1
            addedInst.append( cernInstId )
        except Exception as e:
            print( "ERROR: committing ... got: ", str(e) )
            db.session.rollback()
            try:
               inst = db.session.query(EprInstitute).filter_by(cernId=cernInstId).one()
            except Exception as e:
                db.session.rollback()
                print( "ERROR: no institute found for ", str(i) )
                continue

            if inst.cernId == cernInstId and \
               inst.name.lower() == iName.lower() and \
                inst.country.lower() == i['country'].lower() : # duplicate entry, ignore
                print( "duplicate found for %s, ignoring" % inst.name.encode('ascii', 'xmlcharrefreplace') )
            else:
                print( "ERROR inserting inst with : cernId %i shortname %s country %s " % ( cernInstId,
                                                                                           iName.encode('ascii', 'xmlcharrefreplace'),
                                                                                           i['country'].encode('ascii', 'xmlcharrefreplace') ) )
                print( "      found in DB         : %s " % str( db.session.query(EprInstitute).filter_by(cernId=cernInstId).all() ) )

    print( "imported %i (%i OK) institutes in %f sec." % (len(institutes), nOK, time.time()-start) )
    print( '\n' )


def fix_missing_inst_fields():
    """
    Updates the institute table by setting the cms_status to reflect the value in JSON dumps.
    SQL pre-requisites:
        ALTER TABLE insts ADD COLUMN cms_status VARCHAR(20) NOT NULL DEFAULT 'No';
        ALTER TABLE insts_version ADD COLUMN cms_status VARCHAR(20) NOT NULL DEFAULT 'No';
        ALTER TABLE insts_version ADD COLUMN cms_status_mod BOOLEAN NOT NULL DEFAULT FALSE;

    This method is useful only if the preceding import ignored the cms_status and the columns were added as above.
    The import script has been modified to handle cms_status with any subsequent full import and thus render this
    method useless.
    """
    stored_insts = {inst.code: inst for inst in db.session.query(EprInstitute).all()}
    if len(stored_insts.keys()) == 0:
        return

    db.session.autoflush = False

    with open('imports/iCMS_Institute.json', 'r') as instFile:
        read_insts = json.load(instFile, encoding='latin-1')

    for inst in [i for i in read_insts if i['code'] in stored_insts.keys()]:
        if stored_insts[inst['code']].cms_status != inst['cmsStatus']:
            stored_insts[inst['code']].cms_status = inst['cmsStatus']

    db.session.flush()
    db.session.commit()


def createCategories():

    catInfo = { "Administrative" : False,
                "Doctoral Student" : True,
                "Engineer" : True,
                "Engineer Electronics" : True,
                "Engineer Mechanical" : True,
                "Engineer Software" : True,
                "Non-Doctoral Student" : False,
                "Other" : False,
                "Physicist" : True,
                "Technician" : False,
                "Theoretical Physicist" : True,
              }
    for cat, ok in catInfo.items():
        cat = Category(name=cat, ok=ok )
        try:
            db.session.add(cat)
            db.session.commit()
        except IntegrityError as e:
            db.session.rollback()
            if 'UNIQUE constraint failed' in str(e):
                pass
            else:
                print( "got error when creating categories" )
                raise e

def getCatNameMap():

    start = time.time()
    with open('imports/iCMS_MemberActivity.json', 'r') as instFile:
        cats = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    catNameMap = {}
    for i in cats:
        catNameMap[ i['id'] ] = i['name']

    return catNameMap

def convertName(firstName, lastName):

    fullName = None

    try:
        fullName = '%s, %s' % (lastName.encode('utf-8'),
                               firstName.encode('utf-8') )
        return fullName
    except Exception as e:
        print( "name conversion without decoding failed: %s" % str(e) )

    for coding in [ 'latin-1', 'iso-8859-1', 'iso-8859-15']:
        try:
            fullName = '%s, %s' % (lastName.decode(coding).encode('utf-8', 'xmlcharrefreplace'),
                                   firstName.decode(coding).encode('utf-8', 'xmlcharrefreplace'))
            return fullName
        except Exception as e:
            print( "name conversion with %s failed: %s" % (coding, str(e)) )
            continue
    return None


def updateUserAuthReq():

    start = time.time()
    with open('imports/iCMS_Person.json', 'r') as instFile:
        users = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'len: ', len(users) )

    print( users[0] )

    existingUsers = db.session.query(EprUser.cmsId).all()
    catNameMap = getCatNameMap()

    start = time.time()
    nExMem = 0
    nOK = 0
    for u in users:

        cmsId = int(u['cmsId'])
        if (cmsId,) not in existingUsers: continue

        uDB = db.session.query(EprUser).filter_by(cmsId=cmsId).one()
        uDB.authorReq = int( u[ 'isAuthor' ] )

    try:
        db.session.commit()
    except Exception as e:
        print( "ERROR updating authorReq for user %i, got: %s" % (cmsId, str(e)) )
        db.session.rollback()

    print( "done" )
    return


def updateUsers(selInstCode=None, selCmsId=None, year=2016, addNew=False):

    # # add a default admin for testing:
    # a = User(name='ap', hrId=0, cmsId=0, instId=64931)
    # aRole = Role.query.filter_by(name='Administrator').first()
    # a.role_id = aRole.id
    # db.session.add(a)

    start = time.time()
    with open('imports/iCMS_Person.json', 'r') as instFile:
        users = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'len: ', len(users) )

    # print users[0]

    # get all users for all years, so we can skip the ones not known at all ...
    existingUsers = db.session.query(EprUser.cmsId).all()

    catNameMap = getCatNameMap()

    if selInstCode:
        print( "Updating only users for institute ", selInstCode )

    if selCmsId:
        print( "Updating only user for cmsId %i " % selCmsId )

    start = time.time()
    nExMem = 0
    nOK = 0
    nUnknown = 0
    fakeUserNameIndex = 0
    for u in users:

        if not addNew and (int( u['cmsId'] ),) not in existingUsers:
            nUnknown += 1
            # print "skipping update of non-existing user ", str(u).encode('ascii', 'xmlcharrefreplace')
            continue

        if selCmsId and int(selCmsId) != int(u['cmsId']): continue

        iCode = u['instCode'].encode('utf-8')  # .encode('ascii', 'xmlcharrefreplace')
        # if 'exmember' in u['status'].lower() :
        #     nExMem += 1
        #     continue # ignore exmembers for now

        if selInstCode and selInstCode.lower() not in iCode.lower(): continue

        print( '\n processing user %s iCode %s ' % (u['cmsId'], iCode) )
        try:
            inst = db.session.query(EprInstitute).filter_by(code=iCode).one()
        except Exception as e:
            if 'No row was found for one()' in str(e) and \
                'exmember' in u['status'].encode('ascii', 'xmlcharrefreplace').lower() :
                print( 'IGNORING exmember %s,%s (%s) as no main institute could be found' % (
                        u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                        u['instCode'].encode('ascii', 'xmlcharrefreplace').lower() ) )
                continue
            else:
                print( "ERROR from DB when looking for institute code %s, user %s (skipping) : %s" % (str(iCode), str(u).encode('ascii', 'xmlcharrefreplace'), str(e)) )
            continue

        print( "found institute code %s id %i for year %i" % (inst.code, inst.id, inst.year) )

        fullName = convertName(u['firstName'], u['lastName'])
        if not fullName:
            print( "name could not be converted, skipping " )
            continue

        if not u['activityId']:
            print( "user w/o category: %s -- ignoring ", u['cmsId'] )
            continue

        print( "updating user %s (%s)" %  (u['cmsId'], fullName) )

        user = None
        try:
            user = db.session.query(EprUser).filter_by(cmsId=int(u['cmsId'])).one()
        except Exception as e:
            if "No row was found for one" in str(e):
                pass
            else:
                raise e

        if user:
            print( "found user: ", user, ' going to update ...' )

            user.username = u['niceLogin']
            user.name = fullName
            user.hrId = int(u['hrId'])
            user.cmsId = int(u['cmsId'])
            user.mainInst = int(inst.id)
            user.authorReq= bool( u[ 'isAuthor' ] )
            user.status= u['status']
            user.isSuspended= bool(u['isAuthorSuspended'])
            user.category= int(u['activityId'])
            user.year=year

            print( " ...  updated user: ", user )

            try:
                db.session.commit()
                nOK += 1
            except Exception as e:
                print( "ERROR: got: ", str(e) )
                db.session.rollback()
                print( "ERROR updating user with : name %s,%s hrId %i cernId %i inst %s " % (
                             u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                             u['hrId'],u['cmsId'],
                             u['instCode'].encode('ascii', 'xmlcharrefreplace')) )

        else: # user not found for this year, create ...
            print( 'user %s not found, going to create ...' % (fullName, ) )

            user = EprUser(
                        username = u['niceLogin'],
                        name = fullName,
                        hrId = int(u['hrId']),
                        cmsId = int(u['cmsId']),
                        instId = int(inst.cernId),
                        authorReq= bool( u[ 'isAuthor' ] ),
                        status= u['status'],
                        isSusp= bool(u['isAuthorSuspended']),
                        categoryName= catNameMap[ u['activityId'] ],
                        year=year,
                        mainInst=int(inst.id)
                        )

            try:
                db.session.add(user)
                db.session.commit()
                nOK += 1
            except Exception as e:
                print( "ERROR: got: ", str(e) )
                db.session.rollback()
                print( "ERROR updating user with : name %s,%s hrId %i cernId %i inst %s " % (
                             u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                             u['hrId'],u['cmsId'],
                             u['instCode'].encode('ascii', 'xmlcharrefreplace')) )

    print( "imported %i (of %i, skipped %i unknown users) users in %f sec." % (nOK, len(users), nUnknown, time.time()-start) )
    print( "found %i exmembers" % nExMem )

def showInst(limit=10):

    import pprint
    inst = db.session.query(EprInstitute).all()
    if limit < 0:
        pprint.pprint( inst )
    else:
        pprint.pprint( inst[:limit] )

def updateAll( ):
    fix_missing_inst_fields()
    updateInst()
    updateUsers()

if __name__ == '__main__':

    updateAll( )
