
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

# run like:
# (export PYTHONPATH=.:$PYTHONPATH; time ./apRun.sh ./scripts/sendMailsToCBI-Nov2020.py | tee mailNov2020.log )

from __future__ import print_function

import datetime

import app.smtp as smtp

from app import db
from app.models import EprUser, Manager, TimeLineInst, AllInstView

from app.ldapAuth import getUserEmail
from app.sendEmails import send_email_detail


plLe50 = '''
Dear {firstName},

I was looking at the EPR tool and noticed that your institution has pledged {pledge:4.1f} which is less than 50% of required EPR for 2020. While I know your group is hard at work contributing to CMS, I would like to point out that it is much easier to put in pledges for EPR now as opposed to remember everything months later.

If your project for 2020 has been affected by covid, please contact the engagement office (cms-engagement-office@cern.ch) or your project manager for help in finding a suitable task for the end of 2020 and the beginning of 2021. 

Please also keep in mind that Institutional Responsibility can be a good way to consistently have enough EPR for your group. There is also some documentation on how to make a pledge for an InstResp task, you can find it at:

https://icms.cern.ch/docs/iCMS-EPR/userGuide/instMgt/instRespPledges.html

If you have any questions please contact the engagement office, as we are here to help you.

Sincerely,
Rachel 

'''

msgFooter = '''
------------------------------------------------------------------------
--------  Message sent for Rachel Bartek (WASHINGTON-CUA) on behalf of the CMS Engagement Office
--------  Recipients: Team leaders (CBIs/CBDs) of %s
--------  Please send replies to this message to the submitter cms-engagement-office@cern.ch which is set as the default reply address
------------------------------------------------------------------------
'''


def getMailTemplate(fraction):

    if float(fraction) < 0.5: 
        return plLe50

    return None

def cleanName(name):

    newName = name # .encode( 'ascii', 'xmlcharrefreplace' )
    newName = newName.replace( '&#252;', 'ue' )\
                     .replace( '&#233;', 'e' )

    return newName.strip()

def sendMailToCBIs(app):

    selYear = 2020
    insts = TimeLineInst.query.filter_by(year=selYear, cmsStatus='Yes').order_by(TimeLineInst.code).all()
    print( "found %s insts" % len(insts) )

    ignoreList = ['ZZZ', 'TIFR']

    alreadySent = []

    mailsDone = []

    # print (" %16s  | eprFrac | %s | %100s | Dear %s " % ('instCode', '#', 'CBIs/CBDs', 'firstName(s)') )
    for inst in insts:

        # if len(mailsDone) > 10: break

        if inst.code in ignoreList: continue

        if inst.code in alreadySent:
            print( "Mail for %s already sent - skipping." % inst.code )
            continue

        # print( 'processing', inst )

        try:
            instView = db.session.query(AllInstView)\
                        .filter(AllInstView.code == inst.code)\
                        .filter(AllInstView.year == selYear)\
                        .one()
        except Exception as e:
            print( "ERROR searching for %s in allInstView, got: %s" % ( inst.code, str(e) ) )

        mailBody = getMailTemplate(instView.pledgedFract)
        if mailBody is None:
            print( "%s seems to be OK, no mail sent" % inst.code)
            continue

        if float(instView.actAuthors) < 0.05:
            print( "%s has no authors, no mail sent" % inst.code)
            continue

        instMgrs = db.session.query(EprUser)\
                             .join(Manager, Manager.userId == EprUser.id)\
                             .filter(Manager.itemType == 'institute')\
                             .filter(Manager.itemCode == inst.code) \
                             .filter( Manager.year == selYear) \
                             .filter( Manager.status == 'active')\
                             .all()

        fNameList = [ cleanName( x.name.rsplit(',')[-1] ) for x in instMgrs ]
        firstNames = ', '.join( fNameList )
        emailsTo = [ getUserEmail( x.username ) for x in instMgrs ]

        print( '%s has %4.2f pledged (%4.2f%%, nAuth: %s) mail going to be send ' % 
                ( inst.code, instView.pledged, instView.pledgedFract, instView.actAuthors ) )
        mailBody = mailBody.format( firstName=firstNames, pledge=instView.pledged )
        mailBody += msgFooter % inst.code

        # print ( '*'*80+'\n')
        # print( mailBody )

        # msg = Message( )
        # msg.subject = '[iCMS-EPR] Summary on the EPR 2016 for %s' % inst.code
        # msg.fromAddress = 'cms-engagement-office@cern.ch'
        # msg.toAddresses = emailsTo
        # msg.ccAddresses = ['cms-engagement-office@cern.ch']
        # msg.replyToAddress = 'cms-engagement-office@cern.ch'
        # msg.body = mailBody

        # logEmail(msg, fNamePrefix='instEPRsummary2016')

        # status, msg = send_email_detail( to = ','.join( emailsTo ), 
        #                    subject = '[iCMS-EPR] Low pledges for %s in %d' % (inst.code, selYear),
        #                    sender = 'cms-engagement-office@cern.ch',
        #                    body = mailBody,
        #                    bcc = '%s,%s' % (app.config['ICMS_MAIL_BCC'],'cms-engagement-office@cern.ch'), 
        #                    reply_to = 'cms-engagement-office@cern.ch', 
        #                    source_app = 'epr')
        # print( '%s> status: %s - msg: %s ' % (inst.code, status, msg) )

        mailsDone.append(inst.code)

        # activate the next line to really send the mail ... 
        # smtp.SMTP( ).sendEmail( msg.subject, msg.body, msg.fromAddress, msg.toAddresses, msg.ccAddresses, replyToAddress=msg.replyToAddress )

    print("\nMails were ( NOT :-) ) sent on %s to the following (%d) institutes:\n ['%s'] \n" % (datetime.datetime.now(), len(mailsDone),  "', '".join(mailsDone)))

if __name__ == '__main__':

    #
    # to be called from bash in an EPR environment like:
    # (source ./venv/bin/activate; export PYTHONPATH=`pwd`; export NLS_LANG=AMERICAN_AMERICA.UTF8; python ./scripts/sendMailsToCBI.py )
    #

    from app import create_app
    app = create_app('default')
    with app.app_context():
        # Extensions like Flask-SQLAlchemy now know what the "current" app
        # is while within this block. Therefore, you can now run........

        sendMailToCBIs(app)
