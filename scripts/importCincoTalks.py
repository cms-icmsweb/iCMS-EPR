
#  Copyright (c) 2021 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os, sys
import time
import datetime
import dateutil.parser

import json

import hashlib

from sqlalchemy import and_
import sqlalchemy as sa

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db, create_app

from app.main.JsonDiffer import Comparator

from icms_orm import epr_bind_key, epr_schema_name

from config import config

def class_to_json(cls):
    retval = {}
    for (key, value) in cls.__dict__.items():
        if key.startswith('__') and key.endswith('__') : continue
        if key.startswith('_sa_instance') : continue
        if isinstance(value, (datetime.datetime, datetime.date, datetime.time)):
            retval[key] = value.isoformat()[:10] # keep only the date part ....
        else:
            try:
                retval[key] = value.encode('utf-8')
            except AttributeError:
                retval[key] = value

    return retval


class CincoFullSpeakerInfo(db.Model):
    __tablename__ = 'FULLSPEAKERINFO'
    __bind_key__ = 'CincoDB'
    __table_args__ = { "implicit_returning":  False, 'schema' : 'cms_cc' }

    cern_id = db.Column( 'cern_id', db.Integer, nullable=False, primary_key=True )
    conf_id = db.Column( 'conf_id', db.Integer, nullable=False, primary_key=True )
    pres_id = db.Column( 'pres_id', db.Integer, nullable=False, primary_key=True )

    cernusername  = db.Column( 'cernusername', db.String(255) )
    inst_code     = db.Column( 'inst_code', db.String(50) )
    country_code  = db.Column( 'country_code', db.String(2) )
    country       = db.Column( 'country', db.String(1024) )
    inst_name     = db.Column( 'inst_name', db.String(255) )
    conf_start    = db.Column( 'conf_start', db.DateTime )
    conf_name     = db.Column( 'conf_name', db.String(1024) )
    pres_title    = db.Column( 'pres_title', db.String(1024) )
    pres_category = db.Column( 'pres_category', db.String(8) )

    def to_json(self):
        return class_to_json(self)

class IcmsFullSpeakerInfo(db.Model):
    __tablename__ = 'fullspeakerinfo'
    __table_args__ = {'implicit_returning': True, 'schema': 'cms_cc'}
    __bind_key__ = epr_bind_key()

    cern_id = db.Column( 'cern_id', db.Integer, nullable=False, primary_key=True )
    conf_id = db.Column( 'conf_id', db.Integer, nullable=False, primary_key=True )
    pres_id = db.Column( 'pres_id', db.Integer, nullable=False, primary_key=True )

    cernusername  = db.Column( 'cernusername', db.String(255) )
    inst_code     = db.Column( 'inst_code', db.String(50) )
    country_code  = db.Column( 'country_code', db.String(2) )
    country       = db.Column( 'country', db.String(1024) )
    inst_name     = db.Column( 'inst_name', db.String(255) )
    conf_start    = db.Column( 'conf_start', db.DateTime )
    conf_name     = db.Column( 'conf_name', db.String(1024) )
    pres_title    = db.Column( 'pres_title', db.String(1024) )
    pres_category = db.Column( 'pres_category', db.String(8) )

    def to_json(self):
        return class_to_json(self)

    def update_from_json(self, jNew):
        
        cern_id       = jNew[ 'cern_id' ]
        conf_id       = jNew[ 'conf_id' ]
        pres_id       = jNew[ 'pres_id' ]

        cernusername  = jNew[ 'cernusername' ]
        inst_code     = jNew[ 'inst_code' ]
        country_code  = jNew[ 'country_code' ]
        country       = jNew[ 'country' ]
        inst_name     = jNew[ 'inst_name' ]
        conf_start    = dateutil.parser.isoparse( jNew[ 'conf_start' ] )
        conf_name     = jNew[ 'conf_name' ]
        pres_title    = jNew[ 'pres_title' ]
        pres_category = jNew[ 'pres_category' ]

        db.session.add( self )

def makeDateTime(inString):

    dString, tString = inString.split()
    mon,day,yr = [int(x) for x in dString.split('/')]
    hr, min, sec = [int(x) for x in tString.split(':')]

    return datetime.datetime(yr,mon,day, hr,min, sec)


def importCincoSpeakerInfo():

    cincoEntries = (db.session.query( CincoFullSpeakerInfo ).all())
    
    existingEntries = (db.session.query( IcmsFullSpeakerInfo ).all())

    print("\nfound %s entries in CINCO DB, %s in iCMS DB" % (len(cincoEntries), len(existingEntries) ) )

    cincoJsons = [ x.to_json() for x in cincoEntries ]
    eprJsons   = [ x.to_json() for x in existingEntries ]

    # calculate hashes for a quick check:
    cincoHashes = {}
    for item in cincoJsons:
        # key0 = hashlib.md5( json.dumps(item).encode('utf-8') ).digest()
        key0 = '%s - %s - %s' % (item['cern_id'], item['conf_id'], item['pres_id'])
        if key0 in cincoHashes.keys():
            print('dup found in cinco for ', key0)
        cincoHashes[ key0 ] = item

    eprHashes = {}
    eprNew = {}
    for item in eprJsons:
        # key0 = hashlib.md5( json.dumps(item).encode('utf-8') ).digest()
        key0 = '%s - %s - %s' % (item['cern_id'], item['conf_id'], item['pres_id'])
        if key0 in eprHashes.keys():
            print('dup found in epr for ', key0)
        eprHashes[ key0 ] = item
        if key0 not in cincoHashes.keys():
            eprNew[key0] = item

    cincoNew = {}
    for h, j in cincoHashes.items():
        if h not in eprHashes.keys():
            cincoNew[ h ] = j

    print( 'Found %d items only in Cinco, %d only in EPR' % (len(cincoNew), len(eprNew)) )

    plComp = Comparator()

    count = 0
    nUpdated = 0
    for k, jOld in eprHashes.items():
        count += 1
        # if count > 7: break
        jNew = cincoHashes[k]
        jDiff = plComp.compare_dicts(jOld, jNew)
        if '_update' in jDiff:
            print( '\ndiff found for %s: %s' % (k, jDiff) )
            print( ' epr  : ', jOld )
            print( ' cinco: ', jNew )
            
            cern_id, conf_id, pres_id = k.split(' - ')
            eprItem = (db.session.query( IcmsFullSpeakerInfo )
                         .filter( IcmsFullSpeakerInfo.cern_id == cern_id)
                         .filter( IcmsFullSpeakerInfo.conf_id == conf_id)
                         .filter( IcmsFullSpeakerInfo.pres_id == pres_id)
                         .one())

            for k, v in jDiff['_update'].items():
                if 'conf_start' in k:
                    # print( 'updating start: ', dateutil.parser.isoparse( v ), type(dateutil.parser.isoparse( v )) )
                    eprItem.conf_start = dateutil.parser.isoparse( v )
                else:
                    eprItem[ k ] = v

            print('\n==> ', eprItem.to_json() )
            print( '\n')

            nUpdated += 1
            db.session.add( eprItem )
    
    nAdded = 0
    for key0, item in cincoNew.items():
        newEntry = IcmsFullSpeakerInfo()
        for k, v in item.items():
            newEntry[k] = v
        nAdded += 1
        ## db.session.add( newEntry )

        print('added ', newEntry.to_json() )


    print('updated %s, added %s entries' % (nUpdated, nAdded) )

#     try:
#         db.session.commit()
#     except Exception as e:
#         print("ERROR when committing changes -- got:", str(e))


if __name__ == '__main__':

    theApp = create_app('default')
    with theApp.app_context():

        importCincoSpeakerInfo()
