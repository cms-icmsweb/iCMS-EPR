
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import sys
from pprint import pprint

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app.models import *
from uuid import uuid4
import logging

logging.basicConfig(format = '%(asctime)-15s: %(message)s')
logger = logging.getLogger("hgcal18")
logger.setLevel(logging.INFO)

def updateHGCAL(srcYear, tgtYear, verbose=False):

    hgcalSrc = Project.query.filter_by(year=srcYear,name="HGCAL").one()
    ahSrc = CMSActivity.query.filter_by(year=srcYear,projectId=hgcalSrc.id).all()
    tSrc = Task.query.filter_by(year=srcYear).filter(Task.activityId.in_([x.id for x in ahSrc])).all()
    
    hgcalTgt = Project.query.filter_by(year=tgtYear,name="HGCAL").one()
    ahTgt = CMSActivity.query.filter_by(year=tgtYear,projectId=hgcalTgt.id).all()
    tTgt = Task.query.filter_by(year=tgtYear).filter(Task.activityId.in_([x.id for x in ahTgt])).all()

    actMap = {}
    for oldAct in ahSrc:
        for newAct in ahTgt:
            if oldAct.id not in actMap.keys() and oldAct.name == newAct.name:
                actMap[ oldAct.id ] = newAct
                print( "mapping old: ()%s,%s) new (%s, %s)" % (oldAct.name, oldAct.description, newAct.name, newAct.description) )
    
    print( "mapped %d activities " % ( len( actMap.keys() ) ) )
    pprint(actMap)
    
    print( "found %d tasks in %d activities for %d" % (len(tSrc), len(ahSrc), srcYear) )
    print( "found %d tasks in %d activities for %d" % (len(tTgt), len(ahTgt), tgtYear) )

    doneList = []
    newTasks = [ ]
    for task in tSrc:
        if (task.activityId, task.name, task.description) in [(x.activityId, x.name, x.description) for x in tTgt]:
            print( "task %s already in, skipping" % task )
            continue

        if (task.activityId, task.name, task.description) in doneList:
            print( "found duplicate task %s, ignoring ... " % task )
            continue

        print( "copying task to %d" % tgtYear )
        doneList.append( (task.activityId, task.name, task.description) )

        newT = Task( name=task.name,
                     desc=task.description if task.description else task.name,
                     act = actMap[ task.activityId ],
                     needs=task.neededWork,
                     pctAtCERN=task.pctAtCERN,
                     tType=task.tType,
                     comment = task.comment,
                     code=str( uuid4( ) ),
                     year=tgtYear,
                     shiftTypeId=task.shiftTypeId,
                     status='ACTIVE',
                     locked=task.locked,
                     kind='CORE',
                     )
        newTasks.append( newT )

    if verbose : logging.info( "\nnew Tasks:" )
    for nt in newTasks :
        if verbose : logging.info( ' --- %d %s' % ( nt.year, nt.name.encode('utf-8', 'xmlcharrefreplace') if nt.name else 'n/a' ) )
        commitNew( nt )

    print( "Copied %d tasks from %d to %d" % (len(doneList), srcYear, tgtYear) )

def showTaskDiff( tSrc, tTgt ):

    sSrc = set( [(x.name, x.description) for x in tSrc] )
    sTgt = set( [(x.name, x.description) for x in tTgt] )
    # pprint(sSrc)
    # pprint(sTgt)
    
    print( '16-18' )
    pprint( sSrc-sTgt )
    print( '18-16 : ' )
    pprint( sTgt-sSrc )
    

if __name__ == '__main__':

    from app import create_app
    theApp = create_app('default')
    with theApp.app_context():
        updateHGCAL(2016, 2018, verbose=True)

# checks/fixing/iterating in DB:
#
# -- select * from epr.projects where name like '%HGC%';
#
# -- select name, description, year from epr.cms_activities where project_id in (82,132) order by name;
# 
# -- delete from epr.tasks where activity_id in (select id from epr.cms_activities where project_id=82) and kind='CORE';
# -- select * from epr.tasks where activity_id in (select id from epr.cms_activities where project_id=82);
# 
# -- delete from epr.tasks where activity_id in (select id from epr.cms_activities where project_id=132) and kind='CORE';
# -- select * from epr.tasks where activity_id in (select id from epr.cms_activities where project_id=132);
