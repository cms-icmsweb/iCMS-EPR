#!/usr/bin/env python

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

# tasks which were created using the tool during 2016 had the "kind" set to "Core"
# and were not found during the cloning exercise to create the 2017 entries, as
# the script was looking for kind of "CORE", ignoring the "Core" ones.
# This script looks for the "Core" tasks in 2016 (there were six at the time of writing)
# then copies them to 2017 finding the corresponding activity, and adds the 2017 managers.
# Finally it changes the kind for 2016 to "CORE".

import time
import json
import sys
import copy
import difflib
from uuid import uuid4

# from sqlalchemy.exc import IntegrityError

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db
from app.models import Project, CMSActivity, Task, Category
from app.models import Manager

from app.models import commitNew, commitUpdated


def fixCoreTasks( fromYear, toYear, verbose=False ) :

    # tasks
    coreTasks = Task.query.filter_by( year=fromYear, status='ACTIVE', kind="Core" ).all( )
    if verbose : print( 'found %s active core tasks in %s ' % (len( coreTasks ), fromYear) )
    newTasks = [ ]
    newMgrs  = [ ]
    tskCodeMap = { }
    for t in coreTasks :
        if t.name == None :
            print( "==> task with no name found (code %s) - skipping" % t.code )
            continue

        act2017 = CMSActivity.query.filter_by(name=t.activity.name, year=2017).one()
        if not act2017:
            if verbose : print( "ERROR: task %s:\n\t no activity found in 2017 for %s -- skipping" % (t.code, t.activity) )
            continue
        else:
            if verbose : print( "task %s:\n\t new activity found for %s in 2017 for %s " % (t.code, t.activity.code, act2017.code) )

        newT = Task( name=t.name,
                     desc=t.description if t.description else t.name,
                     act = act2017,
                     needs=t.neededWork,
                     pctAtCERN=t.pctAtCERN,
                     tType=t.tType,
                     comment = t.comment,
                     code=str( uuid4( ) ),
                     year=toYear,
                     shiftTypeId=t.shiftTypeId,
                     status='ACTIVE',
                     locked=t.locked,
                     kind='CORE',
                     )
        newT.year = toYear
        newTasks.append( newT )
        tskCodeMap[ t.code ] = newT

        # handle managers for the tasks (not tested, as no mgrs were found for the "Core" tasks in 2016.
        tskMgrs = Manager.query.filter_by(year=fromYear, itemType='task', itemCode=t.code, status='active').all()
        if tskMgrs:
            print( "\nfound managers for %s: %s " % (t.code, str(tskMgrs) ) )
        else:
            print( '\nno explicit managers were found for %s in %s' % (t.code, fromYear) )
        for tm in tskMgrs:
            nm = Manager(itemType = 'task',
                         itemCode = t.code,
                         year     = toYear,
                         userId   = tm.userId,
                         comment  = tm.comment,
                         role     = tm.role,
                         egroup   = tm.egroup)
            newMgrs.append( nm )

    if verbose : print( "\nnew Tasks:" )
    for nt in newTasks :
        if verbose : print( ' --- ', nt.code, nt.name.encode('utf-8', 'xmlcharrefreplace') )
        commitNew( nt )

    if verbose : print( "\nnew Managers:" )
    for nm in newMgrs:
        if verbose : print( ' ... ', nm )
        commitNew( nt )

    # finally convert the kind to "CORE"
    for t in coreTasks:
        t.kind = 'CORE'
        commitUpdated( t )


if __name__ == '__main__':

    from app import create_app
    theApp = create_app('default')
    with theApp.app_context():

        fixCoreTasks( fromYear=2016, toYear=2017, verbose=True )
