#!/usr/bin/env python3

#  Copyright (c) 2020 CERN, Geneva, Switzerland

# run from main app dir via:
# TEST_DATABASE_URL=postgres://ap@localhost/icms PYTHONPATH=.:$PYTHONPATH ./scripts/updateMuonMgrs-2020.py

import time
import datetime

import sqlalchemy
import re

from app import db
from app.models import EprUser, Manager, Task, CMSActivity, Project, Level3Name, Permission, commitNew

def getLevel3NameMap(selYear=2020):
    lvl3Map = {}
    lvl3Items = db.session.query(Level3Name).filter(Level3Name.startYear>=selYear).filter(Level3Name.endYear<=selYear).all()
    for item in lvl3Items:
        lvl3Map[item.id] = item.name
    return lvl3Map

def getTaskList( pattern ):
    patRe = re.compile( r'.*\b%s\b.*' % pattern )

    tList = ( db.session.query(Task, Level3Name.name)
                .join( CMSActivity, Task.activityId == CMSActivity.id )
                .join( Level3Name, Task.level3 == Level3Name.id )
                .join( Project, CMSActivity.projectId == Project.id )
                .filter( Task.year == 2020 )
                .filter( Project.name == 'Muon' )
                .all()
            )

    # print( "found %d Muon tasks: " % len(tList) )
    
    filteredList = []
    l3Names = set()
    for t, l3 in tList:
        l3Names.add( l3 )
        if patRe.match( t.name ) or patRe.match( l3 ):
            filteredList.append( t )

    # print( "found %d filtered Muon tasks matching: '%s'" % (len(filteredList), pattern) )
    # print( '%s' %  [x.name for x in filteredList] )

    return filteredList

def updateManagers():
    lvl3NameMap = getLevel3NameMap()

    mgrs = { 'DT': [ 6379, # Redondo, Ignacio
                        4246, # Battilana, Carlo
                    ],
                'CSC': [ 2654, # Clare, Robert
                        5123, # Wang, Jian (FLORIDA-UNIV)
                        ],
                'RPC': [ 1307, # Pugliese, Gabriella
                        ],
                'GEM': [  881, # Riccardi, Cristina
                        2593, # Sharma, Archana (CERN)
                        ],
            }
    newMgrs = []
    for pat in [ 'DT', 'CSC', 'RPC', 'GEM']:
        tasks = getTaskList( pat )
        print( "found %d filtered Muon tasks matching: '%s'" % (len(tasks), pat) )
        for t in tasks:
            print( 'going to add manager(s) %s to task %s (%s) - %s ' % (mgrs[pat], t.code, lvl3NameMap[t.level3], t.name) )
            for mId in mgrs[pat]:
                newMgrs.append( Manager( itemType='task', 
                                            itemCode = t.code, 
                                            userId = mId, 
                                            comment='', 
                                            year=2020, 
                                            role=Permission.MANAGETASK) )
    print( 'new mgrs: \n %s' % newMgrs )
    for mgr in newMgrs:
        db.session.add(mgr)
    db.session.commit()
    print('... added to DB')
    

if __name__ == '__main__':
    from app import create_app
    app = create_app('default')
    with app.app_context():
        # Extensions like Flask-SQLAlchemy now know what the "current" app
        # is while within this block. Therefore, you can now run........
        # db.create_all()
        updateManagers()