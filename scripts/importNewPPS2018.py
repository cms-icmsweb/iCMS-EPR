
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import sys
import copy
import difflib
from uuid import uuid4

from app import db
from app.models import Project, CMSActivity, Task, Pledge, EprUser

from app.models import commitUpdated, commitNew, CMSDbException

# OpenPyXL - A Python library to read/write Excel 2010 xlsx/xlsm files
#
# for info and docs see: https://openpyxl.readthedocs.org/en/default/index.html
#
from openpyxl import load_workbook

headerMap = enumerate(  [u'Project', u'Activity',
                         u'Task',
                         u'neededWork',
                         u'User'
                         ] )

def remap():
    res = {}
    for item in headerMap:
        k, v = item
        res[v] = k+1
    return res

indexMap = remap()

print( "headermap: ", str(headerMap) )
print( 'indexmap : ', indexMap )

selYear = 2018

def analyseRow(row):

    info = {}
    for cell in row:
        if cell.value: # found something, check where we are:
            info[cell.column] = cell.value

    return info

def analyseSheet(workSheet):

    projs = []
    acts  = []
    tasks = []
    pledges = []
    pName = ''
    aName = ''
    for row in workSheet.iter_rows(row_offset=1):
        info = analyseRow(row)
        # print('info:', info)
        # info: {1: u'PPS', 2: u'DAQ system', 3: u'Laboratory operations ', 4: 4L}
        if not info: continue # ignore empty rows

        if 4 not in info.keys(): continue # ignore short lines

        # ignore header row:
        if info[1] == 'Project' and info[2] == 'Activity': continue

        if 1 in info.keys():
            if not pName or info[1] != pName: pName = info[1]
        else:
            info[1] = pName

        if 2 in info.keys():
            if not aName or info[2] != aName: aName = info[2]
        else:
            info[2] = aName

        if info[1] not in projs: projs.append( info[1] )
        if info[2] not in acts: acts.append( info[2] )

        # print "got:", info
        tasks.append( info )

        if 5 in info.keys():
            pledges.append( info )

    return projs, acts, tasks, pledges

def makeNewProject(pName):

    p = Project(name=pName, desc='', code=str(uuid4()), year=selYear)
    try:
        commitNew(p)
    except Exception as e:
        print( "ERROR committing new project %s, got %s" % (pName, str(e)) )
        raise e

    return

def makeNewActivity(aName, proj):

    p = CMSActivity(name=aName, desc='', proj=proj, code=str(uuid4()), year=selYear)
    try:
        commitNew(p)
    except Exception as e:
        print( "ERROR committing new activity %s, got %s" % (aName, str(e)) )
        raise e

    return

def convertString(inString):

    try:
        outString = inString.encode('utf-8', 'xmlcharrefreplace').replace('&', 'and')
        return outString
    except UnicodeEncodeError:
        outString = inString.decode('latin-1').encode('ascii', 'xmlcharrefreplace').replace('&', 'and')
        return outString
    except:
        raise

def createTask(task, act, proj):

    # fill in missing values
    for i in range(len(indexMap)):
        if i in task : continue
        if i == 8 : task[i] = 0 # percentage at CERN
        elif i == 11 : task[i] = -1 # needed work
        else: task[i] = ''

    taskName = convertString( task[ indexMap['Task'] ] )
    taskDesc = taskName
    taskSkills = ''

    if Task.query.filter_by(name=taskName, activityId=act.id, year=selYear).all() != []:
        print("Skipping already existing task %s for actId %i actName %s " % (str(task), act.id, act.name))
        return False

    # use defaults for most of the items:
    comment = taskSkills
    pctAtCern = 100
    kind = "CORE"
    taskType = 'Perennial'

    taskNew = None
    try:
        taskNew = Task( name = taskName,
                        desc = taskDesc,
                        act  = act,
                        code = str(uuid4()),
                        needs     = float( task[ indexMap['neededWork'] ] ),
                        pctAtCERN = pctAtCern,
                        tType     = taskType,
                        comment   = comment,
                        level=1, parent='',
                        shiftTypeId = '',
                        status = 'ACTIVE',
                        locked = False,
                        kind = kind,
                        year=selYear,
                    )
    except ValueError as e:
        print("ERROR creating task %s for proj %s act %s - got %s " % (task[ indexMap['Task Name'] ], proj.name, act.name, str(e)))
        return False

    if not taskNew:
        print("ERROR: can not create task for %s " % (task[ indexMap['Task Name'] ], ))
        raise Exception('error creating task')

    try:
        commitNew(taskNew)
    except Exception as e:
        print("ERROR: got: ", str(e))
        raise e

    return True

def importPledges():
    inFileName = 'imports/NewPPSActitiviesTasksPledges2018-2018-10-24.xlsx'
    wb = load_workbook(filename=inFileName, read_only=True)

    sheets = wb.get_sheet_names()

    print("++> found the following worksheets:", sheets)

    oldHeaders = []
    for sheetName in sheets:

        if 'Sheet 1 - Pledges from PPS for ' != sheetName: continue

        workSheet = wb.get_sheet_by_name(sheetName)

        headers = []
        for row in workSheet['A1:D1']:
            for cell in row:
                headers.append( cell.value )

        proj, act, tasks, pledges = analyseSheet(workSheet)

        print("-"*80)
        print( "worksheet : ", sheetName)
        print( len(proj) , "projects  : ", proj)
        print( len(act)  , 'activities: ', act)
        print( len(tasks), 'tasks ')
        print( len(pledges), 'pledges')
        for p in pledges:
            print( '   ', p)

        print( indexMap )
        print( "-"*80,'\n' )

        nOK = 0
        for p in pledges:

            taskName = p[ indexMap['Task'] ].replace('&', 'and')
            userName = p[ indexMap['User'] ]
            workTimePld = p[ indexMap['neededWork'] ]
            user = None

            if userName.strip() in ['F. Garcia', 'T. Novak']:
                print( "ERROR: ambiguous/non-found user name: ", userName )

            try:
                task = Task.query.filter_by(name=taskName, year=selYear).one()
            except Exception as e:
                if "No row was found for " in str(e):
                    print( "ERROR, task for %s not found !!! " % taskName )
                    raise e
                else:
                    raise e

            try:
                initial, lastName = userName.strip().split('.')
            except ValueError:
                try:
                    initial, lastName = userName.strip().split( ' ' )
                except Exception as e:
                    print( "ERROR splitting name for %s - got %s " % (userName, str(e)) )

            try:
                user = db.session.query(EprUser).filter(EprUser.name.like('%%%s%%' % lastName.strip())).first()
            except Exception as e:
                if "No row was found for " in str(e):
                    print( "ERROR, user for %s not found !!! " % userName )
                    # raise e
                else:
                    raise e

            if not user:
                print( "ERROR, user for %s not found !!! " % userName )
                continue
            print( nOK, "found user for %s: %s - %s" % (userName, user.name, user.username) )

            p0 = Pledge.query.filter_by(code='%s+%s+%s' % (task.id, user.id, user.mainInst)).all()
            if len(p0) > 0:
                print( "Pledge already existing !?!?!? code: '%s+%s+%s'" % (task.id, user.id, user.mainInst) )
            else:
                pl = Pledge(taskId=task.id, userId=user.id, instId=user.mainInst,workTimePld=workTimePld,workTimeAcc=0., workTimeDone=0., status='new', year=selYear, workedSoFar=0.)
                commitNew(pl)
                print( "++ created new pledge for task %s user %s timePledged %s months " % (task.name, user.name, workTimePld) )

            nOK += 1


def importNewPAT():
    inFileName = 'imports/NewPPSActitiviesTasksPledges2018-2018-10-24.xlsx'
    wb = load_workbook(filename=inFileName, read_only=True)

    sheets = wb.get_sheet_names()

    print( "++> found the following worksheets:", sheets )

    oldHeaders = []
    for sheetName in sheets:

        if 'Sheet 1 - Tasks from PPS for 20' != sheetName: continue

        workSheet = wb.get_sheet_by_name(sheetName)

        headers = []
        for row in workSheet['A1:D1']:
            for cell in row:
                headers.append( cell.value )

        # raises if headers are different in a workSheet
        diffHeaders( headers, oldHeaders, sheetName )

        proj, act, tasks, pledges = analyseSheet(workSheet)

        print( "-"*80 )
        print( "worksheet : ", sheetName )
        print( len(proj) , "projects  : ", proj )
        print( len(act)  , 'activities: ', act )
        print( len(tasks), 'CORE tasks ' )# , tasks
        # for t in tasks:
        #     print( '   ', t )

        print( indexMap )

        # create projects and tasks first, then find and hand over the activity for the task:

        nOK = 0
        for t in tasks:

            projName = t[ indexMap['Project'] ]
            actName  = t[ indexMap['Activity'] ]

            try:
                proj = Project.query.filter_by(name=projName, year=selYear).one()
            except Exception as e:
                if "No row was found for " in str(e):
                    print( "ERROR, shoudl not need to create a new project for: ", projName )
                    raise e
                    # db.session.autoflush=False
                    # makeNewProject(projName)
                    # db.session.autoflush=False
                else:
                    raise e

            proj = Project.query.filter_by(name=projName, year=selYear).one()

            try: # to find the corresponding activity:
                act  = CMSActivity.query.filter_by(name=actName, projectId=proj.id, year=selYear).one()
            except Exception as e:
                if "No row was found for " in str(e):
                    print( "creating new activity %s in %s " %(actName, proj.name) )
                    makeNewActivity(actName, proj)
                elif "Multiple rows were found for one" in str(e):
                    print( "ERROR multiple rows found for act %s, projId %i :" % (actName, proj.id) )
                    print( '\n'.join( CMSActivity.query.filter_by(name=actName, projectId=proj.id, year=selYear).all() ) )
                    raise e
                else:
                    raise e

            act  = CMSActivity.query.filter_by(name=actName, projectId=proj.id, year=selYear).one()

            if not createTask(t, act, proj):
                print( "ERROR creating task %s " % (t,) )
                continue
            print( 'created task for:', t )
            nOK += 1

    print( "imported %i new tasks for selYear" % nOK )

def diffHeaders( headers, oldHeaders, sheetName ) :

    if not oldHeaders :
        oldHeaders = copy.copy( headers )
        # print( 'copied headers to:', oldHeaders
    else :
        try :
            delta = list( difflib.context_diff( oldHeaders, headers ) )
        except Exception as e :
            print( "processing %s got: %s" % (sheetName, str( e )) )
            print( "old: ", oldHeaders )
            print( 'new: ', headers )
            raise e

        if len( delta ) > 0 :
            msg = "found different headers for %s:" % (sheetName,)
            for line in delta :
                sys.stdout.write( line )
            raise Exception( msg )
        else :
            print( "headers match ... " )

def importNewPPS2018():
    # importNewPAT()
    # importPledges()
    print( "De-activated -- please edit the script to activate ... " )
