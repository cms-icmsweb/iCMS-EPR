
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from __future__ import print_function

import datetime

import app.smtp as smtp

from app import db
from app.models import EprUser, Manager, TimeLineInst, AllInstView

from app.ldapAuth import getUserEmail
from app.sendEmails import Message, send_async_email, logEmail


ge100 = '''
Dear Colleagues,

According to the EPR tables for 2016, your group has pledged and delivered to CMS not only the effort that was expected, but even more.

We send this message to acknowledge your efforts and to extend our warm thanks to you and your colleagues for helping maintain our experiment at the forefront of physics.

Since your contributions are sufficient to qualify new members of your group for authorship via the institute effort, please check for colleagues having fulfilled the waiting time of one year and whom you would like to add to the list of authors.

We look forward to the continuation of your contributions in 2017.

With our best regards,
your Spokesperson and Engagement Office teams
Joel, Guenther, Roberto,
Kerstin, Archana, Gavin, Harrison, Leandar
'''

ge80lt100 = '''
Dear Colleagues,

According to the EPR tables for 2016, your group has pledged and actually contributed very significantly to CMS.

We send this message to acknowledge your efforts and to extend our warm thanks to you and your colleagues for helping maintain our experiment at the forefront of physics. We look forward to the continuation of your contributions in 2017.

Since your EPR 2016 contributions are slightly below 100%, new members of your group unfortunately do not qualify for authorship via the institute effort. Please check if the member list of your group is correct and if all pledges have been entered. In case of corrections esp. if 100% is accomplished, please notify the team of the Engagement Office, and check for colleagues having fulfilled the waiting time of one year and whom you would like to add to the list of authors.

Please contact the team of the Engagement Office if we can be of help.

With our best regards,
your Spokesperson and Engagement Office teams
Joel, Guenther, Roberto,
Kerstin, Archana, Gavin, Harrison, Leandar
'''

ge60lt80 = '''
Dear Colleagues,

According to the EPR tables for 2016, your group has pledged and actually contributed to CMS a good fraction of the effort that was expected.

We send this message to acknowledge your efforts and to extend our thanks to you and your colleagues for your contributions.  We would also like to take this opportunity to encourage you to increase your participation in these important "experimental physics responsibilities" that are so necessary for our experiment, especially in 2017.

Please contact the team of the Engagement Office if we can be of help.

With our best regards,
your Spokesperson and Engagement Office teams
Joel, Guenther, Roberto,
Kerstin, Archana, Gavin, Harrison, Leandar
'''

ge20lt60 = '''
Dear Colleagues,

We are currently contacting all CMS institutes that have not been able to contribute to the common "experimental physics responsibilities" (EPR) of the experiment at the desired level in 2016.  As we complete the first months of 2017, it is time to take stock of the situation and to make concrete plans for covering all the necessary tasks.  As we all know, our success is not guaranteed: CMS will need a concentrated and intense effort from everyone in the collaboration.

We kindly ask that you  let our Engagement Office team  know of any specific problems that your group has faced in 2016, your expectation of whether the problems will be resolved in the current year, and also ways in which CMS may be able to help you and your colleagues. If you feel that e-mail does not lend itself well to such a discussion, we are happy to arrange a meeting between you and the Engagement Office, possibly via vidyo/skype, at your earliest convenience.

With our best regards,
your Spokesperson and Engagement Office teams
Joel, Guenther, Roberto,
Kerstin, Archana, Gavin, Harrison, Leandar
'''

lt20 = '''
Dear Colleagues,

We are currently contacting all CMS institutes that were unable to contribute to the common "experimental physics responsibilities" (EPR) of the experiment at the desired level in 2016.  As we complete the first months of 2017, it is time to take stock of the situation and to make concrete plans for covering all the necessary tasks.  Our success as a collaboration is not guaranteed: we will need a concentrated and intense effort from everyone in the collaboration.

We therefore kindly ask  you to let our Engagement Office team know of problems that your group faced in 2016, your expectation of whether the problems will be resolved in the current year, and also ways in which CMS may be able to help you and your colleagues. If you feel that e-mail does not lend itself well to such a discussion, we are happy to arrange a meeting between you and the Engagement Office, possibly via vidyo/skype, at your earliest convenience.

With our best regards,
your Spokesperson and Engagement Office teams
Joel, Guenther, Roberto,
Archana, Gavin, Harrison, Leandar
'''


msgFooter = '''
------------------------------------------------------------------------
--------  Message sent for Archana Sharma (CERN) on behalf of the CMS Engagement Office
--------  Recipients: Team leaders (CBIs/CBDs) of %s
--------  Please send replies to this message to the submitter cms-engagement-office@cern.ch which is set as the default reply address
------------------------------------------------------------------------
'''


def getMailTemplate(fraction):

    if fraction<0.20: return lt20
    elif fraction >=0.20 and fraction < 0.60: return ge20lt60
    elif fraction >=0.60 and fraction < 0.80: return ge60lt80
    elif fraction >=0.80 and fraction < 1.00: return ge80lt100
    elif fraction >=1.00 : return ge100
    else:
        print ('getMailTemplate> ERROR - you never should have come here !!! fraction is: %2.2f' % fraction)

    return None

def cleanName(name):

    newName = name.encode( 'ascii', 'xmlcharrefreplace' )
    newName = newName.replace( '&#252;', 'ue' )\
                     .replace( '&#233;', 'e' )

    return newName
def sendMailToCBIs():

    selYear = 2016
    insts = TimeLineInst.query.filter_by(year=selYear, cmsStatus='Yes').order_by(TimeLineInst.code).all()
    print( "found %s insts" % len(insts) )

    ignoreList = ['ZZZ', 'TIFR']

    alreadySent = ['DESY' , 'LONDON-IC', 'FLORIDA-STATE', 'SOFIA-UNIV']

    mailsDone = []

    # print (" %16s  | eprFrac | %s | %100s | Dear %s " % ('instCode', '#', 'CBIs/CBDs', 'firstName(s)') )
    for inst in insts:

        if inst.code in ignoreList: continue

        if inst.code in alreadySent:
            print( "Mail for %s already sent - skipping." % inst.code )
            continue

        instMgrs = db.session.query(EprUser)\
                             .join(Manager, Manager.userId == EprUser.id)\
                             .filter(Manager.itemType == 'institute')\
                             .filter(Manager.itemCode == inst.code) \
                             .filter( Manager.year == selYear) \
                             .filter( Manager.status == 'active')\
                             .all()

        fNameList = [ cleanName( x.name.rsplit(',')[-1] ) for x in instMgrs ]
        firstNames = ', '.join( fNameList )
        emailsTo = [ getUserEmail( x.username ) for x in instMgrs ]

        try:
            instView = db.session.query(AllInstView.eprAccFract)\
                        .filter(AllInstView.code == inst.code)\
                        .filter(AllInstView.year == selYear)\
                        .one()
        except Exception as e:
            print( "ERROR searching for %s in allInstView, got: %s" % ( inst.code, str(e) ) )

        mailBody = getMailTemplate(instView.eprAccFract).replace('Dear Colleagues,', 'Dear %s, ' % firstNames)
        mailBody += msgFooter % inst.code

        # print ( '*'*80+'\n')
        # print( mailBody )

        msg = Message( )
        msg.subject = '[iCMS-EPR] Summary on the EPR 2016 for %s' % inst.code
        msg.fromAddress = 'cms-engagement-office@cern.ch'
        msg.toAddresses = emailsTo
        msg.ccAddresses = ['cms-engagement-office@cern.ch']
        msg.replyToAddress = 'cms-engagement-office@cern.ch'
        msg.body = mailBody

        logEmail(msg, fNamePrefix='instEPRsummary2016')

        mailsDone.append(inst.code)

        # smtp.SMTP( ).sendEmail( msg.subject, msg.body, msg.fromAddress, msg.toAddresses, msg.ccAddresses, replyToAddress=msg.replyToAddress )

    print("\nMails were ( NOT :-) ) sent on %s to the following institutes:\n ['%s'] \n" % (datetime.datetime.now(), "', '".join(mailsDone)))

if __name__ == '__main__':

    #
    # to be called from bash in an EPR environment like:
    # (source ./venv/bin/activate; export PYTHONPATH=`pwd`; export NLS_LANG=AMERICAN_AMERICA.UTF8; python ./scripts/sendMailsToCBI.py )
    #

    from app import create_app
    app = create_app('default')
    with app.app_context():
        # Extensions like Flask-SQLAlchemy now know what the "current" app
        # is while within this block. Therefore, you can now run........

        sendMailToCBIs()
