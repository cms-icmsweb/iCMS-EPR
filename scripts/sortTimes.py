
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import re

with open('/Users/ap/cernbox/iCMSetAl/instTimes.txt', 'r') as iFile:
    lines = iFile.readlines()

out = {}
for line in lines:
    words = line.split()
    out[words[-1]] = line.strip()

for t in sorted( out.keys() ):
    print( out[t] )
