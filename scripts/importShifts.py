
#  Copyright (c) 2015-2022 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

# import shifts from the CMS Online Shift tool, adjusting weights if needed.

import xml.etree.ElementTree as ET
import os, sys
import time
import datetime
import json

from pprint import pprint

from sqlalchemy import and_
import sqlalchemy

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db, create_app
from app.models import EprUser, Shift as EprShift, ShiftGrid

from app.main.ShiftOverview import ShiftOverview, checkShiftCategory, checkROC

from config import config

if 'TEST_DATABASE_URL' in os.environ :
    impRetVal = True
else:
    impRetVal = False

# From Frank, Oct 18, 2017:
# New tables to manage the shift weights (from Frank Glege, 18-Oct-2017):
# cms_shiftlist.shifttypes  : contains details on which shifts belongs to which "System"
# cms_shiftlist.shiftgrid   : contains the templates which are defined for each shift-entity
# to be used when implementing "conversion scaling" ... likely together with parsing/interpreting the date/time
# to see if a shift is of "day/evening/night" and "week/weekend" type.
# Q: where are the flavour_names ???
# from the data: three possible "flavourNames": "main", "main 2nd", and "trainee" ...

# get a list of the relevant parts of the system:
# SELECT t.SHIFT_TYPE_ID, t.SUB_SYSTEM, t.SHIFT_TYPE, g.ORDER_NUM, g.START_HOUR, g.END_HOUR FROM CMS_SHIFTLIST.SHIFTGRID g JOIN CMS_SHIFTLIST.SHIFTTYPES t ON t.SHIFT_TYPE_ID=g.SHIFT_TYPE_ID ORDER BY t.SUB_SYSTEM, t.SHIFT_TYPE, g.ORDER_NUM;

useOracleDB = True
if useOracleDB:
    class OnlineShiftGrid(db.Model):
        __tablename__ = 'SHIFTGRID'
        __bind_key__ = 'onlShiftDB'
        __table_args__ = { 'implicit_returning' : impRetVal,
                            'schema' : 'cms_shiftlist' }

        shiftTypeId = db.Column( 'shift_type_id', db.Integer , nullable=False, primary_key=True )
        subSystem   = db.Column( 'sub_system', db.String(200), primary_key=True )
        shiftType   = db.Column( 'shift_type', db.String(200), nullable=False )
        orderNum    = db.Column( 'order_num', db.Integer , nullable=False, primary_key=True )
        startHour  = db.Column( 'start_hour', db.DateTime )
        endHour    = db.Column( 'end_hour'  , db.DateTime )


    #-ap: not sure what this is useful for in our context:
    # class OnlineShiftType(db.Model):
    #     __tablename__ = 'SHIFTTYPES'
    #     __bind_key__ = 'onlShiftDB'
    #     __table_args__ = {"implicit_returning":  impRetVal,
    #                       'schema' : 'cms_shiftlist' }
    #
    #     shiftTypeId = db.Column( 'shift_type_id', db.Integer , nullable=False )
    #     shiftType   = db.Column( 'shift_type', db.String(200), nullable=False )
    #     subSystem   = db.Column( 'sub_system', db.String(200) )
    #-ap end

    class OnlineShift(db.Model):
        __tablename__ = 'SHIFTDATA'
        __bind_key__ = 'onlShiftDB'
        __table_args__ = {"implicit_returning":  impRetVal,
                        'schema' : 'cms_shiftlist' }

        #desc cms_shiftlist.shiftData;
        # Name                                      Null?    Type
        # ----------------------------------------- -------- ----------------------------
        # SHIFT_ID                                  NOT NULL NUMBER
        # SHIFTER_ID                                NOT NULL NUMBER
        # SUB_SYSTEM                                         VARCHAR2(200)
        # SHIFT_TYPE                                NOT NULL VARCHAR2(200)
        # SHIFT_TYPE_ID                             NOT NULL NUMBER
        # SHIFT_START                               NOT NULL DATE
        # SHIFT_END                                 NOT NULL DATE
        # FLAVOUR_NAME                                       VARCHAR2(64)
        # WEIGHT                                             NUMBER

        shiftId     = db.Column( 'shift_id', db.Integer, primary_key=True )
        shifterId   = db.Column( 'shifter_id', db.Integer, nullable=False )

        subSystem   = db.Column( 'sub_system', db.String(200) )
        shiftType   = db.Column( 'shift_type', db.String(200), nullable=False )
        shiftTypeId = db.Column( 'shift_type_id', db.Integer , nullable=False )

        shiftStart  = db.Column( 'shift_start', db.DateTime, nullable=False )
        shiftEnd    = db.Column( 'shift_end'  , db.DateTime, nullable=False )
        flavourName = db.Column( 'flavour_name', db.String(64) )
        weight      = db.Column( db.Float )


        def to_json(self):
            json_data = {
                    'shiftId' : self.shiftId,
                    'shifterId' : self.shifterId,

                    'subSystem' : self.subSystem,
                    'shiftType' : self.shiftType,
                    'shiftTypeId' : self.shiftTypeId,

                    'shiftStart' : self.shiftStart.strftime('%Y-%m-%dT%H%M%S %z'),
                    'shiftEnd' : self.shiftEnd.strftime('%Y-%m-%dT%H%M%S %z'),
                    'flavourName' : self.flavourName,
                    'weight' : self.weight,
            }
            return json_data

        def __repr__( self ) :
            msg = '<OnlineShift '
            for k, v in self.to_json( ).items( ) :
                msg += '%s: %s, ' % (k, str( v ))
            msg += '>'
            return msg


docShiftInfo = {}

def makeDateTime(inString):

    dString, tString = inString.split()
    mon,day,yr = [int(x) for x in dString.split('/')]
    hr, min, sec = [int(x) for x in tString.split(':')]

    return datetime.datetime(yr,mon,day, hr,min, sec)

def getWeight(shiftEntry):

    # Note: this is only used for pre-2023 shifts ... 

    startTime = shiftEntry.shiftStart
    endTime   = shiftEntry.shiftEnd

    # determine scaling factor as a function of day/time
    # see also presentation at CB-112 (Feb 2017):
    # https://indico.cern.ch/event/605565/contributions/2445977/attachments/1406703/2149710/20170203_EPR_Shifts2017_vF.pdf
    # and the central shift twiki for 2018 at:
    # https://twiki.cern.ch/twiki/bin/viewauth/CMS/CentralShifts2018

    scalingFactor = 1.
    duration = (endTime-startTime).days*86400 + (endTime-startTime).seconds
    if startTime.hour in [7,15,23] and duration == 8*3600: # "normal" 8-hour shift
        # weekday: Monday is 0 and Sunday is 6, so Fri,Sat is 4,5
        if startTime.weekday() in [4, 5] and startTime.hour == 23:
                scalingFactor = 2.0        # w/e night shift: 2.5
        elif startTime.weekday() in [5, 6] and startTime.hour in [7, 15]:
                scalingFactor = 1.5        # w/e day/eve shift: 2.0
        else: # 'normal' weekday:
            if   startTime.hour == 23:
                scalingFactor = 1.5        # weekday night shift: 1.5
    elif duration == 86400: # full day shift:
        if startTime.weekday() in [4, 5] and startTime.hour == 00:
            scalingFactor = 1.5
    elif duration == 604800: # week shift, weight 8
        scalingFactor = 8.0

    # if float(shiftEntry.weight) > 0. and scalingFactor != float( shiftEntry.weight ):
    #     print( 'found inconsistent weights: from DB: %4.2f - calc: %4.2f - for %s (%d) shiftStart %s end %s - subsystem %s, type %s (%d)' %
    #            (shiftEntry.weight, scalingFactor, startTime.strftime('%a'), duration, startTime, endTime, shiftEntry.subSystem, shiftEntry.shiftType, shiftEntry.shiftTypeId)  )

    if shiftEntry.weight is None: 
        return None, scalingFactor

    return float( shiftEntry.weight ), scalingFactor

def calculateWeightPre2023(shiftEntry, year):

    newWeight = 0.
    scalingFactor = 1.
    if ( shiftEntry.subSystem == 'RPC' and
         shiftEntry.shiftTypeId == 12 and
         shiftEntry.weight == 0. and
         year >= 2017 ): newWeight = 1.
    elif year >= 2021 and checkShift( shiftEntry ):
        newWeight, scalingFactor = getWeight( shiftEntry )
        newWeight = 1. * scalingFactor
        print("++> INFO: Weight of NULL for shift %s reset to %s %s - %s " % (shiftEntry, newWeight, scalingFactor, newWeight) )
    else:
        if shiftEntry.weight is None:
            newWeight = 0.
        else:
            newWeight, scalingFactor = getWeight( shiftEntry )

    # ignore known entries with weight=0
    if shiftEntry.weight is not None and newWeight == 0. and shiftEntry.subSystem != 'TC' and shiftEntry.shiftTypeId != 92:
        print("++> WARNING: Weight of 0. (but not NULL) found for shift %s " % shiftEntry)

    return newWeight

def calculateWeight(shiftEntry, year):
    if year < 2023:
        return calculateWeightPre2023(shiftEntry, year)
    
    # ensure the starting point for "known" shifts is 1. even if they are zero or None
    sWeight = shiftEntry.weight
    if (sWeight is None or sWeight == 0) and checkShift( shiftEntry ):
        sWeight = 1.

    newStart, newEnd, newWeight = checkROC( shiftEntry )
    if (newWeight is None or newWeight == 0) and checkShift( shiftEntry ):
        newWeight = sWeight

    # re-map to the format the Overview expects:
    shift = [ shiftEntry.shifterId,
              year,
              sWeight,
              newStart,
              newEnd,
              shiftEntry.subSystem,
              shiftEntry.shiftType,
              shiftEntry.flavourName,
              shiftEntry.shiftTypeId,
    ]

    proposal = 'proposal-1' if year == 2023 else 'proposal-2'
    so = ShiftOverview(year = year, proposal=proposal)
    shiftKey = f'{shiftEntry.subSystem}-{shiftEntry.shiftType}-{shiftEntry.flavourName}'  # key: subSys-type-flavour
    isCentral, cat = checkShiftCategory( shiftKey, proposal )
    newWeight = so.getShiftWeight( shift, isCentral, cat )

    if cat == 'doc1':  # "doc" in shift[6].lower():
        global docShiftInfo
        startTime = shift[3]
        endTime = shift[4]
        shiftKey = f'{shift[5]}-{shift[6]}-{shift[7]}'  # key: subSys-type-flavour
        duration = ((endTime-startTime).days*86400 + (endTime-startTime).seconds)/86400.
        if shiftKey not in docShiftInfo:
            docShiftInfo[shiftKey] = set()
        docShiftInfo[shiftKey].add( duration )

    # check if the shift happens on a CERN holiday and double the weight, if so:
    #-toDo: check if the check should be done in the CERN TZ or the ROC one ... 
    isHol, holCsp = so.cspForHolidayShift( newStart )
    if isHol:
        newWeight = holCsp
        print( f'+++>> found shift starting at {newStart} to be at a CERN holiday, setting weights to {newWeight}' )

    return newWeight

def checkShift( shiftEntry ):

     #               subSystem,     shiftType ,    shiftTypeId,   flavourName
    knownShifts = [ ( "CSC",         "DOC",              37,        "main" ),
                    ( "Central",     "DCS",               2,        "main" ),
                    ( "Central",     "DCS",               2,        "trainee" ),
                    ( "Central",     "Shift_leader",      8,        "main" ),
                    ( "HCAL",        "DOC",              26,        "main" ),
                    ( "HCAL",        "DOC",              26,        "main 2nd" ),
                    ( "HCAL",        "DOC",              26,        "trainee" ),
                    ( "TRK",         "DOC",              32,        "main" ),
                    ( "TRK",         "DOC",              32,        "main 2nd" ),
                    ( "TRK",         "DOC",              32,        "trainee" ),
                    ( "TRK",         "Off line remote",  44,        "main" ),
                    ( "TRK",         "Off line remote",  44,        "main 2nd" ),
                    ( "TRK",         "Off line remote",  44,        "trainee" ),
                    ( "GEM",         "Offline",         120,        "main" ),
                    ( "CT-PPS",      "DOC",              90,        "main" ),
                    ( "CT-PPS",      "DOC",              90,        "main 2nd" ),
                    ( "CT-PPS",      "DOC",              90,        "trainee" ),
                    ( "RPC",         "Detector O&M",     12,        "main" ),
                    ('Pixel',        'DOC',              46,        'main' ),
                    ('Pixel',        'DOC',              46,        'main 2nd' ),
                    ('Pixel',        'DOC',              46,        'trainee' ),
    ]
    if (shiftEntry.subSystem, shiftEntry.shiftType, shiftEntry.shiftTypeId, shiftEntry.flavourName) in knownShifts:
        return True
    
    return False

class JsonShift():
    def __init__(self):
        self.shiftId = None
        self.shifterId = None
        self.subSystem = None
        self.shiftType = None
        self.shiftTypeId = None
        self.shiftStart = None
        self.shiftEnd = None
        self.flavourName = None
        self.weight = None

def shiftsFromJson(jsonFileName):

    onlineShifts = [  ]
    with open( jsonFileName, 'r') as jF:
        allShiftsJson = json.load ( jF )
        
        for json_data in allShiftsJson:
            shift = JsonShift()
            shift.shiftId = json_data[ 'shiftId' ]
            shift.shifterId = json_data[ 'shifterId' ]
            shift.subSystem = json_data[ 'subSystem' ]
            shift.shiftType = json_data[ 'shiftType' ]
            shift.shiftTypeId = json_data[ 'shiftTypeId' ]
            shift.shiftStart = datetime.datetime.strptime(json_data[ 'shiftStart' ], '%Y-%m-%dT%H%M%S ' )
            shift.shiftEnd = datetime.datetime.strptime( json_data[ 'shiftEnd' ], '%Y-%m-%dT%H%M%S ' )
            shift.flavourName = json_data[ 'flavourName' ]
            shift.weight = json_data[ 'weight' ]
            onlineShifts.append( shift )

    print( f'\n==> found {len(onlineShifts)} shifts in json file {jsonFileName}')
    return onlineShifts

def check(year):
    jsonFileName = 'allShifts.json'
    shiftsToJson( year, jsonFileName)

    newShifts = shiftsFromJson( jsonFileName )


def shiftsToJson(year, jsonFileName):

    # select * from  cms_shiftlist.shiftData where to_char(shift_start,'YYYY') = '2016';

    onlineShifts = (db.session.query(OnlineShift)
                              .filter( and_( OnlineShift.shiftStart > datetime.date(year,1,1),
                                             OnlineShift.shiftEnd < datetime.date(year+1,1,1) ) )
                              .all())

    print("\n==> found %s shifts for %s in online DB" % (len(onlineShifts), year))

    [ print(s) for s in onlineShifts[:10] ]

    allShiftsJson = [ x.to_json() for x in onlineShifts ]
    with open( jsonFileName, 'w') as jF:
        json.dump ( allShiftsJson, jF )


def importShiftsFromDB(year):

    # select * from  cms_shiftlist.shiftData where to_char(shift_start,'YYYY') = '2016';

    onlineShifts = (db.session.query(OnlineShift)
                              .filter( and_( OnlineShift.shiftStart > datetime.date(year,1,1),
                                             OnlineShift.shiftEnd < datetime.date(year+1,1,1) ) )
                              .all())

    print("\n==> found %s shifts for %s in online DB" % (len(onlineShifts), year))

    [ print(s) for s in onlineShifts[:10] ]

    return onlineShifts

def importShifts(year):
    
    global useOracleDB
    if useOracleDB:
        onlineShifts = importShiftsFromDB(year)
    else:
        jsonFileName = 'allShifts-2025.json'
        onlineShifts = shiftsFromJson( jsonFileName )

    userList = db.session.query(EprUser.hrId).all()
    existingShiftIds = db.session.query(EprShift.shiftId).filter(EprShift.year==year).all()

    start = time.time()
    nNew = 0
    nUpdated = 0
    nNullShifts = 0
    mappedIDs = []
    
    nullShiftsSkipped = {}
    zeroShiftsFound = {}
    
    for shift in onlineShifts:

        # Note: since 2017 it is possible to define "free schedule" shifts,
        # which may have times different from the standard shift times. These
        # shifts have their weight set to "null" in the online DB and are
        # ignored when importing.

        shiftKey = (shift.subSystem, shift.shiftType, shift.shiftTypeId, shift.flavourName)

        if shift.weight is None:
            if not checkShift( shift ):
                # print("++> INFO: skipping unknown shift with weight NULL ('free schedule shift') found for shift %s " % shift)
                nNullShifts += 1
                if shiftKey not in nullShiftsSkipped:
                    nullShiftsSkipped[shiftKey] = { 'count': 0, 'users': set() }
                nullShiftsSkipped[shiftKey]['count'] += 1
                nullShiftsSkipped[shiftKey]['users'].add( shift.shifterId )
                continue

        if ( (shift.shifterId,) not in userList ) :
            if shift.shifterId != 0:
                print("skipping shift %s as user %s not found " % (shift.shiftId, shift.shifterId))
            continue

        newStart, newEnd, sWeight = checkROC( shift )

        newWeight = calculateWeight( shift, year )
        if newWeight is None: newWeight = 0.
        if newWeight == 0.:
            if shiftKey not in zeroShiftsFound:
                zeroShiftsFound[shiftKey] = { 'count': 0, 'users': set() }
            zeroShiftsFound[shiftKey]['count'] += 1
            zeroShiftsFound[shiftKey]['users'].add( shift.shifterId )

        if ( (shift.shiftId,) in existingShiftIds ) : # update existing shift
            eprShift = db.session.query(EprShift).filter_by(shiftId=shift.shiftId).one()
            # check and update the existing values
            updateNeeded = False
            if (shift.shifterId != eprShift.userId) :
               updateNeeded = True
               eprShift.userId = shift.shifterId
            if (shift.subSystem != eprShift.subSystem) :
               updateNeeded = True
               eprShift.subSystem = shift.subSystem
            if (shift.shiftType != eprShift.shiftType) :
               updateNeeded = True
               eprShift.shiftType = shift.shiftType
            if (shift.shiftTypeId != eprShift.shiftTypeId) :
               updateNeeded = True
               eprShift.shiftTypeId = shift.shiftTypeId
            if (newStart != eprShift.shiftStart) :
               updateNeeded = True
               eprShift.shiftStart = newStart
            if (newEnd != eprShift.shiftEnd) :
               updateNeeded = True
               eprShift.shiftEnd = newEnd
            if (shift.flavourName != eprShift.flavourName) :
               updateNeeded = True
               eprShift.flavourName = shift.flavourName

            if ( newWeight != eprShift.weight) :
               updateNeeded = True
               eprShift.weight = newWeight

            if updateNeeded:
                nUpdated += 1
                db.session.add( eprShift )
        else:

            s = EprShift( shiftId = shift.shiftId,
                       hrId = shift.shifterId,
                       subSys = shift.subSystem,
                       sType = shift.shiftType,
                       sTypeId = shift.shiftTypeId,
                       sStart = newStart,
                       sEnd = newEnd,
                       flavourName = shift.flavourName,
                       weight = newWeight,
                       year = year,
                 )
            db.session.add(s)

        mappedIDs.append( shift.shiftId )  # store the shifts found ...
    try:
       db.session.commit()
       nNew += 1
    except sqlalchemy.exc.IntegrityError as e:
       print("ERROR, user with id %i not found for shift_id %i (%s)" % \
             (int(shift.find('SHIFTER_ID').text), int(shift.find('SHIFT_ID').text), str(e)))
       db.session.rollback()

    deletedShifts = list( set([ x[0] for x in existingShiftIds] ) - set(mappedIDs) )
    print("found %s deleted shifts - from %d existing, %s mapped" % (len(deletedShifts), len(existingShiftIds), len(mappedIDs)))
    print(deletedShifts)

    delShifts = db.session.query(EprShift).filter( EprShift.shiftId.in_( deletedShifts) ).all()
    for delShift in delShifts:
        print("id: %s - %s " % ( delShift.shiftId, str(delShift) ))
        db.session.delete( delShift )
    db.session.commit()

    print("found %d 'free-schedule' shifts with weight null" % nNullShifts)

    print( f'null shifts skipped:' )
    for sKey, val in nullShiftsSkipped.items():
        count = val['count']
        users = val['users']
        print( f'{sKey} : {count} - {users}')

    print( f'weight-zero shifts found:' )
    for sKey, val in zeroShiftsFound.items():
        count = val['count']
        users = val['users']
        print( f'{sKey} : {count} - {users}')

    print("processing %d shifts (%i new, %i updated) took %f sec" % ( len(onlineShifts), nNew, nUpdated, time.time()-start ))

    global docShiftInfo
    print( f'\ndurations of ({len( docShiftInfo.keys() )}) "doc1" type shifts: ')
    pprint( docShiftInfo, indent=4 )

def importShiftGridFromDB():

    shiftGridEntries = db.session.query(OnlineShiftGrid).all()

    if not shiftGridEntries:
        print("ERROR: no entries found in table ShiftGrid in online DB ... nothing done.")
        return

    # first delete the content of the table in postgres:
    db.session.query(ShiftGrid).delete()
    db.session.commit()

    # then add them back one by one
    sgList = []
    for item in shiftGridEntries:
        # print( item )
        sgList.append( ShiftGrid(subSys=item.subSystem,
                                   sType=item.shiftType,
                                   sTypeId=item.shiftTypeId,
                                   startHour=item.startHour,
                                   endHour=item.endHour,
                                   orderNum=item.orderNum,
                                   )
                       )
    for sg in sgList:
        db.session.add(sg)
    db.session.commit()


def importShiftGridFromJsonFile():

    import json
    with open('shiftGrid.json', 'r') as fp:
        shiftGridEntries = json.load(fp)

    if not shiftGridEntries:
        print("ERROR: no entries found in table ShiftGrid in online DB ... nothing done.")
        return

    # first delete the content of the table in postgres:
    db.session.query(ShiftGrid).delete()
    db.session.commit()

    # then add them back one by one
    for item in shiftGridEntries:
        # print( item )
        sg = ShiftGrid( subSys=item['subSystem'],
                        sType=item['shiftType'],
                        sTypeId=item['shiftTypeId'],
                        startHour=datetime.datetime.strptime( '000%s' % item['startHour'].strip(), '%Y-%m-%dT%H%M%S'),
                        endHour=datetime.datetime.strptime( '000%s' % item['endHour'].strip(), '%Y-%m-%dT%H%M%S'),
                        orderNum=item['orderNum'],
                       )
        db.session.add(sg)
    db.session.commit()


def main():
    theApp = create_app('default')
    with theApp.app_context():

        if 'check' in sys.argv[1:]:
            check( 2024 )
            return 

        # print "onlShiftDB from: ", theApp.config['SQLALCHEMY_ONLSHIFTDB_URI']

        # Extensions like Flask-SQLAlchemy now know what the "current" app
        # is while within this block. Therefore, you can now run........
        # db.create_all(bind=None)
        # importShifts(year=2015)
        # ...
        
        actMonth = datetime.datetime.today().month
        actYear  = datetime.datetime.today().year
        if actMonth < 2: # in Jan, also import the shifts from the previous year to ensure a clean book-closing
            importShifts( year=actYear-1 )

        importShifts( year=actYear )

        # importShiftGridFromDB()

if __name__ == '__main__':
    main()
