
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import csv

from app import db
from app.models import EprInstitute, EprUser, Role, Category

# Note: when running the script, you need to set:
#
#       export NLS_LANG=AMERICAN_AMERICA.UTF8
#
#       so that Oracle actually _uses_ UTF-8

def updatePeopleNames():

    csvFile = 'imports/iCMS-non-ascii-people.csv'

    index = 0
    with open(csvFile, 'rb') as csvfile:
        csvReader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in csvReader:
           index += 1
           if index == 1 : continue # skip header line
           # if index > 10: break
           print( row )
           try:
              cmsId = int(row[0])
              name = '%s, %s' % tuple(row[1:])
              print( "name :", name )
              user = db.session.query(EprUser).filter_by(cmsId=int(row[0])).one()
              user.name=name 
              db.session.commit()
           except Exception as e:
             print( 'cmsId: %i - got: %s' % (int(row[0]), str(e)) )

def updateInstituteNames():

    csvFile = 'imports/iCMS-non-ascii-institutes.csv'

    # "Country","Code","Name","Address","Short Name"

    index = 0
    with open(csvFile, 'rb') as csvfile:
        csvReader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in csvReader:
           index += 1
           if index == 1 : continue # skip header line
           # if index > 10: break
           print( row )
           try:
              code = row[1]
              name = row[-1] # shortName
              print( "name :", name )
              inst = db.session.query(EprInstitute).filter_by(code=code).one()
              inst.name=name
              db.session.commit()
           except Exception as e:
             print( 'code: %s - got: %s' % (row[1], str(e)) )

