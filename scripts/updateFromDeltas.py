
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import tarfile
import json
import glob
import datetime

from sqlalchemy.exc import IntegrityError

from app import db
from app.models import EprInstitute, EprUser, Pledge, Task

from app.models import commitUpdated, commitNew

from app.main.JsonDiffer import Comparator

from .updateInstUsers import getCatNameMap , convertName

year = 2016

class Options(object):
    def __init__(self):
        self.include = None
        self.exclude = None
        self.ignore_append = None

def updateInstFromDelta(tf):

    newInsts=json.loads(tf.extractfile('delta_iCMS_Institute.json').readlines()[0])

    # print( newInsts )

    exitingInsts = db.session.query(EprInstitute.code).all()
    print( "found %i existing institutes " % len(exitingInsts) )

    for i in newInsts:

        if 'forreference' in i['cmsStatus'].lower(): continue

        if (i['code'],) not in exitingInsts:
            print( "creating new inst for code %s, status %s " % (i['code'], i['cmsStatus']) )
            print( i )

        else:
            cernInstId = int( i['cernInstId'] )
            iName = None
            if i['shortName']:
                iName = i['shortName'].encode('utf-8')    # .encode('ascii', 'xmlcharrefreplace')
            if not iName and i['name']:
                iName = i['name'].encode("utf-8")
            if not iName:
                print( "ERROR: Ignoring Institute w/o name : ", str(i).encode('ascii', 'xmlcharrefreplace') )
                continue
            countryName = i['country'].decode('latin-1').encode("utf-8")[0].upper()+i['country'].decode('latin-1').encode("utf-8")[1:].lower()

            instCode = i['code'].decode('latin-1').encode("utf-8")

            instDb = db.session.query(EprInstitute).filter_by(code=instCode).one()
            if cernInstId != instDb.cernId : print( "inst %s: cern code changed: %i => %i " % (instCode, instDb.cernId, cernInstId) )
            if i['cmsStatus'] != instDb.status: print( "inst %s: status changed: %s => %s " % (instCode, instDb.status, i['cmsStatus']) )

            print( "updating inst from ", instDb, ' to ', i['code'] )

def updateUserFromDelta(tf):

    newUsers=json.loads(tf.extractfile('delta_iCMS_Person.json').readlines()[0])

    exitingCmsIds = db.session.query(EprUser.cmsId).all()
    print( "found %i existing users" % len(exitingCmsIds) )

    catNameMap = getCatNameMap()

    for nU in newUsers:
        if not nU['status'].startswith('CMS') :
            # print( "ignoring non-CMS* status %s for cmsId %d, login %s, inst %s" % (nU['status'],nU['cmsId'], nU['niceLogin'], nU['instCode'])
            continue

        if (nU['cmsId'],) not in exitingCmsIds:
            print( "creating new user for cmsId %d, login %s, inst %s status %s " % (nU['cmsId'], nU['niceLogin'], nU['instCode'], nU['status']) )
        else:
            fullName = convertName(nU['firstName'], nU['lastName'])
            try:
                inst = db.session.query(EprInstitute).filter_by(code=nU['instCode']).one()
            except Exception as e:
                if 'No row was found for one' in str(e):
                    print( 'skipping - institute not found for ', nU['instCode'] )
                    continue

            user = EprUser(username  = nU['niceLogin'],
                        name      = fullName,
                        hrId      = int(nU['hrId']),
                        cmsId     = int(nU['cmsId']),
                        instId    = int(inst.cernId),
                        authorReq = bool( nU[ 'isAuthor' ] ),
                        status    = nU['status'],
                        isSusp    = bool(nU['isAuthorSuspended']),
                        categoryName = catNameMap[ nU['activityId'] ],
                        year = year,
                        mainInst=int(inst.id)
                        )

            opts = Options()
            opts.exclude = ['code', 'year', 'name']
            uComp = Comparator(opts=opts)
            uDiff = uComp.compare_dicts(old_obj = user.to_json(),
                                        new_obj = db.session.query(EprUser).filter_by(cmsId=nU['cmsId']).one().to_json())
            if '_update' not in uDiff.keys(): continue

            if nU['niceLogin'] is None and user.username and user.username.startswith('fakeUsername'):
                uDiff['_update'].remove('username')
                if len( uDiff['_update'] ) == 0: continue

            print( "updating user for cmsId %d, new login %s, new inst %s new status %s " % (nU['cmsId'], nU['niceLogin'], nU['instCode'], nU['status']), end='' )
            print( uDiff, end='' )
            print( ' db: ', db.session.query(EprUser).filter_by(cmsId=nU['cmsId']).one() )


    # abort for now to make sure no fake user is created ...
    db.session.rollback()


def updateEPR2015FromDelta( tf ):

    newPledges = json.loads(tf.extractfile('delta_iCMS_Pledge.json').readlines()[0])

    exitingPledgeCodes = db.session.query(Pledge.code).filter(Pledge.year==2015).all()
    print( "found %i existing %i new or updated pledges in %s" % (len(exitingPledgeCodes), len(newPledges), tf.name) )

    nNewPl = 0
    nOK = 0
    for i in newPledges:
        # print( i

        try:
            taskCodeStr = '[[%s-%s]-%s]%%' % (i['projectId'],
                                              i['activityId'],
                                              i['taskId'] )
            task = db.session.query(Task).filter( Task.code.like(taskCodeStr) ).one()
        except Exception as e:
            if "No row was found for " in str(e):
                print( "\n**> ERROR: no task found for taskCode %s (pledgeId %s) (skipping pledge)" % (taskCodeStr, i['id']) )
                continue
            else:
                print( "\n**> ERROR: tryring to find taskId %s for pledge %s: %s" % (i['taskId'], i['id'], str(e)) )
                continue
        # print( "Task code %s found for pledge" % task.code

        try:
            inst = db.session.query(EprInstitute).filter_by(code=i['instCode']).one()
        except Exception as e:
            if "No row was found for " in str(e):
                print( "ERROR: no institute (%s) found for taskId %s (pledgeId %s) (skipping pledge)" % (i['instCode'], i['taskId'], i['id']) )
                continue
            else:
                print( "ERROR: tryring to find institute (%s) for pledge %s: %s" % (i['instCode'], i['id'], str(e)) )
                continue

        try:
            user = db.session.query(EprUser).filter_by( cmsId=int(i['cmsId']) ).one()
        except Exception as e:
            if "No row was found for " in str(e):
                print( "ERROR: no user (%s) found for taskId %s (pledgeId %s) (skipping pledge)" % (i['cmsId'], i['taskId'], i['id']) )
                continue
            else:
                print( "ERROR: tryring to find institute (%s) for pledge %s: %s" % (i['instCode'], i['id'], str(e)) )
                continue

        print( inst.code )

        plCode = '+'.join([str(x) for x in [task.id, user.id, inst.id]])

        workPld  = float(i['amount']) * ( float(i['percentage'])/100. )
        workAcc  = float(i['totalAccepted']) * ( float(i['percentage'])/100. )
        workDone = float(i['total']) * ( float(i['percentage'])/100. )
        if (plCode,) not in exitingPledgeCodes:
            pledge = Pledge( taskId=task.id,
                             userId=user.id,
                             instId=inst.id,
                             workTimePld = workPld,
                             workTimeAcc = workAcc,
                             workTimeDone = workDone,
                             status='done',
                             year=2015,
                             workedSoFar=workDone)
            commitNew(pledge)
            nNewPl += 1
            nOK += 1

        else: # already in, update it:
            pledge = Pledge.query.filter_by(code = plCode).one()

            oldPlJson = pledge.to_json()
            # update times by percentage. there are 145 (out of 5820) pledges which have this in 2015
            pledge.workTimePld = workPld
            pledge.workTimeAcc = workAcc
            pledge.workTimeDone = workDone
            pledge.timestamp = datetime.datetime.utcnow()
            newPlJson = pledge.to_json()

            plComp = Comparator()
            plDiff = plComp.compare_dicts(oldPlJson, newPlJson)
            if '_update' in plDiff:
                print( "updating pledge: for user %s inst %s " % (user.username, inst.code) )
                for key,val in plDiff['_update'].items():
                    print( ' ... %15s: %5.2f => %5.2f' % (key, oldPlJson[key], val) )

                commitUpdated(pledge)
                nOK += 1

    # db.session.rollback()

    return len(newPledges), nNewPl

def doAll():

    allKeys = ['delta_iCMS_User.json', 'delta_iCMS_Pledge.json',
               'delta_iCMS_MoData.json', 'delta_iCMS_ProjectManagement.json',
               'delta_iCMS_Person.json', 'delta_iCMS_Institute.json']

    ntot = 0
    nnew = 0
    for i in range(43,135):
        print( '\n---> updating for ', i )
        try:
            tarFileName = glob.glob( '/data/epr-sync/iCMS_dumps_%i/epr-jsons-deltas_*.tar.bz2' % i )[0]
            if not tarFileName: continue

            tf = tarfile.open(tarFileName, 'r:bz2')
            # make sure we have the right orders for the dependencies
            # if 'delta_iCMS_Institute.json' in tf.getnames(): updateInstFromDelta(tf)
            # if 'delta_iCMS_Person.json' in tf.getnames(): updateUserFromDelta(tf)
            if 'delta_iCMS_Pledge.json' in tf.getnames():
                res = updateEPR2015FromDelta( tf )
                ntot += res[0]
                nnew += res[1]

        except IndexError:
            break
        except:
            raise

    print( allKeys )

    print( 'overall %i new or updated pledges, %i new.' % (ntot, nnew) )

