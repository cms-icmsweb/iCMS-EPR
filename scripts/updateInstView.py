
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import codecs
import time
import datetime
import logging

logging.basicConfig(format = '%(asctime)-15s: %(message)s')
logger = logging.getLogger("updateInstView")
logger.setLevel(logging.WARNING)

import sys, os
sys.path.append(os.getcwd())

import sqlalchemy as sa

from flask import current_app

from app.main.Helpers import getInstituteInfo, getGuestPledgeInstSummary
from app import create_app,db
from app.models import AllInstView

def updateAllInstView(year):

    start = time.time()
    instInfo = getInstituteInfo('all', year=year)

    msg = "got info for %s in %f sec." % (year, (time.time()-start))
    logger.warning( msg )

    start = time.time()
    for item in instInfo:
        instCode = item['code']

        try:
            instName = codecs.encode(item['name'], 'utf-8').decode('utf-8')
        except Exception as e:
            current_app.logger.error('\nERROR encoding instCode %s' % instCode)
            current_app.logger.error('\nERROR encoding instName: ', item['name'])
            raise e
        instSummary = item['summary']
        userInfo = item['userInfo']

        # check if inst(name,year) exists in view, if so: update, otherwise: create
        inst = None
        try:
            inst = db.session.query(AllInstView).filter_by(code=instCode, year=year).one()
        except:
            current_app.logger.warning("No inst with code '%s' for year %d found in View ... " % (instCode, year) )

        # check if the institute has any guest pledges, if so, add them up:
        try :
            guestPledges = getGuestPledgeInstSummary( instCode=instCode, year=year )
        except sa.orm.exc.NoResultFound :
            guestPledges = None

        # if there are guest pledges for the institute, update the summary info
        if guestPledges :
            instSummary[ 'Pledged' ] += guestPledges[ 'Pledged' ]
            instSummary[ 'Accepted' ] += guestPledges[ 'Accepted' ]
            instSummary[ 'Done' ] += guestPledges[ 'Done' ]
            instSummary[ 'EPRaccounted' ] += guestPledges[ 'Done' ]
            instSummary[ 'EPRworkDone' ] += guestPledges[ 'Done' ]

        if inst: # institute is already in DB, update info:
            if year < 2022:
                inst.expected = instSummary['sumAthDue'] + instSummary['sumEprDue']
                inst.pledged  = instSummary['AthPld'] + instSummary['AppPld']
                inst.accepted = instSummary['AthAcc'] + instSummary['AppAcc']
                inst.done     = instSummary['EPRworkDone']
            else:
                inst.expected = instSummary['sumAthDue']
                inst.pledged  = instSummary['AthPld']
                inst.accepted = instSummary['AthAcc']
                inst.done     = instSummary['AthDone']

            inst.authors  = instSummary['Authors']
            inst.actAuthors = instSummary['actAuthors']
            inst.sumEprDue = instSummary['sumEprDue']

            inst.shiftsPld    = instSummary['ShiftsPld']
            inst.shiftsDone   = instSummary['ShiftsDone']
            inst.eprPledged   = instSummary['EPRpledged']
            inst.eprAccounted = instSummary['EPRaccounted']

            inst.timestamp = datetime.datetime.utcnow()

            inst.pledgedFract = 0
            inst.doneFract = 0
            inst.eprPldFrac = 0
            inst.eprAccFrac = 0
            if float( inst.expected ) > 0:
                inst.pledgedFract = float(inst.pledged)/float(inst.expected)
                inst.doneFract    = float(inst.done)/float(inst.expected)
                inst.eprPldFract  = float( inst.eprPledged ) / float( inst.expected )
                inst.eprAccFract  = float( inst.eprAccounted ) / float( inst.expected )
            #-2017-01-10: as discussed with Kerstin:
            # if an institute has no due's, they have automatically done 100% of their work
            elif float( inst.expected ) == 0:
                inst.pledgedFract = 1.
                inst.doneFract    = 1.
                inst.eprPldFract  = 1.
                inst.eprAccFract  = 1.

            logger.info( "updated inst info for %s (%s users)"  % (instName, len(userInfo)) )

        else: # institute is not yet in table, create new entry:
            allInstView = AllInstView( instCode, instName, instSummary, year=year )
            db.session.add(allInstView)
            logger.info( "added inst info for %s", instName )

        # commit the session for this institute
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            logger.error( "ERROR adding instView for %s. Got: %s" % (instName, str(e)) )

        logger.info( f'updated {instCode} to: {inst}' )

    logger.warning( "processing %s took %s sec. " % ( year, str(time.time()-start) ) )

def main(year=None):

    if year is None:
        year = datetime.date.today().year

    app = create_app('default')
    app_context = app.app_context()
    app_context.push()

    # db.create_all()

    updateAllInstView(year)

if __name__ == '__main__':
    main()
