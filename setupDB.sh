#!/usr/bin/env bash 

source ./venv-py3/bin/activate

# source ./test.env

export ICMS_COMMON_ROLE=icms
export ICMS_EPR_ROLE=epr
export ICMS_TOOLKIT_ROLE=toolkit
export ICMS_READER_ROLE=icms_reader
export ICMS_LEGACY_USER=icms_legacy

export POSTGRES_DB=icms_test
export POSTGRES_HOST_AUTH_METHOD=trust

echo "setting up ... DB " ${POSTGRES_DB} " gitlab-ci flag: " ${POSTGRES_ENV_GITLAB_CI}

# use this when running on gitlab-CI
if [ ${POSTGRES_ENV_GITLAB_CI} ]; then
  echo "Setting up for gitlab-ci ... "
  set -x
  export POSTGRES_USER=postgres
  export POSTGRES_HOST=postgres
  # ensure the DB is empty and the schemata are there
  # createuser -h ${POSTGRES_HOST} -U ${POSTGRES_USER} --superuser ${POSTGRES_USER}
  dropdb --if-exists -h ${POSTGRES_HOST} -U ${POSTGRES_USER} ${POSTGRES_DB}
  createdb -h ${POSTGRES_HOST} -U ${POSTGRES_USER} ${POSTGRES_DB} 
  source test.env 
  cd ./venv-py3/src/icms-orm
  PYTHONPATH=./ python icms_orm/configuration/db_bootstrap_script.py postgres | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB}
  cd -
else
  export POSTGRES_HOST=db
  export POSTGRES_USER=icms
  export POSTGRES_PASSWORD=icms
#   dropuser --if-exists -h ${POSTGRES_HOST} -U ${POSTGRES_USER} --superuser ${POSTGRES_USER}
#   createuser -h ${POSTGRES_HOST} -U ${POSTGRES_USER} --superuser ${POSTGRES_USER}
#   echo "ALTER USER ${POSTGRES_USER} WITH ENCRYPTED PASSWORD 'docker';" | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -a
  dropdb --if-exists -h ${POSTGRES_HOST} -U ${POSTGRES_USER} ${POSTGRES_DB}
  createdb -h ${POSTGRES_HOST} -U ${POSTGRES_USER} ${POSTGRES_DB}
  source test.env 
  cd ./venv-py3/src/icms-orm
  PYTHONPATH=./ python icms_orm/configuration/db_bootstrap_script.py postgres | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB}
  cd -
fi

export TEST_DATABASE_URL=postgresql://${POSTGRES_USER}@${POSTGRES_HOST}/${POSTGRES_DB}

cat empty_schema-icms.sql | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB} >loadEmptySchema.log

# some more cleanup: 
echo "alter schema public owner to ${ICMS_COMMON_ROLE};" | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB} -a
echo "alter schema epr owner to ${ICMS_COMMON_ROLE};"    | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB} -a
# make sure we find the sequences for the tables in epr.*:
echo "ALTER ROLE ${ICMS_COMMON_ROLE} SET search_path = epr,public;" | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB} -a
echo "ALTER ROLE ${ICMS_EPR_ROLE} SET search_path = epr,public;"    | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB} -a
echo "ALTER ROLE ${ICMS_READER_ROLE} SET search_path = epr,public;" | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB} -a

echo "ALTER ROLE ${POSTGRES_USER} SET search_path = epr,public;" | psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} -d ${POSTGRES_DB} -a

echo "Setup of DB done."
