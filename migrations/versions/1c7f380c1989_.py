"""empty message

Revision ID: 1c7f380c1989
Revises: None
Create Date: 2016-12-16 12:14:50.523443

"""

# revision identifiers, used by Alembic.
revision = '1c7f380c1989'
down_revision = None

from alembic import op
import sqlalchemy as sa
import sqlalchemy_utils


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('activity',
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.Column('verb', sa.Unicode(length=255), nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), nullable=False),
    sa.Column('data', sqlalchemy_utils.types.json.JSONType(), nullable=True),
    sa.Column('object_type', sa.String(length=255), nullable=True),
    sa.Column('object_id', sa.BigInteger(), nullable=True),
    sa.Column('object_tx_id', sa.BigInteger(), nullable=True),
    sa.Column('target_type', sa.String(length=255), nullable=True),
    sa.Column('target_id', sa.BigInteger(), nullable=True),
    sa.Column('target_tx_id', sa.BigInteger(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_activity_transaction_id'), 'activity', ['transaction_id'], unique=False)
    op.create_table('categories',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=80), nullable=True),
    sa.Column('author_ok', sa.Boolean(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('cms_activities_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('name', sa.String(length=200), autoincrement=False, nullable=True),
    sa.Column('description', sa.String(length=1000), autoincrement=False, nullable=True),
    sa.Column('project_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('name_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('description_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('project_id_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_cms_activities_version_end_transaction_id'), 'cms_activities_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_cms_activities_version_operation_type'), 'cms_activities_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_cms_activities_version_transaction_id'), 'cms_activities_version', ['transaction_id'], unique=False)
    op.create_table('insts',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=80), nullable=True),
    sa.Column('cernid', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=200), nullable=True),
    sa.Column('country', sa.String(length=150), nullable=True),
    sa.Column('cms_status', sa.String(length=40), nullable=False),
    sa.Column('comment', sa.String(length=1000), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('cernid'),
    sa.UniqueConstraint('code')
    )
    op.create_table('insts_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('cernid', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('name', sa.String(length=200), autoincrement=False, nullable=True),
    sa.Column('country', sa.String(length=150), autoincrement=False, nullable=True),
    sa.Column('cms_status', sa.String(length=40), autoincrement=False, nullable=True),
    sa.Column('comment', sa.String(length=1000), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('cernid_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('name_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('country_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('cms_status_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('comment_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_insts_version_end_transaction_id'), 'insts_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_insts_version_operation_type'), 'insts_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_insts_version_transaction_id'), 'insts_version', ['transaction_id'], unique=False)
    op.create_table('managers_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('item_type', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('item_code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('userid', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('role', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('comment', sa.String(length=1000), autoincrement=False, nullable=True),
    sa.Column('status', sa.String(length=20), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('item_type_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('item_code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('userid_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('role_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('comment_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('status_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_managers_version_end_transaction_id'), 'managers_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_managers_version_operation_type'), 'managers_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_managers_version_transaction_id'), 'managers_version', ['transaction_id'], unique=False)
    op.create_table('pledges_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('user_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('inst_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('task_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('work_pld', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('work_acc', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('work_done', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('work_so_far', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('status', sa.String(length=20), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('user_id_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('inst_id_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('task_id_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('work_pld_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('work_acc_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('work_done_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('work_so_far_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('status_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_pledges_version_end_transaction_id'), 'pledges_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_pledges_version_operation_type'), 'pledges_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_pledges_version_transaction_id'), 'pledges_version', ['transaction_id'], unique=False)
    op.create_table('projects',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=80), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=80), nullable=True),
    sa.Column('description', sa.String(length=1000), nullable=True),
    sa.Column('max_task_level', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('code')
    )
    op.create_table('projects_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('name', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('description', sa.String(length=1000), autoincrement=False, nullable=True),
    sa.Column('max_task_level', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('name_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('description_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('max_task_level_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_projects_version_end_transaction_id'), 'projects_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_projects_version_operation_type'), 'projects_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_projects_version_transaction_id'), 'projects_version', ['transaction_id'], unique=False)
    op.create_table('roles',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=64), nullable=True),
    sa.Column('default', sa.Boolean(), nullable=True),
    sa.Column('permissions', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_index(op.f('ix_roles_default'), 'roles', ['default'], unique=False)
    op.create_table('tasks_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('name', sa.String(length=200), autoincrement=False, nullable=True),
    sa.Column('description', sa.String(length=2000), autoincrement=False, nullable=True),
    sa.Column('activity_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('needed_work', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('pct_at_cern', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('t_type', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('shift_type_id', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('comment', sa.String(length=2000), autoincrement=False, nullable=True),
    sa.Column('earliest_start', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('latest_end', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('level', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('parent', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('name_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('description_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('activity_id_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('needed_work_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('pct_at_cern_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('t_type_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('shift_type_id_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('comment_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('earliest_start_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('latest_end_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('level_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('parent_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_tasks_version_end_transaction_id'), 'tasks_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_tasks_version_operation_type'), 'tasks_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_tasks_version_transaction_id'), 'tasks_version', ['transaction_id'], unique=False)
    op.create_table('time_line_inst_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('inst_code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('cms_status', sa.String(length=40), autoincrement=False, nullable=True),
    sa.Column('comment', sa.String(length=1000), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('inst_code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('cms_status_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('comment_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_time_line_inst_version_end_transaction_id'), 'time_line_inst_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_time_line_inst_version_operation_type'), 'time_line_inst_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_time_line_inst_version_transaction_id'), 'time_line_inst_version', ['transaction_id'], unique=False)
    op.create_table('time_line_user_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('user_cms_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('inst_code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('main_project', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('is_author', sa.Boolean(), autoincrement=False, nullable=True),
    sa.Column('is_suspended', sa.Boolean(), autoincrement=False, nullable=True),
    sa.Column('status', sa.String(length=40), autoincrement=False, nullable=True),
    sa.Column('category', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('due_applicant', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('due_author', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('year_fraction', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('comment', sa.String(length=1000), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('user_cms_id_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('inst_code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('main_project_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('is_author_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('is_suspended_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('status_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('category_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('due_applicant_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('due_author_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_fraction_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('comment_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_time_line_user_version_end_transaction_id'), 'time_line_user_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_time_line_user_version_operation_type'), 'time_line_user_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_time_line_user_version_transaction_id'), 'time_line_user_version', ['transaction_id'], unique=False)
    op.create_table('userinst_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('user_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('inst_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_userinst_version_end_transaction_id'), 'userinst_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_userinst_version_operation_type'), 'userinst_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_userinst_version_transaction_id'), 'userinst_version', ['transaction_id'], unique=False)
    op.create_table('users_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('username', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('name', sa.String(length=200), autoincrement=False, nullable=True),
    sa.Column('hrid', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('cmsid', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('main_inst', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('role_id', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('auth_req', sa.Boolean(), autoincrement=False, nullable=True),
    sa.Column('is_suspended', sa.Boolean(), autoincrement=False, nullable=True),
    sa.Column('status', sa.String(length=40), autoincrement=False, nullable=True),
    sa.Column('category', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('comment', sa.String(length=1000), autoincrement=False, nullable=True),
    sa.Column('code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('username_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('name_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('hrid_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('cmsid_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('main_inst_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('role_id_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('auth_req_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('is_suspended_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('status_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('category_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('comment_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id')
    )
    op.create_index(op.f('ix_users_version_end_transaction_id'), 'users_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_users_version_operation_type'), 'users_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_users_version_transaction_id'), 'users_version', ['transaction_id'], unique=False)
    op.create_table('v_all_inst',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=100), nullable=True),
    sa.Column('name', sa.String(length=200), nullable=True),
    sa.Column('expected', sa.Float(), nullable=True),
    sa.Column('pledged', sa.Float(), nullable=True),
    sa.Column('pledged_fract', sa.Float(), nullable=True),
    sa.Column('accepted', sa.Float(), nullable=True),
    sa.Column('done', sa.Float(), nullable=True),
    sa.Column('done_fract', sa.Float(), nullable=True),
    sa.Column('authors', sa.Float(), nullable=True),
    sa.Column('phys_users', sa.Float(), nullable=True),
    sa.Column('sum_epr_due', sa.Float(), nullable=True),
    sa.Column('shifts_pld', sa.Float(), nullable=True),
    sa.Column('epr_pledged', sa.Float(), nullable=True),
    sa.Column('epr_pld_fract', sa.Float(), nullable=True),
    sa.Column('shifts_done', sa.Float(), nullable=True),
    sa.Column('epr_accounted', sa.Float(), nullable=True),
    sa.Column('epr_acc_fract', sa.Float(), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('cms_activities',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=80), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=200), nullable=True),
    sa.Column('description', sa.String(length=1000), nullable=True),
    sa.Column('project_id', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['project_id'], ['projects.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('code')
    )
    op.create_table('time_line_inst',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('inst_code', sa.String(length=80), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('cms_status', sa.String(length=40), nullable=False),
    sa.Column('comment', sa.String(length=1000), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['inst_code'], ['insts.code'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=80), nullable=True),
    sa.Column('name', sa.String(length=200), nullable=True),
    sa.Column('hrid', sa.Integer(), nullable=True),
    sa.Column('cmsid', sa.Integer(), nullable=True),
    sa.Column('main_inst', sa.Integer(), nullable=True),
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.Column('auth_req', sa.Boolean(), nullable=True),
    sa.Column('is_suspended', sa.Boolean(), nullable=True),
    sa.Column('status', sa.String(length=40), nullable=True),
    sa.Column('category', sa.Integer(), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('comment', sa.String(length=1000), nullable=True),
    sa.Column('code', sa.String(length=80), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['category'], ['categories.id'], ),
    sa.ForeignKeyConstraint(['main_inst'], ['insts.id'], ),
    sa.ForeignKeyConstraint(['role_id'], ['roles.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('cmsid'),
    sa.UniqueConstraint('code'),
    sa.UniqueConstraint('hrid')
    )
    op.create_table('epr_due',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('inst_id', sa.Integer(), nullable=True),
    sa.Column('work_due', sa.Float(), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['inst_id'], ['insts.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('managers',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=80), nullable=True),
    sa.Column('item_type', sa.String(length=80), nullable=True),
    sa.Column('item_code', sa.String(length=80), nullable=True),
    sa.Column('userid', sa.Integer(), nullable=True),
    sa.Column('role', sa.Integer(), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('comment', sa.String(length=1000), nullable=True),
    sa.Column('status', sa.String(length=20), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['userid'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('shifts',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('shift_id', sa.Integer(), nullable=True),
    sa.Column('shifter_id', sa.Integer(), nullable=True),
    sa.Column('sub_system', sa.String(length=100), nullable=True),
    sa.Column('shift_type', sa.String(length=100), nullable=True),
    sa.Column('shift_type_id', sa.Integer(), nullable=True),
    sa.Column('shift_start', sa.DateTime(), nullable=True),
    sa.Column('shift_end', sa.DateTime(), nullable=True),
    sa.Column('flavour_name', sa.String(length=20), nullable=True),
    sa.Column('weight', sa.Float(), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['shifter_id'], ['users.hrid'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('shift_id')
    )
    op.create_table('tasks',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=80), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=200), nullable=True),
    sa.Column('description', sa.String(length=2000), nullable=True),
    sa.Column('activity_id', sa.Integer(), nullable=True),
    sa.Column('needed_work', sa.Float(), nullable=True),
    sa.Column('pct_at_cern', sa.Integer(), nullable=True),
    sa.Column('t_type', sa.String(length=80), nullable=True),
    sa.Column('shift_type_id', sa.String(length=80), nullable=True),
    sa.Column('comment', sa.String(length=2000), nullable=True),
    sa.Column('earliest_start', sa.String(length=80), nullable=True),
    sa.Column('latest_end', sa.String(length=80), nullable=True),
    sa.Column('level', sa.Integer(), nullable=True),
    sa.Column('parent', sa.String(length=80), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['activity_id'], ['cms_activities.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('code')
    )
    op.create_table('time_line_user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_cms_id', sa.Integer(), nullable=True),
    sa.Column('inst_code', sa.String(length=80), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('main_project', sa.String(length=80), nullable=True),
    sa.Column('is_author', sa.Boolean(), nullable=True),
    sa.Column('is_suspended', sa.Boolean(), nullable=True),
    sa.Column('status', sa.String(length=40), nullable=True),
    sa.Column('category', sa.Integer(), nullable=True),
    sa.Column('due_applicant', sa.Float(), nullable=True),
    sa.Column('due_author', sa.Float(), nullable=True),
    sa.Column('year_fraction', sa.Float(), nullable=True),
    sa.Column('comment', sa.String(length=1000), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['category'], ['categories.id'], ),
    sa.ForeignKeyConstraint(['inst_code'], ['insts.code'], ),
    sa.ForeignKeyConstraint(['user_cms_id'], ['users.cmsid'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('transaction',
    sa.Column('issued_at', sa.DateTime(), nullable=True),
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.Column('remote_addr', sa.String(length=50), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_transaction_user_id'), 'transaction', ['user_id'], unique=False)
    op.create_table('userinst',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('inst_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['inst_id'], ['insts.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('pledges',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=80), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('inst_id', sa.Integer(), nullable=True),
    sa.Column('task_id', sa.Integer(), nullable=True),
    sa.Column('work_pld', sa.Float(), nullable=True),
    sa.Column('work_acc', sa.Float(), nullable=True),
    sa.Column('work_done', sa.Float(), nullable=True),
    sa.Column('work_so_far', sa.Float(), nullable=True),
    sa.Column('status', sa.String(length=20), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['inst_id'], ['insts.id'], ),
    sa.ForeignKeyConstraint(['task_id'], ['tasks.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('code')
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('pledges')
    op.drop_table('userinst')
    op.drop_index(op.f('ix_transaction_user_id'), table_name='transaction')
    op.drop_table('transaction')
    op.drop_table('time_line_user')
    op.drop_table('tasks')
    op.drop_table('shifts')
    op.drop_table('managers')
    op.drop_table('epr_due')
    op.drop_table('users')
    op.drop_table('time_line_inst')
    op.drop_table('cms_activities')
    op.drop_table('v_all_inst')
    op.drop_index(op.f('ix_users_version_transaction_id'), table_name='users_version')
    op.drop_index(op.f('ix_users_version_operation_type'), table_name='users_version')
    op.drop_index(op.f('ix_users_version_end_transaction_id'), table_name='users_version')
    op.drop_table('users_version')
    op.drop_index(op.f('ix_userinst_version_transaction_id'), table_name='userinst_version')
    op.drop_index(op.f('ix_userinst_version_operation_type'), table_name='userinst_version')
    op.drop_index(op.f('ix_userinst_version_end_transaction_id'), table_name='userinst_version')
    op.drop_table('userinst_version')
    op.drop_index(op.f('ix_time_line_user_version_transaction_id'), table_name='time_line_user_version')
    op.drop_index(op.f('ix_time_line_user_version_operation_type'), table_name='time_line_user_version')
    op.drop_index(op.f('ix_time_line_user_version_end_transaction_id'), table_name='time_line_user_version')
    op.drop_table('time_line_user_version')
    op.drop_index(op.f('ix_time_line_inst_version_transaction_id'), table_name='time_line_inst_version')
    op.drop_index(op.f('ix_time_line_inst_version_operation_type'), table_name='time_line_inst_version')
    op.drop_index(op.f('ix_time_line_inst_version_end_transaction_id'), table_name='time_line_inst_version')
    op.drop_table('time_line_inst_version')
    op.drop_index(op.f('ix_tasks_version_transaction_id'), table_name='tasks_version')
    op.drop_index(op.f('ix_tasks_version_operation_type'), table_name='tasks_version')
    op.drop_index(op.f('ix_tasks_version_end_transaction_id'), table_name='tasks_version')
    op.drop_table('tasks_version')
    op.drop_index(op.f('ix_roles_default'), table_name='roles')
    op.drop_table('roles')
    op.drop_index(op.f('ix_projects_version_transaction_id'), table_name='projects_version')
    op.drop_index(op.f('ix_projects_version_operation_type'), table_name='projects_version')
    op.drop_index(op.f('ix_projects_version_end_transaction_id'), table_name='projects_version')
    op.drop_table('projects_version')
    op.drop_table('projects')
    op.drop_index(op.f('ix_pledges_version_transaction_id'), table_name='pledges_version')
    op.drop_index(op.f('ix_pledges_version_operation_type'), table_name='pledges_version')
    op.drop_index(op.f('ix_pledges_version_end_transaction_id'), table_name='pledges_version')
    op.drop_table('pledges_version')
    op.drop_index(op.f('ix_managers_version_transaction_id'), table_name='managers_version')
    op.drop_index(op.f('ix_managers_version_operation_type'), table_name='managers_version')
    op.drop_index(op.f('ix_managers_version_end_transaction_id'), table_name='managers_version')
    op.drop_table('managers_version')
    op.drop_index(op.f('ix_insts_version_transaction_id'), table_name='insts_version')
    op.drop_index(op.f('ix_insts_version_operation_type'), table_name='insts_version')
    op.drop_index(op.f('ix_insts_version_end_transaction_id'), table_name='insts_version')
    op.drop_table('insts_version')
    op.drop_table('insts')
    op.drop_index(op.f('ix_cms_activities_version_transaction_id'), table_name='cms_activities_version')
    op.drop_index(op.f('ix_cms_activities_version_operation_type'), table_name='cms_activities_version')
    op.drop_index(op.f('ix_cms_activities_version_end_transaction_id'), table_name='cms_activities_version')
    op.drop_table('cms_activities_version')
    op.drop_table('categories')
    op.drop_index(op.f('ix_activity_transaction_id'), table_name='activity')
    op.drop_table('activity')
    ### end Alembic commands ###
