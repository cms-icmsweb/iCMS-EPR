
source ./venv-py3/bin/activate;

# Note: when not at CERN, depending on your tunnelling setup, you may need one pair of these activated:
# export LDAP_URI=ldaps://localhost:9999 
# export LDAP_AUTH_URI=ldaps://localhost:9999 
# export LDAP_URI=ldaps://localhost:8636
# export LDAP_AUTH_URI=ldaps://localhost:9636

echo "running with gitlab-ci flag: " ${POSTGRES_ENV_GITLAB_CI}

export POSTGRES_HOST=postgres
export POSTGRES_DB=icms_test
export POSTGRES_HOST_AUTH_METHOD=trust

# use this when running on gitlab-CI
if [ ${POSTGRES_ENV_GITLAB_CI} ]; then
  echo "Setting up for gitlab-ci ... "
  export POSTGRES_USER=runner
else
  export POSTGRES_USER=postgres
fi

export TEST_DATABASE_URL=postgresql+psycopg2://${POSTGRES_USER}:@${POSTGRES_HOST}/${POSTGRES_DB} 
export TEST_ICMS_DATABASE_URL=postgresql+psycopg2://${POSTGRES_USER}:@${POSTGRES_HOST}/${POSTGRES_DB}

# for testing, use fake auth/egroups:
export EPR_LOGIN_USE_LOCAL=1
export USE_FAKE_EGROUPS=1

export ICMS_EPR_COVERAGE=1

python3 $*
