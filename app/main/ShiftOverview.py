import logging
import json
from datetime import datetime, timedelta, date

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from ..models import EprInstitute, EprUser, AllInstView, TimeLineUser as TLU, AllInstView, Shift, ShiftTaskMap
from icms_orm.common import Institute, Country  # , Region
from .Helpers import getCspPerAuthor, getCSQperAuthor

logging.basicConfig( format='%(asctime)-15s - %(levelname)s: %(message)s', level=logging.DEBUG )

def findInstAtTimestamp(tlu, tsr):

    # tsr = datetime.strptime( timeStampReq, '%Y-%m-%dT%H:%M:%S.000Z' )

    prev = None
    minDist = timedelta(days=99999)
    for inst, entry in tlu.items():
        tsHere = entry[3]
        # tsHere = datetime.strptime( tsAct, '%Y-%m-%dT%H:%M:%S.000Z' )
        # if int(entry['hrid']) in [693006]:
        #     logging.info( f'found tls for {entry["hrid"]} at {tsr} : {inst} -- {entry} -- tsHere/tsr: {tsHere} / {tsr} ; tsHere <= tsr is {tsHere <= tsr}' )

        if tsHere <= tsr:
            dist = tsr - tsHere
            if dist < minDist:
                minDist = dist
                prev = inst

    return prev

def checkROC( shift ):
    newStart = shift.shiftStart
    newEnd = shift.shiftEnd
    newWeight = shift.weight
    
    # check if the shift is in a ROC, update start/end times accordingly:
    if '-ROC-' in shift.shiftType:
        # print( f'--> INFO: found ROC type shift: {shift.shiftType} ... processing')
        rocName = shift.shiftType[ shift.shiftType.find('-ROC-')+5: ]

        rocOffsetMap = { 'DESY': 0., 'FNAL': -8., 'China': +8. }

        if rocName not in rocOffsetMap:
            print( f'++> WARNING: rocName {rocName} for shiftType {shift.shiftType} not found in rocOffsetMap -- offset reset to 0!!' )
        rocOffset = rocOffsetMap[ rocName ] if rocName in rocOffsetMap else 0
        newStart = shift.shiftStart + timedelta( hours=rocOffset)
        newEnd = shift.shiftEnd + timedelta( hours=rocOffset)
        # print( f'--> INFO: updated ROC type shift start/end from: {shift.shiftStart} / {shift.shiftEnd} == to == {newStart} / {newEnd}')
    
        # check/recalculate weight if needed:
        if rocOffset != 0.:
            newWeights = { 'weekday-dayOrEvening': 1.0, 'weekday-night': 2.0, 
                           'weekend-dayOrEvening': 2.0, 'weekend-night': 3.0, }
            duration = (newEnd-newStart).days*86400 + (newEnd-newStart).seconds

            # weekday: Monday is 0 and Sunday is 6, so Fri,Sat is 4,5
            dayInWeek = 'weekend' if newStart.weekday() in [5,6] else 'weekday'
            dayTime = 'night' if (newStart.hour >= 23 or newStart.hour < 5) else 'dayOrEvening' 
            # check a few special cases: night shifts starting on Fri at/after 23:00 count as w/e ... 
            if newStart.hour in [21, 23] and newStart.weekday() == 4:
                dayTime = 'night'
                dayInWeek = 'weekend'    
            if newStart.hour in [21, 23] and newStart.weekday() == 6: # while the shift on the night of Sun->Mon is weekday
                dayTime = 'night'
                dayInWeek = 'weekday'

            if duration <= 12*3600: # "normal" 8- or 12- hour shifts
                newWeight = newWeights[ f'{dayInWeek}-{dayTime}' ]
            elif duration == 86400: # full day shift:
                dayTime = 'allDay'
                if newStart.weekday() in [5, 6]:      # weekend full day shift
                    dayInWeek = 'weekend'
                    newWeight = 1.0
                else:
                    dayInWeek = 'weekday'
                    newWeight = 1.0
                    
            elif duration == 604800: # week shift, weight 8; only "dayOrEvening"
                dayTime = 'allWeek'
                dayInWeek = 'week'
                newWeight = 8.0
            else:
                logging.info( f'WARNING: found an unknown shift duration of {duration} - start/end: {newStart} - {newEnd} ' )

    return newStart, newEnd, newWeight
    

holidayShifts = None

class ShiftOverview(object):
    
    def __init__(self, year=2022, proposal=None) -> None:
        
        self.deBugPrint = False

        self.icrMap = None
        self.stMap = {}
        self.userTLs = {}
        self.multiInsts = {}
        self.instAuthMap = {}
        self.allShifts = {}
        self.year = int(year)
        self.proposal = proposal.lower()
        
        self.cspToEPR = 1. ## show CSP directly .... # was: getCSPtoEPR() if self.year < 2023 else 1.0      # 0.0462 = (1./5.) * (12./52.)

        self.cspAuth = getCspPerAuthor( self.year ) if self.year < 2023 else getCSQperAuthor( self.proposal, self.year )
        logging.debug( f'==> CSP per author in {self.year} is {self.cspAuth}')
        
        self.shiftsByInst = []
        self.shiftsByCountry = []
        self.shiftsByRegion = []

        self.getHolidayShifts(year)


    def log(self, msg, shifterId = -1):
        if shifterId in [693367, 657934]:
            print( msg )


    def getHolidayShifts( self, year ):

        global holidayShifts

        if year < 2025: return

        if holidayShifts is not None: # we've successfully read the file once
            return

        if holidayShifts == {}: # no holidays found in file 
            msg = f"No holidays found in Holidays.yaml file for the requested year {year}"
            logging.error( msg )
            print( msg )
            return False

        # going to read the file and extract the info         

        with open('Holidays.yaml', 'r') as hF:
            items = load( hF, Loader )

        holidayShifts = None
        for item in items:
            # print (item)
            for yr, hols in item.items():
                # print( f'Found holidayShifts in {yr} to be: {hols}')
                if yr == year:
                    holidayShifts = {}
                    for item in hols:
                        (shiftStart, csp), = item.items()
                        holidayShifts[ datetime.strptime( shiftStart,'%Y-%m-%dT%H:%M:%S') ] = csp
                    break
            if holidayShifts: break

        print( f'Found holidayShifts in {yr} to be: ')
        [ print( x, c ) for x, c in holidayShifts.items() ]

    def cspForHolidayShift( self, shiftStart ):
        global holidayShifts

        # print( f'checking shiftStart {shiftStart} ({type(shiftStart)}) for holidays ... ', end='' )
        if shiftStart in holidayShifts.keys(): 
            cspShift = holidayShifts[ shiftStart ]
            # print( f'found ... {cspShift}' )
            return True, cspShift

        print( f' NOT found ... ' )
        return False, 0.

    def analyseTLs( self, userTLsIn ):
        
        for item in userTLsIn:
            hrId = item[0]
            thisInst = item[1]
            if hrId in [370283]:
                if self.deBugPrint: logging.debug( f"found: {hrId} {thisInst} - {item} " )
            if hrId not in self.userTLs:
                self.userTLs[hrId] = { thisInst: list(item) }
            else: # got a duplicate for the hrId, check if the inst is the same
                if self.deBugPrint: logging.debug( f"duplicate found: {hrId} {thisInst} - {self.userTLs[hrId]} " )
                if thisInst in self.userTLs[hrId].keys():
                    for prevInst, prevItem in self.userTLs[hrId].items():
                        if thisInst == prevInst: # same inst, update year_fraction
                            # logging.info( f"... going to update previous entry for {hrId} {thisInst} from {self.userTLs[hrId][prevInst]} by {item[2]} " )
                            self.userTLs[hrId][prevInst][2] += item[2]
                            if self.deBugPrint: logging.debug( f"... going to update previous entry for {hrId} {thisInst} from {self.userTLs[hrId][prevInst]['year_fraction']} by {item['year_fraction']} " )
                else: # new inst not yet known to this user
                    self.userTLs[hrId][thisInst] = list(item)
                    if self.deBugPrint: logging.debug( f"... added new entry for {hrId} {thisInst} to {self.userTLs[hrId][thisInst]} " )
                    
        # logging.info( f'\nSummary:')
        for k, tl in self.userTLs.items():
            if self.deBugPrint: logging.debug( f'{k}: {tl} ' )
            if len(tl.keys()) > 1:
                # if int(k) == 693006: logging.info( f'found multiple insts for {k}: {tl}' )
                self.multiInsts[k] = tl
        
        return 

        ##
        # found multiple insts for 853416:  12-10-2022 from Napoli -> hephy
        #   {'HEPHY': {'hrid': 853416, 'inst_code': 'HEPHY', 'year_fraction': 0.221917808219178, 'timestamp': '2022-10-11T22:00:00.000Z'}, 
        #   'NAPOLI': {'hrid': 853416, 'inst_code': 'NAPOLI', 'year_fraction': 0.7780821917808219, 'timestamp': '2021-12-31T23:00:00.000Z'}}
        ##
        
    def getDataFromDB(self, session):
        
        icr = ( session.query(EprInstitute.code, EprInstitute.cmsStatus, EprInstitute.country,
                              Country.code.label('country_code'), Country.code.label('region'))   # Region.name.label('region'))
                       .join( Institute, EprInstitute.code == Institute.code)
                       .join( Country, Country.code == Institute.country_code)
                       # .join( Region, Region.code == Country.region_code)
                       .order_by( EprInstitute.code )
                       .all())
        if self.deBugPrint: logging.debug( f'===>>> icr: {icr[:3]} ')
        self.icrMap = { x[0]: { 'code': x[0], 'cms_status': x[1], 'country': x[2], 'country_code': x[3], 'region': getRegion(x[2]) } for x in icr }
        self.icrMap['CERN'] = {
            "code" : "CERN",
            "cms_status" : "Yes",
            "country" : "CERN",
            "country_code" : "CERN",
            "region" : "CERN"
        }

        shTsMapRaw = (session.query( ShiftTaskMap.subSystem, 
                                     ShiftTaskMap.shiftType, 
                                     ShiftTaskMap.flavourName,
                                     ShiftTaskMap.year, 
                                     ShiftTaskMap.weight, 
                                     ShiftTaskMap.addedValueCredits
                                    )
                            .filter( ShiftTaskMap.year == self.year)
                            .filter( ShiftTaskMap.status == 'ACTIVE')
                            .all())
        # [ logging.info(x) for x in shTsMapRaw[:5]]
        self.stMap = { f'{x[0]}-{x[1]}-{x[2]}': x for x in shTsMapRaw }    # key: subSys-type-flavour
        if self.deBugPrint: logging.debug( f'===>> {len(self.stMap)} entries in STM ')
        
        self.allShifts = (session.query( Shift.userId, Shift.year, Shift.weight.label('shift_weight'),
                                         Shift.shiftStart, Shift.shiftEnd, Shift.subSystem, Shift.shiftType, Shift.flavourName)
                            .filter( Shift.year == self.year)
                            # .filter( Shift.shiftStart <= datetime.now())
                            .all())
        if self.deBugPrint: logging.debug( f'\n {len(self.allShifts)} Shifts: \n')
        # [ logging.info(x) for x in self.allShifts[:5]]

        userTLsRaw = (session.query(EprUser.hrId, TLU.instCode, TLU.yearFraction, TLU.timestamp)
                             .join(TLU, TLU.cmsId == EprUser.cmsId)
                             .filter( TLU.year == self.year)
                             .filter( EprUser.hrId > 0)
                             .order_by( EprUser.hrId, TLU.yearFraction)
                             .all())
        if self.deBugPrint: logging.debug( f'===>>>  {len(userTLsRaw)} TLs: {userTLsRaw[:3]} ')
        self.analyseTLs( userTLsRaw )

        instAuthors = (session.query( AllInstView.code, AllInstView.year, AllInstView.actAuthors.label('authors'))
                              .filter( AllInstView.year == self.year )
                              .order_by( AllInstView.code, AllInstView.year.desc() )
                              .all())
        self.instAuthMap = { x[0]: x for x in instAuthors }
        if self.deBugPrint: logging.debug( f'\ {len(self.instAuthMap)} entries in instAuthMap: \n')
        # [ logging.info( f'{i}: {x}' ) for i,x in self.instAuthMap.items() if i in ['CERN'] ]


    def getScalingFactor(self, startTime, endTime, propSelIn=None):
        duration = (endTime-startTime).days*86400 + (endTime-startTime).seconds
        scalingFactor = 1.0

        propSel = propSelIn if propSelIn else self.proposal
        
        if propSel.lower() == 'proposal-1':
            newWeights = { 'weekday-dayOrEvening': 1.0, 'weekday-night': 2.0, 
                           'weekend-dayOrEvening': 2.0, 'weekend-night': 3.0, }
        elif propSel.lower() == 'proposal-2':
            newWeights = { 'weekday-dayOrEvening': 1.5, 'weekday-night': 3.0, 
                           'weekend-dayOrEvening': 3.0, 'weekend-night': 4.0, }
        else: # "normal" shifts
            newWeights = { 'weekday-dayOrEvening': 1.0, 'weekday-night': 1.5,
                           'weekend-dayOrEvening': 1.5, 'weekend-night': 2.0, }

        # weekday: Monday is 0 and Sunday is 6, so Fri,Sat is 4,5
        dayInWeek = 'weekend' if startTime.weekday() in [5,6] else 'weekday'
        dayTime = 'night' if (startTime.hour >= 23 or startTime.hour < 5) else 'dayOrEvening' 
        # check a few special cases: night shifts starting on Fri at/after 23:00 count as w/e ... 
        if startTime.hour in [21,23] and startTime.weekday() == 4:
            dayTime = 'night'
            dayInWeek = 'weekend'    
        if startTime.hour in [21,23] and startTime.weekday() == 6: # while the shift on the night of Sun->Mon is weekday
            dayTime = 'night'
            dayInWeek = 'weekday'

        if duration <= 10*3600: # "normal" 8-hour shift
            scalingFactor = newWeights[ f'{dayInWeek}-{dayTime}' ]
        elif duration == 86400: # full day shift:
            dayTime = 'allDay'
            if startTime.weekday() in [5, 6]:      # weekend full day shift
                dayInWeek = 'weekend'
                scalingFactor = 1.0
            else:
                dayInWeek = 'weekday'
                scalingFactor = 1.0
        elif duration == 86400/2: # 12 h shift:
            if startTime.weekday() in [5, 6]:      # weekend full day shift
                dayInWeek = 'weekend'
                scalingFactor = 1.0
            else:
                dayInWeek = 'weekday'
                scalingFactor = 1.0
                
        elif duration == 604800: # week shift, weight 8; only "dayOrEvening"
            dayTime = 'allWeek'
            dayInWeek = 'week'
            scalingFactor = 8.0
        elif duration == 345600: # half-week shift, weight undefined
            scalingFactor = 1.
        else:
            logging.warning( f'WARNING: found an unknown shift duration of {duration} - start/end: {startTime} - {endTime} ' )

        # logging.info( f'getShiftWeight>> {self.proposal} {startTime} - {endTime} = {shiftKey} {cat} {isCentral} ({duration}): {dayInWeek} - {dayTime} :: {weight} - {scalingFactor}' )
                
        return scalingFactor
        
    def getShiftWeight(self, shift, isCentral, cat):
        weight = shift[2]

        if self.deBugPrint: 
            self.log( f'\n++getShiftWeight++> {self.year} - {shift}', shift[0] )

        if weight is None:
            logging.warning( f'Found shift with illegal weight {weight}: {shift} ' )
        if weight == 0. and shift[5:8] != ['TC', 'Safety tour', 'main']:
            # logging.warning( f'Found shift with illegal weight {weight}: {shift} ' )
            return weight

        if self.year < 2023: return weight

        return self.calculateWeightProp(shift, weight, isCentral, cat)

    def calculateWeightProp(self, shift,  weight, isCentral, cat):

        startTime = shift[3]
        endTime = shift[4]

        if self.year >= 2024:
            if self.deBugPrint: self.log( f'===>>> checking shift {shift} - {isCentral}, {cat}', shift[0])
            # get scaling factor for central shifts according to day/time for proposal-2
            scalingFactor = self.getScalingFactor( startTime, endTime, 'proposal-2')
            if cat in [ 'cat1', 'cat2' ]:
                return scalingFactor
            
            nominalShiftCSPs = 1.
            scalingFactor = 1.
            # from slide 13 of Plenary talk in Dec'23 CMS week and https://twiki.cern.ch/twiki/bin/viewauth/CMS/CentralShifts2024#Central_credit_accounting
            if shift[5:8] == ['Central', 'Run field manager', 'main']: nominalShiftCSPs = 10
            elif shift[5:8] == ['TRG', 'HLT_DOC', 'main']: nominalShiftCSPs = 8
            elif shift[5:8] == ['Off-line', 'ORM', 'main']: nominalShiftCSPs = 4.5
            elif cat == 'doc1': 
                nominalShiftCSPs = 8.
                # some doc1 type shifts have a single shift with a duration of a week, some have seven shifts of a day
                # so we'll have to re-calculate the scaling factor if shift duration is less than a week:
                duration = (endTime-startTime).days*86400 + (endTime-startTime).seconds
                if duration == 86400:
                    scalingFactor = (duration/604800.)
                    # if startTime.weekday() in [5, 6]: scalingFactor *= 1.5
                if self.deBugPrint: self.log( f'===>>> doc1 shift for {self.year} rescaled: {nominalShiftCSPs}*{duration/604800.}={scalingFactor} - {shift}', shift[0])
            else: # not a special shift ... 
                scalingFactor = self.getScalingFactor( startTime, endTime )
                if self.deBugPrint: self.log( f'===>>> unhandled shift for {self.year}, {scalingFactor} - {shift}', shift[0])
            if self.deBugPrint: self.log( f'===>>> found scaling factor {scalingFactor} for shift in {self.year}: {shift}', shift[0])
            if self.deBugPrint: self.log( f'===>>> final weight {scalingFactor}*{nominalShiftCSPs}={scalingFactor * nominalShiftCSPs} for shift in {self.year}: {shift}', shift[0])
            return scalingFactor * nominalShiftCSPs

        # handle shifts according to the relevant proposal:
        prop = 'standard'
        if self.year == 2023: prop = 'proposal-1'
        if not isCentral: return weight * self.getScalingFactor( startTime, endTime, prop )  # not a central shift, just return the weight as they are
        if cat not in ['cat1', 'cat2']: return weight * self.getScalingFactor( startTime, endTime, prop )   # central shift, but cat2 or DOC1, just return the weight as they are

        scalingFactor = self.getScalingFactor( startTime, endTime )  # get factor according to proposal

        if self.deBugPrint: self.log( f'===>>> final scaling factor {scalingFactor} for shift in {self.year}: {shift}', shift[0])

        return scalingFactor

    def recalculateShifts(self):

        # for k, entry in self.multiInsts.items():
        #     ts0 = '2022-06-01T22:00:00.000Z'
        #     # logging.info( f'going to check {entry} for {ts0}')
        #     inst = findInstAtTimestamp( entry, ts0)
        #     # if not inst:
        #     #     logging.info( f'-- Nothing found in {entry} for {ts0}')

        instExp = {}
        for inst, item in self.instAuthMap.items():
            instExp[inst] = self.cspAuth * item[2]

        logging.info( f'recalculateShifts> cspAuth = {self.cspAuth}' )

        instShifts = {}
        instShiftsCentral = {}
        instShiftsCentralDone = {}
        illegalShifterIds = set()
        cat1Shifts = 0
        remapWeigths2024Prop1 = { 1.5: 1.0, 3.0: 2.0, 4.0: 3.0 }
        for shift in self.allShifts:
            shiftKey = f'{shift[5]}-{shift[6]}-{shift[7]}'  # key: subSys-type-flavour
            isCentral, cat = checkShiftCategory( shiftKey, self.proposal )
            shifterId = shift[0]
            shiftStart = shift[3]
            
            if shifterId == 0:
                illegalShifterIds.add( shiftKey )
                continue

            if cat == 'cat1':
                cat1Shifts += 1

            # if cat == 'cat1':
            #     logging.info( f'shiftStart {type(shiftStart)} = {shiftStart} - {datetime.now()} -- < {shiftStart < datetime.now()}' )

            shift_weight = self.getShiftWeight(shift, isCentral, cat)

            #-ap: quick-and-dirty hack to "re-scale" the weights for proposal-2 from the shift DB to the correct weights for proposal-1 -- if needed:
            if self.year == 2024 and self.proposal.lower() == 'proposal-1' and cat == 'cat1' :
                # msg = f'==>>> remapped weight for prop-1 shift {shiftKey} from {shift_weight} '
                shift_weight = remapWeigths2024Prop1[shift_weight]
                # msg += f' to {shift_weight} ... '
                # logging.warning( msg )

            if shiftKey in self.stMap:
                stm_weight = self.stMap[shiftKey][4]
                stm_avc = self.stMap[shiftKey][5]
            else:
                stm_weight = 1.
                stm_avc = 0.
            sub_system = shift[5]
            inst, tl = list(self.userTLs[shifterId].items())[0]
            if inst is None:
                logging.warning( f'recalculateShifts> No inst found for shift {shift} -- skipping ... ' )
                continue

            if shifterId in self.multiInsts.keys():
                inst = findInstAtTimestamp( self.multiInsts[shifterId] , shiftStart )
                # if someone has taken a shift before being associated with the institute, try with the actual time:
                if inst is None:
                    inst = findInstAtTimestamp( self.multiInsts[shifterId] , datetime.now() )
                # if int(shifterId) in [693006]:
                #     logging.info( f"++> finding {inst} for {shiftStart} in {self.multiInsts[shifterId]}" )
            weight = self.cspToEPR * (shift_weight*stm_weight)*(1. + (stm_avc/7.) )
            if inst not in instShifts:
                instShifts[inst] = 0.
            instShifts[inst] += weight
            if isCentral:
                if inst not in instShiftsCentral:
                    instShiftsCentral[inst] = 0.
                instShiftsCentral[inst] += weight
                if shiftStart < datetime.now(): # count done shifts separately 
                    if inst not in instShiftsCentralDone:
                        instShiftsCentralDone[inst] = 0.
                    instShiftsCentralDone[inst] += weight
                # else: 
                #     logging.info( f'===>>> found pledged shift for {inst}: {shift} ')

            # if int(shifterId) in [693006]:
            #     logging.info( f'shift: {shift} -- multi: {shifterId in self.multiInsts.keys()}', end='')
            #     if shifterId in self.multiInsts.keys(): logging.info( f'--> {self.userTLs[shifterId]}', end='' )
            #     logging.info( f' -- for {shifterId} - found inst {inst} -- adding {weight}, sum={instShifts[inst]}' )
        
        if len(illegalShifterIds) > 0:
            logging.warning( f'recalculateShifts> found {len(illegalShifterIds)} illegal shifterIds with value 0 ... ' )
            with open( 'fooFile-shifterIDs.txt', 'w') as ff:
                ff.write( '\n'.join( [ f'{k}' for k in illegalShifterIds ] ) )

        logging.warning( f'====>>>>  found {cat1Shifts} cat-1 shifts (for known users: HRid>0) for {self.year}')

        return instShifts, instShiftsCentral, instShiftsCentralDone, instExp
        
    def calcInstShiftOverview(self):

        instShifts, instShiftsCentral, instShiftsCentralDone, instExp = self.recalculateShifts()
        
        countryInfo = {}
        regionInfo = {}
        yearFrac = (date.today() - date(date.today().year, 1, 1 )).days/365.
        if None in instShifts.keys():
            logging.warning( f'calcInstShiftOverview>  shift key None found: {instShifts[None]}' )
        for inst in sorted( [ x for x in instShifts.keys() if x is not None ] ):
            if inst == 'CERN-based': continue
            if inst not in instExp: continue
            shifts = instShifts[inst]
            shiftsCentral = instShiftsCentral[inst] if inst in instShiftsCentral else 0.
            shiftsCentralDone = instShiftsCentralDone[inst] if inst in instShiftsCentralDone else 0.
            ratio = shiftsCentral/shifts if shifts > 0 else 0
            expected = instExp[inst]
            cCspOvrExp = ( shiftsCentral / expected ) if expected > 0 else 0.
            cCspDoneOvrExpByNow = ( shiftsCentralDone / (expected*yearFrac) ) if expected > 0 else 0.
            allOvrExp = ( shifts / expected ) if expected > 0 else 0.
            country = self.icrMap[inst]['country']
            region  = getRegion(country)
            status  = self.icrMap[inst]['cms_status']
            nAuth   = self.instAuthMap[inst][2]
            self.shiftsByInst.append( ['', inst, country, nAuth, expected, shiftsCentral, shiftsCentralDone, cCspOvrExp, cCspDoneOvrExpByNow, shifts, status ] )

            if inst in ['CERN', 'FERMILAB', "BARI"]:
                logging.info( f'--==++>> {inst} : {shiftsCentralDone}/({expected}*{yearFrac}) = {cCspDoneOvrExpByNow}') 

            try:
                if country not in countryInfo: # nAuth, expected, shiftsCentral, shiftsCentralDone, cspAllOvrExp, cspCentralOvrExp, cCspDoneOvrExpByNow, shiftsAll, region
                    countryInfo[country] = [ 0., 0., 0., 0., 0., 0., 0., region ]
                countryInfo[country][0] += nAuth
                countryInfo[country][1] += expected
                countryInfo[country][2] += shiftsCentral
                countryInfo[country][3] += shiftsCentralDone
                countryInfo[country][-2] += shifts
            except KeyError:
                continue

            try:
                if region not in regionInfo: # nAuth, expected, shiftsCentral, shiftsCentralDone, cspAllOvrExp, cspCentralOvrExp, cCspDoneOvrExpByNow, shiftsAll
                    regionInfo[region] = [ 0., 0., 0., 0., 0., 0., 0.]
                regionInfo[region][0] += nAuth
                regionInfo[region][1] += expected
                regionInfo[region][2] += shiftsCentral
                regionInfo[region][3] += shiftsCentralDone
                regionInfo[region][-1] += shifts
            except KeyError as e:
                raise(e)

        # logging.info( '\nBy Country\n')
        for c in sorted( countryInfo.keys() ):
            entry = countryInfo[c]
            expected = entry[1]
            shiftsCentral = entry[2]
            shiftsCentralDone = entry[3]
            entry[4] = shiftsCentral/expected if expected > 0. else 0.
            entry[5] = ( shiftsCentralDone / (expected*yearFrac) ) if expected > 0. else 0.

            self.shiftsByCountry.append( ['', c, entry[-1] ] + entry[:-1] )
        
        # logging.info( '\nBy Region\n')
        for r in sorted( regionInfo.keys() ):
            entry = regionInfo[r]
            expected = entry[1]
            shiftsCentral = entry[2]
            shiftsCentralDone = entry[3]
            entry[4] = shiftsCentral/expected if expected > 0. else 0.
            entry[5] = ( shiftsCentralDone / (expected*yearFrac) ) if expected > 0. else 0.
            
            self.shiftsByRegion.append( ['', r, ] + entry[:] )

    def printInstShiftOverview(self):

        logging.info( f'\nBy Institute ({len(self.shiftsByInst)} entries)\n')
        logging.info( f'"instCode", "all shifts", "central shifts", "ratio central/all", "expected", "centralShifts/expected"')
        for shift in self.shiftsByInst:
            logging.info( f'"{shift[0]}", {shift[1]:5.2f},  {shift[2]:5.2f}, {shift[3]:3.3f}, {shift[4]:5.2f}, {shift[5]:3.3f}')
        
        logging.info( f'\nBy Country ({len(self.shiftsByCountry)} entries)\n')
        logging.info( f'"Country", "Region", "all shifts", "central shifts", "ratio central/all", "expected", "centralShifts/expected"')
        for shift in self.shiftsByCountry:
            logging.info( f'"{shift[0]}", "{shift[1]}", {shift[2]:5.2f},  {shift[3]:5.2f}, {shift[4]:3.3f}, {shift[5]:5.2f}, {shift[6]:3.3f}')
        
        logging.info( f'\nBy Region ({len(self.shiftsByRegion)} entries)\n')
        logging.info( f'"Region", "all shifts", "central shifts", "ratio central/all", "expected", "centralShifts/expected"')
        for shift in self.shiftsByRegion:
            logging.info( f'"{shift[0]}", {shift[1]:5.2f},  {shift[2]:5.2f}, {shift[3]:3.3f}, {shift[4]:5.2f}, {shift[5]:3.3f}')


    def getShiftOverview(self, session):
        self.getDataFromDB(session)
        self.calcInstShiftOverview()
        
        return self.shiftsByInst, self.shiftsByCountry, self.shiftsByRegion


def getRegion(countryName):

    with open( 'regionMap-2024-03-04.json', 'r') as jF:
        rm = json.load( jF )
        crMap = { x['country_name'].upper(): [ x["country_code"], x["region_name"] ] for x in rm }

    if countryName not in crMap.keys(): return "UNKNOWN"

    return crMap[countryName][1]

def checkShiftCategory( shiftOrkey, targetProposal ):
    
    key = shiftOrkey
    if type(shiftOrkey) != type( '' ):
        key = f'{shiftOrkey[5]}-{shiftOrkey[6]}-{shiftOrkey[7]}'  # key: subSys-type-flavour
    # cat1 = {'Central-DCS-main': ['Central', 'DCS', 'main'], 'Central-DCS-main 2nd': ['Central', 'DCS', 'main 2nd'], 'Central-DCS-trainee': ['Central', 'DCS', 'trainee'], 'Central-DCS expert-main': ['Central', 'DCS expert', 'main'], 'Central-Shift_leader-main': ['Central', 'Shift_leader', 'main'], 'Central-Shift_leader-main 2nd': ['Central', 'Shift_leader', 'main 2nd'], 'Central-Shift_leader-trainee': ['Central', 'Shift_leader', 'trainee'], 'DAQ-Shifter-main': ['DAQ', 'Shifter', 'main'], 'DAQ-Shifter-main 2nd': ['DAQ', 'Shifter', 'main 2nd'], 'DAQ-Shifter-trainee': ['DAQ', 'Shifter', 'trainee']}
    # cat2 = {'DQM-P5-main': ['DQM', 'P5', 'main'], 'DQM-P5-main 2nd': ['DQM', 'P5', 'main 2nd'], 'TRG-Shifter-main': ['TRG', 'Shifter', 'main'], 'TRG-Shifter-main 2nd': ['TRG', 'Shifter', 'main 2nd'], 'TRG-Shifter-trainee': ['TRG', 'Shifter', 'trainee']}
    # doc1 = {'Central-Run field manager-main': ['Central', 'Run field manager', 'main'], 'Central-Run field manager-main 2nd': ['Central', 'Run field manager', 'main 2nd'], 'TRG-L1_DOC-main': ['TRG', 'L1_DOC', 'main'], 'TRG-L1_DOC-trainee': ['TRG', 'L1_DOC', 'trainee'], 'TRG-HLT_DOC-main': ['TRG', 'HLT_DOC', 'main'], 'DAQ-DOC-main': ['DAQ', 'DOC', 'main'], 'DQM-System operation-main': ['DQM', 'System operation', 'main'], 'DQM-System operation-main 2nd': ['DQM', 'System operation', 'main 2nd'], 'Pixel-DOC-main': ['Pixel', 'DOC', 'main'], 'Pixel-DOC-main 2nd': ['Pixel', 'DOC', 'main 2nd'], 'Pixel-DOC-trainee': ['Pixel', 'DOC', 'trainee'], 'TRK-DOC-main': ['TRK', 'DOC', 'main'], 'TRK-DOC-main 2nd': ['TRK', 'DOC', 'main 2nd'], 'TRK-DOC-trainee': ['TRK', 'DOC', 'trainee'], 'ECAL-DOC-main': ['ECAL', 'DOC', 'main'], 'HCAL-DOC-main': ['HCAL', 'DOC', 'main'], 'DT-DOC-main': ['DT', 'DOC', 'main'], 'RPC-DOC-main': ['RPC', 'DOC', 'main'], 'CSC-DOC-main': ['CSC', 'DOC', 'main'], 'GEM-DOC-main': ['GEM', 'DOC', 'main'], 'BRIL-DOC-main': ['BRIL', 'DOC', 'main'], 'CT-PPS-DOC-main': ['CT-PPS', 'DOC', 'main']}

    cat1 = { 'Central-DCS-main': ['Central', 'DCS', 'main'], 
             'Central-Shift_leader-main': ['Central', 'Shift_leader', 'main'], 
             'DAQ-Shifter-main': ['DAQ', 'Shifter', 'main']
            }
    cat2 = { 'DQM-Shifter-P5-main': ['DQM', 'Shifter-P5', 'main'], 
             'DQM-Shifter-ROC-China-main': ['DQM', 'Shifter-ROC-China', 'main'], 
             'DQM-Shifter-ROC-DESY-main': ['DQM', 'Shifter-ROC-DESY', 'main'], 
             'DQM-Shifter-ROC-FNAL-main': ['DQM', 'Shifter-ROC-FNAL', 'main'], 
             'TRG-Shifter-main': ['TRG', 'Shifter', 'main']
            }
    
    #-toDo: double check with list of "13 shifters" for 2024 and re-factor 
    doc1 = { 'Central-Run field manager-main': ['Central', 'Run field manager', 'main'], 
             'Central-Run field manager-main 2nd': ['Central', 'Run field manager', 'main 2nd'], 
             'TRG-L1_DOC-main': ['TRG', 'L1_DOC', 'main'], 
             'TRG-HLT_DOC-main': ['TRG', 'HLT_DOC', 'main'], 
             'TRG-HLT_DOC-main 2nd': ['TRG', 'HLT_DOC', 'main 2nd'], 
             'TRG-HLT Operations Manager-main': ['TRG', 'HLT Operations Manager', 'main'], 
             'TRG-HLT Operations Manager-main 2nd': ['TRG', 'HLT Operations Manager', 'main 2nd'], 
             'DAQ-DOC-main': ['DAQ', 'DOC', 'main'], 
             'Off-line-ORM-main': ['Off-line', 'ORM', 'main'], 
             'Pixel-DOC-main': ['Pixel', 'DOC', 'main'], 
             'TRK-DOC-main': ['TRK', 'DOC', 'main'], 
             'ECAL-DOC-main': ['ECAL', 'DOC', 'main'], 
             'HCAL-DOC-main': ['HCAL', 'DOC', 'main'], 
             'DT-DOC-main': ['DT', 'DOC', 'main'], 
             'RPC-DOC-main': ['RPC', 'DOC', 'main'], 
             'CSC-DOC-main': ['CSC', 'DOC', 'main'], 
             'GEM-DOC-main': ['GEM', 'DOC', 'main'], 
             'BRIL-DOC-main': ['BRIL', 'DOC', 'main'], 
             'CT-PPS-DOC-main': ['CT-PPS', 'DOC', 'main']
            }

    # if 'Central' in shiftOrkey[5]:
    #     logging.info( f'request to check {shiftOrkey} for {targetProposal} -- key={key} in cat1: {key in cat1.keys()}')
    
    if targetProposal.lower() == 'proposal-1':
        if key in cat1.keys(): return True, 'cat1'
    elif targetProposal.lower() == 'proposal-2':
        cat = None
        if key in cat1.keys():
            cat = 'cat1'
        elif key in cat2.keys():
            cat = 'cat2'
        elif key in doc1.keys():
            cat = 'doc1'
        else:
            cat = None
        if cat is not None: return True, cat
    else:
        logging.info( f'******> ERROR: unknown proposal value: {targetProposal} ')

    return False, None
