
from icms_orm.common import ApplicationAsset

from flask import current_app


def get_application_assets_by_name(name):
    return ApplicationAsset.session().query(ApplicationAsset).filter(
        ApplicationAsset.name.like('%{name}%'.format(name=name))).all()

class EprAssets(object):

    def __init__(self):
        self.__assets = {}
        self.name = 'epr_year_params'
        self.appName = 'epr'

    def __initialiseAssets(self):

        # print( '==> initialising assets' )
        try:
            self.__assets = ApplicationAsset.retrieve(name=self.name)

            defaultYear = self.__assets[ 'pledgeYear' ]
            current_app.logger.debug( 'EprAssets:initialise> Assets initialised to %s for defaultYear %s' % (self.__assets, defaultYear) )
            return True
        except RuntimeError as e:
            if 'application not registered on db instance and no application bound to current context' in str(e):
                current_app.logger.error( 'EprAsset:initialise> could not retrieve asset from DB, got: %s' % str(e) )
                return False

        current_app.logger.error( 'EprAssets:initialise>> should not come here ...' )

        return False

    def getAssets( self, key, defaultValue = None ):

        if not self.__assets:
            self.__initialiseAssets()

        if self.__assets and key == 'all':
            return self.__assets

        if not self.__assets or key not in self.__assets:
            return defaultValue

        return self.__assets[key]

    def updateAssets( self, key, value ):

        if not self.__assets:
            self.__initialiseAssets()

        self.__assets[key] = value

        try:
            ApplicationAsset.store(self.name, self.__assets, application=self.appName)
        except Exception as e:
            current_app.logger.error( 'EprAssets:updateAssets> could not store Assets after update (%s: %s) - got: %s' % (key, value, str(e)) )
            return False

        self.__initialiseAssets()

        current_app.logger.info( 'EprAssets:updateAssets>> successfully updated Assets (%s: %s) ...' % (key, value) )

        return True
