Dear all,

   a new pledge for user {{ plUserName }} ({{ instCode }}) was created (by {{ curUserName }}):
{% if instCode != plInst.code %}
    For institute {{ plInst.code }} ("Guest pledge") {% endif %}

    Project  :  {{ pledge.projName }}
    Activity :  {{ pledge.actName }}
    Task     :  {{ pledge.taskName }} (type: {{ pledge.task.tType }})

    Pledged work: {{ pledge.workPledged }} month(s) for {{ pledge.year }}

    The pledge can be edited at: https://{{ webHost }}/epr/pledge/{{ pledge.id }}

Sincerely,
The iCMS-EPR bot.

PS: for reference, the code of the new pledge is: '{{ pledge.code }}'
https://icms.cern.ch/epr/showPledge/{{ pledge.code }}
