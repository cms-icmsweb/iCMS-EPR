import json

from flask import jsonify, request, current_app
from flask_login import login_required, current_user

from . import api
from .errors import forbidden
from .. import db

from ..main.EprAssets import EprAssets

from ..main.Helpers import getPledgeYear

from ..main.ShiftOverview import ShiftOverview

@api.route('/getAsset/<string:name>', methods=['GET'])
@login_required
def getAsset(name='all'):

    year = int( request.form.get('year', getPledgeYear()) )

    assets = EprAssets().getAssets(name, year)

    return jsonify( assets )


@api.route('setAsset', methods=['POST'])
@api.route('setAsset/<string:key>/<string:val>', methods=['GET'])
@login_required
def setAsset(key=None, val=None):

    if not current_user.is_wizard(): 
        return forbidden( "only admins can set assets")

    year = int( request.form.get('year', getPledgeYear()) )

    current_app.logger.debug( "got request to set asset: form='%s'" % str(request.form) )

    if request.method == 'POST':
        key = request.form.get( 'key', None )
        val = request.form.get( 'value', None )

    current_app.logger.debug( f"got request to set asset: key: '{key}' val: '{val}' ")

    assets = EprAssets().getAssets('all', year)
    
    if not key or not val: 
        current_app.logger.error( f'setAsset> key and value must be specified, got {key} - {val}' )
    else:
        try:
            val = int(val)
        except ValueError:
            pass
        except Exception as e:
            raise e
        EprAssets().updateAssets(key, val)

    assetsUpdated = EprAssets().getAssets('all', year)

    return jsonify( assetsUpdated )


@api.route('setArrayAsset', methods=['POST'])
@api.route('setArrayAsset/<string:key0>/<string:key1>/<string:val>', methods=['GET'])
@login_required
def setArrayAsset(key0=None, key1=None, val=None):

    if not current_user.is_wizard(): 
        return forbidden( "only admins can set assets")

    year = int( request.form.get('year', getPledgeYear()) )

    current_app.logger.debug( "setArrayAsset> got request to set array asset: form='%s'" % str(request.form) )

    if request.method == 'POST':
        key0 = request.form.get( 'key0', None )
        key1 = request.form.get( 'key1', None )
        val = request.form.get( 'value', None )

    current_app.logger.debug( f"setArrayAsset> got request to set array asset: keys: '{key0}' - '{key1}' val: '{val}' ")

    if not (key0 and key1 and val): 
        current_app.logger.error( f"setArrayAsset> two keys and value must be specified, got '{key0}' - '{key1}' - {val}'" )
    else:
        try:
            val = int(val)
        except ValueError:
            try:
                val = float(val)
            except ValueError:
                pass
        except Exception as e:
            raise e
        
        prevAssets = EprAssets().getAssets(key0, year)
        prevAssets[key1] = val
        EprAssets().updateAssets(key0, prevAssets)

    assetsUpdated = EprAssets().getAssets('all', year)

    return jsonify( assetsUpdated )

