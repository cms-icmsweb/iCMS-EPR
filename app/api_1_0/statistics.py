import time
import datetime
import json

from flask import jsonify, request, current_app, url_for
from flask_login import login_required, current_user

from . import api
from .. import db, cache

from sqlalchemy import and_, orm, case, func

from ..models import EprInstitute, AllInstView, Pledge, Task, TimeLineUser, TimeLineInst, EprUser, Project, CMSActivity, commitUpdated
from ..main.Helpers import getProjUserInstPledgeSummary, getProjRegUserCounts, getProjAuthWork, getProjTaskInfo

from ..main.Helpers import getPledgeYear, getInstUsers

from sqlalchemy_extensions import group_concat, QueryFactory

@api.route('/instSummary', methods=['GET', 'POST'])
@login_required
def instSummary():

    maxYear = datetime.date.today().year
    # only show the actual year if we're in Dec:
    if datetime.date.today().month > 11:
        maxYear += 1
    yearList = range(2016, maxYear)
    # return the fraction of the overall EPR accounted work (done+shifts) as "doneFract" (eprAccFract) instead of only "work done" (doneFract)
    res = (db.session.query(AllInstView.year, AllInstView.code, EprInstitute.country, 
                            AllInstView.actAuthors, AllInstView.eprAccFract, EprInstitute.cmsStatus)
                     .join(EprInstitute, EprInstitute.code == AllInstView.code)
                     .filter( AllInstView.year.in_( yearList ) )
                     .group_by( AllInstView.code, EprInstitute.country, EprInstitute.cmsStatus, AllInstView.year, AllInstView.eprAccFract, AllInstView.actAuthors )
                     .order_by( AllInstView.code )
                     .all())


    data = {}
    for year, code, country, nAuth, eprDoneFrac, status in res:
        # if year not in years: years.append(year)
        # if code not in codes: codes.append(code)
        if code not in data:
            data[ code ] = { 'country': country, 
                             'status': status if status != 'Yes' else 'Full', 
                             'nAuth': { year: nAuth }, 'eprDoneFrac' : { year: eprDoneFrac } }
        else:
            data[ code ]['nAuth'].update( { year: nAuth } )
            data[ code ]['eprDoneFrac'].update( { year: eprDoneFrac } )

    summ = []
    for code, v in data.items():
        row = [ '', code, v['country'], v['status'] ]
        for yr in yearList:
            row += [ '%.2f' % v['nAuth'][yr] if yr in v['nAuth'] else 0., 
                     '%.2f' % v['eprDoneFrac'][yr] if yr in v['eprDoneFrac'] else 0. ]
        summ.append( row )

    return jsonify( { 'data' : summ, 'yearList': list(yearList), 'raw': data }  )


@api.route('/pledgeInfo/<int:year>', methods=['GET', 'POST'])
@login_required
def pledgeInfo(year=None):
    
    actYear = year if year else getPledgeYear()

    allPl = (db.session.query(Pledge.workTimePld, Pledge.workTimeAcc, Pledge.workTimeDone, 
                              Pledge.status,
                              Pledge.timestamp,
                              Task.name, CMSActivity.name, Project.name)
                    .join( Task, Task.id == Pledge.taskId )
                    .join( CMSActivity, CMSActivity.id == Task.activityId )
                    .join( Project, Project.id == CMSActivity.projectId )
                    .filter(Pledge.year == actYear)
                    .all())
    
    return jsonify( { 'data' : allPl } )


@api.route('/projSummary', methods=['GET', 'POST'])
@login_required
def projSummary():
    
    projId = -1
    try:
        projId = int(request.form.get('proj', -1))
    except ValueError as e:
        if "invalid literal for int() with base 10: ''" in str(e):
            pass
        else:
            raise e

    yearStr = request.form.get( 'year', None )

    proj = projId
    year = int( yearStr ) if yearStr is not None else getPledgeYear()

    info = []
    if int(proj) > 0: # specific project requested ... 
        prId, prName = (db.session.query( Project.id, Project.name )
                      .filter( Project.id == int(proj) )
                      .all())        
        info.append( ['', prName] + getProjSummStatInfo( proj, year ) )
    else:     # all projects:
        allProjIds = (db.session.query( Project.id, Project.name )
                      .filter( Project.year == year )
                      .filter( Project.status.ilike( 'active' ) )
                      .all())
        current_app.logger.info( f"\nprojSummary> found {len(allProjIds)} projIDs ... ")

        for prId, prName in allProjIds:
            row = ['', prName]
            current_app.logger.info( f"projSummary> going to fetch info for projID {prId} ... ")
            pssi = getProjSummStatInfo( int(prId), year )
            row += pssi
            info.append( row )

    current_app.logger.info( f"\nprojSummary> found {len(info)} entries for project(s)... ")
    current_app.logger.info( f"projSummary> {info} ")
    current_app.logger.info( "\n")

    return jsonify( { 'data': info } )

def getProjApplicantCount( proj, year):
    
    subQuery = (db.session.query( TimeLineUser.cmsId.distinct() )
            .join( EprUser, EprUser.cmsId == TimeLineUser.cmsId )
            .join( Pledge, Pledge.userId == EprUser.id )
            .join( Task, Task.id == Pledge.taskId )
            .join( CMSActivity, CMSActivity.id == Task.activityId )
            .join( Project,  Project.id == CMSActivity.projectId )
            .filter( Pledge.year == year )
            .filter( TimeLineUser.year == year )
            .filter( Pledge.status != 'rejected' )
            .filter( Project.status == 'ACTIVE' )
            .filter( Task.status == 'ACTIVE' )                    
            .filter( Project.id == proj )
            .filter( TimeLineUser.dueApplicant > 0.01 )
            )

    res = subQuery.count()
    current_app.logger.info( f"getProjApplicantCount> applicant count for {proj} in {year}: {res} " )

    return res     


def getProjPldRejInfo( proj, year):
    
    subQuery = (db.session.query( func.sum( Pledge.workTimePld ) )
            .join( Task, Task.id == Pledge.taskId )
            .join( CMSActivity, CMSActivity.id == Task.activityId )
            .join( Project,  Project.id == CMSActivity.projectId )
            .filter( Pledge.year == year )
            .filter( Pledge.status == 'rejected' )
            .filter( Project.status == 'ACTIVE' )
            .filter( Task.status == 'ACTIVE' )                    
            .filter( Project.id == proj )
            )

    result, = subQuery.all()[0]

    current_app.logger.info( f"getProjPldRejInfo> pledged work rejected for {proj} in {year}: {result} " )

    return result

def getProjSummStatInfo( proj, year ):

    current_app.logger.info( "getProjSummStatInfo> getting user/inst/country pledge info for %s in %s " % (proj, year) )
    uaicData = getProjUserInstPledgeSummary( proj, year )
    current_app.logger.info( "getProjSummStatInfo> uaicData: %s " % str(uaicData) )

    current_app.logger.info( "getProjSummStatInfo> getting registered user/inst/country projectCounts info for %s in %s " % (proj, year) )
    prc = getProjRegUserCounts( proj, year )
    current_app.logger.info( "getProjSummStatInfo> prc: %s " % str(prc) )

    current_app.logger.info( "getProjSummStatInfo> getting the pledged/accepted/done work of all/authors for a given project/year for %s in %s " % (proj, year) )
    paw = getProjAuthWork( proj, year )
    current_app.logger.info( "getProjSummStatInfo> paw: %s " % str(paw) )

    resTI = getProjTaskInfo( proj, year )

    nTasks   = resTI['other'][0][2] if resTI['other'] else 0.
    nIRTasks = resTI['instResp'][0][2]  if resTI['instResp'] else 0.
    nQual    = resTI['quali'][0][2] if resTI['quali'] else 0.
    workNeeded = resTI['other'][0][1]  if resTI['other'] else 0.
    workNeeded += resTI['instResp'][0][1]   if resTI['instResp'] else 0.
    workNeeded += resTI['quali'][0][1] if resTI['quali'] else 0.

    nApplicants = getProjApplicantCount(proj, year)

    pldRejInfo = getProjPldRejInfo( proj, year )
    pldRej = pldRejInfo if pldRejInfo else 0.
    
    pName = prc[ str(proj) ]
    workAll = [ float(x) for x in paw[ str(proj) ]['all'].split(',') ]
    outData = [ nIRTasks, nTasks, 
               prc[pName]['nUsers'] if pName in prc else -1.,
               prc[pName]['nAuthors'] if pName in prc else -1.,
               nApplicants,
               prc[pName]['nInsts'] if pName in prc else -1.,
               # prc[pName]['nInstAuth'] if pName in prc else -1.,
               prc[pName]['nCountries'] if pName in prc else -1.,
               # prc[pName]['nCountriesAuth'] if pName in prc else -1.,
               uaicData[ str(proj) ]['nUsers'], 
               uaicData[ str(proj) ]['nAuthors'],
               uaicData[ str(proj) ]['nInsts'], 
               uaicData[ str(proj) ]['nCountries'], 
               workNeeded,
               workAll[2], # work done
               workAll[2]/workNeeded if workNeeded > 0 else 0., # fraction done
               workAll[0], # work pledged
               workAll[0]/workNeeded if workNeeded > 0 else 0., # fraction pledged
               workAll[1], # work accepted
               workAll[1]/workNeeded if workNeeded > 0 else 0., # fraction accepted
               pldRej, 
               pldRej/workNeeded if workNeeded > 0 else 0., # fraction rejected
    ]
    
    return outData
