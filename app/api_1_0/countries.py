import json

from flask import jsonify, request, current_app, url_for
from flask_login import login_required

from . import api
from .. import db
from ..models import EprInstitute, AllInstView
from ..main.Helpers import countrySummary

from ..main.Helpers import getPledgeYear

from ..main.ShiftOverview import ShiftOverview

@api.route('/countryInfo/<string:name>', methods=['GET', 'POST'])
@login_required
def countryInfo(name):

    year = int( request.form.get('year', getPledgeYear()) )

    countryInstInfo, summary = countrySummary( name, year )

    return jsonify( { 'country' : name,
                      'data' : countryInstInfo,
                      'year' : year,
                      'summary' : summary
                    } )


@api.route('/countryOverview', methods=['GET', 'POST'])
@login_required
def countryOverview():

    year = int( request.form.get('year', getPledgeYear()) )

    countryList = db.session.query( EprInstitute.country).distinct().all()

    keys = ['Expected' ,'Pledged' ,'Accepted' ,'Done' ,'Authors' ,'actAuthors' ,'ShiftsDone' ,'sumEprDue']

    countryData = {}
    for a, i in ( db.session.query(AllInstView, EprInstitute)
                            .filter(AllInstView.code == EprInstitute.code)
                            .filter(AllInstView.year == year)
                            .all() ) :
        c = i.country
        if c not in countryData.keys():
            countryData[c] = { 'nInst' : 1 }
            instInfo = a.to_json()
            for k in instInfo.keys():
                countryData[c][k] = instInfo[k]
        else:
            countryData[c]['nInst'] += 1
            instInfo = a.to_json()
            for k in keys:
                countryData[c][k] += instInfo[k]
        countryData[c]['name'] = c
        countryData[c]['code'] = ''

    countryInfo = []
    for item in countryList:
        c = item[0]
        if c in countryData.keys():

            countryData[ c ][ 'Expected' ] += countryData[c]['sumEprDue']
            countryData[c]['PledgedFract'] = 0
            if float( countryData[c]['Expected'] ) > 0:
                countryData[c]['PledgedFract'] = float(countryData[c]['Pledged'])/float(countryData[c]['Expected'])

            countryData[c]['DoneFract'] = 0
            if float( countryData[c]['Expected'] ) > 0:
                countryData[c]['DoneFract'] = float(countryData[c]['Done'])/float(countryData[c]['Expected'])

            countryInfo.append( countryData[c] )

    return jsonify( { 'country' : 'all',
                      'data' : countryInfo,
                      'year' : year
                    } )


@api.route('/countryShiftOverview', methods=['GET', 'POST'])
@login_required
def countryShiftOverview():
    year = int( request.form.get('year', getPledgeYear()) )
    proposal = request.form.get('proposal', 'Proposal-1')

    current_app.logger.info( f"countryShiftOverview> got request for country shift overview for year {year} - {proposal}" )

    so = ShiftOverview(year, proposal)
    instShiftInfo, countryShiftInfo, regionShiftInfo = so.getShiftOverview(db.session)
    # print( f'===> got: ')
    # print( f'Inst : {instShiftInfo}')
    # print( f'Cntry: {countryShiftInfo}')
    # print( f'Reg  : {regionShiftInfo}')
    
    return jsonify( { 'data' : countryShiftInfo } )


@api.route('/regionShiftOverview', methods=['GET', 'POST'])
@login_required
def regionShiftOverview():
    year = int( request.form.get('year', getPledgeYear()) )
    proposal = request.form.get('proposal', 'Proposal-1')
    
    current_app.logger.info( f"regionShiftOverview> got request for country shift overview for year {year} - {proposal}" )

    so = ShiftOverview(year, proposal)
    instShiftInfo, countryShiftInfo, regionShiftInfo = so.getShiftOverview(db.session)
    # print( f'===> got: ')
    # print( f'Inst : {instShiftInfo}')
    # print( f'Cntry: {countryShiftInfo}')
    # print( f'Reg  : {regionShiftInfo}')
    
    return jsonify( { 'data' : regionShiftInfo } )


