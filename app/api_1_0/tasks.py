from flask import jsonify, request, current_app, url_for, flash, abort
from flask_login import login_required, current_user

import json
import datetime

from sqlalchemy import exc, func, desc, and_, case, orm, or_

from . import api
from .. import db
from ..models import Permission, Task, Manager, EprUser, EprInstitute, Pledge, commitUpdated, CMSActivity, Project, Level3Name
from ..main.MailHelpers import getMgrEmails

from ..main.Helpers import getPledgeYear
from ..main.HistoryHelpers import getTaskHistory

@api.route('/deleteTask', methods=['POST'])
@login_required
def deleteTask():

    code    = request.form.get('taskCode', None)
    target  = request.form.get('target', None)
    selYear = request.form.get('year', getPledgeYear())

    current_app.logger.info("request to delete task code %s (year %s, user %s)" % (code, selYear, current_user.cmsId) )

    if not (current_user.is_administrator() or \
            current_user.canManage('task', code, year=selYear) ):
        msg = 'You (%s) are not a manager for this task, sorry.' % current_user.username
        current_app.logger.error(msg)
        flash(msg, 'error')
        return jsonify( { 'status' : msg } ), 403

    task = db.session.query(Task).filter_by(code=code, year=selYear).one()
    current_app.logger.info("found task to delete as code %s : %s " % (task.code, task) )

    if task.neededWork != 0:
        msg = 'ERROR can not delete task %s, work needed is %s and not zero' % (task.name, task.neededWork)
        current_app.logger.warning( msg )
        return jsonify( { 'status' : msg } ), 400

    activePledges = (db.session.query(Pledge).filter_by( taskId=task.id )
                     .filter( Pledge.status != 'rejected' )
                     .filter( Pledge.year == selYear )
                     .all( ))

    if ( len(activePledges) != 0):
        msg = 'ERROR can not delete task %s, found %s non-rejected pledges ' % (task.name, len(activePledges))
        current_app.logger.warning( msg )
        return jsonify( { 'status' : msg } ), 400

    # here we have a valid delete request, update status of task:
    task.status = 'DELETED'
    commitUpdated( task )

    return jsonify( {'status': 'Task successfully deleted'}), 200


@api.route('/tasks')
@login_required
def getAllTasks():

    data = []
    for t in db.session.query(Task).all():
        data.append( t.to_json() )

    current_app.logger.debug( '\n data is %s ' % str(data) )

    return jsonify( { 'data' : data } )

@api.route('/task/<int:id>')
@login_required
def get_task(id):
    task = db.session.query(Task).filter(Task.id==id).one()
    if not task:
        return abort(404)
    return jsonify(task.to_json())

@api.route('/taskByName/<string:name>')
@login_required
def get_taskByName(name):

    year = request.form.get('year', getPledgeYear())

    try:
        task = db.session.query(Task).filter_by(name=name.replace('_','/'), year=year).one()
        return jsonify(task.to_json())
    except Exception as e :
        current_app.logger.error( 'no task found for %s - got %s' % ( name.replace('_','/'), str(e) ) )
        return jsonify( { "error": 'no task found for %s ' % name.replace('_','/') } )

@api.route('/taskByCode', methods=['POST'])
@login_required
def taskByCode():

    code = request.form.get( 'code', None )

    if code is None:
        current_app.logger.error( 'no valid code given to search for taskByCode ' )
        return jsonify( { "error" : 'no code given ... ' } )

    code = code.replace('_', '/')

    try :
        task = db.session.query(Task).filter_by( code=code ).one( )
    except Exception as e :
        current_app.logger.warning( 'no task found for %s - got %s' % (code, str(e)) )
        return jsonify( { "error" : 'no task found for %s ' % code } )

    mgrs = set( )
    try :
        for m, uName in (db.session.query( Manager, EprUser.name )
                                 .filter( Manager.itemType == 'task' )
                                 .filter( Manager.itemCode == task.code )
                                 .filter( Manager.userId == EprUser.id )
                                 .filter( Manager.role == Permission.MANAGETASK )
                                 .filter( Manager.status == 'active' )
                                 .all( )) :
            if m.egroup :
                mgrs.add( '%s.cern.ch'  %m.egroup )
            else:
                print ( '\n===> non-eg mgr found: uName, m: ', uName, m)
                mgrs.add( uName )
    except Exception as e :
        current_app.logger.error( 'Error when trying to get managers for task %s - got %s ' % (code, str( e )) )
        return jsonify( { "error" : 'error getting managers for task %s - got %s ' % (code, str( e )) } )

    return jsonify( { 'task' : task.to_json( ),
                      'mgrs' : '<br/>'.join( mgrs ),
                      'numMgrs' : len( mgrs ),
                      'mgrEmails' : ','.join( getMgrEmails( 'task', task.code ) )
                      } )

@api.route('/taskInfoForTaskCodes', methods=['POST'])
@login_required
def taskInfoForTaskCodes():

    taskCodesStr = request.form.get( 'taskCodes', None )
    year = int(request.form.get('year', getPledgeYear()))

    if not taskCodesStr:
        msg =  'taskInfoForTaskCodes> no taskCodes found in request '
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    taskCodes = taskCodesStr.split(',')
    tasks = ( db.session.query( Task ).filter(Task.code.in_(taskCodes)).all() )

    tInfo = [ x.to_json() for x in tasks ]

    return jsonify( { 'taskInfo' : tInfo } )

@api.route('/taskHistory', methods=['POST'])
@login_required
def taskHistory():

    taskCode = request.form.get( 'taskCode', None )
    year = int(request.form.get('year', getPledgeYear()))

    current_app.logger.debug( "taskHistory> got request for history of task: %s" % str(request.form) )

    if not taskCode:
        return jsonify( { 'data' : { 'code' : taskCode, 'history' : [] } } )

    taskCode = taskCode.replace(':', '+')
    taskHistory = getTaskHistory(taskCode, year)

    current_app.logger.debug( "taskHistory> returning task history of length %d" % len(taskHistory) )

    return jsonify( { "data" : taskHistory } )


@api.route('/instRespTasks', methods=['POST'])
@login_required
def instRespTasks():
    current_app.logger.debug( "instRespTasks> got request: %s" % str(request.form) )

    selYear = int(request.form.get('year', getPledgeYear()))
    instCodeReq = request.form.get( 'instCode', None )

    current_app.logger.debug( "instRespTasks> got request for InstResp tasks for year %s" % selYear )

    # subquery summing the pledge-related values over Pledges related to the current year, institute
    # only InstResp tasks
    sq_instRespEprWork = db.session.query(
        Pledge.instId.label('instId'), EprInstitute.code.label('instCode'),
        func.sum(Pledge.workTimePld).label('sumPledged'),
        func.sum(Pledge.workTimeAcc).label('sumAccepted'),
        func.sum(Pledge.workTimeDone).label('sumWorkTimeDone'),
        Task.id.label( 'taskId' ),
    )\
        .join(Task, Pledge.taskId == Task.id)\
        .join(EprInstitute, Pledge.instId == EprInstitute.id)\
        .filter(and_(Task.tType.like('InstResp'), Pledge.year == selYear))\
        .filter(Pledge.status.notlike('%rejected%'))\
        .group_by( Task.id, Pledge.instId, EprInstitute.code ).subquery()

    res = db.session.query(sq_instRespEprWork).all()
    instWorkByTask = {}
    for  instId, instCode, pl, ac, wd, taskId in res:
        if taskId not in instWorkByTask: instWorkByTask[taskId] = []
        instWorkByTask[taskId].append( (instCode, pl, ac, wd) )

    current_app.logger.info('instRespTasks> instWorkByTask: %s ' % str(instWorkByTask) )

    res = (db.session.query(Task.name, Task.description, Task.neededWork,
                            Project.name, CMSActivity.name, Task.id,
                            )
                .join( CMSActivity, Task.activityId == CMSActivity.id)
                .join( Project, CMSActivity.projectId == Project.id)
                .filter( Task.year == selYear )
                .filter( Task.status == 'ACTIVE' )
                .filter( Task.tType == 'InstResp')
                .all())

    # current_app.logger.info('instRespTasks> InstResp tasks found: %s ' % str(res) )

    data = []
    for n, d, wn, p, a, taskId in res:
        wList = instWorkByTask[taskId] if taskId in instWorkByTask else [("-",0,0,0)]
        iNames = ''
        plSum = 0.
        acSum = 0.
        wdSum = 0.
        iWork = {}
        for ic, pl, ac, wd in wList:
            iNames += '%s,' % ic
            plSum += pl
            acSum += ac
            wdSum += wd
            iWork[ic] = (pl, ac, wd)
        data.append( { 'sel': ' ', 'name': n, 'desc': d, 'workNeeded': wn,
                       'proj': p, 'act': a, 'taskId': taskId,
                       'pledged': plSum, 'accepted': acSum, 'wDone': wdSum,
                       'thisPld': 0, 'pldInst': iNames[:-1],
                       'instWork': iWork } )

    # current_app.logger.info( 'instRespTasks> found: %s' % str(data) )

    return jsonify( { 'data': data } )

@api.route('/searchLvl3Names', methods=['GET'])
@login_required
def searchLvl3Names():

    search = request.form.get( 'term', None )
    if search is None:
        search = request.args.get( 'term', None )

    if search is None :
        current_app.logger.error('no search term given/found ... method="%s"' % request.method)
        return jsonify ( [] )

    search_term = '%%%s%%' % search
    current_app.logger.debug('searchLvl3Names> search term is "%s"' % search_term )

    selYear = datetime.date.today().year

    # Merging all the queries as previously 3132 would only return Jen, Hung-Che (aka fakeUsername-3132)
    resDB = ( db.session.query(Level3Name.id, Level3Name.name)
            .filter( Level3Name.name.ilike(search_term) )
            .filter( Level3Name.startYear <= selYear )
            # .filter( 
            #     or_(
            #         Level3Name.endYear >= selYear,
            #         Level3Name.endYear is None
            #         ),
            # )
            .order_by(Level3Name.name)
            .all())

    current_app.logger.info( "searchLvl3Names results for %s: %s " % (search_term, str(resDB), ))

    return json.dumps( [ '%s : %s' % (x[0], x[1]) for x in resDB ] )
