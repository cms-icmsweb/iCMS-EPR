
from datetime import datetime, date

from flask import jsonify, request, current_app, url_for, session
from flask_login import login_required, current_user
from sqlalchemy.orm import exc as sa_exc
from sqlalchemy import distinct, and_, func

from sqlalchemy_extensions import group_concat

from . import api
from ..models import Permission, commitNew, Project, CMSActivity, Manager, Task, Shift, ShiftTaskMap, ShiftGrid
from .. import db
from ..main.Helpers import getShiftInfo, getShiftInfoUser, getShiftSummary, getShiftTaskSummary, getShiftDetailsUser

from ..main.Helpers import getPledgeYear
import json
import os

class ShiftInformation(object):

    def __init__(self):
        self.fileName = 'shiftGrid-2024.json'
        if 'TEST_DATABASE_URL' in os.environ:
            if 'test' in os.environ['TEST_DATABASE_URL'].lower():
                current_app.logger.info("found 'TEST_DATABASE_URL' in environ, using shiftgrids from test/ ... ")
                self.fileName = 'tests/shiftGrid-2020.json'

        self.fileContent = None

    def readFromFile(self, force=False):

        if not self.fileContent or force:
            with open(self.fileName, 'r') as jsonFile:
                self.fileContent = json.load(jsonFile)['SHIFTGRID']

    def getSubSystemList(self):
        if not self.fileContent:
            self.readFromFile()

        subSysList = []
        for item in self.fileContent:
            if [item['SUB_SYSTEM']] not in subSysList:
                subSysList.append( [item['SUB_SYSTEM']] )
        return subSysList

    def getEntities(self):
        if not self.fileContent:
            self.readFromFile()

        entities = []
        for item in self.fileContent:
            if (item['SUB_SYSTEM'], item['SHIFT_TYPE']) not in [(x[0], x[2]) for x in entities]:
                for flav in ['main', 'main 2nd', 'trainee']:
                    entities.append( (item['SUB_SYSTEM'], flav, item['SHIFT_TYPE']) )
        return entities

    def getShiftTypes(self, subSysIn):
        if not self.fileContent:
            self.readFromFile()

        subSys = subSysIn
        if type(subSysIn) == type([]): subSys = subSysIn[0]

        sTypes = []
        for item in self.fileContent:
            if item['SUB_SYSTEM'].lower() == subSys.lower():
                if item['SHIFT_TYPE'] not in sTypes:
                    sTypes.append( item['SHIFT_TYPE'] )
        return sTypes

    def getShiftTypeId(self, subSys, sType, flavName):
        if not self.fileContent:
            self.readFromFile()

        selYear = getPledgeYear()

        current_app.logger.info('getShiftTypeId> shiftTypeID from file: %s' % self.fileContent )

        for item in self.fileContent:
            if ( item['SUB_SYSTEM'] == subSys and
                 item['SHIFT_TYPE'] == sType and
                 selYear > 2019
            ) :
                current_app.logger.debug( 'getShiftTypeId> shiftTypeID found for %s/%s/%s in %s' % (subSys, sType, flavName, selYear) )
                return item['SHIFT_TYPE_ID']

        current_app.logger.error('getShiftTypeId> shiftTypeID NOT found for %s/%s/%s in %s' % (subSys, sType, flavName, selYear))

        return -1

@api.route('/shiftInfo/<string:subSys>', methods=['POST'])
@login_required
def shiftInfo(subSys):

    year = int(request.form.get('year', getPledgeYear()))

    current_app.logger.info( "shiftInfo> request for %s in %i " %  (subSys, year) )

    shifts = None
    institutes = None
    data, institutes = getShiftInfo( subSys, year )

    current_app.logger.info( "shiftInfo> got: %i items for %s in %s" % (len(data), subSys, year) )
    current_app.logger.info( "shiftInfo> data[:5]: %s " % str(data[:5]) )

    return jsonify( { 'shifts' : data, 'institutes' : institutes } )


@api.route('/shiftInfoUser/<string:username>', methods=['POST'])
@login_required
def shiftInfoUser(username):

    year = int(request.form.get('year', getPledgeYear()))

    current_app.logger.info( "shiftInfoUser> request for %s in %i " %  (username, year) )

    data, institutes = getShiftInfoUser( username, year )

    current_app.logger.info( "shiftInfoUser> got: %i items for %s in %s" % (len(data), username, year) )
    current_app.logger.info( "shiftInfoUser> data[:5]: %s " % str(data[:5]) )

    return jsonify( { 'shifts' : data, 'institutes' : institutes } )


@api.route( '/getShiftSubSystems', methods=[ 'GET' ] )
@login_required
def getShiftSubSystems():

    selYear = session.get('year')

    si = ShiftInformation()
    subSysList = si.getSubSystemList()

    current_app.logger.info( 'getShiftSubSystems> %d subsystems found for %s: %s' % (len(subSysList), selYear, str(subSysList)) )

    return jsonify( { 'result': 'OK', 'data' : subSysList } )

def getShiftSubSystemsFromBookedShifts():

    selYear = session.get('year')
    try:
        subSysList = (db.session.query( Shift.subSystem.distinct() )
                        .filter(Shift.year == selYear)
                        .all() )
        current_app.logger.info( 'subsystems found for %s: %s' % (selYear, str(subSysList)) )
    except Exception as e:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when retrieving SubSystems for "%d" - got "%s".' % (selYear, str(e)) } )
    return jsonify( { 'result': 'OK', 'data' : subSysList } )


@api.route( '/sumShiftsForTask/<string:taskCode>', methods=[ 'GET' ] )
@login_required
def sumShiftsForTask(taskCode):

    selYear = session.get('year')

    current_app.logger.info( "api.sumShiftsForTask> got request for sum of shifts for task %s in %d" % (taskCode, selYear) )

    sumInfo = getShiftTaskSummary(taskCode, selYear)

    return jsonify( { 'result': 'OK', 'data' : sumInfo } )

@api.route( '/getShiftTaskMap', methods=[ 'GET' ] )
@login_required
def getShiftTaskMap():

    selYear = session.get('year')

    isMgr = False
    if ( current_user.is_administrator() or current_user.isManager(mgtType='project', year=selYear) ) :
        isMgr = True

    current_app.logger.info( '\napi.getShiftTaskMap++++> user %s isMgr=%s ... \n' % (current_user.username, isMgr) )

    # see if the current user is a PM:
    userProjects = ( db.session.query(Manager.itemCode)
                               .filter(Manager.year == selYear,
                                       Manager.itemType == 'project',
                                       Manager.status == 'active',
                                       Manager.role == Permission.MANAGEPROJ,
                                       Manager.userId == current_user.id)
                               .all() )

    # get all projects to show:
    allProjects = (db.session.query( Manager.itemCode )
                                  .filter( Manager.year == selYear,
                                           Manager.itemType == 'project',
                                           Manager.role == Permission.MANAGEPROJ,
                                           Manager.status == 'active' )
                                  .all( ))

    current_app.logger.info( "api.getShiftTaskMap> found %d projects (all) in %d : %s" % (len(allProjects), selYear, str(allProjects) ) )
    current_app.logger.info( "api.getShiftTaskMap> found %d projects for %s in %d : %s" % (len(userProjects), current_user.username, selYear, str(userProjects) ) )

    # get the corresponding activities:
    allActs = ( db.session.query( CMSActivity.id )
                               .join(Project, Project.id == CMSActivity.projectId)
                               .filter( Project.code.in_( [x[0] for x in allProjects ] ))
                               .all())
    current_app.logger.info( "api.getShiftTaskMap> found %d activities (all) in %d : %s" % (len(allActs), selYear, str(allActs) ) )

    # get all the shift tasks for these projects:
    uptList0 = []
    if allActs:
        uptList0 = ( db.session.query( Task )
                              .filter( Task.activityId.in_( [x[0] for x in allActs] ) )
                              .filter( Task.year == selYear)
                              .filter( Task.status == 'ACTIVE')
                              .filter( Task.shiftTypeId.like('%hift%') )
                              .filter( Task.name != None)
                              .all() )
    current_app.logger.info( "api.getShiftTaskMap> found %d tasks for %s in %d " % (len(uptList0), current_user.username, selYear ) )

    # current_app.logger.debug( "api.getShiftTaskMap> tasks are: %s" % str(uptList0) )
    current_app.logger.debug( "api.getShiftTaskMap> user %s can manage these %s tasks: %s " % (current_user.username,
                                                                          len(uptList0),
                                                                          str( [ x.name.encode('ascii', 'xmlcharrefreplace') for x in uptList0 if x.name is not None] ) ) )

    stmTableTemplate = """
    <div class="brick padded">
    <table id="subTable" align="center">
        <thead>
        <tr>
            <th>SubSys</th>
            <th>flavour</th>
            <th>type</th>
            <th>nUsers</th>
            <th>cspDone</th>
            <th>cspPld</th>
            <th>weightScale</th>
            %s
        </tr>
        </thead>
        <tbody>
        %s
        </tbody>
    </table>
    </div>
    """

    stmRowTemplate = '''
    <tr>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        %s
    </tr>
    '''

    uptList = []
    for task in uptList0:
        pName = task.activity.project.name

        if task is None: continue

        stmList = None
        try:
            stmList = db.session.query(ShiftTaskMap).filter_by(year=selYear, taskCode=task.code, status='ACTIVE').all()
        except sa_exc.NoResultFound :
            pass

        shiftMapInfo = ''
        stmInfoList = []
        stmTableRows = ''
        removeButton = None
        for stm in stmList:
            stmInfo = { 'subSystem'   : stm.subSystem,
                        'flavourName' : stm.flavourName,
                        'shiftType'   : stm.shiftType,
                        'id'          : stm.id,
                        'weightScale' : stm.weight,
                        }

            nUser, cspDone, cspPld = getShiftSummary( selYear, stm )

            removeButton = ''
            if ( (current_user.is_administrator()) or (isMgr and (task.activity.project.code,) in userProjects) ) :
                removeButton = '<td><a href=%s>Remove</a></td>' % url_for( 'manage.removeSTM', idReq=stm.id )

            stmTableRows += stmRowTemplate % (stmInfo['subSystem'], stmInfo['flavourName'], stmInfo['shiftType'],
                                              nUser, cspDone, cspPld, stmInfo['weightScale'], removeButton )

            stmInfoList.append( stmInfo )

        manageButton = ''
        if ((current_user.is_administrator( )) or (isMgr and (task.activity.project.code,) in userProjects)) :
            manageButton = '<button id="add-mapping">Add mapping</button>'

        stmTable = ''
        if stmTableRows:
            if removeButton is not None:
                stmTable = stmTableTemplate % ('<th></th>', ''.join(stmTableRows))
            else:
                stmTable = stmTableTemplate % ('', ''.join( stmTableRows ))

        uptItem = { "pName" : pName,
                    "task" : { 'tType' : task.tType,
                               'shiftTypeId' : task.shiftTypeId,
                               'name' : task.name,
                               'code' : task.code,
                             },
                    'shiftMapInfo' : stmTable,
                    "stm" : stmInfoList,
                    'manageButton' : manageButton,
                    }
        if uptItem not in uptList: uptList.append( uptItem )

    current_app.logger.info( "api.getShiftTaskMap2> prepared %d shift-tasks for %s in %d " % (len(uptList), current_user.username, selYear ) )
    current_app.logger.debug( "api.getShiftTaskMap2> tasks are: %s" % str(uptList) )

    return jsonify( { 'result': 'OK', 'data' : uptList } )

@api.route( '/getShiftTypes', methods=[ 'POST' ] )
@login_required
def getShiftTypes( ) :

    selYear = session.get('year')

    if not ( current_user.is_administrator() or current_user.isManager(mgtType='project', year=selYear) ) :
        return jsonify(
            { 'result' : 'ERROR',
              'msg'    : 'Sorry you are not allowed to manage shifts' } )

    year = int(request.form.get('year', getPledgeYear()))
    subSysReq = json.loads( request.form.get('subSystem', '""') )

    if subSysReq.strip() == '':
        return jsonify(
            { 'result' : 'ERROR',
              'msg'    : 'No subSystem specified' } )

    current_app.logger.debug("getShiftTypes> got request for %d and %s" % (year, subSysReq))

    si = ShiftInformation()
    sTypes = si.getShiftTypes( subSysReq )

    current_app.logger.info('getShiftTypes> found shiftTypes: %s' % str(sTypes) )

    return jsonify( { 'result': 'OK', 'data' : { subSysReq: sTypes } } )

@api.route( '/getShiftEntities', methods=[ 'POST' ] )
@login_required
def getShiftEntities( ) :

    selYear = session.get('year')

    if not ( current_user.is_administrator() or current_user.isManager(mgtType='project', year=selYear) ) :
        return jsonify(
            { 'result' : 'ERROR',
              'msg'    : 'Sorry you are not allowed to manage shifts' } )

    year = int(request.form.get('year', getPledgeYear()))
    subSysReq = request.form.get('subSystems', [])

    current_app.logger.debug("getShiftEntities> got request for %d and %s" % (year, subSysReq))

    si = ShiftInformation()
    entities = si.getEntities()

    shifts = {}
    for subSys, sFlav, sTyp in entities:
        if subSys not in subSysReq: continue
        if subSys in shifts.keys():
            if (sFlav, sTyp ) not in shifts[ subSys ]:
                shifts[ subSys ].append( (sFlav, sTyp ) )
        else:
            shifts[subSys] = [ ( sFlav, sTyp ) ]

    current_app.logger.info('getShiftEntities> found shiftEntities: %s' % str(shifts) )

    return jsonify( { 'result': 'OK', 'data' : shifts } )

# REST API for shiftTaskMap - POST, GET, DELETE :

@api.route('/shiftTaskMap', methods=['POST'])
@login_required
def newShiftTaskMap( ):

    year  = int(request.form.get('year', getPledgeYear()))

    msg = ''
    for k,v in request.form.items():
        msg += ' ... %s : %s \n' % (k, v)
    current_app.logger.debug( "shiftTaskMap> request for \n%s\n ... in %i " %  (msg, year) )

    tCode = request.form.get('taskCode', None)
    if not tCode:
        return jsonify( { 'result': 'ERROR', 'msg' : 'Task for code "%s" not found.' % tCode } )

    try:
        task = db.session.query(Task).filter_by(year=year, status='ACTIVE', code=tCode).one()
    except Exception as e:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when retrieving task for code "%s" - got "%s".' % (tCode, str(e)) } )

    current_app.logger.debug( 'shiftTaskMap> task found for "%s" ' % (tCode,) )

    subSys = request.form.get('subSystem', None)
    if subSys is None:
        return jsonify( { 'result': 'ERROR',
                          'msg' : 'No Shift-SubSystem specified for task code "%s"' % (tCode,) } )

    current_app.logger.debug( 'shiftTaskMap> subSys found "%s" ' % (subSys,) )

    if ( 'shift' not in task.shiftTypeId.lower() and
         'on-call' not in task.shiftTypeId.lower() ):
        return jsonify( { 'result' : 'ERROR', 'msg' : 'Task for code "%s" is not a shift task (type:"%s").' % (tCode, task.shiftTypeId) } )

    current_app.logger.debug( 'shiftTaskMap> task type OK "%s" ' % (task.shiftTypeId.lower(),) )

    flavType = request.form.get('flavType', None)
    if flavType:
        flName, sType = flavType.split('||')
    else:
        sType = request.form.get('shiftType', None)
        if sType is None:
            return jsonify( { 'result': 'ERROR', 'msg' : 'Invalid shift type specified "%s".' % (sType,) } )

        flName = request.form.get('flavourName', None)
        if flName is None:
            return jsonify( { 'result': 'ERROR', 'msg' : 'Invalid flavourName specified "%s".' % (flName,) } )

    current_app.logger.debug( 'shiftTaskMap> flavType/shiftType type found "%s" (%s/%s) ' % (flavType, flName, sType) )

    si = ShiftInformation()
    sTypeId = si.getShiftTypeId(subSys, sType, flName)
    if sTypeId < 0 :
        return jsonify(
            {'result': 'ERROR',
             'msg': 'ERROR NO shiftTypeId found for subsys/flavour/type/year "%s/%s/%s/%s".' % (
             subSys, flName, sType, year)})

    current_app.logger.debug( 'shiftTaskMap> shiftTypeId for subsys/flavour/type/year "%s/%s/%s/%s" ' % (subSys, flName, sType, year) )

    # check if shift-entity **w/o task** is already used somewhere actively in the STM
    try:
        stmOld = ( db.session.query( ShiftTaskMap )
                              .filter( ShiftTaskMap.year == year,
                                       ShiftTaskMap.status == 'ACTIVE',
                                       ShiftTaskMap.subSystem == subSys,
                                       ShiftTaskMap.shiftType == sType,
                                       ShiftTaskMap.flavourName == flName)
                              .one() )
        if stmOld:
            return jsonify(
                { 'result' : 'ERROR',
                  'msg'    : 'A shift for subsys/flavour/type/year "%s/%s/%s/%s" is already associated with another task. You will first need to remove it there.' % (
                  subSys, flName, sType, year) } )

    except sa_exc.NoResultFound: # nothing found, good
        pass
    except Exception as e:
        current_app.logger.info("got exception when checking for pre-existing stm: %s" % str(e))
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when checking for existence of STM for subsys/flavour/type/year "%s/%s/%s/%s" - got "%s".' % (subSys, flName, sType, year, str(e)) } )

    oldStm = None
    try:
        oldStm = db.session.query(ShiftTaskMap).filter_by(year = year,
                                              subSystem = subSys,
                                              shiftType = sType,
                                              flavourName = flName,
                                              taskCode = tCode,
                                              status = 'ACTIVE').one()
    except sa_exc.NoResultFound:
        pass

    if oldStm:
        return jsonify(
            { 'result' : 'ERROR',
              'msg'    : 'ShiftTaskMap item for subsys/flavour/type/taskCode/year "%s/%s/%s/%s/%s" already existing. Ignoring request' % (
              subSys, flName, sType, tCode, year) } )

    stm = ShiftTaskMap(subSys=subSys, sType=sType, sTypeId=sTypeId, flavourName=flName,
                       taskCode=tCode,
                       status='ACTIVE', weight=1.,
                       year=year )

    commitNew(stm)

    current_app.logger.info( 'ShiftTaskMap item for subsys/flavour/type/taskCode/year "%s/%s/%s/%s/%s" already existing. Ignoring request' % (subSys, flName, sType, tCode, year) )

    return jsonify( { 'result': 'OK',
                      'msg' : "successfully created new shift-task-map entry.",
                      'id' : stm.id } )

@api.route( '/shiftTaskMap/<int:id>', methods=[ 'GET' ] )
@login_required
def shiftTaskMap( id ) :

    try:
        mapEntry = db.session.query(ShiftTaskMap).filter_by(id=id).one()
    except Exception as e:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when retrieving ShiftTaskMap entry "%d" - got "%s".' % (id, str(e)) } )

    return jsonify( { 'result': 'OK', 'data' : mapEntry.to_json() } )

@api.route( '/shiftTaskMap', methods=[ 'DELETE' ] )
@login_required
def delShiftTaskMap() :

    idReq = int( request.form.get('id', -1) )
    if idReq == -1:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'No id found in request.' } )

    current_app.logger.info("api.delShiftTaskMap> got request to delete STM with id %s " % idReq )

    mapEntry = None
    try:
        mapEntry = db.session.query(ShiftTaskMap).filter_by(id=idReq, status='ACTIVE').one()
    except sa_exc.NoResultFound:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'No ShiftTaskMap entry for id "%s" found.' % (idReq,) } )
    except Exception as e:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when retrieving ShiftTaskMap entry "%s" - got "%s".' % (idReq, str(e)) } )

    mapEntry.status = 'DELETED'
    mapEntry.timestamp = datetime.now()

    db.session.add(mapEntry)
    db.session.commit()

    return jsonify( { 'result': 'OK', 'msg' : 'ShiftTaskMap entry for id "%s" deleted.' % (idReq,) } )


@api.route( '/getShiftWeightScales', methods=[ 'GET' ] )
@login_required
def getShiftWeightScales() :

    selYear = session.get('year')

    isMgr = False
    if ( current_user.is_administrator() or current_user.isManager(mgtType='project', year=selYear) ) :
        isMgr = True

    managedProjects = [ x[0] for x in (db.query( Manager.itemCode )
                         .filter( Manager.year==selYear )
                         .filter( Manager.itemType == 'project')
                         .filter( Manager.status == 'active' )
                         .filter( Manager.role == Permission.MANAGEPROJ )
                         .filter( Manager.userId == current_user.id )
                         .all()
                       ) ]

    # only handle mapped items
    stmListDB = ( db.session.query( ShiftTaskMap.subSystem, ShiftTaskMap.shiftType,
                                    ShiftTaskMap.flavourName, ShiftTaskMap.weight, ShiftTaskMap.addedValueCredits,
                                    ShiftTaskMap.id,
                                    ShiftTaskMap.taskCode )
                            .filter_by( year=selYear, status='ACTIVE' )
                            .distinct(ShiftTaskMap.subSystem, ShiftTaskMap.shiftType, ShiftTaskMap.flavourName)
                            .all() )

    current_app.logger.info('api::getShiftWeightScales> found %d entries ' % len(stmListDB))

    # https://twiki.cern.ch/twiki/bin/view/CMS/CentralShifts2017#Central_credit_accounting

    stmList = []
    for x in stmListDB:
        t0 = db.session.query(Task).filter_by(code=x[-1]).one()
        proj = t0.activity.project
        isPM = False
        if current_user.is_administrator() or proj.code in managedProjects: isPM = True
        # print "proj.name/code: %s/%s PM: %s - x: %s" % (proj.name, proj.code, isPM, str(x))
        stmList.append( [x[0],x[1],x[2],x[3],x[4],x[5],isPM] )

    current_app.logger.debug('api::getShiftWeightScales> found : %s ' % str(stmList))

    return jsonify( { 'data' : stmList } )

@api.route( '/shiftWeightScale', methods=[ 'POST' ] )
@login_required
def shiftWeightScale() :

    year = int(request.form.get('year', getPledgeYear()))

    id   = int(request.form.get('id', -1))
    if id == -1:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'No id found in request.' } )

    newScale = float( request.form.get('newScale', -1) )
    if id == -1:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'No weightScale found in request.' } )

    current_app.logger.info("api.shiftWeightScale> got request to set weight for id %s in %s to %s " % (id, year, newScale) )

    mapEntry = None
    try:
        mapEntry = db.session.query(ShiftTaskMap).filter_by(id=id).one()
    except sa_exc.NoResultFound:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'No ShiftTaskMap entry for id "%s" found.' % (id,) } )
    except Exception as e:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when retrieving ShiftTaskMap entry "%s" - got "%s".' % (id, str(e)) } )

    mapEntry.weight = newScale
    mapEntry.timestamp = datetime.now()

    db.session.add(mapEntry)
    db.session.commit()

    current_app.logger.info( "api.shiftWeightScale> weightScale for id %s updated." % id )

    return jsonify( { 'result': 'OK',
                      'msg' : 'ShiftTaskMap weightScale for id %s updated to %s.' % (id, newScale) } )

@api.route( '/shiftAVC', methods=[ 'POST' ] )
@login_required
def shiftAVC() :

    year = int(request.form.get('year', getPledgeYear()))

    id   = int(request.form.get('id', -1))
    if id == -1:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'No id found in request.' } )

    newAVC = float( request.form.get('newAVC', -1) )
    if id == -1:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'No addedValueCredits found in request.' } )

    current_app.logger.info("api.shiftAVC> got request to set addedValueCredits for id %s in %s to %s " % (id, year, newAVC) )

    mapEntry = None
    try:
        mapEntry = db.session.query(ShiftTaskMap).filter_by(id=id).one()
    except sa_exc.NoResultFound:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'No ShiftTaskMap entry for id "%s" found.' % (id,) } )
    except Exception as e:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when retrieving ShiftTaskMap entry "%s" - got "%s".' % (id, str(e)) } )

    mapEntry.addedValueCredits = newAVC
    mapEntry.timestamp = datetime.now()

    db.session.add(mapEntry)
    db.session.commit()

    current_app.logger.info( "api.shiftAVC> addedValueCredits for id %s updated." % id )

    return jsonify( { 'result': 'OK',
                      'msg' : 'ShiftTaskMap addedValueCredits for id %s updated to %s.' % (id, newAVC) } )

@api.route( '/getShiftGridSubSystems', methods=[ 'GET' ] )
@login_required
def getShiftGridSubSystems():

    try:
        subSysList = (db.session.query( ShiftGrid.subSystem.distinct() )
                        .all() )
        current_app.logger.info( 'subsystems found : %s' % str(subSysList) )
    except Exception as e:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when retrieving SubSystems - got "%s".' % str(e) } )
    return jsonify( { 'result': 'OK', 'data' : [ x[0] for x in subSysList] } )

@api.route( '/getShiftGridFlavTypes', methods=[ 'POST' ] )
@login_required
def getShiftGridFlavTypes() :

    subSys = json.loads(request.form.get('subSystems', None))
    current_app.logger.info('getShiftGridFlavTypes> request for subsystem %s (%s)' % (subSys, type(subSys)) )
    if not subSys:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'getShiftGridFlavTypes> No subSystem specified.' } )

    try:
        entries = db.session.query(ShiftGrid.subSystem, ShiftGrid.shiftType).distinct().filter(ShiftGrid.subSystem.in_(subSys)).all()
    except Exception as e:
        return jsonify(
            { 'result' : 'ERROR',
              'msg' : 'Exception when retrieving ShiftTaskMap entry for "%d" - got "%s".' % (subSys, str(e)) } )

    current_app.logger.info('getShiftGridFlavTypes> found entries: %s' % str(entries) )

    shifts = {}
    for subSys, sTyp in entries:
        for sFlav in [ 'main', 'main 2nd', 'trainee' ] :
            if subSys in shifts.keys():
                if (sFlav, sTyp ) not in shifts[ subSys ]:
                    shifts[ subSys ].append( (sFlav, sTyp ) )
            else:
                shifts[subSys] = [ ( sFlav, sTyp ) ]

    current_app.logger.info('getShiftGridFlavTypes> found shiftGrid: %s' % str(shifts) )

    return jsonify( { 'result': 'OK', 'data' : shifts } )


@api.route('/shiftDetailsUser/<string:username>', methods=['GET'])
@login_required
def shiftDetailsUser(username):

    current_app.logger.debug( f"shiftDetailsUser> args for {username}: {request.args}"   )

    year = int( request.args.get('year', getPledgeYear()) )
    hrId  = int( request.args.get('shifterId', -1) )
    shiftType  = request.args.get('shiftType', -1)
    subSys = request.args.get('shiftSubSys', None)
    flavName = request.args.get('shiftFlavourName', None)

    current_app.logger.info( f"shiftDetailsUser> request for {username}: {hrId} - {subSys} {shiftType} {flavName} in {year}"   )

    data = getShiftDetailsUser( username, hrId, subSys, shiftType, flavName, year )

    current_app.logger.debug( f'shiftDetailsUser> data: {data} ' )

    current_app.logger.info( f"shiftDetailsUser> got: {len(data['shifts'])} shifts for {hrId} in {year}" )
    current_app.logger.debug( f"shiftDetailsUser> data[:5]: {str(data['shifts'][:5])} " )

    return jsonify( { 'wScal':  data['wScal'], 'shiftDetails' : data['shifts'], 'wSum' : data['wSum']  } )
