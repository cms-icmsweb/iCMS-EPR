from ast import AugAssign
import copy
import json
import re
import sqlalchemy

from flask import jsonify, request, current_app, url_for, session, abort
from flask_login import login_required, current_user

from . import api
from ..models import EprUser, Shift, EprInstitute, Category, TimeLineInst, TimeLineUser, Manager, Permission, CMSActivity, Task, getEgroupsForUser
from ..main.Helpers import getPledgeInfo, addToFavs, getAggregateInfo, getMyManagedTasks, getMyTasksPledgeInfo
from ..main.Helpers import formatMyManagedTasks, getMyIRTasksPledgeInfo
from ..ldapAuth import getPrimUserByDN, getPrimUserByHrId, searchForUser, getUserEmail

from icms_orm.common import UserPreferences

from .. import db

from ..main.Helpers import getPledgeYear

@api.route('/aggregate', methods=['GET', 'POST'])
@login_required
def aggregate():

    startYear = request.form.get( 'startYear', 1999 )
    instCode  = request.form.get( 'instCode', None )
    cmsIdsStr = request.form.get( 'cmsIds', None )

    cmsIds = cmsIdsStr.split(',')

    agg = getAggregateInfo(startYear, cmsIds)

    return jsonify( { 'data' : agg } )

@api.route('/allUsers', methods=['GET', 'POST'])
@login_required
def getAllUsers():

    data = []
    for u, i, c in db.session.query(EprUser, EprInstitute, Category).join(EprInstitute).join(Category).all():
        data.append( {  'username': u.username,
                        'name': u.name,
                        'category' : c.name,
                        'cmsId' : u.cmsId,
                        'hrId' : u.hrId,
                        'mainInst' : u.mainInst,
                        'instName' : i.name,
                        'suspended' : u.isSuspended,
                        'status' : u.status,
                        } )

    return jsonify( { 'data' : data } )

@api.route('/managerUsers', methods=['GET'])
@login_required
def managerUsers():

    users = (db.session.query(EprUser.id, EprUser.name, EprInstitute.code, EprUser.cmsId, EprUser.hrId)
                       .join(EprInstitute, EprInstitute.id == EprUser.mainInst)
                       .all())

    data = []
    for uId, uName, iCode, uCmsId, uHrId in users:
        data.append( { 'id' : uId,
                       'name' : uName,
                       'instCode' : iCode,
                       'cmsId' : uCmsId,
                       'hrId' : uHrId,
                    } )
    return jsonify( { 'data' : data } )

@api.route('/searchUser', methods=['GET'])
@login_required
def searchUsers():

    search = request.form.get( 'term', None )
    if search is None:
        search = request.args.get( 'term', None )

    if search is None :
        current_app.logger.error('no search term given/found ... method="%s"' % request.method)
        return jsonify ( [] )

    search_term = '%%%s%%' % search
    current_app.logger.debug('searchUsers> search term is "%s"' % search_term )

    # Merging all the queries as previously 3132 would only return Jen, Hung-Che (aka fakeUsername-3132)
    resDB = db.session.query(EprUser.name, EprUser.mainInst, EprUser.hrId).\
            filter(
                sqlalchemy.or_(
                    sqlalchemy.or_(
                        EprUser.name.ilike(search_term),
                        EprUser.username.ilike(search_term)
                    ),
                    sqlalchemy.or_(
                        sqlalchemy.sql.expression.cast(EprUser.cmsId, sqlalchemy.String(11)).like(search_term),
                        sqlalchemy.sql.expression.cast(EprUser.hrId, sqlalchemy.String(11)).like(search_term)
                    )
                )
            ).order_by(EprUser.name).all()

    current_app.logger.info( "searchUser results for %s: %s " % (search_term, str(resDB), ))

    results = []
    for name, instId, hrId in resDB:
        try:
            inst = db.session.query(EprInstitute).filter_by(id=instId).one()
            # note: if this format gets changed, update also it's usage below and in other places
            #       #relyOnSearchFormat
            results.append( '%s - %s (CERNid: %i)' % (name, inst.code, hrId) )
        except sqlalchemy.orm.exc.NoResultFound:
            pass

    return json.dumps( results )

@api.route('/users', methods=['GET', 'POST'])
@login_required
def getUsers():

    data = []
    for u in db.session.query(EprUser).all():
        data.append( u.to_json() )

    # current_app.logger.info( '\n data is %s ' % str(data) )

    return jsonify( { 'data' : data } )

@api.route('/user/<int:id>', methods=['GET', 'POST'])
@login_required
def get_user(id):
    user = db.session.query(EprUser).filter(EprUser.id==id).one()
    if not user:
        return abort(404)
    return jsonify(user.to_json())

@api.route('/userByName/<string:name>', methods=['GET'])
@login_required
def userByName(name):

    #       #relyOnSearchFormat
    try:
        hrId = int(re.match(r'.*\(CERNid:\s*(\d+)\)\s*', name).group(1))
    except AttributeError:
        return jsonify( { 'error': 'illegal search string found: "%s" ' % name } )

    # current_app.logger.info( "api::userByName> got hrId %i from %s" % (hrId, name.encode('ascii','ignore')) )

    user = db.session.query(EprUser).filter_by( hrId=hrId ).one()

    # now get the institute-info for the user in the selected year:

    tlUser = None
    try:
        tlUser = db.session.query(TimeLineUser).filter_by(cmsId=user.cmsId, year=session['year']).order_by(TimeLineUser.timestamp.desc()).first()
    except:
        current_app.logger.warning("Could not find timeline entry for user %s in %i" % (user.username, session['year']) )

    try :
        tlInst = db.session.query(TimeLineInst).filter_by(code=tlUser.instCode, year=session['year']).order_by(TimeLineInst.timestamp.desc()).first()
        timeLinesUser = [ x.to_json() for x in db.session.query(TimeLineUser).filter_by(cmsId=user.cmsId, year=session['year']).order_by(TimeLineUser.timestamp.desc()).all() ]
        tlInstJson = tlInst.to_json()
    except AttributeError as e:
        timeLinesUser = []
        tlInstJson = {}
        inst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()
        current_app.logger.warning( "Attribute Error when trying to find timeline entry for institute %s (user %s) in %i" % (inst.code, user.username, session[ 'year' ]) )
    except:
        inst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()
        current_app.logger.warning( "Could not find timeline entry for institute %s in %i" % (inst.code, session[ 'year' ]) )

    pM = db.session.query(Manager).filter_by(userId=user.id, year=session['year'], itemType='project', role=Permission.MANAGEPROJ, status='active').all()
    projects = [m.itemCode for m in pM]

    aM = db.session.query(Manager).filter_by(userId=user.id, year=session['year'], itemType='activity', role=Permission.MANAGEACT, status='active').all()
    acts = [db.session.query(CMSActivity).filter_by(code=m.itemCode).one().name for m in aM]

    tM = db.session.query(Manager).filter_by(userId=user.id, year=session['year'], itemType='task', role=Permission.MANAGETASK, status='active').all()
    tasks = [db.session.query(Task).filter_by(code=m.itemCode).one().name for m in tM]

    iM = db.session.query(Manager).filter_by(userId=user.id, year=session['year'], itemType='institute', role=Permission.MANAGEINST, status='active').all()
    insts = [m.itemCode for m in iM]

    return jsonify( { 'user': user.to_json(), 'inst': tlInstJson,
                      'projectsManaged' : projects,
                      'actsManaged' : acts,
                      'tasksManaged' : tasks,
                      'instsManaged' : insts,
                      'timeLines' : timeLinesUser } )

@api.route('/userByCmsId/<int:cmsId>', methods=['GET', 'POST'])
@login_required
def get_userByCmsId(cmsId):
    user = db.session.query(EprUser).filter_by(cmsId=cmsId).one()
    inst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()

    return jsonify( { 'user': user.to_json(), 'inst': inst.to_json() } )

@api.route('/searchUserLdap', methods=['GET'])
@login_required
def searchUsersLdap():

    search = request.form.get( 'term', None )
    if search is None:
        search = request.args.get( 'term', None )

    if search is None :
        current_app.logger.error('no search term given/found ... method="%s"' % request.method)
        return jsonify ( [] )

    result = searchForUser(search)

    return json.dumps( result )

@api.route('/userByHrId/', methods=['GET', 'POST'])
@api.route('/userByHrId/<int:hrId>', methods=['GET', 'POST'])
@login_required
def getUserByHrId(hrId=None):
    # print "\n--getUserByHrId> got: ", hrId,'\n'
    if hrId is None:
        return jsonify( { 'user' : 'None', 'inst' : 'None' } )
    user = db.session.query(EprUser).filter_by(hrId=hrId).one()
    inst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()

    return jsonify( { 'user': user.to_json(), 'inst': inst.to_json() } )

@api.route('/ldapUserByHrId/', methods=['GET', 'POST'])
@api.route('/ldapUserByHrId/<int:hrId>', methods=['GET', 'POST'])
def ldapUserByHrId(hrId=None):
    # print "\n--ldapUserByHrId> got: ", hrId,'\n'

    if hrId is None:
        return jsonify( { 'user' : 'None', 'inst' : 'None' } )

    result = getPrimUserByHrId(hrId)
    # print('===> result: ', result)
    if result is None:
        return jsonify( { 'user' : 'None', 'inst' : 'None' } )

    # convert result into a dict with standard keywords:
    resOut = prepareResult( result )

    return jsonify( { 'user': resOut, 'inst' : 'None' } )

@api.route('/ldapUserByDN/', methods=['GET', 'POST'])
@api.route('/ldapUserByDN/<string:dn>', methods=['GET', 'POST'])
def ldapUserByDN(dn=None):

    if dn is None:
        return jsonify( { 'user' : 'None', 'inst' : 'None' } )

    result = getPrimUserByDN(dn)

    if result is None:
        return jsonify( { 'user' : 'None', 'inst' : 'None' } )

    # convert result into a dict with standard keywords:
    resOut = prepareResult( result )

    return jsonify( { 'user': resOut, 'inst' : 'None' } )

def prepareResult( result ) :
    resOut = { 'username' : result[ 'cn' ][ 0 ].decode( 'utf-8' ),
               'name'     : '%s, %s' % (result[ 'sn' ][ 0 ].decode( 'utf-8' ).strip( "'" ),
                                        result[ 'givenName' ][ 0 ].decode( 'utf-8' ).strip( "'" )),
               'hrId'     : [ x.decode( 'utf-8' ) for x in result[ 'employeeID' ] ],
               }
    return resOut

@api.route('/shiftsForUser/<string:name>', methods=['GET'])
@login_required
def getShiftsForUser(name):

    #-todo: use a unique ID (cmsId, hrId) for this one ...
    user = db.session.query(EprUser).filter_by(name=name).one()

    shiftList = db.session.query(Shift).filter_by(userId=user.hrId, year=session['year']).all()

    data = []
    summary = { 'nShifts' : 0, 'sumWeight' : 0 }
    for s in shiftList:
        data.append( s.to_json() )
        summary['nShifts'] += 1
        summary['sumWeight'] += s.weight

    return jsonify( {'summary' : summary, 'shifts' : data } )

@api.route('/userPledge', methods=['POST'])
@login_required
def userPledges():

    username = request.form.get('username', None)
    year = request.form.get('year', getPledgeYear())

    current_app.logger.info( 'api:userPledge> request for user %s in %s ' % (username, year) )

    if not username:
        msg = "Error: no user found for username %s" % username
        current_app.logger.error( msg )
        return jsonify( { 'status' : 'error', 'msg' : msg } )

    data = getPledgeInfo(username, year)
    current_app.logger.info( 'api:userPledge> pledges for user %s in %s: %s ' % (username, year, str(data)) )

    return jsonify( { 'allProjects' : data }  )

@api.route('/changeUserInst', methods=['POST'])
@login_required
def changeUserInst():

    instCode = request.form.get('instCode', None)
    cmsId = request.form.get('cmsId', None)
    year = request.form.get('year', getPledgeYear())

    user, oldInst = db.session.query(EprUser, EprInstitute.code).join(EprInstitute, EprUser.mainInst == EprInstitute.id).filter(EprUser.cmsId == cmsId).one()

    if (not (current_user.is_administrator( )
             # or current_user.canManage('institute', oldInst, year=year)
             # or current_user.canManageUser(user, year=year)
    )) :
        msg = "Error: Sorry you are not allowed to change institute for user %s (new inst %s) " % (cmsId, instCode)
        current_app.logger.error( msg )
        return jsonify( { 'status' : 'error', 'msg' : msg } )

    current_app.logger.info( "changeUserInst> currentUser %s can manage %s " % (current_user.username, user.username) )
    current_app.logger.info( "changeUserInst> got request for user %s oldInst %s, newInst %s " % (cmsId, oldInst, instCode) )
    current_app.logger.info( "changeUserInst> ... though this is not yet implemented .... " )

    #-toDo: actually update the inst for the user, AND make a new TimeLineUser entry ...

    return jsonify( { 'status' : 'success' }  )

@api.route('/addToFavourites', methods=['POST'])
@login_required
def addToFavourites():

    taskCode = request.form.get('taskCode', None)
    plCode = request.form.get('pledgeCode', None)
    hrId = request.form.get('hrId', None)
    year = request.form.get('year', getPledgeYear())

    if not taskCode and not plCode:
        msg = "ERROR: neither taskCode nor pledgeCode requested"
        current_app.logger.warning(msg)
        return jsonify( { 'status' : 'error', 'message' : msg } )

    if not hrId:
        hrId = current_user.hrId

    current_app.logger.info('request to add to favourites for hrId %s : plCode %s - taskCode %s ' % (hrId, plCode, taskCode) )

    addToFavs( hrId, [plCode], taskCode )

    return jsonify( { 'status' : 'success' }  )


@api.route('/removeFromFavourites', methods=['POST'])
@login_required
def removeFromFavourites():

    taskCode = request.form.get('taskCode', None)
    plCode = request.form.get('pledgeCode', None)
    hrId = request.form.get('hrId', None)
    year = request.form.get('year', getPledgeYear())

    current_app.logger.info('request to remove from favourites for hrtId %s : plCode %s - taskCode %s ' % (hrId, plCode, taskCode) )

    if not taskCode and not plCode:
        msg = "ERROR: neither taskCode nor pledgeCode requested"
        return jsonify( { 'status' : 'error', 'message' : msg } )

    if not hrId:
        hrId = current_user.hrId

    try:
        fav = db.session.query(UserPreferences).filter_by(hrid = hrId).filter_by(app_name='epr').one()
    except sqlalchemy.orm.exc.NoResultFound: # nothing in yet, make a new/fresh object:
        return jsonify( { 'status' : 'success' } ) # no favourites store yet ...

    newPrefs = copy.copy( fav.prefs )
    if taskCode:
        taskList = newPrefs['tasks'][:]
        try:
            taskList.remove( taskCode )
        except ValueError: # not in list ...
            pass
        newPrefs.update( { 'tasks' : taskList })
    if plCode:
        plList = newPrefs['pledges'][:]
        try:
            plList.remove( plCode )
        except ValueError: # not in list ...
            pass
        newPrefs.update( { 'pledges' : plList })

    # now copy the new item, so that sqlalchemy sees it:
    fav.prefs = newPrefs

    db.session.add(fav)
    db.session.commit()

    if taskCode:
        current_app.logger.info( 'task code %s removed from preferences' % taskCode )
    if plCode:
        current_app.logger.info( 'pledge code %s removed from preferences' % plCode )

    return jsonify( { 'status' : 'success' }  )


@api.route('/managedTasks', methods=['GET', 'POST'])
@api.route('/managedTasks/<string:userName>', methods=['GET', 'POST'])
@login_required
def getManagedTasksForUser(userName=None):

    if not userName:
        userName = request.form.get('userName', None)
    if not userName:
        userName = current_user.username

    year = request.form.get('year', getPledgeYear())

    (directTasks, tasksWithEgroups, userEgroups) = getMyManagedTasks( userName, year )
   
    managedTasks = [ x.to_json() for x in directTasks ]
    knownTasks = [ x.code for x in directTasks ]
    for t, eGroup in tasksWithEgroups:
        current_app.logger.info( f'getManagedTasksForUser> {userName} : checking {eGroup} in {userEgroups} : {eGroup in userEgroups}' )
        if eGroup in userEgroups:
            if t.code not in knownTasks:
                managedTasks.append( t.to_json() )
                knownTasks.append( t.code )

    current_app.logger.info( f'getManagedTasksForUser> {userName} : found a total of {len(managedTasks)} managed tasks.' )
    current_app.logger.debug( f'{managedTasks}' )

    return jsonify( {'userEgroups' : userEgroups, 'data' : formatMyManagedTasks( knownTasks ) } )


@api.route('/managedTaskPledges', methods=['GET', 'POST'])
@api.route('/managedTaskPledges/<string:userName>', methods=['GET', 'POST'])
@login_required
def getManagedTaskPledgesForUser(userName=None):

    if not userName:
        userName = request.form.get('userName', None)
    if not userName:
        userName = current_user.username

    year = request.form.get('year', getPledgeYear())

    pledgeInfo = getMyTasksPledgeInfo( userName, year )
   
    current_app.logger.info( f'getManagedTaskPledgesForUser> {userName} : found a total of {len(pledgeInfo)} pledges for managed tasks.' )
    current_app.logger.debug( f'{pledgeInfo}' )

    return jsonify( { 'allProjects': pledgeInfo } )


@api.route('/managedIRTaskPledges', methods=['GET', 'POST'])
@api.route('/managedIRTaskPledges/<string:userName>', methods=['GET', 'POST'])
@login_required
def getManagedIRTaskPledgesForUser(userName=None):

    if not userName:
        userName = request.form.get('userName', None)
    if not userName:
        userName = current_user.username

    year = request.form.get('year', getPledgeYear())

    pledgeInfo = getMyIRTasksPledgeInfo( userName, year, irTasks=True )
   
    current_app.logger.info( f'getManagedIRTaskPledgesForUser> {userName} : found a total of {len(pledgeInfo)} pledges for managed InstResp tasks.' )
    current_app.logger.debug( f'{pledgeInfo}' )

    return jsonify( { 'allProjects': pledgeInfo } )

