import time
from datetime import date
import collections
import json

import sqlalchemy as sa
from sqlalchemy_extensions import QueryFactory
from sqlalchemy import and_, func, or_

from flask import jsonify, request, current_app, session, url_for
from flask_login import login_required, current_user

from . import api
from .. import db, cache

from ..models import Permission, Project, EprUser, Task, CMSActivity, Pledge, Manager, \
    EprCeiling, Shift, ShiftTaskMap as STM, TimeLineUser, EprInstitute, Level3Name

from icms_orm.common import Institute, EprAggregate

from ..main.Helpers import getProjectInfo, getProjectCeilings, commitNew, commitUpdated, \
    getCSPtoEPR, getProjUserInstPledgeSummary, getProjRegUserCounts, getProjNameMap, getProjNameIdMap, getMuonSubProjList, \
    getProjAuthWork, getProjShiftWork
    
from ..main.MailHelpers import getMgrEmails

from ..main.Helpers import getPledgeYear, editTaskTmpl, getTaskShiftTypeIds

from ..main.HistoryHelpers import toJson

@api.route('/allProjects', methods=['GET', 'POST'])
@login_required
def allProjects():

    data = []
    for i in db.session.query(Project).all():
        data.append( i.to_json() )

    return jsonify( { 'projects' : data }  )

class Totals(object):
    def __init__(self):
        self.needs = 0.
        self.pld = 0.
        self.acc = 0.
        self.done = 0.
        return

    def update(self, needs, pld, acc, done):
        self.pld += pld
        self.acc += acc
        self.done += done
        self.needs += needs
        return

    def toJson(self):
        return { 'needs' : self.needs,
                 'pld' : self.pld,
                 'acc' : self.acc,
                 'done' : self.done
                 }

    def __repr__(self):
        return 'needs: %s, pld: %s, acc: %s, done: %s' % (self.needs, self.pld, self.acc, self.done)

@api.route('/projectTaskInfo/<string:code>', methods=['GET', 'POST'])
@login_required
def projectTaskInfo(code):

    year = int(request.form.get('year', getPledgeYear()))
    initialVersion = request.form.get('initial', False)
    current_app.logger.info( "initial is '%s' for year %d" % (initialVersion, year) )

    q = db.session.query(Task.id).filter( Task.year == year).filter( Task.status!='DELETED' )
    if code.lower() != 'all':
        q = q.join( CMSActivity, CMSActivity.id == Task.activityId )\
            .join( Project, Project.id == CMSActivity.projectId )\
            .filter( Project.code == code )
    itemList = [ x[0] for x in q.all() ]

    level3DB = (db.session.query(Level3Name.id, Level3Name.name)
                          .filter( or_( Level3Name.startYear == -1, Level3Name.startYear <= year ) )
                          .filter( or_( Level3Name.endYear == -1, Level3Name.endYear >= year ) )
                          .all())
    lvl3Map = {}
    for id, name in level3DB:
        lvl3Map[id] = name

    totals = Totals()
    projTasks = []
    queryTime = 0
    procTime = 0
    if initialVersion :  # return an empty set of items

        (p, a, t) = (None, None, None)
        for tIn  in itemList:
            t = db.session.query(Task).filter_by(id=tIn).one().versions.all()[ 0 ]
            if t is None :
                current_app.logger.warning( "****> no task version found for task id %s " % tIn )
                continue
            a = db.session.query(CMSActivity).filter_by( id = t.activityId).one().versions[ 0 ]
            if a is None :
                current_app.logger.warning( "****> no act version found for act id %s " % t.activityId )
                continue
            p = db.session.query(Project).filter_by(id=a.projectId).one().versions[ 0 ]
            if p is None :
                current_app.logger.warning( "****> no proj version found for proj id %s " % a.projectId )
                continue

        tJson = 'unknown'
        if t:
            tJson = toJson(t)
            if t.level3 and t.level3 in lvl3Map.keys():
                tJson.update( { 'level3': lvl3Map[t.level3] } )

        totals.update(needs=0., pld=0., acc=0., done=0.)
        projTasks.append( { 'checkbox' : '',
                            'pName' : p.name if p else 'unknown',
                            'aName' : a.name if a else 'unknown',
                            'task'  : tJson,
                            'sumDone' : 0.,
                            'pctDone' : 0.,
                            'sumPledged' : 0.,
                            'pctPld' : 0.,
                            'sumAcc' : 0.,
                            'pctAcc' : 0.,
                            'editTask' : 0.,
                            'newPledge' : 0.,
                          } )
    else:
        startTime = time.time()
        subQuery = (db.session.query( Task.id.label('actId'),
                                      func.sum( Pledge.workTimeDone ).label('wtd'),
                                      func.sum( Pledge.workTimeAcc ).label('wta'),
                                      func.sum( Pledge.workTimePld ).label('wtp')
                                    )
            .join( Pledge, Pledge.taskId == Task.id )
            .filter( Task.id.in_( itemList ) )
            .filter( Task.status == 'ACTIVE' )
            .filter( Task.year == year )
            .filter( Pledge.status != 'rejected' )
            .filter( Pledge.year == year )
            .group_by( Task.id )
            .subquery() )

        projTaskInfo = ( db.session.query(Task,
                                          CMSActivity.id, CMSActivity.name,
                                          Project.id, Project.name,
                                         func.coalesce( subQuery.c.wtd, 0.),
                                         func.coalesce( subQuery.c.wta, 0.),
                                         func.coalesce( subQuery.c.wtp, 0.)
                                         )
                        .outerjoin( subQuery, subQuery.c.actId == Task.id )
                        .join( CMSActivity, CMSActivity.id == Task.activityId )
                        .join( Project, Project.id == CMSActivity.projectId )
                        .filter( Task.id.in_( itemList ) )
                        .all())

        queryTime = time.time() - startTime
        # print '(%s - %s [%s])++1> ' % ( queryTime, len(projTaskInfo), year ), projTaskInfo

        shiftPATSumm = getShiftSummaryNew( year, groupingColumns=[ Task.id ] )
        # print '---> shiftPATSumm: ', shiftPATSumm

        startTime = time.time()
        for (task, actId, actName, pId, pName, sumDone, sumAcc, sumPl) in projTaskInfo:
            tId = int(task.id)

            neededWork = getNeededWork( taskId = tId )

            (nUserShifts, sumDoneShifts, sumPlShifts) = ( 0, 0., 0. )
            if tId in shiftPATSumm.keys():
                (nUserShifts, sumDoneShifts, sumPlShifts) = shiftPATSumm[task.id]

            # if nUserShifts+sumDoneShifts+sumPlShifts :
            #     print '... task %d/%s - found nUserShifts %s, sumDoneShifts %s, sumPlShifts %s' % (task.id, task.name, nUserShifts, sumDoneShifts, sumPlShifts)

            editTask = ''
            newPledge = ''
            if code.lower() != 'all' :
                # see if the user can manage this item
                userCanManage = current_user.is_administrator() or \
                                current_user.canManage( 'task', task.code, year=year ) or \
                                current_user.canManage( 'activity', task.activity.code, year=year ) or \
                                current_user.canManage( 'project', task.activity.project.code, year=year )

                # if ( (t.tType and 'shift' not in t.tType.lower()) and userCanManage ):
                if (userCanManage) :  # allow to edit shift tasks ...
                    # current_app.logger.info( '%s can manage task %s ' % (current_user.username, str(t.name)) )
                    editTask = editTaskTmpl % (url_for( 'manage.task', code='%s' % str(task.code) ),)

                # ensure that shift tasks can not have pledges
                if task.shiftTypeId and task.shiftTypeId.strip().lower() in getTaskShiftTypeIds() :
                    newPledge = ''
                else:
                    newPledge = '<a href="%s"> <img src=%s alt="new pledge" height="21" width="21"/> </a>' % \
                                (url_for( 'main.newPledge', taskCode=task.code ), '/static/details_open.png')

                if task and task.tType == 'InstResp':
                    newPledge = ''
                    if current_user.isTeamLeader('any', year):
                        newPledge = '<a href="%s"> <img src=%s alt="new pledge" height="21" width="21"/> </a>' % \
                                    (url_for('main.newInstRespPledge'), '/static/details_open.png')

            (pctPld, pctAcc, pctDone, pctNew, pctRej) = calculateFractions(neededWork, sumDone, sumAcc, sumPl, 0, 0, nUserShifts, sumDoneShifts, sumPlShifts)

            totals.update(needs=task.neededWork, pld=float(sumPl+sumDoneShifts+sumPlShifts), acc=sumAcc, done=sumDone+sumDoneShifts)
            tJson = 'unknown'
            if task:
                tJson = task.to_json()
            if task.level3 and task.level3 in lvl3Map.keys():
                tJson.update({'level3': lvl3Map[task.level3]})

            projTasks.append( { 'checkbox' : '',
                                'pName' : pName,
                                'aName' : actName,
                                'task'  : tJson,
                                'sumDone' : sumDone + sumDoneShifts,
                                'pctDone' : pctDone,
                                'sumPledged' : float(sumPl) + float( sumPlShifts + sumDoneShifts ),
                                'pctPld' : pctPld,
                                'sumAcc' : sumAcc,
                                'pctAcc' : pctAcc,
                                'editTask' : editTask,
                                'newPledge' : newPledge,
                              } )

        procTime = time.time() - startTime
        # print '(%s - %s [%s])++2> ' % (procTime, len( projTasks ), year), str(totals)

    current_app.logger.info( 'projectTaskInfo> code %s totals: %s for %d tasks in %s (%s) sec.' % (code, str(totals), len(projTasks), procTime, queryTime) )

    return jsonify( { 'projectTasks' : projTasks } )

@api.route('/projectActInfo/<string:code>', methods=['GET', 'POST'])
@login_required
def projectActInfo(code):

    year = int(request.form.get('year', getPledgeYear()))
    initialVersion = request.form.get('initial', '')
    current_app.logger.info( "initial is '%s' for year %d" % (initialVersion, year) )

    q = db.session.query(CMSActivity.id).filter( CMSActivity.year == year).filter( CMSActivity.status!='DELETED' )
    if code.lower() != 'all':
        q = q.join( Project, Project.id == CMSActivity.projectId )\
             .filter( Project.code == code )
    actList = [ x[0] for x in q.all() ]
    # print "got %s actIds" % len(actList)

    totalsAct = Totals()
    projActs = []
    nTasks = 0
    queryTime = 0
    procTime = 0

    if initialVersion : # return an empty set of items
        p = None
        a = None
        for aIn  in actList:
            a = db.session.query(CMSActivity).filter_by(id=aIn).one().versions[ 0 ]
            if a is None :
                current_app.logger.warning( "****> no act version found for act id %s " % aIn )
                continue
            p = db.session.query(Project).filter_by(id=a.projectId).one().versions[ 0 ]
            if p is None :
                current_app.logger.warning( "****> no proj version found for proj id %s " % a.projectId )
                continue

        totalsAct.update(needs=0., pld=0., acc=0., done=0.)
        projActs.append( { 'checkbox' : '',
                            'pName' : p.name if p else 'unknown',
                            'aName' : a.name if a else 'unknown',
                            'neededWork' : 0.,
                            'sumDone' : 0.,
                            'pctDone' : 0.,
                            'sumPledged' : 0.,
                            'pctPld' : 0.,
                            'sumAcc' : 0.,
                            'pctAcc' : 0.,
                            'nPlNew' : 0.,
                            'wtpNew' : 0.,
                            'pctNew' : 0.,
                            'nPlRej' : 0.,
                            'wtpRej' : 0.,
                            'pctRej' : 0.,
                           } )

    else:
        startTime = time.time()
        subQuery = (db.session.query( Task.activityId.label('actId'),
                                      func.sum( Pledge.workTimeDone ).label('wtd'),
                                      func.sum( Pledge.workTimeAcc ).label('wta'),
                                      func.sum( Pledge.workTimePld ).label('wtp')
                                    )
            .join( Pledge, Pledge.taskId == Task.id )
            .filter( Task.activityId.in_( actList ) )
            .filter( Task.status == 'ACTIVE' )
            .filter( Task.year == year )
            .filter( Pledge.status != 'rejected' )
            .filter( Pledge.year == year )
            .group_by( Task.activityId )
            .subquery() )
        projActInfo = ( db.session.query(CMSActivity,
                                         func.coalesce( subQuery.c.wtd, 0.),
                                         func.coalesce( subQuery.c.wta, 0.),
                                         func.coalesce( subQuery.c.wtp, 0.)
                                         )
                        .outerjoin( subQuery, subQuery.c.actId == CMSActivity.id )
                        .filter( CMSActivity.id.in_( actList ) )
                        .all())
        queryTime = time.time() - startTime
        # print '(%s - %s [%s])++1> ' % ( queryTime, len(projActInfo), year ), projActInfo

        shiftPATSumm = getShiftSummaryNew( year, groupingColumns=[ CMSActivity.id ] )
        # print '---> ', shiftPATSumm

        startTime = time.time()
        for (act, sumDone, sumAcc, sumPl) in projActInfo:
            aId = act.id
            proj = db.session.query(Project).filter_by( id = act.projectId ).one()
            (nNew, wtpNew, nRej, wtpRej) = getPledgeWTPCount( actId=aId )

            neededWork = getNeededWork( actId = aId )

            (nUserShifts, sumDoneShifts, sumPlShifts) = ( 0, 0., 0. )

            if aId in shiftPATSumm.keys():
                (nUserShifts, sumDoneShifts, sumPlShifts) = shiftPATSumm[aId]

            # if nUserShifts+sumDoneShifts+sumPlShifts :
            #     print '... act %d/%s - found nUserShifts %s, sumDoneShifts %s, sumPlShifts %s' % (act.id, act.name, nUserShifts, sumDoneShifts, sumPlShifts)

            (pctPld, pctAcc, pctDone, pctNew, pctRej) = calculateFractions(neededWork, sumDone, sumAcc, sumPl, wtpNew, wtpRej, nUserShifts, sumDoneShifts, sumPlShifts)

            totalsAct.update( needs=neededWork, pld=float( sumPl+sumDoneShifts+sumPlShifts ), acc=sumAcc, done=sumDone+sumDoneShifts )
            projActs.append( { 'checkbox'   : '',
                               'pName'      : proj.name,
                               'aName'      : act.name,
                               'neededWork' : neededWork,
                               'sumDone'    : sumDone + sumDoneShifts,
                               'pctDone'    : pctDone,
                               'sumPledged' : float( sumPl ) + float( sumPlShifts + sumDoneShifts ),
                               'pctPld'     : pctPld,
                               'sumAcc'     : sumAcc,
                               'pctAcc'     : pctAcc,
                               'nPlNew'     : nNew,
                               'wtpNew'     : wtpNew,
                               'pctNew'     : pctNew,
                               'nPlRej'     : nRej,
                               'wtpRej'     : wtpRej,
                               'pctRej'     : pctRej,
                               } )

        procTime = time.time() - startTime
        # print '(%s - %s [%s])++2> ' % (procTime, len( projActs ), year), str(totalsAct)
        # print '... > nUserShifts %s, sumDoneShifts %s, sumPlShifts %s' % ( nUserShifts, sumDoneShifts, sumPlShifts)

    current_app.logger.info( "projectActInfo> code %s found %i acts in %s (%s) sec. " % (code, len(projActs), procTime, queryTime) )
    current_app.logger.info( 'projectActInfo> code %s totalsAct: %s for %d tasks ' % (code, str(totalsAct), nTasks) )

    return jsonify( { 'projectActs' : projActs } )

def getPledgeWTPCount( projId = None, actId = None, taskId = None ):

    nNew = 0
    nRej = 0
    wtpNew = 0.
    wtpRej = 0.

    if projId is None and actId is None and taskId is None:
        current_app.logger.warning( "getPledgeWTPCount> no ID given, returning empty !!! ")
        return nNew, wtpNew, nRej, wtpRej

    # we rely here on the fact that we do get an ID for any activity which implicitly "binds" the query to the year of that PAT-ID

    statList = ['new', 'rejected']
    q = db.session.query( Pledge.status, func.sum( Pledge.workTimePld ), func.count() )\
                    .join( Task, Pledge.taskId == Task.id )\
                    .filter( Task.status == 'ACTIVE' )\
                    .join( CMSActivity, CMSActivity.id == Task.activityId )\
                    .filter( CMSActivity.status != 'DELETED' )\
                    .filter( Pledge.status.in_( statList ) )
    if projId:
        q = q.filter( CMSActivity.projectId == projId )
    if actId:
        q = q.filter( CMSActivity.id == actId )
    if taskId:
        q = q.filter( Task.id == taskId )

    res = q.group_by( Pledge.status ).all()

    nNew = 0
    nRej = 0
    wtpNew = 0.
    wtpRej = 0.

    for item in res:
        (s, valSum, valCnt) = item
        if s == 'new':
            wtpNew = valSum
            nNew = valCnt
        else:
            wtpRej = valSum
            nRej = valCnt

    if not nNew : nNew = 0
    if not wtpNew : wtpNew = 0
    if not nRej : nRej = 0
    if not wtpRej : wtpRej = 0

    return nNew, wtpNew, nRej, wtpRej

def getNeededWork( projId=None, actId=None, taskId=None ):
    neededWork = 0

    if projId is None and actId is None and taskId is None:
        current_app.logger.warning( "getNeededWork> no ID given, returning empty !!! ")
        return neededWork

    # we rely here on the fact that we do get an ID for any activity which implicitly "binds" the query to the year of that PAT-ID

    if projId:
        neededWork, = (db.session.query( func.sum( Task.neededWork ) )
                       .join( CMSActivity, Task.activityId == CMSActivity.id )
                       .filter( Task.status == 'ACTIVE' )
                       .filter( CMSActivity.projectId == projId )
                       .one())
    elif actId:
        neededWork, = (db.session.query( func.sum( Task.neededWork ) )
                       .join( CMSActivity, Task.activityId == CMSActivity.id )
                       .filter( Task.status == 'ACTIVE' )
                       .filter( CMSActivity.id == actId )
                       .one())
    elif taskId:
        neededWork, = (db.session.query( func.sum( Task.neededWork ) )
                       .filter( Task.status == 'ACTIVE' )
                       .filter( Task.id == taskId )
                       .one())

    if not neededWork : neededWork = 0.

    return neededWork

def calculateFractions(neededWork, sumDone, sumAcc, sumPl, wtpNew, wtpRej, nUserShifts, sumDoneShifts, sumPlShifts ):
    pctDone = 0.
    pctPld = 0.
    pctAcc = 0.
    pctNew = 0.
    pctRej = 0.

    sumDoneAll = sumDone + sumDoneShifts
    sumPlAll = sumPl + sumPlShifts + sumDoneShifts

    if float( neededWork ) > 0 :
        pctDone = float( sumDoneAll ) / float( neededWork )
        pctPld = float( sumPlAll ) / float( neededWork )
        pctAcc = float( sumAcc ) / float( neededWork )
        pctNew = float( wtpNew ) / float( neededWork )
        pctRej = float( wtpRej ) / float( neededWork )

    return pctPld, pctAcc, pctDone, pctNew, pctRej

def getShiftSummaryNew( selYear, groupingColumns ):

    # if groupingColumns == [Project.id] and selYear < 2019:
    #     return getShiftProjSummaryPre2019( proj, selYear )

    resDone = _get_shifts_pat_info(db.session, selYear, groupingColumns, future=False)
    if selYear == date.today().year:
        resPld  = _get_shifts_pat_info(db.session, selYear, groupingColumns, future=True)
    else:
        resPld = {} # older years do not have any pledges left ...

    spSum = {}
    for pId in set( list(resDone.keys()) + list(resPld.keys()) ):
        if pId in resDone or pId in resPld:
            (sDone, sPld) = (0., 0.)
            if pId in resDone: sDone = resDone[pId]
            if pId in resPld : sPld = resPld[ pId ]
            spSum[pId] = (0, sDone, sPld)

    # print( '+=+=+> spSum: ', spSum )

    return spSum

def _get_shifts_pat_info(db_session, year, grouping_column, inst_code=None, cms_id=None, proj_id=None, act_id=None, task_id=None, future=False):

    years = sorted(isinstance(year, collections.Iterable) and year or [year])
    new_years = [_year for _year in years if 2017 > _year >= 2015]
    newest_years = [_year for _year in years if _year >= 2017]
    queries = []
    # handling grouping column being actually a collection of columns
    group_by_cols = (isinstance(grouping_column, collections.Iterable) and [c for c in grouping_column] or [grouping_column]) + (len(years) > 1 and [Shift.year] or [])
    for year_range in (new_years, newest_years):
        if year_range:
            # request by RunCoord in Jan 2020 (ICMSEPR-200):
            # effective CSP per shift day = (baseline per day * effort) + (added value credit * baseline per day/8.0)
            #    = ( weight * (scale + avc/8.) )
            if year_range[0] >= 2020:
                q = db_session.query(func.sum(Shift.weight * (STM.weight + (STM.addedValueCredits / 8.)) / 21.7),
                                     *group_by_cols)
            elif year_range[0] >= 2017:
                    q = db_session.query(
                        func.sum(Shift.weight * STM.weight * (1. + (STM.addedValueCredits / 7.)) / 21.7),
                        *group_by_cols)
            else:
                q = db_session.query(func.sum(Shift.weight / 21.7), *group_by_cols)

            q = q.join(STM, and_(Shift.subSystem == STM.subSystem,
                                   Shift.shiftType == STM.shiftType,
                                   Shift.flavourName == STM.flavourName,
                                   Shift.year == STM.year))\
                 .filter(STM.status.ilike('ACTIVE'))\
                 .join( Task, Task.code == STM.taskCode )\
                 .join( CMSActivity, CMSActivity.id == Task.activityId )\
                 .join( Project, Project.id == CMSActivity.projectId ) \
                 .filter( Task.status == 'ACTIVE' )\
                 .filter( Task.year.in_(year_range) )\
                 .filter( Project.year.in_(year_range) )

            q = q.filter( Shift.year.in_(year_range) )

            if inst_code:
                q = q.filter(TimeLineUser.instCode == inst_code)
            if cms_id:
                q = q.filter(TimeLineUser.cmsId == cms_id)
            if proj_id:
                q = q.filter( Project.id == proj_id )
            if act_id:
                q = q.filter( CMSActivity.id == act_id )
            if task_id:
                q = q.filter( Task.id == task_id )

            if year == date.today().year:
                if future:
                    q = q.filter(Shift.shiftEnd >= date.today())
                else:
                    q = q.filter( Shift.shiftEnd < date.today() )

            q = q.group_by(*group_by_cols)
            queries.append(q)

    return __merge_query_results(queries)

def __merge_query_results(queries):
    ret_map = {}
    for q in queries:
        rows = q.all()
        for row in rows:
            submap = ret_map
            for key in row[1:-1]:
                submap[key] = submap.get(key, {})
                submap = submap[key]
            key = row[-1]
            submap[key] = submap.get(key, 0)
            submap[key] += row[0]
    return ret_map


def getShiftProjSummaryPre2019( proj, selYear ):

    currentShiftDay = date.today( )
    if currentShiftDay.year != selYear :
        currentShiftDay = date( selYear, 12, 31 )  # last day of selected year

    sq1 = QueryFactory.sq_time_line_user(year=selYear, inst_code=None, status='CMS%')

    sq3 = (db.session.query( Shift.userId.label( 'hrId' ),
                             func.sum( Shift.weight ).label( 'sumShiftWeight' ) )
                     .filter( Shift.year == selYear )
           )

    if selYear > 2015:
        sq3 = sq3.join(EprUser, EprUser.hrId == Shift.userId).join(sq1, EprUser.cmsId == sq1.c.cmsId)\
            .filter(and_(Shift.shiftStart > sq1.c.firststamp,
                         Shift.shiftStart < sq1.c.endDate))
    else:
        sq3 = sq3.filter(and_(Shift.shiftStart >= date(selYear, 1, 1), Shift.shiftEnd <= date(selYear, 12, 31)))

    if selYear > 2016: # use only mapped shifts for 2017 onwards:
        sq3 = sq3.join(STM, \
                       and_( Shift.subSystem == STM.subSystem, \
                             Shift.shiftType == STM.shiftType, \
                             Shift.flavourName == STM.flavourName,
                             Shift.year == STM.year) )\
                 .join( Task, Task.code == STM.taskCode )\
                 .join( CMSActivity, CMSActivity.id == Task.activityId ) \
                 .join( Project, Project.id == CMSActivity.projectId ) \
                 .filter( STM.status == 'ACTIVE')\
                 .filter( CMSActivity.status != 'DELETED')\
                 .filter( Task.status == 'ACTIVE') \
                 .filter( Project.id == proj.id )

    sq3 = sq3.group_by(Shift.userId)

    # now split into shifts done and future (pledged) shifts:
    sq3Done = sq3.filter(Shift.shiftEnd     < currentShiftDay).subquery()
    sq3Pld  = sq3.filter(Shift.shiftStart  >= currentShiftDay).subquery()

    shiftSummary = (db.session.query( sq3Done.c.hrId,
                                      sq3Done.c.sumShiftWeight.label( "shiftsDone" ),
                                      sq3Pld.c.sumShiftWeight.label( "shiftsPld" )
                     )
                    .outerjoin(sq3Pld, sq3Done.c.hrId == sq3Pld.c.hrId)
                    .distinct( )
                    .all( ))

    cspDone = 0
    cspPld = 0
    for item in shiftSummary :
        cspDone += item.shiftsDone if item.shiftsDone else 0
        cspPld += item.shiftsPld if item.shiftsPld else 0
    nUser = len( shiftSummary )
    # current_app.logger.debug("Shift summary for shift %s/%s/%s: nUser %s, cspDone %s, cspPld %s " %
    #                          (STMReq.subSystem, STMReq.shiftType, STMReq.flavourName, nUser, cspDone, cspPld) )

    return nUser, cspDone*getCSPtoEPR( selYear ), cspPld*getCSPtoEPR( selYear )

@api.route('/projectSummary', methods=['GET', 'POST'])
@login_required
def projectSummary() :

    year = int(request.form.get('year', getPledgeYear()))
    initialVersion = request.form.get('initial', '')
    current_app.logger.info( "initial is '%s' for year %d" % (initialVersion, year) )

    pidList = [ x[0] for x in db.session.query(Project.id).filter( Project.year == year).filter( Project.status!='DELETED' ).all() ]
    # print "got %d projIds : %s" % ( len(pidList), pidList )

    totalsPrj = Totals()
    projSumm = []
    queryTime = 0
    procTime = 0

    if initialVersion : # return an empty set of items
        for pIn  in pidList:
            p = db.session.query(Project).filter_by(id=pIn).one().versions[ 0 ]
            if p is None :
                current_app.logger.warning( "****> no proj version found for proj id %s " % pIn )
                continue
        totalsPrj.update(needs=0., pld=0., acc=0., done=0.)
        projSumm.append( { 'checkbox' : '',
                            'pName' : p.name,
                            'neededWork' : 0.,
                            'sumDone' : 0.,
                            'pctDone' : 0.,
                            'sumPledged' : 0.,
                            'pctPld' : 0.,
                            'sumAcc' : 0.,
                            'pctAcc' : 0.,
                            'nPlNew' : 0.,
                            'wtpNew' : 0.,
                            'pctNew' : 0.,
                            'nPlRej' : 0.,
                            'wtpRej' : 0.,
                            'pctRej' : 0.,
                           } )

    else:
        startTime = time.time()
        actList = [ x[ 0 ] for x in db.session.query( CMSActivity.id ).filter( CMSActivity.projectId.in_(pidList) ).filter( CMSActivity.status != 'DELETED' ).all() ]

        subQuery = (db.session.query( Task.activityId.label('actId'),
                                      func.sum( Pledge.workTimeDone ).label('wtd'),
                                      func.sum( Pledge.workTimeAcc ).label('wta'),
                                      func.sum( Pledge.workTimePld ).label('wtp')
                                    )
            .join( Pledge, Pledge.taskId == Task.id )
            .filter( Task.activityId.in_( actList ) )
            .filter( Task.status == 'ACTIVE' )
            .filter( Task.year == year )
            .filter( Pledge.status != 'rejected' )
            .filter( Pledge.year == year )
            .group_by( Task.activityId )
            .subquery() )

        projSumInfo = ( db.session.query(Project,
                                         func.sum( func.coalesce( subQuery.c.wtd, 0.) ),
                                         func.sum( func.coalesce( subQuery.c.wta, 0.) ),
                                         func.sum( func.coalesce( subQuery.c.wtp, 0.) )
                                         )
                        .join( CMSActivity, Project.id ==  CMSActivity.projectId )
                        .outerjoin( subQuery, subQuery.c.actId == CMSActivity.id )
                        .filter( Project.id.in_( pidList ) )
                        .group_by( Project.id )
                        .all())
        queryTime = time.time() - startTime

        shiftProjSumm = getShiftSummaryNew( year, groupingColumns=[ Project.id ] )

        startTime = time.time()
        for (proj, sumDone, sumAcc, sumPl) in projSumInfo:
            pId = proj.id

            (nNew, wtpNew, nRej, wtpRej) = getPledgeWTPCount( projId = pId )

            neededWork = getNeededWork( projId = pId )

            (nUserShifts, sumDoneShifts, sumPlShifts) = ( 0, 0., 0. )
            if pId in shiftProjSumm.keys():
                (nUserShifts, sumDoneShifts, sumPlShifts) = shiftProjSumm[pId]

            # if nUserShifts+sumDoneShifts+sumPlShifts :
            #     print '... proj %s/%s - found nUserShifts %s, sumDoneShifts %s, sumPlShifts %s' % (proj.id, proj.name, nUserShifts, sumDoneShifts, sumPlShifts)

            (pctPld, pctAcc, pctDone, pctNew, pctRej) = calculateFractions(neededWork, sumDone, sumAcc, sumPl, wtpNew, wtpRej, nUserShifts, sumDoneShifts, sumPlShifts)

            totalsPrj.update( needs=neededWork, pld=float( sumPl+sumDoneShifts+sumPlShifts ), acc=sumAcc, done=sumDone+sumDoneShifts )
            projSumm.append( { 'checkbox'   : '',
                               'pName'      : proj.name,
                               'neededWork' : neededWork,
                               'sumDone'    : sumDone + sumDoneShifts,
                               'pctDone'    : pctDone,
                               'sumPledged' : float( sumPl ) + float( sumPlShifts + sumDoneShifts ),
                               'pctPld'     : pctPld,
                               'sumAcc'     : sumAcc,
                               'pctAcc'     : pctAcc,
                               'nPlNew'     : nNew,
                               'wtpNew'     : wtpNew,
                               'pctNew'     : pctNew,
                               'nPlRej'     : nRej,
                               'wtpRej'     : wtpRej,
                               'pctRej'     : pctRej,
                               } )

        procTime = time.time() - startTime
        # print '(%s - %s [%s])++2> ' % (procTime, len( projSumm ), year), str(totalsPrj)
        # print '... > nUserShifts %s, sumDoneShifts %s, sumPlShifts %s' % ( nUserShifts, sumDoneShifts, sumPlShifts)

    current_app.logger.info( "projectSummary> found %i projs in %d (%d) sec. " % (len(projSumm), procTime, queryTime) )
    current_app.logger.info( 'projectSummary> totalsPrj: %s ' % str(totalsPrj) )

    return jsonify( { 'projectSummary' : projSumm } )

@api.route('/projByName/<string:name>', methods=['GET', 'POST'])
@login_required
def projByName(name):
    proj = db.session.query(Project).filter_by(name=name, year=session['year']).one()

    return jsonify( proj.to_json() )

@api.route('/projectInfo', methods=['GET', 'POST'])
@login_required
def projectInfo() :

    proj = int(request.form.get('proj', -1))
    year = int(request.form.get('year', getPledgeYear()))
    userName = request.form.get('user', None)
    allUsers = request.form.get('allUsers', True)

    individualIRTPledges = request.form.get('individualIRTPledges', False)

    current_app.logger.info('projectInfo> form: %s' % str(request.form) )
    current_app.logger.info('projectInfo> request for %s/%s/%s/%s/%s ' % (proj, year, userName, allUsers, individualIRTPledges))

    user = None
    if userName:
        user = db.session.query(EprUser).filter_by(username=userName).one()

    projItems = getProjectInfo(proj, user, allUsers, year=year, individualIRTPledges=individualIRTPledges)

    return jsonify( { 'allProjects' : projItems } )

@api.route('/projActivities/<string:code>', methods=['GET', 'POST'])
@login_required
def projActivities(code):

    if code.strip() == '': return jsonify( { 'data': [] } )

    try:
        proj = db.session.query(Project).filter_by(code=code).one()
    except Exception as e:
        return jsonify( { 'data': ['ERROR: project %s not found - got %s' % (code, str(e)) ] } )

    aList = []
    for a in proj.activities:
        if a.status != "ACTIVE" : continue
        aList.append( { 'name' : a.name, 'code': a.code } )

    return jsonify( {'data' : aList } )

@api.route('/projectByTaskCode', methods=['POST'])
@login_required
def projectByTaskCode():

    code = request.form.get( 'code', None )

    code = code.replace('_', '/')
    try:
        task = db.session.query(Task).filter_by(code=code).one()
    except Exception as e:
        msg =  'no task found for code %s - got %s ' % (code, str(e))
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    activity = task.activity
    project  = activity.project

    mgrs = set()
    try:
        for m, uName in (db.session.query(Manager, EprUser.name)
                           .filter(Manager.itemType == 'project')
                           .filter(Manager.itemCode == project.code)
                           .filter(Manager.userId==EprUser.id)
                           .filter(Manager.role==Permission.MANAGETASK)
                           .filter(Manager.status=='active')
                           .all()):
           mgrs.add(uName)
    except Exception as e:
        msg = 'ERROR when getting managers for proj with task-code %s - got %s ' % (code, str(e))
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    return jsonify( {'project': project.to_json(),
                     'mgrs' : '<br/>'.join(mgrs),
                     'numMgrs' : len(mgrs),
                     'mgrEmails' : ','.join( getMgrEmails('project', project.code) )
                     } )

@api.route('/projCeilings', methods=['POST'])
@login_required
def projCeilings():

    year = int(request.form.get('year', getPledgeYear()))
    prjList = request.form.get("projects", [])

    projects = json.loads( prjList )

    data = getProjectCeilings( year, projects )

    return jsonify( { "data": data } )

@api.route('/updateCeilings', methods=['POST', 'GET'])
@login_required
def updateCeilings():

    if not current_user.is_administrator() :
        msg = 'You are not allowed to update project ceilings ... '
        current_app.logger.warning(msg)
        return jsonify( { "status": "ERROR", "message" : msg } ), 401

    year = getPledgeYear()
    ceilings = []
    if request.method == 'POST':
        year = int(request.form.get('year', getPledgeYear()))
        ceilings = request.form.get("data", [])

    current_app.logger.info( f"updateCeilings> {request.method} going to update ceiling for {year}, data: {ceilings} " )

    nChanged = 0
    for item in ceilings.split('&'):
        code, ceil = item.split('=')
        newCeil = float(ceil)
        current_app.logger.info( f"updateCeilings> found code {code} with ceiling {ceil} - newCeil {newCeil}")
        if newCeil < 0: continue # not yet set, don't touch ...

        oldCeil = -1
        oldCeilRaw = db.session.query(EprCeiling.ceiling).filter(EprCeiling.code==code).one_or_none()
        if oldCeilRaw is not None: 
            oldCeil = oldCeilRaw[0]

        current_app.logger.info( f"updateCeilings> old ceiling {oldCeilRaw} - float(oldCeil) {float(oldCeil)} ")

        if float(oldCeil) == float(newCeil): continue # nothing old found or no change, don't touch

        current_app.logger.info( "updateCeilings> going to update ceiling for %s from %s to %s " % (code, oldCeil, newCeil) )
        nChanged += 1
        if oldCeil == -1: # no value for this exists in the DB so far, create a new one and commit this
            ceil = EprCeiling(code,year, newCeil)
            commitNew( ceil )
        else:
            ceil = db.session.query(EprCeiling).filter(EprCeiling.code==code).one()
            ceil.ceiling = newCeil
            commitUpdated( ceil )

    msg = "%d ceilings successfully updated" % nChanged
    current_app.logger.info(msg)

    return jsonify( { "status": "OK", "message" : msg } )

@api.route('/projectCounts', methods=['GET', 'POST'])
@login_required
def projectCounts() :

    projId = -1
    try:
        projId = int(request.form.get('proj', -1))
    except ValueError as e:
        if "invalid literal for int() with base 10: ''" in str(e):
            pass
        else:
            raise e

    year = int(request.form.get('year', getPledgeYear()))

    current_app.logger.info( "projectCounts> getting registered user/inst/country projectCounts info for %s in %s " % (projId, year) )

    outData = getProjRegUserCounts( projId, year )
    current_app.logger.info( "projectCounts> outData: %s " % str(outData) )

    return jsonify( { 'data': outData } )

@api.route( '/projectUserInsts', methods=[ 'GET', 'POST' ] )
@login_required
def projectUserInsts() :

    projId = -1
    try:
        projId = int(request.form.get('proj', -1))
    except ValueError as e:
        if "invalid literal for int() with base 10: ''" in str(e):
            pass
        else:
            raise e

    yearStr = request.form.get( 'year', None )

    proj = projId
    year = int( yearStr ) if yearStr is not None else getPledgeYear()

    current_app.logger.info( "projectUserInsts> getting user/inst/country pledge info for %s in %s " % (proj, year) )

    outData = getProjUserInstPledgeSummary( proj, year )

    return jsonify( outData )


@api.route( '/projectAuthWork', methods=[ 'GET' ] )
@login_required
def projectAuthWork() :

    proj = int( request.args.get( 'proj', -1 ) )
    year = int( request.args.get( 'year', getPledgeYear() ) )
    current_app.logger.info( "projectAuthWork> getting user/inst/country work info for %s in %s " % (proj, year) )

    outData = getProjAuthWork( proj, year )

    return jsonify( outData )

@api.route( '/projStats', methods=[ 'GET', 'POST' ] )
@login_required
def projStats() :

    proj = int( request.args.get( 'proj', -1 ) )
    year = int( request.args.get( 'year', getPledgeYear() ) )
    current_app.logger.info( "projStats> getting stats info for %s in %s " % (proj, year) )

    prjNameMap = { k.lower() : v for k,v in getProjNameMap().items() }
    prjRevNameMap = getProjNameMap(reverse=True, lowerCase=True)
    prjNameIdMap = { k : v for k,v in getProjNameIdMap(year).items() }
    prjIdNameMap = { v : k for k,v in prjNameIdMap.items() }

    # print( "projStats> ++++ prjNameMap   %s  " % str(prjNameMap) )
    # print( "projStats> ++++ prjRevNameMap   %s  " % str(prjRevNameMap) )
    # print( "projStats> ++++ prjNameIdMap %s  " % str(prjNameIdMap) )
    # print( "projStats> ++++ prjIdNameMap %s  " % str(prjIdNameMap) )

    ups = getProjUserInstPledgeSummary( proj, year )
    prc = getProjRegUserCounts( proj, year )
    paw = getProjAuthWork( proj, year )

    outData = []
    for pId in ups.keys():
        if pId == '-1' : continue ## ignore dummy entry when querying all projects
        prjName = prjIdNameMap[pId]

        # contributions to pledges
        v= ups[pId]
        row = [ '', prjName, v['nUsers'],  v['nInsts'], v['nCountries'], v['nAuthors'] ]

        # registered for project (careful: in 2015/16 the muons were separated, so they don't show yet)
        if prjName in [ x.lower() for x in getMuonSubProjList()]:
            prjName = 'Muon'
        if prjIdNameMap[pId] in prjNameMap:
            prjName = prjNameMap[ prjIdNameMap[pId] ]

        prcName = prjNameMap[ prjName.lower() ] if prjName.lower() in prjNameMap else prjName
        try:
            # for 2015 and 2016 sum up the individual contributions to the muon subprojects.
            # this should likely go to the getProjRegUserCounts() function, thoguh I don't
            # presently see where exactly ...
            if year < 2017 and prcName.lower() == 'muon':
                muSum = {}
                for muSubName in getMuonSubProjList():
                    v = prc[muSubName.lower()]
                    for k in ['nUsers', 'nAuthors', 'nInsts', 'nInstAuth', 'nCountries', 'nCountriesAuth']:
                        if k not in muSum.keys():
                            muSum[k] = v[k]
                        else:
                            muSum[ k ] += v[ k ]
                    v = muSum
                    row += [ v[ 'nUsers' ], v[ 'nAuthors' ], v[ 'nInsts' ], v[ 'nInstAuth' ], v[ 'nCountries' ],
                             v[ 'nCountriesAuth' ] ]
            else:
                v = prc[prcName.lower()]
                row += [ v['nUsers'], v['nAuthors'], v['nInsts'], v['nInstAuth'], v['nCountries'], v['nCountriesAuth'] ]

        except KeyError:
            current_app.logger.error( "projStats> ERROR: no proj with name %s (prjName: %s) found for pId %s in %s " % (prcName.lower(), prjName, pId, year) )
            current_app.logger.info( "projStats> ==== prc: %s " % str(prc) )
            current_app.logger.info( "projStats> ==== prjRevNameMap: %s " % str(prjRevNameMap) )
            row += [-1]*6

        # work pld/acc/done by all and authors
        v = paw[pId]
        row += list(v['all']) + list(v['authors'])

        outData.append( row )

    current_app.logger.info( "projStats> found stats info for %s in %s: %s " % (proj, year, str(outData)) )

    return jsonify( { 'data': outData } )

