
source ./venv-py3/bin/activate 
export PYTHONUNBUFFERED=1
export PYTHONPATH=.:..:../icms-legacy-orm:.:${PYTHONPATH}

export FLASK_DEBUG=1

export TOOLKIT_CONFIG=DEV

if [ `hostname` = "macap5.dyndns.cern.ch" ] ; then 
    echo "not setting LDAP URI"
else
    export LDAP_URI=ldaps://localhost:8636
    export LDAP_AUTH_URI=ldaps://localhost:9636
fi

export LC_ALL=en_US.UTF-8

# set up for Testing env ...
export TEST_DATABASE_URL=postgresql+psycopg2://ap@localhost/icms
export TEST_ICMS_DATABASE_URL=postgresql+psycopg2://ap@localhost/icms
export EPR_LOGIN_USE_LOCAL=1
export USE_FAKE_EGROUPS=1

cd /Users/ap/PycharmProjects/iCMS-EPR/

./manage.py runserver --host 0.0.0.0 --port 5010
