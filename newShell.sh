#!/usr/bin/env bash

source ./venv-3.9/bin/activate

# export TEST_DATABASE_URL=postgresql://ap@localhost/CovSelTestDB
# export TEST_DATABASE_URL=postgresql://ap@localhost/TestDB

PYTHONPATH=./venv-3.9/src/icms-orm/:./venv-3.9/src/icms-common:${PYTHONPATH} python3 -i ./manage.py shell
