install:
	pip3 install -r requirements.txt

run:
	FLASK_DEBUG=1 TOOLKIT_CONFIG=DEV python3 manage.py runserver --host 0.0.0.0 --port 5010