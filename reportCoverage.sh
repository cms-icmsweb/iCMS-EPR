
source ./venv-py3/bin/activate;

# echo '---'
# which python3
# python3 -c 'import coverage;print("coverage module at:", coverage.__path__)'
# which coverage
# coverage --version
# echo '---'


echo "coverage: starting: "
# ls -altr ; ls -al cov* .cov*

/bin/rm -rf ./coverage_html_report
coverage html

echo "coverage: html done: "
# ls -altr ; ls -al cov* .cov*

coverage report

# echo "coverage: combine-report done: "
# ls -altr ; ls -al cov* .cov*

# ls -al coverage_html_report/ 