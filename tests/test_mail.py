

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from .BaseTestClass import *

from app.sendEmails import send_email
from app.models import EprInstitute

log = logging.getLogger( "Testing.test_mail" )
log.setLevel(logging.DEBUG)

class PledgeInfo(object):
    pass

class MailTestCase(BaseTestCase):

    def setUp(self):
        BaseTestCase.setUp( self )
        self.defaultUser = 'tester'
        log.info( 'test_sendMail> MailTestCase set up ... ' )

    def doSendMail(self, to='pfeiffer@mail.cern.ch'):

        inst = db.session.query(EprInstitute).filter_by(code='CERN', year=self.selYear).one()
        task = Task()

        pInfo = PledgeInfo()
        pInfo.reason = 'testing'
        pInfo.year   = self.selYear

        pInfo.taskName = 'testTask'
        pInfo.actName  = 'testAct'
        pInfo.projName = 'testProj'

        pInfo.workPledged  = 3.
        pInfo.workAccepted = 3.
        pInfo.workDone     = 3.
        pInfo.workedSoFar  = 0.
        pInfo.status       = 'new'
        pInfo.code         = '42+42+42'

        pInfo.task         = task

        res = send_email( to=to,
                    subject='testMail - new pledge',
                    template = 'mail/newPledge',
                    plUserName='pfeiffer',
                    curUserName='curUserName',
                    instCode='testInst',
                    plInst=inst,
                    pledge=pInfo,
                    )

        return res

    def test_sendMail(self):
        res = self.doSendMail()

        log.info( 'test_sendMail> response: %s' % str(res) )
        self.assertIn( 'Mail for subject [iCMS-EPR] testMail - new pledge sent to pfeiffer@mail.cern.ch', str(res) )

    def test_sendMailFail(self):

        # send the mail a second time, should fail ... =
        res = self.doSendMail()
        log.info( 'test_sendMailFail> response: %s' % str(res) )
        self.assertNotIn( 'Mail for subject [iCMS-EPR] testMail - new pledge sent to pfeiffer@mail.cern.ch', str(res) )
        self.assertIn( 'found 1 existing mail(s) with hash ', str(res) )

    def test_sendMailFailNoToAddr(self):

        # send the mail a second time, should fail ... =
        res = self.doSendMail(to = None)
        log.info( 'test_sendMailFail> response: %s' % str(res) )
        self.assertNotIn( 'Mail for subject [iCMS-EPR] testMail - new pledge sent to pfeiffer@mail.cern.ch', str(res) )
        self.assertIn( 'send_email> no addressees given (subj:', str(res) )

