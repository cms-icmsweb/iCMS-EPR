
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

try:
    from BaseTestClass import *
except ImportError:
    from .BaseTestClass import *

log = logging.getLogger( "Testing.test_client" )
log.setLevel(logging.DEBUG)

class ClientTestCase(BaseTestCase):

    def test_createNewPledgeLastYearFail1( self ) :
        userName = 'a4m1'
        user = db.session.query(EprUser).filter_by( username=userName ).one( )
        self.assertFalse( user.is_administrator( ) )
        self.login( user.username )

        lastYear = getPledgeYear() - 1
        response = self.client.get( url_for( 'main.setYear', selYear=lastYear ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))
        self.assertIn('Pledges in %d' % lastYear, response.data.decode('utf-8'))

        response = self.client.get( url_for( 'main.newPledge', taskCode='t4a' ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'New pledges are not allowed for %d' % lastYear, response.data.decode('utf-8') )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )

        self.logout( )

    def NOtest_createNewPledgeLastYearOKWhenOpen( self ) :
        userName = 'p1m1'
        user = db.session.query(EprUser).filter_by( username=userName ).one()
        self.assertFalse( user.is_administrator() )
        self.login( user.username )

        lastYear = getPledgeYear() - 1
        response = self.client.get( url_for( 'main.setYear', selYear=lastYear ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )
        self.assertIn( 'Pledges in %d' % lastYear, response.data.decode('utf-8') )

        response = self.client.get( url_for( 'main.newPledge', taskCode='t4a' ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertNotIn( 'New pledges are not allowed for %d' % lastYear, response.data.decode('utf-8') )
        self.assertNotIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )
        self.assertIn( 'iCMS - EPR New Pledge', response.data.decode('utf-8') )
        self.assertIn( 'Creating New Pledge in %s' % lastYear, response.data.decode('utf-8') )

        self.logout()

    def test_createNewPledgeLastYearFailWhenClosed( self ) :
        userName = 'p1m1'
        user = db.session.query(EprUser).filter_by( username=userName ).one()
        self.assertFalse( user.is_administrator() )
        self.login( user.username )

        lastYear = getPledgeYear() - 1
        response = self.client.get( url_for( 'main.setYear', selYear=lastYear ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )
        self.assertIn( 'Pledges in %d' % lastYear, response.data.decode('utf-8') )

        response = self.client.get( url_for( 'main.newPledge', taskCode='t4a' ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'New pledges are not allowed for %d' % lastYear, response.data.decode('utf-8') )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )
        self.assertNotIn( 'iCMS - EPR New Pledge', response.data.decode('utf-8') )
        self.assertNotIn( 'Creating New Pledge in %s' % lastYear, response.data.decode('utf-8') )

        self.logout()


    def test_home_page(self):

        self.login()
        response = self.client.get( url_for('main.index'), follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'iCMS - EPR Pledge Overview\n for Tester, Master', response.data.decode('utf-8') )

        self.logout()

        response = self.client.get( url_for('main.index'), follow_redirects=True)
        # log.debug("got response data %s " % str(response.data.decode('utf-8')) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'iCMS-EPR - Login', response.data.decode('utf-8') )

    def test_toggleHelp(self):

        self.login()
        response = self.client.get( url_for('main.toggleHelp', state=True), follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'iCMS - EPR Pledge Overview\n for Tester, Master', response.data.decode('utf-8'))

    def test_findProjectForActivity( self ):

        response = self.client.post(
            url_for('main.findProjectForActivity'),
            data = { 'project' : 'p1' },
            follow_redirects=True
        )
        self.assertNotEqual(response.status_code, 200)
        # log.debug("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('Sorry, you are not allowed to create activities for this project', response.data.decode('utf-8'))


    def test_createNewPledgeFail1( self ) :

        userName = 'p1m1'
        user = db.session.query(EprUser).filter_by( username=userName ).one( )
        self.assertFalse( user.is_administrator( ) )
        self.login( user.username )

        response = self.client.get( url_for('main.newPledge', taskCode='Please'), follow_redirects=True)
        # log.debug("got response data %s " % str(response.data.decode('utf-8')) )

        self.assertEqual(response.status_code, 200)
        self.assertIn( 'Could not find a task for code Please', response.data.decode('utf-8') )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )

        self.logout()

    def test_createNewPledge2015Fail1( self ) :
        userName = 'p1m1'
        user = db.session.query(EprUser).filter_by( username=userName ).one( )
        self.assertFalse( user.is_administrator( ) )
        self.login( user.username )

        response = self.client.get( url_for( 'main.setYear', selYear=2015 ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8')[:30000] ) )
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8')[:30000])
        self.assertIn('Pledges in 2015', response.data.decode('utf-8')[:30000])

        response = self.client.get( url_for( 'main.newPledge', taskCode='t1' ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8')[:30000] ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'New pledges are not allowed for 2015', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8')[:30000] )

        self.logout( )

    def test_createNewPledgeNextYearFail( self ) :
        selYear = getPledgeYear()+1
        userName = 'p1m1'
        user = db.session.query(EprUser).filter_by( username=userName ).one( )
        self.assertFalse( user.is_administrator( ) )
        self.login( user.username )

        response = self.client.get( url_for( 'main.setYear', selYear=selYear ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))
        self.assertIn('Pledges in %s' % selYear, response.data.decode('utf-8'))

        response = self.client.get( url_for( 'main.newPledge', taskCode='t1' ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'New pledges are not allowed for %s' % selYear, response.data.decode('utf-8') )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )

        self.logout( )

    def test_createNewPledgeDefaultYearOK( self ) :

        self.selYear = getPledgeYear()
        userName = 'p1m1'
        user = db.session.query(EprUser).filter_by( username=userName ).one( )
        self.assertFalse( user.is_administrator( ) )
        self.login( user.username )

        response = self.client.get( url_for( 'main.setYear', selYear=self.selYear ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))
        self.assertIn('Pledges in %s' % self.selYear, response.data.decode('utf-8'))

        response = self.client.get( url_for( 'main.newPledge', taskCode='t1' ), follow_redirects=True )
        # log.debug( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'iCMS - EPR New Pledge', response.data.decode('utf-8') )
        self.assertIn( 'Creating New Pledge in %s' % self.selYear, response.data.decode('utf-8') )

        self.logout( )
