#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from .BaseTestClass import *

log = logging.getLogger( "Testing.test_history" )
log.setLevel(logging.INFO)

class HistoryTestCase( BaseTestCase ) :

    def setUp( self ) :
        BaseTestCase.setUp( self )
        self.defaultUser = 'tester'


    def test_getPledgeHistory( self ):

        plCode = '2+5+1'
        response = self.callAndCheckPost( endPointName='api.pledgeHistory',
                                          data={ 'plCode' : plCode, 'year' : self.selYear},
                                          jsonKey=None, jsonLen=0,
                                          retCodeExp=200, followRedir=True )

        log.info( 'test_getPledgeHistory> response: %s' % str(response) )
        log.info( 'test_getPledgeHistory> len(history): %s' % len(response['data']) )

        # self.assertEqual( response['code'], plCode )
        self.assertEqual( len(response['data']), 3 )

        # results contain timestamps, so they can't be identical ... revert to "spot-checks" :(

        # print('checking pledgehistory:')
        for item in response['data']:
            for k, v in item.items(): # all three items should have the status modified
                if k == 'status_mod': self.assertTrue( v )

    def test_getTaskHistory( self ):

        tCode = 't1'
        response = self.callAndCheckPost( endPointName='api.taskHistory',
                                          data={ 'taskCode' : tCode, 'year' : self.selYear},
                                          jsonKey=None, jsonLen=0,
                                          retCodeExp=200, followRedir=True )

        log.info( 'test_getTaskHistory> response: %s' % str(response) )
        log.info( 'test_getTaskHistory> len(history): %s' % len(response['data']) )

        # self.assertEqual( response['code'], plCode )
        self.assertEqual( len(response['data']), 5 )

        # results contain timestamps, so they can't be identical ... revert to "spot-checks" :(

        self.assertTrue( response['data'][0]['activityId'] == 1)
        self.assertTrue( response['data'][1]['activityId'] == 7)
        self.assertTrue( response['data'][2]['activityId'] == 1)
        self.assertTrue( response['data'][3]['activityId'] == 1)
        self.assertTrue( response['data'][4]['activityId'] == 1)

        # print('checking task history:')
        # for item in response['data']:
        #     for k, v in item.items():
        #         if '_mod' in k:
        #             colName = k.replace('_mod', '')
        #             if v: print("%s was modified, old value: %s" % (colName, item[colName]))
