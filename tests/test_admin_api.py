
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from .BaseTestClass import *

log = logging.getLogger( "Testing.test_admin_api" )
log.setLevel(logging.DEBUG)

class ApiAdminTestCase(BaseTestCase):

    def setUp(self):
        BaseTestCase.setUp( self )

        self.defaultUser = 'pfeiffer'
        self.login( username= self.defaultUser )

    def callAndCheckPostNoLogin( self, endPointName, jsonKey, jsonLen, data, retCodeExp=200, followRedir=False, **kwargs ) :

        if data:
            if followRedir:
                response = self.client.post( url_for( endPointName, **kwargs ), data=data, follow_redirects=True )
            else:
                response = self.client.post( url_for( endPointName, **kwargs ), data=data )
        else :
            if followRedir:
                response = self.client.post( url_for( endPointName, **kwargs ), follow_redirects=True )
            else:
                response = self.client.post( url_for( endPointName, **kwargs ) )

        self.assertEqual( response.status_code, retCodeExp )

        # log.info( 'callAndCheckPostNoLogin 1> response: %s' % response.data )
        json_response = json.loads( response.data.decode( 'utf-8' ) )
        if jsonKey :
            self.assertGreaterEqual( len( json_response[ jsonKey ] ), jsonLen )
        else :  # only a list is returned
            self.assertGreaterEqual( len( json_response ), jsonLen )

        return json_response

    def test_updateCeilings( self ) :

        # only allowed for admins
        response = self.callAndCheckPost( endPointName='api.updateCeilings',
                                          data={ 'data' : 'p1=1000&p2=42' },
                                          jsonKey='status', jsonLen=0 )
        # log.info( 'test_updateCeilings> len(response): %s' % len(response['mgr']) )
        log.info( 'test_updateCeilings> response.status: %s' % response['status'] )
        self.assertEqual( response['status'], "OK" )

        # double-check the values:
        response = self.callAndCheckPost( endPointName='api.projCeilings',
                                          data={ 'projects' : json.dumps( [ {'p1' : {} }, { 'p2': {} } ] ) },
                                          jsonKey='data', jsonLen=0 )
        
        # log.info( 'test_projCeilings> len(response): %s' % len(response['mgr']) )
        log.info( 'test_updateCeilings> response: %s' % str(response) )
        log.info( 'test_updateCeilings> response.data: %s' % response[ 'data' ] )
        self.assertEqual( response[ 'data' ], [{'ceiling': 1000.0, 'code': 'p1', 'name': {}}, {'ceiling': 42.0, 'code': 'p2', 'name': {}}] )

    def test_splitPledgeFail0( self ) :
        # no params params

        res = self.client.post( url_for( 'api.splitPledge' ),
                                data={ } )

        # log.info( 'test_splitPledgeFail0> response: %s' % res.data )
        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )
        self.assertEqual( json_response[ 'status' ], "ERROR" )
        self.assertIn( 'No valid pledge code found in request', json_response[ 'msg' ] )

    def test_splitPledgeFail1( self ) :
        # invalid params

        res = self.client.post( url_for( 'api.splitPledge' ),
                                data={ 'plCode' : '', 'newInst' : '', 'newInstWork' : 0. } )

        # log.info( 'test_splitPledgeFail1> response: %s' % res.data )
        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )
        self.assertEqual( json_response[ 'status' ], "ERROR" )
        self.assertIn( 'No valid pledge code found in request', json_response[ 'msg' ] )


    def test_splitPledgeFail2( self ) :
        # invalid params

        res = self.client.post( url_for( 'api.splitPledge' ),
                                data={ 'plCode' : '4+7+3', 'newInst' : 'invalidInstCode', 'newInstWork' : 0. } )

        # log.info( 'test_splitPledgeFail2> response: %s' % res.data )
        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )
        self.assertEqual( json_response[ 'status' ], "ERROR" )
        self.assertIn( 'No valid newInst found for instCode invalidInstCode', json_response[ 'msg' ] )

    def test_splitPledgeFail3( self ) :
        # invalid params

        res = self.client.post( url_for( 'api.splitPledge' ),
                                data={ 'plCode' : '4+7+3', 'newInst' : 'FNAL', 'newInstWork' : -99. } )

        # log.info( 'test_splitPledgeFail3> response: %s' % res.data )
        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )
        self.assertEqual( json_response[ 'status' ], "ERROR" )
        self.assertIn( 'Invalid work (-99.0) for newInst FNAL found in request, should be between 0. and 1.7', json_response[ 'msg' ] )

    def test_splitPledge0( self ) :

        # guest pledge -- as admin, we can split even this one.

        res = self.client.post( url_for( 'api.splitPledge' ),
                                data={ 'plCode' : '4+7+2', 'newInst' : 'CERN', 'newInstWork' : 0.5 } )

        # log.info( 'test_splitPledgeFail3> response: %s' % res.data )
        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )
        self.assertEqual( json_response[ 'status' ], "OK" )
        self.assertNotIn( 'Not touching guest pledge', json_response[ 'msg' ] )

    def test_splitPledge1( self ) :

        # this should succeed
        print("... here ... ")
        plListBefore = db.session.query(Pledge).all()
        log.info( "==> found %s pledges before " % len(plListBefore) )

        res = self.client.post( url_for( 'api.splitPledge' ),
                                data={ 'plCode' : '3+7+3',
                                       'newInst' : 'FNAL',
                                       'newInstWork' : 0.7 } )

        log.info( 'test_splitPledge1> response: %s' % res.data )

        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )
        log.info( 'test_splitPledge1> response: %s' % str(json_response) )
        self.assertEqual( json_response[ 'status' ], "OK" )
        self.assertIn( 'pledge 3+7+3 successfully split (0.7 to FNAL)', json_response[ 'msg' ] )

        plListAfter = db.session.query(Pledge).all()
        log.info( "==> found %s pledges after " % len(plListAfter) )
        self.assertEqual( len(plListBefore)+1, len(plListAfter) )

        oldPl = db.session.query(Pledge).filter_by(code='3+7+3').one()
        newPl = [x for x in plListAfter if x not in plListBefore ][0]
        ratio = oldPl.workTimePld/(oldPl.workTimePld+newPl.workTimePld)
        self.assertEqual( newPl.code, '3+7+2' )
        self.assertEqual( newPl.workTimePld,  0.7 )

    def test_splitPledge2( self ) :

        # this should succeed
        print("... here ... ")
        plListBefore = db.session.query(Pledge).all()
        log.info( "==> found %s pledges before " % len(plListBefore) )

        pl0 =  Pledge( 3, 8, 1,
                       3., 2., 1.,
                       'done', self.selYear )
        commitNew(pl0)

        # calculate the expected values for the accepted/done work:
        expectedAcc  = 2. * (2./3.)
        expectedDone = 1. * (2./3.)

        res = self.client.post( url_for( 'api.splitPledge' ),
                                data={ 'plCode' : '3+8+1',
                                       'newInst' : 'FNAL',
                                       'newInstWork' : 2.0 } )

        log.info( 'test_splitPledge1> response: %s' % res.data )

        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )
        self.assertEqual( json_response[ 'status' ], "OK" )
        self.assertIn( 'pledge 3+8+1 successfully split (2.0 to FNAL)', json_response[ 'msg' ] )

        plListAfter = db.session.query(Pledge).all()
        log.info( "==> found %s pledges after " % len(plListAfter) )
        self.assertEqual( len(plListBefore)+2, len(plListAfter) )

        newPl = [x for x in plListAfter if x not in plListBefore ][-1]

        self.assertEqual( newPl.code, '3+8+2' )
        self.assertEqual( newPl.workTimePld,  2.0 )
        self.assertAlmostEqual( newPl.workTimeAcc, expectedAcc, delta=1.e-6 )
        self.assertAlmostEqual( newPl.workTimeDone, expectedDone, delta=1.e-6 )

        # now delete these two pledges to not confuse the counting later:
        db.session.delete(pl0)
        db.session.delete(newPl)
        db.session.commit()


    def test_changeUserInst ( self ):
        res = self.callAndCheckPost( endPointName='api.changeUserInst',
                                    data={ 'instCode':'DESY', 'cmsId' : 9903, 'year' : self.selYear},
                                    jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( 'success', res['status'] )

    def test_addToFavourites ( self ):
        res = self.callAndCheckPost( endPointName='api.addToFavourites',
                                    data={ 'taskCode':'t1', 'pledgeCode' : '1+1+1', 'hrId' : 99003, 'year' : self.selYear},
                                    jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( 'success', res['status'] )

    def test_addToFavouritesOKSelf ( self ):
        res = self.callAndCheckPost( endPointName='api.addToFavourites',
                                    data={ 'taskCode':'t1', 'pledgeCode' : '1+1+1', 'year' : self.selYear},
                                    jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( 'success', res['status'] )

    def test_addToFavouritesFail1 ( self ):
        res = self.callAndCheckPost( endPointName='api.addToFavourites',
                                    data={ 'hrId' : 99003,'year' : self.selYear},
                                    jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( 'error', res['status'] )
        self.assertIn( 'neither taskCode nor pledgeCode requested', res['message'] )

    def test_removeFromFavourites ( self ):
        res = self.callAndCheckPost( endPointName='api.removeFromFavourites',
                                    data={ 'taskCode':'t1', 'pledgeCode' : '1+1+1', 'hrId' : 99003, 'year' : self.selYear},
                                    jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( 'success', res['status'] )

    def test_removeFromFavouritesSelfOK ( self ):
        res = self.callAndCheckPost( endPointName='api.removeFromFavourites',
                                    data={ 'taskCode':'t1', 'pledgeCode' : '1+1+1', 'year' : self.selYear},
                                    jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( 'success', res['status'] )

    def test_removeFromFavouritesFail1 ( self ):
        res = self.callAndCheckPost( endPointName='api.removeFromFavourites',
                                    data={ 'hrId' : 99003,'year' : self.selYear},
                                    jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( 'error', res['status'] )
        self.assertIn( 'neither taskCode nor pledgeCode requested', res['message'] )

    def test_getShiftWeightScales ( self ):
        res = self.callAndCheckGet( endPointName='api.getShiftWeightScales',
                                    data=None,
                                    jsonKey='data', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( [u'pr1', u'sType0', u'main', 1.0, 0.0, 2, True], res['data'] )
        self.assertIn( [u'pr2', u'sType1', u'main', 1.0, 0.0, 1, True], res['data'] )

    def test_shiftWeightScale ( self ):
        res = self.callAndCheckPostNoLogin( endPointName='api.shiftWeightScale',
                                            data={ 'year' : self.selYear, 'id' : 1, 'newScale' : 2.0 },
                                            jsonKey='result', jsonLen=1, retCodeExp=200, followRedir=True )
        log.info ('result: %s' % str(res [ 'result' ]) )
        self.assertIn( 'OK', res['result'] )
        self.assertIn( 'ShiftTaskMap weightScale for id 1 updated to 2.0.', res['msg'] )

    def test_shiftAVC ( self ):
        res = self.callAndCheckPostNoLogin( endPointName='api.shiftAVC',
                                            data={ 'year' : self.selYear, 'id' : 1, 'newAVC' : 2.0 },
                                            jsonKey='result', jsonLen=1, retCodeExp=200, followRedir=True )
        log.info ('result: %s' % str(res [ 'result' ]) )
        self.assertIn( 'OK', res['result'] )
        self.assertIn( 'ShiftTaskMap addedValueCredits for id 1 updated to 2.0.', res['msg'] )

    def test_deleteTaskOK ( self ):
        res = self.callAndCheckPostNoLogin( endPointName='api.deleteTask',
                                            data={ 'year' : self.selYear, 'target' : None, 'taskCode' : 'del0' },
                                            jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        log.info ('status: %s' % res [ 'status' ])

    def test_deleteTaskFail0( self ) :
        # this should fail as the neededWork for this task is != 0
        res = self.callAndCheckPostNoLogin( endPointName='api.deleteTask',
                                            data={ 'year' : self.selYear, 'target' : None, 'taskCode' : 'del1' },
                                            jsonKey='status', jsonLen=1, retCodeExp=400, followRedir=True )
        # log.info( 'status: %s' % res[ 'status' ] )

    def test_deleteTaskFail1( self ) :
        # this should fail as there are still pledges for this task
        res = self.callAndCheckPostNoLogin( endPointName='api.deleteTask',
                                            data={ 'year' : self.selYear, 'target' : None, 'taskCode' : 'del2' },
                                            jsonKey='status', jsonLen=1, retCodeExp=400, followRedir=True )
        log.info( 'test_deleteTaskFail1> status: %s' % str(res) )
        self.assertIn( 'ERROR can not delete task td2, found 1 non-rejected pledges ', res[ 'status' ] )

    def test_deleteActOK( self ) :

        res = self.callAndCheckPostNoLogin( endPointName='api.deleteActivity',
                                            data={ 'year' : self.selYear, 'target' : None, 'actCode' : 'ad1' },
                                            jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )

        log.info( 'test_deleteActOK> status: %s' % str(res) )
        self.assertIn( 'Activity adel1 (%s) successfully deleted' % testYear, res[ 'status' ] )

