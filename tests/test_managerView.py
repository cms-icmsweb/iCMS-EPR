#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import re

from sqlalchemy import func

from .BaseTestClass import *

log = logging.getLogger( "Testing.test_viewPledges" )
logging.basicConfig( stream=sys.stderr )
logging.getLogger( "Testing.test_viewPledges" ).setLevel( logging.DEBUG )

class PledgesTestCase ( BaseTestCase ):

    def test_overview(self):

        u1 = db.session.query(EprUser).filter_by(username='user1').one()
        self.assertFalse(u1.is_administrator())

        self.login(u1.username)

        # access the main summary page for all projects
        response = self.client.get( url_for('main.showProjectTasks', id=0), follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(getHtmlDoc( response )) )

        self.assertEqual(response.status_code, 200)

        htmlDoc = getHtmlDoc( response )
        scriptStart = htmlDoc.find('function flask_moment_render(')
        self.assertNotIn( 'div class="alert alert-warning"', htmlDoc[:scriptStart] )
        self.assertIn( 'iCMS - EPR Tasks/Activities for  all projects  in', getHtmlDoc( response ))

        # ensure we have the three tabs:
        self.assertIn( 'data-toggle="tab">Tasks     </a>', getHtmlDoc( response ) )
        self.assertIn( 'data-toggle="tab">Activities</a>', getHtmlDoc( response ) )
        self.assertIn( 'data-toggle="tab">Projects  </a>', getHtmlDoc( response ) )

        self.logout()

    def test_showProjectInstCountry(self):

        u1 = db.session.query(EprUser).filter_by(username='user1').one()
        self.assertFalse(u1.is_administrator())

        self.login(u1.username)

        # access the main summary page for all projects
        response = self.client.get( url_for('main.showProjectInstCountry', projId=1), follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(getHtmlDoc( response )) )

        self.assertEqual(response.status_code, 200)

        self.assertIn( 'Contributions to pr1', getHtmlDoc( response ) )

    def test_newPledgePost(self):

        u1 = db.session.query(EprUser).filter_by(username='user1').one()
        self.assertFalse(u1.is_administrator())

        self.login(u1.username)

        # make a new pledge via a post request - no tasks, should fail
        response = self.client.post( url_for('main.newPledgePost'),
                                     data = { 'cmsId' : 9900,
                                              'workTimePld': 1.,
                                              'taskCode' : 't3' },
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("\n[0] got response data '%s' " % str(response.data) )
        self.assertEqual( '/makeNewPledge', getHtmlDoc( response ) )

        # make a new pledge via a post request - no cmsId, should work with id from current_user
        response = self.client.post( url_for('main.newPledgePost'),
                                     data = { 'cms_id' : 9900,
                                              'tasks' : 't3' },
                                     follow_redirects=True )
        # log.info("\n[1] got response data %s " % str(getHtmlDoc( response )) )
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'Creating New Pledge', getHtmlDoc( response ) )
        self.assertIn( 'Project/LV1  :</b> pr2', getHtmlDoc( response ) )
        self.assertIn( 'Activity/LV2 :</b> ac2', getHtmlDoc( response ) )
        self.assertIn( 'LV3  :</b> None', getHtmlDoc( response ) )
        self.assertIn( 'Task     :</b> ta3', getHtmlDoc( response ) )


    def test_splitPledge( self ) :

        ap = db.session.query(EprUser).filter_by(username='pfeiffer').one()
        self.assertTrue(ap.is_wizard())

        self.login(ap.username)

        pl = db.session.query(Pledge).filter_by(code='7+7+3').one()
        newInst = db.session.query(EprInstitute).filter_by(code='FNAL').one()
        newInstWork = 0.7 * pl.workTimePld
        log.info('test_splitPledge> split with %s for %s ' % (newInstWork, newInst.code))
        response = self.client.post( url_for('main.splitPledge', plCode=pl.code),
                                     data = { 'newInst' : newInst.code, 'newInstWork' : newInstWork },
                                     follow_redirects=True )
        # log.info("\n[1] got response data %s " % str(response.data) )
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'pledge %s successfully split (%.1f to %s)' % (pl.code, newInstWork, newInst.code), getHtmlDoc( response ) )

    def test_splitPledgeFail1( self ) :

        pl = db.session.query(Pledge).filter_by(code='7+7+3').one()
        newInst = db.session.query(EprInstitute).filter_by(code='FNAL').one()
        newInstWork = 0.7 * pl.workTimePld
        log.info('test_splitPledge> split with %s for %s ' % (newInstWork, newInst.code))
        response = self.client.post( url_for('main.splitPledge', plCode=pl.code),
                                     data = { 'newInst' : newInst.code, 'newInstWork' : newInstWork },
                                     follow_redirects=True )
        # log.info("\n[1] got response data %s " % str(getHtmlDoc( response )) )
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'You (tester) are not allowed to split pledges, sorry.', getHtmlDoc( response ) )

    def test_splitPledgeFail2( self ) :

        newInst = db.session.query(EprInstitute).filter_by(code='FNAL').one()
        newInstWork = 0.999
        log.info('test_splitPledge> split with %s for %s ' % (newInstWork, newInst.code))
        response = self.client.post( url_for('main.splitPledge', plCode='9999'),
                                     data = { 'newInst' : newInst.code, 'newInstWork' : newInstWork },
                                     follow_redirects=True )
        # log.info("\n[1] got response data %s " % str(getHtmlDoc( response )) )
        self.assertEqual(response.status_code, 200)
        self.assertRegexpMatches( getHtmlDoc( response ), re.compile('iCMS - EPR Pledge Overview\s+for Tester, Master\s+in %s' % self.selYear) )

class TaskTestCase ( BaseTestCase ):

    def getQTparams(self, neededWork=1.):
        # level3Name = ('qtLvl3','qtLvl3')
        return { 'name' : 'testQT',
                 'description' : 'testQualTask description ... ',
                 'pctAtCERN' : '10',
                 'tType' : 'Qualification',
                 'locked' : False,
                 'comment' : 'no comment',
                #  'level3' : level3Name,
                 'shiftTypeId' : '',
                 'kind': 'CORE',
                 'neededWork' : neededWork
        }

    def test_makeNewQualificationTask(self):
        user = db.session.query(EprUser).filter_by(username='p1m1').one()
        self.assertFalse( user.is_administrator() )

        self.login(user.username)

        actCode = 'a5'
        params = self.getQTparams()
        response = self.client.post( url_for('main.newTask', actCode=actCode), 
                                     data = params,
                                     follow_redirects=True )
        self.assertIn('A new task was successfully created.', getHtmlDoc( response ) )
        # log.info( response.data[:10000] )

        # clean
        t0 = db.session.query(Task).filter_by(name=params['name']).one()
        db.session.delete(t0)
        db.session.commit()

    def test_makeNewQualificationTask_Fail(self):
        user = db.session.query(EprUser).filter_by(username='p1m1').one()
        self.assertFalse( user.is_administrator() )

        self.login(user.username)

        actCode = 'a5'
        params = self.getQTparams(neededWork=5.)
        response = self.client.post( url_for('main.newTask', actCode=actCode), 
                                     data = params,
                                     follow_redirects=True )
        self.assertIn('ERROR - requested amount of needed work for Qualification Task on top of actual sum (6.0) exceeds threshold for project 2.', getHtmlDoc( response ) )
        self.assertNotIn('A new task was successfully created.', getHtmlDoc( response ) )
        # log.info( response.data[:5000] )

class TaskEditTestCase ( BaseTestCase ):

    def test_makeNewTask(self):
        user = db.session.query(EprUser).filter_by(username='p1m1').one()
        self.assertFalse( user.is_administrator() )

        self.login(user.username)

        response = self.client.get( url_for('main.newTask', actCode='None'), follow_redirects=True )
        self.assertIn('Please select project ...', getHtmlDoc( response ) )
        # log.info( response.data )

    def test_editTask(self):

        user = db.session.query(EprUser).filter_by( username='a3m1' ).one( )
        self.assertFalse( user.is_administrator( ) )
        self.login( user.username )

        response = self.client.get( url_for( 'main.task', id=3 ),
                                     follow_redirects=True )

        self.assertIn( 'Task: task 3 (a2 p2)', getHtmlDoc( response ) )
        # here we should NOT see the field for needed work in the form:
        self.assertNotIn( ' <div class="form-group  required"> <label for="neededWork">', getHtmlDoc( response ) )
        # log.info( response.data )
        # check that neededWork is NOT changed ...

    def test_editTaskAdmin( self ) :
        user = db.session.query(EprUser).filter_by( username='pfeiffer' ).one( )
        self.assertTrue( user.is_wizard( ) )
        self.login( user.username )

        response = self.client.get( url_for( 'main.task', id=3 ),
                                    follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        self.assertIn( 'Task: task 3 (a2 p2)', getHtmlDoc( response ) )
        # here we should see the field for needed work in the form:
        self.assertIn( ' <div class="form-group  required"> <label for="neededWork">', getHtmlDoc( response ) )
        # log.debug( "got response data %s " % str( getHtmlDoc( response ) ) )

        # edit the task, but "cancel" immediately
        task = db.session.query(Task).filter_by(id=3).one()
        response = self.client.post( url_for( 'main.task', id=task.id ),
                                     data={ 'subCan' : 'Cancel', # cancel change
                                            'kind' : 'CORE',
                                            'neededWork' : task.neededWork,
                                            'name' : task.name,
                                            'description' : task.description,
                                            'level3': 1,
                                            },
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )
        # log.debug( "got response data %s " % str( response.data ) )
        self.assertIn( 'Updating task was cancelled ... ', getHtmlDoc( response ) )

        # now edit the task: change the name
        task = db.session.query(Task).filter_by( id=3 ).one( )
        response = self.client.post( url_for( 'main.task', id=task.id ),
                                     data={ 'subCan' : 'Submit',
                                            'name' : task.name+' - updated',
                                            'kind' : 'CORE',
                                            'neededWork' : task.neededWork,
                                            'description' : task.description,
                                            'level3': 1,
                                            },
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )
        # log.debug( "got response data %s " % str( response.data ) )
        # self.assertIn( 'WARNING: kind of task can not be changed, will keep it at: CORE', response.data )
        self.assertIn( 'Task info successfully updated.', getHtmlDoc( response ) )


class MakeActivityTestCase ( BaseTestCase ):

    def test_makeNewActivityFail(self):
        user = db.session.query(EprUser).filter_by(username='user1').one()
        self.assertFalse( user.is_administrator() )

        self.login(user.username)

        # in order to trigger the "not allowed", we need to come here with a
        # form to create a real new activity for an existing project ...
        response = self.client.post( url_for('main.newActivity', projCode='p3'),
                                     data = { 'name' : 'newAct',
                                              'description' : 'new Activity ',
                                              'year' : self.selYear,
                                              'code' : 'newAct-code',
                                              'proj': 2, # pr3
                                             },
                                     follow_redirects=True )
        self.assertNotIn('iCMS - EPR Select Project', getHtmlDoc( response ))
        self.assertIn('Forbidden', getHtmlDoc( response ))

        # log.info( response.data )

    def test_makeNewActivity(self):
        user = db.session.query(EprUser).filter_by(username='p1m1').one()
        self.assertFalse( user.is_administrator() )

        self.login(user.username)

        response = self.client.get( url_for('main.newActivity', projCode='None'), follow_redirects=True )
        self.assertIn('iCMS - EPR Select Project', getHtmlDoc( response ))
        # log.info( response.data )

