
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from .BaseTestClass import *

log = logging.getLogger( "Testing.test_viewPledges" )
logging.basicConfig( stream=sys.stderr )
logging.getLogger( "Testing.test_viewPledges" ).setLevel( logging.DEBUG )

class AdminTestCase ( BaseTestCase ):

    def test_makeProjectFail(self):

        # needs admin
        user = db.session.query(EprUser).filter_by(username='p1m1').one()
        self.assertFalse( user.is_administrator() )

        self.login(user.username)

        response = self.client.get( url_for('main.newProject'), follow_redirects=True )
        self.assertEqual(response.status_code, 403)
        self.assertNotIn('Creating New Project', response.data.decode('utf-8'))
        self.assertIn('Forbidden', response.data.decode('utf-8'))
        # log.info( response.data.decode('utf-8') )

    def test_makeProject( self ) :
        # needs admin
        user = db.session.query(EprUser).filter_by( username='pfeiffer' ).one( )
        self.assertTrue( user.is_wizard( ) )

        self.login( user.username )

        response = self.client.get( url_for( 'main.newProject' ), follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'Creating New Project', response.data.decode('utf-8') )
        # log.info( response.data.decode('utf-8') )

    def test_makeProjectForm( self ) :
        # needs admin
        user = db.session.query(EprUser).filter_by( username='pfeiffer' ).one( )
        self.assertTrue( user.is_wizard( ) )

        self.login( user.username )

        response = self.client.post( url_for( 'main.newProject' ),
                                     data = { 'name' : 'newProj',
                                              'description' : 'New project for testing',
                                              'mgr' : None
                                            },
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'Creating New Project', response.data.decode('utf-8') )
        # log.info( response.data.decode('utf-8') )


    def test_approvePledgeByCode( self ) :
        user = db.session.query(EprUser).filter_by( username='pfeiffer' ).one( )
        self.assertTrue( user.is_wizard( ) )

        self.login( user.username )

        plCode = '2+5+1'
        pl0 = db.session.query(Pledge).filter_by(code=plCode).one()

        # accept the new pledge ..
        response = self.client.post( url_for( 'main.approvePledgeByCode', code=pl0.code.replace('+',':') ),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'Pledge %s approved' % plCode.replace('+', ':'), str(response.data.decode('utf-8')) )

        # ... and set it to done as well
        response = self.client.post( url_for( 'main.approvePledgeByCode', code=pl0.code.replace('+',':') ),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        self.assertIn( 'Pledge %s set to done with 1.200000 done' % plCode, str(response.data.decode('utf-8')) )

