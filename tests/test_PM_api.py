
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from .BaseTestClass import *

log = logging.getLogger( "Testing.test_PM_api" )
log.setLevel(logging.DEBUG)

class ApiPmTestCase(BaseTestCase):

    def setUp(self):
        BaseTestCase.setUp( self )

        self.defaultUser = 'ap'
        self.login( username= 'p2m1' )

    # def tearDown(self):
    #     self.app_context.pop()

    def callAndCheckPostNoLogin( self, endPointName, jsonKey, jsonLen, data, retCodeExp=200, **kwargs ) :

        if data:
            response = self.client.post( url_for( endPointName, **kwargs ), data=data, follow_redirects=True )
        else :
            response = self.client.post( url_for( endPointName, **kwargs ), follow_redirects=True )

        self.assertEqual( response.status_code, retCodeExp )

        # log.info( 'callAndCheckPostNoLogin 1> response: %s' % response.data )
        json_response = json.loads( response.data.decode( 'utf-8' ) )
        if jsonKey :
            self.assertGreaterEqual( len( json_response[ jsonKey ] ), jsonLen )
        else :  # only a list is returned
            self.assertGreaterEqual( len( json_response ), jsonLen )

        return json_response

    def test_udpateManagerFail1(self):

        self.logout()
        self.login( username= 'pfeiffer' )

        data = { 'itemType' : 'activity',
                 'itemCode' : 'a3',
                 'userid'   : 'a', # illegal value ...
                 'role' : '',
                 'status' : '' }

        # get managers for one project, one act, and one task
        response = self.callAndCheckPost( endPointName='api.updateManager',
                              data=data, id=2,
                              jsonKey=None, jsonLen=0 )
        log.info( 'test_udpateManager> response: %s' % response['msg'] )
        self.assertIn( 'Error updating manager for ', response['msg'] )

    def test_udpateManagerFail2(self):

        self.logout()
        self.login( username= 'pfeiffer' )

        data = { 'itemType' : 'activity',
                 'itemCode' : 'a3',
                 'userid'   : 42, # does not exist
                 'role' : '',
                 'status' : '' }

        # get managers for one project, one act, and one task
        response = self.callAndCheckPost( endPointName='api.updateManager',
                              data=data, id=2,
                              jsonKey=None, jsonLen=0 )
        log.info( 'test_udpateManager> response: %s' % response['msg'] )
        self.assertIn( 'Error updating manager for ', response['msg'] )


    def test_udpateManager( self ) :
        self.logout( )
        self.login( username='pfeiffer' )

        userId = db.session.query(EprUser).filter(EprUser.username == 'a3m1').one().id
        data = { 'itemType' : 'activity',
                 'itemCode' : 'a3',
                 'userid'   : userId,
                 'role'     : Permission.MANAGEACT,
                 'status'   : 'deleted' }

        response = self.callAndCheckPost( endPointName='api.updateManager',
                                         data=data,
                                         jsonKey=None, jsonLen=0 )
        log.info( 'test_udpateManager> response: %s' % response[ 'msg'] )
        self.assertNotIn( 'Error updating manager for ', response[ 'msg' ] )
        self.assertIn( 'manager for activity:a3:%d successfully updated to role 4 status deleted' % userId, response[ 'msg' ] )

        self.logout( )
        self.login( username='pfeiffer' )

        data['status'] = 'active'
        response = self.callAndCheckPost( endPointName='api.updateManager',
                                         data=data,
                                         jsonKey=None, jsonLen=0, follow_redirects=True )
        log.info( 'test_udpateManager> response: %s' % response[ 'msg'] )
        self.assertNotIn( 'Error updating manager for ', response[ 'msg' ] )
        self.assertIn( 'manager for activity:a3:%d successfully updated to role 4 status active' % userId, response[ 'msg' ] )

    def test_getShiftTaskMap(self):
        res = self.callAndCheckGet( endPointName='api.getShiftTaskMap',
                                     data=None,
                                     jsonKey='result', jsonLen=1,
                                     name='Tester, Shifter' )
        # log.info( "test_getShiftTaskMap> got: %s " % str(res) )
        log.info( "test_getShiftTaskMap> got: %s " % json.dumps(res, sort_keys=True, indent=4) )
        self.assertIn( 'OK', res[ 'result' ] )
        resData = res['data'][0]
        self.assertEqual( len(resData['stm']), 2 )

        # self.assertDictEqual( {u'subSystem': u'pr2', u'shiftType': u'sType1', u'flavourName': u'main', u'id': 1, u'weightScale': 1.0}, resData['stm'][0] )
        self.assertDictEqual( {u'subSystem': u'pr1', u'shiftType': u'sType0', u'flavourName': u'main', u'id': 2, u'weightScale': 1.0}, resData['stm'][0] )

        self.assertDictEqual( {u"code": u"ts1", u"name": u"dbShift", u"shiftTypeId": u"on-call expert shift", u"tType": u"Perennial"}, resData['task'] )

    def test_getShiftEntities(self):
        res = self.callAndCheckPost( endPointName='api.getShiftEntities',
                                     data={ 'year' : self.selYear, 'subSystems' : ['pr1', 'pr2']},
                                     jsonKey='result', jsonLen=1,
                                     name='Tester, Shifter' )
        log.info( "test_getShiftEntities> got: %s " % str(res) )
        # log.info( "test_getShiftEntities> got: %s " % json.dumps(res, sort_keys=True, indent=4) )
        self.assertIn( 'OK', res[ 'result' ] )
        resData = res['data']
        self.assertEqual( len(resData['pr1']), 6 )
        self.assertListEqual( ["main", "sType0"], resData['pr1'][0])

    def test_deleteTaskOK ( self ):
        res = self.callAndCheckPostNoLogin( endPointName='api.deleteTask',
                                            data={ 'year' : self.selYear, 'target' : None, 'taskCode' : 'del0' },
                                            jsonKey='status', jsonLen=1, retCodeExp=200 )
        log.info ('status: %s' % res [ 'status' ])

    def test_deleteTaskFail0( self ) :
        res = self.callAndCheckPostNoLogin( endPointName='api.deleteTask',
                                            data={ 'year' : self.selYear, 'target' : None, 'taskCode' : 'del1' },
                                            jsonKey='status', jsonLen=1, retCodeExp=400 )
        log.info( 'status: %s' % res[ 'status' ] )

    def test_deleteTaskFail1( self ) :
        res = self.callAndCheckPostNoLogin( endPointName='api.deleteTask',
                                            data={ 'year' : self.selYear, 'target' : None, 'taskCode' : 'del2' },
                                            jsonKey='status', jsonLen=1, retCodeExp=400 )
        log.info( 'status: %s' % res[ 'status' ] )

