
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import re

try:
    from .BaseTestClass import *
except ImportError:
    from BaseTestClass import *

from sqlalchemy.exc import IntegrityError

from app import db

logging.basicConfig( stream=sys.stderr )
log = logging.getLogger( "Testing.test_logic" )
log.setLevel( logging.DEBUG )

def addAndCommit( what ):

    db.session.add( what )
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()

    return

def getProj1Data(pName='p1n'):
    return { 'name' : pName,
             'description' : 'Project '+pName,
             'mgr' : None
            }

def getAct1Data(aName='a1n', pId=3, year=None):
    return { 'name' : aName,
             'description' : 'Activity '+aName,
             'year' : year,
             'code' : aName+'-code',
             'proj': pId,
            }

def getTask1Data(tName='t1new', tDesc='task 1 (a1) new'):
    return { "name" : tName,
             "description" : tDesc,
             "activityId" : 1,
             "pctAtCERN" : 10,
             "tType" : 'Perennial',
             "shiftTypeId" : '',
             "comment" : "no comment",
             "locked" : 'false',
             "kind" : 'CORE', 
             'level3': '1 : Unknown',
             }

def getPledge1AdminData(user, inst=None, tId=None, pl=1., wta = 0., wtd = 0., status='new', selYear=None):

    if not tId:
        tId = db.session.query(Task).filter_by(name='ta1', year=selYear).one().id

    if not inst:
        inst = db.session.query(EprInstitute).filter_by(id=user.mainInst, year=selYear).one()

    return { "taskId" : tId,
             "workTimePld" : pl,
             "workedSoFar" : 0.,
             # make sure the userId field looks like the one from the search:
             "userId" : '%s (CERNid: %i)' % (user.name, user.hrId),
             "instId" : inst.code,
             "workTimeAcc" : wta,
             "workTimeDone" : wtd,
             "status" : status,
             "year" : 2015,
             }

def getPledge1EprUserData(user, tId = 1, pl=1., wsf=0.):
    inst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()
    return { "taskId" : tId,
             "workTimePld" : pl,
             "workedSoFar" : wsf,
             # make sure the userId field looks like the one from the search:
             "userId" : '%s (CERNid: %i)' % (user.name, user.hrId),
             "instId" : inst.code,
             }

def getPledgeQualTaskData(user, tId = 1, pl=1., wsf=0.):
    inst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()
    return { "taskId" : tId,
             "workTimePld" : pl,
             "workedSoFar" : wsf,
             # make sure the userId field looks like the one from the search:
             "userId" : '%s (CERNid: %i)' % (user.name, user.hrId),
             "instId" : inst.code,
             }


class LogicTestCase( BaseTestCase ) :

    def getEprUser(self, name):
        u1 = db.session.query(EprUser).filter_by(username=name).one()
        self.assertIsNotNone(u1)
        # self.assertTrue(u1.can(Permission.READ))
        return u1

    def createTask(self, tName='t1new', tDesc='task 1 (a1) new', need=12):

        a1m1 = self.getEprUser('a1m1')
        # check specifics for act-admin
        self.assertFalse(a1m1.canManage('project', 'p1', year=self.selYear))
        self.assertTrue(a1m1.canManage('activity', 'a1', year=self.selYear))

        # admin creates task ...
        response = self.login(a1m1.username)
        self.assertEqual(response.status_code, 200)
        response = self.client.post( url_for('main.newTask', actCode='a1'),
                                     data = getTask1Data(tName, tDesc),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('A new task was successfully created.', response.data.decode('utf-8'))
        # self.assertIn('iCMS - EPR New Task  for', response.data.decode('utf-8'))

        return

    def createTaskFail0( self, tName='t1new', tDesc='task 1 (a1) new', need=12 ) :
        # create a new task which already exists in the DB
        a1m1 = self.getEprUser( 'a1m1' )
        # check specifics for act-admin
        self.assertFalse( a1m1.canManage( 'project', 'p1', year=self.selYear ) )
        self.assertTrue( a1m1.canManage( 'activity', 'a1', year=self.selYear ) )

        taskData = getTask1Data( tName, tDesc )
        taskData['name'] = 'ta1'

        # admin creates task ...
        response = self.login( a1m1.username )
        self.assertEqual( response.status_code, 200 )
        response = self.client.post( url_for( 'main.newTask', actCode='a1' ),
                                     data=taskData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'ERROR - Integrity error from database, the name of the task is already used in this activity.', response.data.decode( 'utf-8' ) )
        # self.assertIn('iCMS - EPR New Task  for', response.data.decode('utf-8'))

        return

    def createTaskFail1( self, tName='t1new', tDesc='task 1 (a1) new', need=1200 ) :
        # create a new task which exceeds the ceiling for the project
        a1m1 = self.getEprUser( 'a1m1' )
        # check specifics for act-admin
        self.assertFalse( a1m1.canManage( 'project', 'p1', year=self.selYear ) )
        self.assertTrue( a1m1.canManage( 'activity', 'a1', year=self.selYear ) )

        taskData = getTask1Data( tName, tDesc )
        taskData['neededWork'] = need

        # admin creates task ...
        response = self.login( a1m1.username )
        self.assertEqual( response.status_code, 200 )
        response = self.client.post( url_for( 'main.newTask', actCode='a1' ),
                                     data=taskData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'ERROR - requested amount of needed work (%s) on top of actual sum (%s) exceeds ceiling for project', response.data.decode( 'utf-8' ) )
        # self.assertIn('iCMS - EPR New Task  for', response.data.decode('utf-8'))

        return

    def test_qualTask(self):
        appl = db.session.query(EprUser).filter_by(username='appl').one()
        
        response = self.login(appl.username)
        self.assertEqual(response.status_code, 200)

        qualTask = db.session.query(Task).filter(Task.code == 'qta5p5').one()

        plUser = appl
        plData = getPledgeQualTaskData(plUser, tId=qualTask.id)
        # plData.update( { "status": ['new', 'New'], 'workTimePld': 1. } )
        log.info("test_qualTask> pledge data %s " %  plData )

        response = self.client.post(
            url_for('main.newPledge', taskCode=qualTask.code),
            data = plData,
            follow_redirects=True
        )

        page = str(response.data[10000:30000].decode('utf-8'))
        # log.info("got response data %s " %  page )

        self.assertIn( 'A new pledge was successfully created for user appl, task qta5p5.', page)
        self.assertIn('iCMS - EPR Pledge Overview', page )

    def test_qualTaskFailNoAppli(self):

        appl = db.session.query(EprUser).filter_by(username='p1m1').one()
        
        response = self.login(appl.username)
        self.assertEqual(response.status_code, 200)

        qualTask = db.session.query(Task).filter(Task.code == 'qta5p5').one()

        plUser = appl
        plData = getPledgeQualTaskData(plUser, tId=qualTask.id)
        plData.update( { "status": ['new', 'New'], 'workTimePld': 1. } )
        log.info("test_qualTask> pledge data %s " %  plData )

        response = self.client.post(
            url_for('main.newPledge', taskCode=qualTask.code),
            data = plData,
            follow_redirects=True
        )

        page = str(response.data[10000:30000].decode('utf-8'))
        # log.info("got response data %s " %  page )

        self.assertIn( 'Pledges for Qualification tasks can only be created for people who are applicants, user p1m1 is not an applicant.', page)
        self.assertIn('Creating New Pledge in ', page )
        self.assertNotIn( 'A new pledge was successfully created for user appl, task qta5p5.', page)
        self.assertNotIn('iCMS - EPR Pledge Overview', page )

    def test_login_logout(self):

        # make sure all users in the TestDB can login properly
        allEprUsers = db.session.query(EprUser).all()
        for u in allEprUsers:
            self.assertIsNotNone(u)
            response = self.login(u.username, 'fffffff')
            self.assertEqual(response.status_code, 200)
            self.assertIn('iCMS - EPR Pledge Overview', response.data[:1000].decode('utf-8'))

            response = self.logout()
            self.assertEqual(response.status_code, 302)

    def test_stdEprUserPerms(self):

        u1 = db.session.query(EprUser).filter_by(username='user1').one()
        # self.assertTrue(u1.can(Permission.READ))
        self.assertFalse(u1.is_administrator())

    def test_projAdminPerms(self):

        p1m1 = db.session.query(EprUser).filter_by(username='p1m1').one()

        self.assertTrue(p1m1.canManage('project' , 'p1', year=self.selYear))
        self.assertTrue(p1m1.canManage('activity', 'a1', year=self.selYear))
        self.assertTrue(p1m1.canManage('task'    , 't1', year=self.selYear))

        self.assertFalse(p1m1.is_administrator())

    def test_stdEprUserCheckUserChangeCreatePledge(self):

        pm = db.session.query(EprUser).filter_by(username='p1m1').one() # PM is from CERN
        response = self.login(pm.username)
        self.assertEqual(response.status_code, 200)

        # access the main view page
        response = self.client.get(
            url_for('main.index'),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

        plUser = db.session.query(EprUser).filter_by(username='userEth5').one() # userEth5 is from ETHZ
        plData = getPledge1EprUserData(plUser)
        plData.update( { "status": ['new', 'New'], 'workTimePld': 1. } )
        log.info("test_PMcheckUserChangeCreatePledge> pledge data %s " %  plData )

        response = self.client.post(
            url_for('main.newPledge', taskCode='t1'),
            data = plData,
            follow_redirects=False
        )

        page = str(response.data[10000:30000].decode('utf-8'))
        # log.info("got response data %s " %  page )

        self.assertNotIn('<div id="chg-user-dialog-form" class="ui-front" title="Change user" hidden>', page)
        self.assertNotIn('<div id="chg-inst-dialog-form" class="ui-front" title="Change institute" hidden>', page)

        # remove the pledge again ...
        p0 = db.session.query(Pledge)\
                        .filter( Pledge.taskId == plData['taskId'])\
                        .filter( Pledge.userId == plUser.id )\
                        .filter( Pledge.instId == plUser.mainInst)\
                        .one()
        db.session.delete( p0 )
        db.session.commit()


    def test_PMcheckUserChangeCreatePledge(self):

        pm = db.session.query(EprUser).filter_by(username='p1m1').one() # PM is from CERN
        response = self.login(pm.username)
        self.assertEqual(response.status_code, 200)

        # access the main view page
        response = self.client.get(
            url_for('main.index'),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

        plUser = db.session.query(EprUser).filter_by(username='userEth5').one() # userEth5 is from ETHZ
        plData = getPledge1EprUserData(plUser)
        # plData.update( { "status": ['new', 'New'], 'workTimePld': 1. } )
        log.info("test_PMcheckUserChangeCreatePledge> pledge data %s " %  plData )

        response = self.client.post(
            url_for('main.newPledge', taskCode='t1'),
            data = plData,
            follow_redirects=True
        )

        page = str(response.data[10000:30000].decode('utf-8'))
        # log.info("got response data %s " %  page )

        # self.assertIn('iCMS - EPR Pledge Overview', page )
        self.assertIn('<div id="chg-user-dialog-form" class="ui-front" title="Change user" hidden>', page)
        self.assertIn('<div id="chg-inst-dialog-form" class="ui-front" title="Change institute" hidden>', page)

    def test_PMcreatePledge(self):

        pm = db.session.query(EprUser).filter_by(username='p1m1').one() # PM is from CERN
        response = self.login(pm.username)
        self.assertEqual(response.status_code, 200)

        # access the main view page
        response = self.client.get(
            url_for('main.index'),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))

        plUser = db.session.query(EprUser).filter_by(username='userEth5').one() # userEth5 is from ETHZ
        plData = getPledge1EprUserData(plUser)
        plData.update( { "status": ['new', 'New'], 'workTimePld': 1. } )
        log.info("test_PMcreatePledge> pledge data %s " %  plData )

        response = self.client.post(
            url_for('main.newPledge', taskCode='t1'),
            data = plData,
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

        page = str(response.data[10000:30000].decode('utf-8'))
        # log.info("got response data %s " %  page )
        self.assertIn('iCMS - EPR Pledge Overview', page)
        self.assertIn('A new pledge was successfully created for user userEth5, task ta1.', page)
        self.assertNotIn('user is admin', page)

        # remove the pledge again ...
        p0 = db.session.query(Pledge)\
                        .filter( Pledge.taskId == plData['taskId'])\
                        .filter( Pledge.userId == plUser.id )\
                        .filter( Pledge.instId == plUser.mainInst)\
                        .one()
        db.session.delete( p0 )
        db.session.commit()

    def test_AMcreatePledge(self):

        pm = db.session.query(EprUser).filter_by(username='a4m1').one() # AM is from CERN
        response = self.login(pm.username)
        self.assertEqual(response.status_code, 200)

        # access the main view page
        response = self.client.get(
            url_for('main.index'),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))

        plUser = db.session.query(EprUser).filter_by(username='userEth4').one() # userEth4 is from ETHZ
        plData = getPledge1EprUserData(plUser)
        log.info("test_AMcreatePledge> pledge data %s " %  plData )
        response = self.client.post(
            url_for('main.newPledge', taskCode='t1'),
            data = plData,
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

        page = str(response.data[10000:30000].decode('utf-8'))
        # log.info("got response data %s " %  page )
        self.assertIn('A new pledge was successfully created for user userEth4, task ta1.', page)
        self.assertIn('CMS - EPR Pledge Overview', page)
        self.assertNotIn('user is admin', page)

        # remove the pledge again ...
        p0 = db.session.query(Pledge)\
                        .filter( Pledge.taskId == plData['taskId'])\
                        .filter( Pledge.userId == plUser.id )\
                        .filter( Pledge.instId == plUser.mainInst)\
                        .one()
        db.session.delete( p0 )
        db.session.commit()

    def test_stdEprUserAccess(self):

        user = db.session.query(EprUser).filter_by(username='user4').one()
        self.assertIsNotNone(user)

        response = self.login(user.username)
        self.assertEqual(response.status_code, 200)

        # access the main view page
        response = self.client.get(
            url_for('main.index'),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))

        # try to create a project, activity, Task - these should all fail ...
        response = self.client.post(
            url_for('main.newProject'),
            data = getProj1Data(),
            follow_redirects=True
        )
        self.assertNotEqual(response.status_code, 200)
        self.assertIn('Forbidden', response.data.decode('utf-8'))

        response = self.client.post(
            url_for('main.newActivity', projCode='p1'),
            data = getAct1Data(year=self.selYear),
            follow_redirects=True
        )
        self.assertNotEqual(response.status_code, 200)
        self.assertIn('Forbidden', response.data.decode('utf-8'))

        response = self.client.post(
            url_for('main.newTask', actCode='a1'),
            data = getTask1Data(),
            follow_redirects=True
        )
        self.assertNotEqual(response.status_code, 200)
        self.assertIn('Forbidden', response.data.decode('utf-8'))

        # create a pledge for task t1 - that should work
        response = self.client.post(
            url_for('main.newPledge', taskCode='t1'),
            data = getPledge1EprUserData(user),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('A new pledge was successfully created for user', response.data.decode('utf-8'))
        self.assertIn('CMS - EPR Pledge Overview', response.data.decode('utf-8'))
        self.assertNotIn('user is admin', response.data.decode('utf-8'))

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )


    def test_stdEprUserPledgeLockedTaskFail(self):

        user = db.session.query(EprUser).filter_by(username='user4').one()
        self.assertIsNotNone(user)

        response = self.login(user.username)
        self.assertEqual(response.status_code, 200)

        # create a pledge for task t6l - that should fail
        response = self.client.post(
            url_for('main.newPledge', taskCode='t6l'),
            data = getPledge1EprUserData(user),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('Pledges for locked tasks are not allowed.', response.data.decode('utf-8'))
        self.assertIn('CMS - EPR Pledge Overview', response.data.decode('utf-8'))
        self.assertNotIn('user is admin', response.data.decode('utf-8'))


    def test_adminEprUserPledgeLockedTaskOK(self):

        user = db.session.query(EprUser).filter_by(username='p2m1').one()
        self.assertIsNotNone(user)
        self.assertTrue(user.canManage('project' , 'p2', year=self.selYear))

        response = self.login(user.username)
        self.assertEqual(response.status_code, 200)

        t6l = db.session.query(Task).filter_by(code='t6l').one()
        self.assertIsNotNone( t6l )

        # create a pledge for task t6l - that should fail
        plData = getPledge1AdminData(user, tId=t6l.id, selYear=self.selYear)
        response = self.client.post(
            url_for('main.newPledge', taskCode='t6l'),
            data = plData,
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

        self.assertNotIn('Pledges for locked tasks are not allowed.', response.data.decode('utf-8'))
        self.assertIn('A new pledge was successfully created for user p2m1', response.data.decode('utf-8'))
        self.assertIn('CMS - EPR Pledge Overview', response.data.decode('utf-8'))

    def test_projAdminAccess(self):

        log= logging.getLogger( "Testing.test_logic" )

        p1m1 = db.session.query(EprUser).filter_by(username='p1m1').one()
        self.assertIsNotNone(p1m1)
        self.assertTrue(p1m1.canManage('project' , 'p1', year=self.selYear))
        self.assertTrue(p1m1.canManage('activity', 'a1', year=self.selYear))
        self.assertTrue(p1m1.canManage('task'    , 't1', year=self.selYear))

        response = self.login(p1m1.username)
        self.assertEqual(response.status_code, 200)
        # log.info( "\n" + str( response.data.decode('utf-8') ) )

        # access the main view page
        response = self.client.get(
            url_for('main.index'),
            follow_redirects=True
        )
        self.assertEqual(response.status_code,200)
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))

        # try to create a project - this should fail ...
        response = self.client.post(
            url_for('main.newProject'),
            data = getProj1Data(),
            follow_redirects=True
        )
        self.assertNotEqual(response.status_code, 200)
        self.assertIn('Forbidden', response.data.decode('utf-8'))

        # try to create an activity - this should be OK for a ProjAdmin
        response = self.client.post(
            url_for('main.newActivity', projCode='p1'),
            data = getAct1Data(year=self.selYear),
            follow_redirects=True
        )
        self.assertIn('200', str(response.status_code))
        self.assertEqual(response.status_code, 200)
        self.assertIn('A new activity was successfully created', response.data.decode('utf-8'))

        # try to create a task - this should be OK for a ProjAdmin
        tData = getTask1Data(tName='t11new', tDesc='task 1.1 (a1) new')
        tData.update( {'neededWork' : 10.} )
        response = self.client.post(
            url_for('main.newTask', actCode='a1'),
            data = tData,
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('A new task was successfully created', response.data.decode('utf-8'))
        self.assertNotIn('Sorry, you (%s) are not allowed to create tasks for this activity.' % p1m1.username, response.data.decode('utf-8'))

        t1 = db.session.query(Task).filter_by(code='t1').one()
        self.assertIsNotNone( t1 )

        # create a pledge for him/her self - that should work
        response = self.client.post(
            url_for('main.newPledge', taskCode='t1'),
            data = getPledge1AdminData(p1m1, tId=t1.id, selYear=self.selYear),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('A new pledge was successfully created', response.data.decode('utf-8'))

    def test_actAdminAccess(self):

        log= logging.getLogger( "Testing.test_logic" )

        a1m1 = db.session.query(EprUser).filter_by(username='a1m1').one()
        self.assertIsNotNone(a1m1)
        self.assertFalse(a1m1.canManage('project' , 'p1', year=self.selYear))
        self.assertTrue (a1m1.canManage('activity', 'a1', year=self.selYear))
        self.assertTrue (a1m1.canManage('task'    , 't1', year=self.selYear))

        response = self.login(a1m1.username)
        self.assertEqual(response.status_code, 200)

        # access the main view page
        response = self.client.get(
            url_for('main.index'),
            follow_redirects=True
        )
        self.assertEqual(response.status_code,200)
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))

        # try to create a project or activity - these should fail ...
        response = self.client.post(
            url_for('main.newProject'),
            data = getProj1Data(),
            follow_redirects=True
        )
        self.assertNotEqual(response.status_code, 200)
        self.assertIn('Forbidden', response.data.decode('utf-8'))

        response = self.client.post(
            url_for('main.newActivity', projCode='p1'),
            data = getAct1Data(year=self.selYear),
            follow_redirects=True
        )
        self.assertNotEqual(response.status_code, 200)
        self.assertIn('Forbidden', response.data.decode('utf-8'))

        # try to create a task - this should be OK for a ProjAdmin, but not an ActManager
        #-ap 2024-03-19: ActMgrs need to be able to do this, also for the current pledge year
        #    so we change the testing logic to allow this ... 
        response = self.client.post(
            url_for('main.newTask', actCode='a1'),
            data = getTask1Data(tName='t2new', tDesc='task 2 (a1) new'),
            follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Error in the Work needed (person-months) field - This field is required.', response.data.decode('utf-8')[:20000])
        self.assertNotIn('Sorry, you (%s) are not allowed to create tasks for this activity.' % a1m1.username, response.data.decode('utf-8')[:20000])
        # self.assertIn('iCMS - EPR New Task  for ', response.data.decode('utf-8')[:20000])

        t1 = db.session.query(Task).filter_by(code='t1').one()
        self.assertIsNotNone( t1 )

        # create a self-pledge for the newly created task - that should work
        response = self.client.post( url_for('main.newPledge', taskCode='t1'),
                                     data = getPledge1AdminData(a1m1, tId=t1.id, selYear=self.selYear),
                                     follow_redirects=True )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertIn('A new pledge was successfully created', response.data.decode('utf-8'))

        # now check the pledge we created
        pl = db.session.query(Pledge).filter_by(userId=a1m1.id).one()
        self.assertEqual(a1m1.id, pl.userId)
        self.assertEqual('new', pl.status)
        self.assertEqual(1., pl.workTimePld)

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

    def test_workflow_pledge(self):

        log= logging.getLogger( "Testing.test_logic" )

        user = db.session.query(EprUser).filter_by(username='user2').one()
        self.assertIsNotNone(user)

        self.assertFalse(user.canManage('project' ,'p1', year=self.selYear))
        self.assertFalse(user.canManage('activity','a1', year=self.selYear))

        gAdmin = db.session.query(EprUser).filter_by(username='pfeiffer').one()
        self.assertIsNotNone(gAdmin)
        self.assertTrue( gAdmin.is_wizard() )

        # workflow: global admin creates task,
        #           user creates a pledge,
        #           act admin or proj admin approves

        # global admin creates task ... which should be OK
        response = self.login(gAdmin.username)
        self.assertEqual(response.status_code, 200)

        task1Data = getTask1Data(tName='t3new', tDesc='task 3 (a1) new')
        task1Data.update( { 'neededWork' : 42 } )
        log.info( '==> t1data: %s' % task1Data)
        response = self.client.post( url_for('main.newTask', actCode='a1'),
                                     data = task1Data,
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('A new task was successfully created.', response.data.decode('utf-8'))
        self.assertNotIn('Sorry, you are not allowed to create tasks for this activity.', response.data.decode('utf-8'))

        # admin logs out
        response = self.logout()
        self.assertEqual(response.status_code, 302)

        # find back the new task and make sure it's not locked
        tNew = db.session.query(Task).filter_by(name='t3new').one()
        self.assertIsNotNone( tNew )
        self.assertFalse( tNew.locked )

        log.info( '==> task t3new: %s ' % tNew )

        allTasks = db.session.query(Task).all()
        # log.info('\n\nfound tasks %s' % (str(allTasks),))
        self.assertNotEqual( len(allTasks), 0)

        allPlIdsBefore = db.session.query( Pledge.id ).all( )

        # ... user logs in and creates pledge for the new task ...
        response = self.login(user.username)
        self.assertEqual(response.status_code, 200)
        response = self.client.post( url_for('main.newPledge', taskCode=tNew.code),
                                     data = getPledge1EprUserData(user, tId=tNew.id),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('A new pledge was successfully created', response.data.decode('utf-8'))
        self.assertNotIn('Accepted pledge', response.data.decode('utf-8')) # make sure we do not have the admin form

        response = self.logout()
        self.assertEqual(response.status_code, 302)

        allPlIdsAfter = db.session.query( Pledge.id ).all( )

        # log.info('\n 1- found pledges %s' % (str(allPl),))
        self.assertEqual( len(allPlIdsAfter), len(allPlIdsBefore)+1 )

        newPlIdList = [x for x in allPlIdsAfter if x not in allPlIdsBefore]
        newPlId, = newPlIdList[0]
        log.info( "new pledge found with id %s " % newPlId )

        # ... admin approves

        a1m1= db.session.query(EprUser).filter_by(username='a1m1').one()
        self.assertIsNotNone(a1m1)
        self.assertFalse( a1m1.is_administrator() )

        response = self.login(a1m1.username)
        self.assertEqual(response.status_code, 200)

        response = self.client.post( url_for('main.pledge', id=newPlId),
                                     data = getPledge1AdminData(user, tId=tNew.id, wta=1., status="accepted", selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Pledge info successfully updated.', response.data.decode('utf-8'))

        pl = db.session.query(Pledge).filter_by(id=newPlId).one()
        self.assertEqual('accepted', pl.status)

    def test_workflow_pledgeFail(self):

        user = self.getEprUser('user3')
        a1m1 = self.getEprUser('a1m1')

        # check specifics for act-admin
        self.assertFalse(a1m1.canManage('project', 'p1', year=self.selYear))
        self.assertTrue(a1m1.canManage('activity', 'a1', year=self.selYear))

        allPlIdsBefore = db.session.query( Pledge.id ).all( )

        self.login(username=a1m1.username)
        # workflow: user creates a pledge,
        #           act admin or proj admin approves

        # ... and creates a pledge for admin ...
        tsk = db.session.query(Task).filter_by(code='t2').one()
        response = self.client.post( url_for('main.newPledge', taskCode=tsk.code),
                                     data = getPledge1AdminData(a1m1, tId=tsk.id, selYear=self.selYear),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('A new pledge was successfully created', response.data.decode('utf-8'))
        self.assertNotIn('Accepted pledge', response.data.decode('utf-8')) # make sure we do not have the admin form

        # ... and creates a pledge for user ...
        response = self.client.post( url_for('main.newPledge', taskCode=tsk.code),
                                     data = getPledge1AdminData(user, pl=1.4, tId=tsk.id, selYear=self.selYear),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('A new pledge was successfully created', response.data.decode('utf-8'))
        self.assertNotIn('Accepted pledge', response.data.decode('utf-8')) # make sure we do not have the admin form

        # admin logs out
        response = self.logout()
        self.assertEqual(response.status_code, 302)

        allTasks = db.session.query(Task).all()
        # log.info('\n 1 - found tasks %s' % (str(allTasks),))
        self.assertEqual( len(allTasks), len(allTasks))

        # find back the pledges
        allPlIdsAfter = db.session.query( Pledge.id ).all( )

        newPlIdList = [ x for x in allPlIdsAfter if x not in allPlIdsBefore ]
        newPlId1, = newPlIdList[ 0 ]
        newPlId2, = newPlIdList[ 1 ]
        log.info( "new pledges found with ids %s %s" % (newPlId1, newPlId2) )

        allPl = db.session.query(Pledge).all()
        # log.info('\n 1 - found pledges %s' % (str(allPl),))
        self.assertGreaterEqual( len(allPlIdsAfter), len(allPlIdsBefore)+2)

        pl1 = db.session.query(Pledge).filter_by(id=newPlId1).one()
        self.assertEqual(1.0, pl1.workTimePld)
        self.assertEqual(a1m1.id, pl1.userId)

        pl2 = db.session.query(Pledge).filter_by(id=newPlId2).one()
        self.assertEqual(1.4, pl2.workTimePld)
        self.assertEqual(user.id, pl2.userId)

        # ... user tries to edit the plege for the admin ... which he should NOT be able to
        response = self.login(user.username)
        self.assertEqual(response.status_code, 200)

        response = self.client.post( url_for('main.pledge', id=pl1.id),
                                     data = getPledge1EprUserData(user),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))
        self.assertNotIn('Pledge info successfully updated.', response.data.decode('utf-8'))
        self.assertNotIn('Accepted pledge', response.data.decode('utf-8')) # make sure we do not have the admin form
        self.assertIn( 'info could not be updated.', response.data.decode('utf-8') )

        pl1 = db.session.query(Pledge).filter_by(id=newPlId1).one()
        self.assertEqual(1., pl1.workTimePld)
        self.assertEqual(a1m1.id, pl1.userId)

        # ... user tries to edit his pledge ... which he should be able to
        response = self.client.post( url_for('main.pledge', id=newPlId2),
                                     data = getPledge1EprUserData(user, pl=1.8),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('iCMS - EPR Pledge Overview', response.data.decode('utf-8'))
        # self.assertIn('Pledge info successfully updated.', response.data.decode('utf-8'))
        self.assertNotIn('Accepted pledge', response.data.decode('utf-8')) # make sure we do not have the admin form

        response = self.logout()
        self.assertEqual(response.status_code, 302)

        pl2 = db.session.query(Pledge).filter_by(id=newPlId2).one()
        self.assertEqual(1.8, pl2.workTimePld)
        self.assertEqual(user.id, pl2.userId)

        response = self.logout()
        self.assertEqual(response.status_code, 302)

    def test_pledgeHistory(self):

        t1 = db.session.query(Task).filter_by(code='t1').one()
        t2 = db.session.query(Task).filter_by(code='t2').one()
        self.assertIsNotNone(t1)
        self.assertIsNotNone(t2)

        user = self.getEprUser('user4')
        self.assertIsNotNone(user)

        # login ...
        response = self.login(user.username)
        self.assertEqual(response.status_code, 200)

        allPlIdsBefore = db.session.query( Pledge.id ).all( )

        # make a new pledge
        response = self.client.post( url_for('main.newPledge', taskCode='t2'),
                                     data = getPledge1EprUserData(user, pl=0.5),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('A new pledge was successfully created', response.data.decode('utf-8')[:35000])

        self.assertGreater( len(allPlIdsBefore), 0)

        # find back the pledges
        allPlIdsAfter = db.session.query( Pledge.id ).all( )

        newPlIdList = [ x for x in allPlIdsAfter if x not in allPlIdsBefore ]
        newPlId, = newPlIdList[ 0 ]
        log.info( "new pledge found with id %s" % (newPlId, ) )

        self.assertEqual( len(allPlIdsAfter), len(allPlIdsBefore)+1 )

        pl1 = db.session.query(Pledge).filter_by(id=newPlId).one()
        # log.info('found work time pledged: %s ' % pl1.workTimePld)
        self.assertEqual(0.5, pl1.workTimePld)
        self.assertEqual(user.id, pl1.userId)

        # update the pledge:
        response = self.client.post( url_for('main.pledge', id=pl1.id),
                                     data = getPledge1EprUserData(user, pl=1.5), # 1.5 instead of 0.5 months pledged
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertRegexpMatches( response.data.decode('utf-8'), re.compile(r'Pledge \(.*\) info successfully updated') )

        allPl = db.session.query(Pledge).all()
        # log.info('\nfound pledges %s' % (str(allPl),))
        self.assertEqual( len(allPl), len(allPlIdsAfter))

        pl1 = db.session.query(Pledge).filter_by(id=newPlId).one()
        # log.info('1 found work time pledged: %s for user %s (%s) ' % (pl1.workTimePld, user.id, pl1.userId) )
        self.assertEqual(1.5, pl1.workTimePld)
        self.assertEqual(user.id, pl1.userId)

        self.logout()

        a1m1 = self.getEprUser('a1m1')
        self.login(a1m1)

        self.assertIsNotNone(a1m1)
        self.assertFalse(a1m1.canManage('project', 'p1', year=self.selYear))
        self.assertTrue(a1m1.canManage('activity', 'a1', year=self.selYear))

        # ... admin approves pledge
        response = self.login(a1m1.username)
        self.assertEqual(response.status_code, 200)

        response = self.client.post( url_for('main.pledge', id=pl1.id),
                                     data = getPledge1AdminData(user, pl=3.4, wta=1., status="accepted", selYear=self.selYear),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Pledge info successfully updated.', response.data.decode('utf-8'))

        self.logout()

        # user login ...
        response = self.login(user.username)
        self.assertEqual(response.status_code, 200)

        # try to update the pledge again, this should fail now:
        response = self.client.post( url_for('main.pledge', id=pl1.id),
                                     data = getPledge1EprUserData(user, pl=2), # 2 instead of 4 months pledged
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('Pledge (%s) info could not be updated' % pl1.code, response.data.decode('utf-8'))

        # make another new pledge for task 2, same user, same inst, should fail (should edit instead):
        response = self.client.post( url_for('main.newPledge', taskCode='t2'),
                                     data = getPledge1EprUserData(user, tId=db.session.query(Task).filter_by(code='t2').one().id,
                                                               pl=1.3),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('Error from DB when trying to add new pledge.', response.data.decode('utf-8'))

        allPl = db.session.query(Pledge).all()
        # log.info('\n 2 - found pledges %s' % (str(allPl),))
        self.assertEqual( len(allPl), len(allPlIdsAfter) ) # no new pledge expected here

        pl1 = db.session.query(Pledge).filter_by(id=newPlId).one()
        # log.info(' 3 - found work time pledged: %s, accepted: %s, status %s for user id %s' % (pl1.workTimePld, pl1.workTimeAcc, pl1.status, user.id) )
        self.assertEqual(3.4, pl1.workTimePld)
        self.assertEqual(1, pl1.workTimeAcc)
        self.assertEqual('accepted', pl1.status)
        self.assertEqual(user.id, pl1.userId)

        db.session.commit()

        self.logout() # user logs out

    def test_approvePledgeAutoUpdateWorkDone(self):

        u1 = self.getEprUser('user1')
        taskId = 3
        work = 1.5
        # create some pledges for tasks so we can check that not more than needed are approved:
        p0 = Pledge( taskId=taskId, userId=u1.id, instId=u1.mainInst, workTimePld=work, workTimeAcc=0,
                workTimeDone=0, status='new', year=self.selYear, workedSoFar=0 )
        addAndCommit( p0 )

        p0 = db.session.query(Pledge).filter_by(code='%s+%s+%s' % (taskId, u1.id, u1.mainInst)).one()

        p2m1 = self.getEprUser('p2m1')
        self.assertIsNotNone(p2m1)
        self.assertFalse( p2m1.is_administrator() )
        self.assertTrue( p2m1.canManage( 'project' , 'p2', year=self.selYear ) )
        self.assertTrue( p2m1.canManage( 'activity', 'a3', year=self.selYear ) )

        response = self.login(p2m1.username)
        self.assertEqual(response.status_code, 200)

        self.assertEqual( p0.workTimeDone, 0. )
        self.assertEqual( p0.workTimeAcc, 0. )

        # manager now approves pledges individually by editing it and select the pledged work as accepted work:

        # ... this is not yet above the limit of the work needed for the task (plus one month), so it should work:
        response = self.client.post( url_for('main.pledge', id=p0.id ),
                                     data = getPledge1AdminData(u1, tId=taskId, pl=work, wta=work, status="accepted", selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Pledge info successfully updated.', response.data.decode('utf-8'))

        p0read0 = db.session.query(Pledge).filter_by(code='%s+%s+%s' % (taskId, u1.id, u1.mainInst)).one()
        self.assertEqual( p0read0.workTimeAcc, work )
        self.assertEqual( p0read0.workTimeDone, 0. )

        # set pledge to done:
        response = self.client.post( url_for('main.pledge', id=p0.id ),
                                     data = getPledge1AdminData(u1, tId=taskId, pl=work, wta=work, wtd=0., status="done", selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Pledge info successfully updated.', response.data.decode('utf-8'))

        p0read = db.session.query(Pledge).filter_by(code='%s+%s+%s' % (taskId, u1.id, u1.mainInst)).one()
        self.assertEqual( p0read.workTimeAcc, work )
        self.assertEqual( p0read.workTimeDone, p0read.workTimeAcc )

        # now reject these pledges again to allow other tests to use them ...
        p0.status = 'rejected'
        commitUpdated( p0 )

    def test_approvePledgeLimit(self):

        u1 = self.getEprUser('user1')
        u2 = self.getEprUser( 'user2' )

        # create some pledges for tasks so we can check that not more than needed are approved:
        p0 = Pledge( taskId=4, userId=u1.id, instId=u1.mainInst, workTimePld=5., workTimeAcc=0,
                workTimeDone=0, status='new', year=self.selYear, workedSoFar=0 )
        p1 = Pledge( taskId=4, userId=u2.id, instId=u2.mainInst, workTimePld=5., workTimeAcc=0,
                workTimeDone=0, status='new', year=self.selYear, workedSoFar=0 )
        addAndCommit( p0 )
        addAndCommit( p1 )

        p0 = db.session.query(Pledge).filter_by(code='%s+%s+%s' % (4, u1.id, u1.mainInst)).one()
        p1 = db.session.query(Pledge).filter_by(code='%s+%s+%s' % (4, u2.id, u2.mainInst)).one()

        p2m1 = self.getEprUser('p2m1')
        self.assertIsNotNone(p2m1)
        self.assertFalse( p2m1.is_administrator() )
        self.assertTrue( p2m1.canManage( 'project' , 'p2', year=self.selYear ) )
        self.assertTrue( p2m1.canManage( 'activity', 'a3', year=self.selYear ) )

        response = self.login(p2m1.username)
        self.assertEqual(response.status_code, 200)

        # manager now approves pledges individually by editing each of them and select the pledged work as accepted work:

        # ... this is not yet above the limit of the work needed for the task (plus one month), so it should work:
        response = self.client.post( url_for('main.pledge', id=p0.id ),
                                     data = getPledge1AdminData(u1, tId=4, pl=5., wta=5., status="accepted", selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Pledge info successfully updated.', response.data.decode('utf-8'))

        # ... this is now above the limit and should fail:
        response = self.client.post( url_for( 'main.pledge', id=p1.id ),
                                     data=getPledge1AdminData( u2, tId=4, pl=5., wta=5., status="accepted", selYear=self.selYear ),
                                     follow_redirects=True )

        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual( response.status_code, 200 )
        self.assertNotIn( 'Pledge info successfully updated.', response.data.decode('utf-8') )
        self.assertIn( 'Total work time accepted would be 10.00 (5.00 + 5.00), but can not be larger than needed for task 8.00', response.data.decode('utf-8') )

        # set pledge to done:
        response = self.client.post( url_for('main.pledge', id=p0.id ),
                                     data = getPledge1AdminData(u1, tId=4, pl=5., wta=5., wtd=3., status="done", selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Pledge info successfully updated.', response.data.decode('utf-8'))

        # set pledge to done, set workDone to a high value, this should fail
        response = self.client.post( url_for('main.pledge', id=p0.id ),
                                     data = getPledge1AdminData(u1, tId=4, pl=5., wta=5., wtd=15., status="done", selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('Pledge info successfully updated.', response.data.decode('utf-8'))
        self.assertIn( 'Work time done 15.00 can not be larger than pledged 5.00', response.data.decode('utf-8') )

        # try to update work done:
        data = getPledge1AdminData( u1, tId=4, pl=5., wta=5., wtd=4., status="done", selYear=self.selYear )
        data['workedSoFar'] = 5.0
        response = self.client.post( url_for('main.updateWorkDone', id=p0.id ),
                                     data = data,
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('Pledge info successfully updated.', response.data.decode('utf-8'))
        self.assertIn( 'Pledged work done so far successfully updated to 5.0.', response.data.decode('utf-8') )

        # try to update work done with invalid value, should fail:
        data = getPledge1AdminData( u1, tId=4, pl=5., wta=5., wtd=4., status="done", selYear=self.selYear )
        data['workedSoFar'] = 50.0
        response = self.client.post( url_for('main.updateWorkDone', id=p0.id ),
                                     data = data,
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('Pledge info successfully updated.', response.data.decode('utf-8'))
        self.assertIn( 'Errors processing form: invalid value for workedSoFar is 50.0 - should be at least one month, max 12 months.', response.data.decode('utf-8') )

        # try to update work done with invalid value, should fail too:
        data = getPledge1AdminData( u1, tId=4, pl=5., wta=5., wtd=4., status="done", selYear=self.selYear )
        data['workedSoFar'] = 11.0
        response = self.client.post( url_for('main.updateWorkDone', id=p0.id ),
                                     data = data,
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('Pledge info successfully updated.', response.data.decode('utf-8'))
        self.assertIn( 'Illegal amount of work done so far:  11.00 months is larger than work pledged: 5.00.', response.data.decode('utf-8') )

        # now reject these pledges again to allow other tests to use them ...
        p0.status = 'rejected'
        commitUpdated( p0 )
        p1.status = 'rejected'
        commitUpdated( p1 )

    def test_approveMultiplePledges(self):

        u1 = self.getEprUser( 'user1' )
        u2 = self.getEprUser( 'user2' )

        # create some pledges for tasks so we can check that not more than needed are approved:
        p0 = Pledge( taskId=6, userId=u1.id, instId=u1.mainInst, workTimePld=1., workTimeAcc=0,
                workTimeDone=0, status='new', year=self.selYear, workedSoFar=3. )
        p1 = Pledge( taskId=6, userId=u2.id, instId=u2.mainInst, workTimePld=2., workTimeAcc=0,
                workTimeDone=0, status='new', year=self.selYear, workedSoFar=0 )
        addAndCommit( p0 )
        addAndCommit( p1 )

        p2m1 = self.getEprUser('p2m1')
        self.assertIsNotNone(p2m1)
        self.assertFalse( p2m1.is_administrator() )
        self.assertTrue( p2m1.canManage( 'project' , 'p2', year=self.selYear ) )
        self.assertTrue( p2m1.canManage( 'activity', 'a3', year=self.selYear ) )

        response = self.login(p2m1.username)
        self.assertEqual(response.status_code, 200)

        # manager now approves pledges in bulk via the approveSelectedPledges method

        plCodes = [ p0.code, p1.code ]

        # ... approve both pledges, they should go from "new" to "accepted"
        response = self.client.post( url_for('main.approveSelectedPledges'),
                                     data = { 'pledges': json.dumps(plCodes) },
                                     follow_redirects=True )

        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        result = json.loads(response.data.decode('utf-8'))
        self.assertIn('Reset a too large value of workedSoFar (3.00) to workTimePld (1.00)', result['msg'])
        self.assertIn('Pledge %s approved with 1.000000 accepted, 0.000000 done' % p0.code.replace('+', ':'), result['msg'])
        self.assertIn('Pledge %s approved with 2.000000 accepted, 0.000000 done' % p1.code.replace('+', ':'), result['msg'])

        # make sure the pledges are OK in the DB as well:
        p0a = db.session.query(Pledge).filter(Pledge.code == p0.code).one()
        p1a = db.session.query(Pledge).filter(Pledge.code == p1.code).one()

        self.assertEqual( p0a.workTimeAcc, p0.workTimePld )
        self.assertEqual( p0a.workedSoFar, p0a.workTimePld )
        self.assertEqual( p1a.workTimeAcc, p1.workTimePld )
        for pl in [p0a, p1a]:
            self.assertEqual( pl.workTimeAcc, pl.workTimePld )
            self.assertEqual( pl.status, 'accepted' )

        # ... now bulk-approve only one, this should then go from "accepted" to "done"
        response = self.client.post( url_for('main.approveSelectedPledges'),
                                     data = { 'pledges': json.dumps( [p0.code] ) },
                                     follow_redirects=True )

        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        result = json.loads(response.data.decode('utf-8'))
        self.assertNotIn('Reset a too large value of workedSoFar (3.00) to workTimePld (1.00)', result['msg'])
        self.assertIn('INFO> %s : Pledge %s set to done with 1.000000 done' % (p0.code, p0.code), result['msg'])
        
        # make sure the pledges are OK in the DB as well:
        p0b = db.session.query(Pledge).filter(Pledge.code == p0.code).one()
        p1b = db.session.query(Pledge).filter(Pledge.code == p1.code).one()

        self.assertEqual( p0b.workTimeAcc, p0.workTimePld )
        self.assertEqual( p0b.workTimeDone, p0.workTimePld )
        self.assertEqual( p0a.workedSoFar, p0a.workTimePld )
        self.assertEqual( p0a.status, 'done' )
        
        self.assertEqual( p1b.workTimeAcc, p1.workTimePld )
        self.assertEqual( p1b.workTimeDone, 0. )
        self.assertEqual( p1b.status, 'accepted' )

        # remove the pledges again ...
        db.session.delete( p0 )
        db.session.delete( p1 )
        db.session.commit()

    def test_approveMultiplePledgeLimit(self):

        u1 = self.getEprUser( 'user1' )
        u2 = self.getEprUser( 'user2' )

        # create some pledges for tasks so we can check that not more than needed are approved:
        p0 = Pledge( taskId=6, userId=u1.id, instId=u1.mainInst, workTimePld=5., workTimeAcc=0,
                workTimeDone=0, status='new', year=self.selYear, workedSoFar=0 )
        p1 = Pledge( taskId=6, userId=u2.id, instId=u2.mainInst, workTimePld=5., workTimeAcc=0,
                workTimeDone=0, status='new', year=self.selYear, workedSoFar=0 )
        addAndCommit( p0 )
        addAndCommit( p1 )

        p2m1 = self.getEprUser('p2m1')
        self.assertIsNotNone(p2m1)
        self.assertFalse( p2m1.is_administrator() )
        self.assertTrue( p2m1.canManage( 'project' , 'p2', year=self.selYear ) )
        self.assertTrue( p2m1.canManage( 'activity', 'a3', year=self.selYear ) )

        response = self.login(p2m1.username)
        self.assertEqual(response.status_code, 200)

        # manager now approves pledges in bulk, selecting them on the pledges-for-project page and clicking on "approve selected".
        # this sends each pledge to the main.approvePledgeByCode function, the pledged work as accepted work

        # ... this one is not yet above the limit of the work needed for the task, so it should work:
        response = self.client.post( url_for('main.approvePledgeByCode', code=p0.code.replace('+', ':') ),
                                     data = getPledge1AdminData(u1, tId=6, pl=5., wta=5., status="accepted", selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Pledge %s approved with 5.000000 accepted, 0.000000 done' % p0.code.replace('+', ':'), response.data.decode('utf-8'))

        # ... this one is now above the limit and should fail:
        response = self.client.post( url_for( 'main.approvePledgeByCode', code=p1.code.replace('+', ':') ),
                                     data=getPledge1AdminData( u2, tId=6, pl=5., wta=5., status="accepted", selYear=self.selYear ),
                                     follow_redirects=True )

        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )

        # here we should have gotten a 403 with a json { "msg" : ... }:
        self.assertEqual( response.status_code, 403 )
        self.assertNotIn( 'Pledge info successfully updated.', response.data.decode('utf-8') )
        self.assertNotIn('Pledge %s approved with 5.000000 accepted, 0.000000 done' % p1.code.replace('+', ':'), response.data.decode('utf-8'))
        res = json.loads( response.data.decode('utf-8') )
        self.assertIn( 'Total work time accepted would be 10.00 (5.00 + 5.00), but can not be larger than needed for task 8.00 + 1.0 month', res['msg'] )

        # remove the pledges again ...
        db.session.delete( p0 )
        db.session.delete( p1 )
        db.session.commit()

    def test_adminUpdatePledgeLimit(self):

        # test for case ICMSEPR - 63
        # https://its.cern.ch/jira/browse/ICMSEPR-63
        # editing a pledge while updating the accepted work in status 'accepted'

        # (swap the users here to avoid clashes)
        u1 = self.getEprUser( 'user3' )
        u2 = self.getEprUser( 'user4' )

        # task with id=6 has a need of 8 month
        taskForPledge = db.session.query(Task).filter_by(id=6).one()
        # print "\n ==> task: ", taskForPledge

        # create some pledges for tasks so we can check that not more than needed are approved:
        p0 = Pledge( taskId=taskForPledge.id, userId=u1.id, instId=u1.mainInst, workTimePld=3., workTimeAcc=0,
                     workTimeDone=0, status='new', year=self.selYear, workedSoFar=0 )
        p1 = Pledge( taskId=taskForPledge.id, userId=u2.id, instId=u2.mainInst, workTimePld=3., workTimeAcc=0,
                     workTimeDone=0, status='new', year=self.selYear, workedSoFar=0 )
        addAndCommit( p0 )
        addAndCommit( p1 )

        p2m1 = self.getEprUser('p2m1')
        self.assertIsNotNone(p2m1)
        self.assertFalse( p2m1.is_administrator() )
        self.assertTrue( p2m1.canManage( 'project' , 'p2', year=self.selYear ) )
        self.assertTrue( p2m1.canManage( 'activity', 'a3', year=self.selYear ) )

        response = self.login(p2m1.username)
        self.assertEqual(response.status_code, 200)

        # manager now approves pledges in bulk, selecting them on the pledges-for-project page and clicking on "approve selected".
        # this sends each pledge to the main.approvePledgeByCode function, the pledged work as accepted work
        # remember that approval like this will ignore set values and take the value for workTimePld from the pledge

        # ... this one is not yet above the limit of the work needed for the task, so it should work:
        response = self.client.post( url_for('main.approvePledgeByCode', code=p0.code.replace('+', ':') ),
                                     data = getPledge1AdminData(u1, tId=taskForPledge.id, selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        json_response = json.loads( response.data.decode( 'utf-8' ) )
        self.assertIn('Pledge %s approved with 3.000000 accepted, 0.000000 done' % p0.code.replace('+', ':'),
                      json_response['msg'])

        # ... this one is at the limit and should be OK as well:
        response = self.client.post( url_for( 'main.approvePledgeByCode', code=p1.code.replace('+', ':') ),
                                     data=getPledge1AdminData( u2, tId=taskForPledge.id, selYear=self.selYear),
                                     follow_redirects=True )

        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        json_response = json.loads( response.data.decode( 'utf-8' ) )
        self.assertIn('Pledge %s approved with 3.000000 accepted, 0.000000 done' % p1.code.replace('+', ':'), json_response['msg'])

        # now the admin updates a pledge of a user:

        # now try to edit that pledge, update accepted and status as well as pledged
        response = self.client.post(
            url_for( 'main.pledge', id=p0.id ),
            data=getPledge1AdminData( user=p2m1, tId = taskForPledge.id, pl=5., wta=5., status='accepted', selYear=self.selYear ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'Pledge info successfully updated.', response.data.decode('utf-8') )

        # remove the pledges again ...
        db.session.delete( p0 )
        db.session.delete( p1 )
        db.session.commit()

    def test_editPledgeEprUser( self ) :
        user = db.session.query(EprUser).filter_by( username='user4' ).one( )
        self.assertIsNotNone( user )

        response = self.login( user.username )
        self.assertEqual( response.status_code, 200 )

        # access the main view page
        response = self.client.get(
            url_for( 'main.index' ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )

        allPlIdsBefore = db.session.query( Pledge.id ).all( )

        # create a pledge for task t1 - that should work
        response = self.client.post(
            url_for( 'main.newPledge', taskCode='t2' ),
            data=getPledge1EprUserData( user=user, tId=2, pl=1., wsf=0. ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'A new pledge was successfully created for user', response.data.decode('utf-8') )
        self.assertIn( 'CMS - EPR Pledge Overview', response.data.decode('utf-8') )
        self.assertNotIn( 'user is admin', response.data.decode('utf-8') )

        # find back the pledge
        allPlIdsAfter = db.session.query( Pledge.id ).all( )

        newPlIdList = [ x for x in allPlIdsAfter if x not in allPlIdsBefore ]
        newPlId, = newPlIdList[ 0 ]
        log.info( "new pledge found with id %s " % newPlId )

        # now try to edit that pledge
        response = self.client.post(
            url_for( 'main.pledge', id=newPlId ),
            data=getPledge1EprUserData( user=user, tId=2, pl=2., wsf=0. ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertRegexpMatches( response.data.decode('utf-8'), re.compile(r'Pledge \([0-9+]*\) info successfully updated.') )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

        # reject the pledge:
        response = self.client.post(
            url_for( 'main.rejectPledge', id=newPlId ),
            data = { "taskId" : 2,
                     "workTimePld" : 2.,
                     "workedSoFar" : 0.,
                     # make sure the userId field looks like the one from the search:
                     "userId" : '%s (CERNid: %i)' % (user.name, user.hrId),
                     "instId" : 'cern',
                     'reason' : 'testing rejection',
             },
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertRegexpMatches( response.data.decode('utf-8'), re.compile(r'Pledge \(.*\) rejected.' ) )

        # clean up again
        db.session.delete( db.session.query(Pledge).filter_by( id=newPlId ).one() )
        db.session.commit()

    def test_editPledgeAdmin( self ) :
        user = db.session.query(EprUser).filter_by( username='a3m1' ).one( )
        self.assertIsNotNone( user )

        response = self.login( user.username )
        self.assertEqual( response.status_code, 200 )

        # access the main view page
        response = self.client.get(
            url_for( 'main.index' ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )

        allPlIdsBefore = db.session.query(Pledge.id).all()

        # create a pledge for task t1 - that should work
        response = self.client.post(
            url_for( 'main.newPledge', taskCode='t4' ),
            data=getPledge1AdminData( user=user, tId=3, pl=1., wta=0., selYear=self.selYear),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'A new pledge was successfully created for user', response.data.decode('utf-8') )
        self.assertIn( 'CMS - EPR Pledge Overview', response.data.decode('utf-8') )
        self.assertNotIn( 'user is admin', response.data.decode('utf-8') )

        # find back the pledge
        allPlIdsAfter = db.session.query(Pledge.id).all()

        newPlIdList = [x for x in allPlIdsAfter if x not in allPlIdsBefore]
        newPlId, = newPlIdList[0]
        log.info( "new pledge found with id %s " % newPlId )

        # now try to edit that pledge
        response = self.client.post(
            url_for( 'main.pledge', id=newPlId ),
            data=getPledge1EprUserData( user=user, tId = 3, pl=2., wsf=0. ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'Pledge info successfully updated.', response.data.decode('utf-8') )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

        # clean up again
        db.session.delete( db.session.query(Pledge).filter_by(id=newPlId).one() )
        db.session.commit( )

    def test_userEditPledge14MonthFail( self ) :
        user = db.session.query(EprUser).filter_by( username='user1' ).one( )
        self.assertIsNotNone( user )

        response = self.login( user.username )
        self.assertEqual( response.status_code, 200 )

        task = db.session.query(Task).filter_by(code='t2').one()
        plCode = '%d+%d+%d' % (task.id, user.id, user.mainInst)
        log.info('test_userEditPledge14MonthFail> looking for pledge code %s' % plCode)
        pld  = db.session.query(Pledge).filter_by(code=plCode).one()
        # create a pledge for task t1 for 14 months, that should fail
        response = self.client.post(
            url_for( 'main.pledge', id=pld.id ),
            data=getPledge1EprUserData( user, tId=task.id, pl=14. ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'Info for pledge id %d in %d' % (pld.id, self.selYear), response.data.decode('utf-8') )
        self.assertIn( 'Illegal amount of work pledged:  14.00 months is not between 0 and 12 months', response.data.decode('utf-8') )

        self.assertNotIn( 'A new pledge was successfully created for user', response.data.decode('utf-8') )
        self.assertNotIn( 'user is admin', response.data.decode('utf-8') )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

    def test_userCreatePledge14MonthFail( self ) :
        user = db.session.query(EprUser).filter_by( username='user1' ).one( )
        self.assertIsNotNone( user )

        response = self.login( user.username )
        self.assertEqual( response.status_code, 200 )

        task = db.session.query(Task).filter_by(code='t2').one()
        plCode = '%d+%d+%d' % (task.id, user.id, user.mainInst)
        log.info('test_userEditPledge14MonthFail> looking for pledge code %s' % plCode)
        pld  = db.session.query(Pledge).filter_by(code=plCode).one()
        # create a pledge for task t1 for 14 months, that should fail
        response = self.client.post(
            url_for( 'main.pledge', id=pld.id ),
            data=getPledge1EprUserData( user, tId=task.id, pl=14. ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'Info for pledge id %d in %d' % (pld.id, self.selYear), response.data.decode('utf-8') )
        self.assertIn( 'Illegal amount of work pledged:  14.00 months is not between 0 and 12 months', response.data.decode('utf-8') )

        self.assertNotIn( 'A new pledge was successfully created for user', response.data.decode('utf-8') )
        self.assertNotIn( 'user is admin', response.data.decode('utf-8') )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

    def test_editPledgeEprUserFail( self ) :
        user4 = db.session.query(EprUser).filter_by( username='user4' ).one( )
        self.assertIsNotNone( user4 )

        response = self.login( user4.username )
        self.assertEqual( response.status_code, 200 )

        task5 = db.session.query(Task).filter_by(code='t5').one()
        self.assertIsNotNone( task5 )

        # access the main view page
        response = self.client.get(
            url_for( 'main.index' ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'iCMS - EPR Pledge Overview', response.data.decode('utf-8') )

        allPlCodesBefore = db.session.query( Pledge.code ).all( )

        # create a pledge for task t5 - that should work
        response = self.client.post(
            url_for( 'main.newPledge', taskCode=task5.code ),
            data=getPledge1EprUserData( user=user4, tId=task5.id, pl=1., wsf=0. ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'A new pledge was successfully created for user', response.data.decode('utf-8') )
        self.assertIn( 'CMS - EPR Pledge Overview', response.data.decode('utf-8') )
        self.assertNotIn( 'user is admin', response.data.decode('utf-8') )

        # find back the pledge
        allPlsAfter = db.session.query( Pledge ).all( )

        newPlList = [ x for x in allPlsAfter if x.code not in allPlCodesBefore ]
        log.info( "===+++===> new pledge list: %s" % str([x.code for x in newPlList]) )

        newPl = newPlList[ 0 ]
        newPlId = newPl.id
        log.info( "===+++===> new pledge found with id %s (code %s)" % (newPlId, newPlList[0].code) )

        # login as manager (t2 is p1/a1) and accept pledge:
        a1m1= db.session.query(EprUser).filter_by(username='a1m1').one()
        self.assertIsNotNone(a1m1)
        self.assertFalse( a1m1.is_administrator() )

        response = self.login(a1m1.username)
        self.assertEqual(response.status_code, 200)

        response = self.client.post( url_for('main.pledge', id=newPlId),
                                     data = getPledge1AdminData(user4, tId=task5.id, wta=1., status="accepted", selYear=self.selYear),
                                     follow_redirects=True )
        # log.info("got response data \n%s\n " % str(response.data.decode('utf-8')) )
        self.assertEqual(response.status_code, 200)
        self.assertIn('Pledge info successfully updated.', response.data.decode('utf-8'))

        pl = db.session.query(Pledge).filter_by(id=newPlId).one()
        self.assertEqual('accepted', pl.status)

        log.info( "===+++===> new pledge with id %s (code %s) has status %s " % (newPlId, newPlList[0].code, pl.status) )

        # now login back as user and try to modify it - shold fail:

        response = self.login( user4.username )
        self.assertEqual( response.status_code, 200 )

        # now try to edit that pledge
        response = self.client.post(
            url_for( 'main.pledge', id=newPlId ),
            data=getPledge1EprUserData( user=user4, tId=task5.id, pl=2., wsf=0. ),
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'Pledge (%s) info could not be updated.' % newPl.code, response.data.decode('utf-8') )

        log.info( "===+++===> new pledge with id %s (code %s, status %s) could correctly not be modified" % (newPlId, newPlList[0].code, pl.status) )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

        # reject the pledge as admin:
        # login as manager (t2 is p1/a1) and accept pledge:
        a1m1= db.session.query(EprUser).filter_by(username='a1m1').one()
        self.assertIsNotNone(a1m1)
        self.assertFalse( a1m1.is_administrator() )

        response = self.login(a1m1.username)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            url_for( 'main.rejectPledge', id=newPlId ),
            data = { "taskId" : task5.id,
                     "workTimePld" : 2.,
                     "workedSoFar" : 0.,
                     # make sure the userId field looks like the one from the search:
                     "userId" : '%s (CERNid: %i)' % (user4.name, user4.hrId),
                     "instId" : 'cern',
                     'reason' : 'testing rejection',
             },
            follow_redirects=True
        )
        self.assertEqual( response.status_code, 200 )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertRegexpMatches( response.data.decode('utf-8'), re.compile(r'Pledge \(.*\) rejected.') )

        log.info( "===+++===> successfully rejected pledge id %s (code %s) " % (newPlId, newPlList[0].code) )

        # clean up again
        db.session.delete( db.session.query(Pledge).filter_by( id=newPlId ).one() )
        db.session.commit()
